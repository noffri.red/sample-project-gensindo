<?php if (count($trending) > 0) { 
    ?>
    <div class="head">
        <div class="title">Trending Topik</div>
    </div>
    <ul>
        <?php
        foreach ($trending as $value) {
            $link = 'https://www.sindonews.com/topic/' . $value['topic_id'] . '/' . slug($value['topic']);;
            $title = cleanWords($value['topic']);
        ?>
            <li><a target="_blank" href="<?php echo $link  ?>">#<?php echo $title ?></a></li>

        <?php } ?>
    </ul>
<?php } ?>