<?php

function google_analytics($google_account_id){
    $google_analytics_code = "
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-W53VC57');</script><noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-W53VC57\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
        <script>window.dataLayer = window.dataLayer || [];</script>";
    
    $google_analytics_code .= "
        <script>
            _atrk_opts = { atrk_acct:\"p03zj1aEsk00MA\", domain:\"sindonews.com\",dynamic: true};
            (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = \"https://certify-js.alexametrics.com/atrk.js\"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>
        <noscript><img src=\"https://certify.alexametrics.com/atrk.gif?account=p03zj1aEsk00MA\" style=\"display:none\" height=\"1\" width=\"1\" alt=\"alexametrics\"></noscript>";

    $google_analytics_code .= '
        <script>
            var _comscore = _comscore || [];
            _comscore.push({c1:"2",c2:"9013027"});
            (function() {
                var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
                s.src = (document.location.protocol == "https:" ? "https://sb" : "https://b") + ".scorecardresearch.com/beacon.js";
                el.parentNode.insertBefore(s, el);
            })();
        </script>
        <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=2.0&cj=1" alt="comscore"></noscript>';

    return $google_analytics_code;
}

function google_analytics_arsip($google_account_id){
    $ci = & get_instance();
    $path = '/' . $ci->uri->segment(1);
    $geturl = parse_url(current_url());
    $host = $geturl['host'];
    $extract = explode('.',$host);
    $subdomain = $extract[0];

    $google_analytics_code = "
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-W53VC57');</script><noscript><iframe src=\"https://www.googletagmanager.com/ns.html?id=GTM-W53VC57\" height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>
        <script>window.dataLayer = window.dataLayer || [];</script>
        <script>
            window.dataLayer.push({
                pageCategory: '" . $subdomain . " pages',
                pageAction: '" . $subdomain . " - " . $path . "',
                pageLabel: '" . $subdomain . " - " . $path . "'
            });
        </script>";
    
    $google_analytics_code .= "
        <script>
            _atrk_opts = { atrk_acct:\"p03zj1aEsk00MA\", domain:\"sindonews.com\",dynamic: true};
            (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = \"https://certify-js.alexametrics.com/atrk.js\"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>
        <noscript><img src=\"https://certify.alexametrics.com/atrk.gif?account=p03zj1aEsk00MA\" style=\"display:none\" height=\"1\" width=\"1\" alt=\"alexametrics\"></noscript>";

    $google_analytics_code .= '
        <script>
            var _comscore = _comscore || [];
            _comscore.push({ c1: "2", c2: "9013027" });
            (function() {
              var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
              s.src = "https://sb.scorecardresearch.com/cs/9013027/beacon.js";
              el.parentNode.insertBefore(s,el);
            })();
        </script>
        <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=3.6.0&cj=1" alt="comscore tracking"></noscript>';

    return $google_analytics_code;
}

function google_analytics_old($google_account_id){
    $ci = & get_instance();
    $path = '/' . $ci->uri->segment(1);
    $geturl = parse_url(current_url());
    $host = $geturl['host'];
    $extract = explode('.',$host);
    $subdomain = $extract[0];

    $google_analytics_code = "
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=" . $google_account_id . "\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '" . $google_account_id . "', {'link_attribution': true});
            gtag('set',{'cookie_flags':'SameSite=None;Secure'});
            setTimeout(abr,30000);function abr(){gtag('event','" . $subdomain . " - " . $path . "',{event_category: '" . $subdomain . " pages',event_label: '" . $subdomain . " - " . $path . "',value: '" . $subdomain . " - " . $path . "'});}            
        </script>";

    $google_analytics_code .= "
        <script>
            _atrk_opts = { atrk_acct:\"p03zj1aEsk00MA\", domain:\"sindonews.com\",dynamic: true};
            (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = \"https://certify-js.alexametrics.com/atrk.js\"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>
        <noscript><img src=\"https://certify.alexametrics.com/atrk.gif?account=p03zj1aEsk00MA\" style=\"display:none\" height=\"1\" width=\"1\" alt=\"alexametrics\"></noscript>";

    $google_analytics_code .= '
        <script>
            var _comscore = _comscore || [];
            _comscore.push({ c1: "2", c2: "9013027" });
            (function() {
              var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
              s.src = "https://sb.scorecardresearch.com/cs/9013027/beacon.js";
              el.parentNode.insertBefore(s,el);
            })();
        </script>
        <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=3.6.0&cj=1" alt="comscore tracking"></noscript>';

    $google_analytics_code .= '
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({\'gtm.start\':new Date().getTime(),event:\'gtm.js\'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!=\'dataLayer\'?\'&l=\'+l:\'\';j.async=true;j.src=\'https://www.googletagmanager.com/gtm.js?id=\'+i+dl;f.parentNode.insertBefore(j,f);})(window,document,\'script\',\'dataLayer\',\'GTM-N5W9HTX\');</script><noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N5W9HTX" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>';
    
    return $google_analytics_code;
}

function google_analytics_adult($google_account_id){
    $ci = & get_instance();
    $path = '/' . $ci->uri->segment(1);
    $geturl = parse_url(current_url());
    $host = $geturl['host'];
    $extract = explode('.',$host);
    $subdomain = $extract[0];

    $google_analytics_code = "
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=" . $google_account_id . "\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '" . $google_account_id . "', {'link_attribution': true});
            setTimeout(abr,30000);function abr(){gtag('event','" . $subdomain . " - " . $path . "',{event_category: '" . $subdomain . " pages',event_label: '" . $subdomain . " - " . $path . "',value: '" . $subdomain . " - " . $path . "'});}            
        </script>";

    $google_analytics_code .= "
        <script>
            _atrk_opts = { atrk_acct:\"p03zj1aEsk00MA\", domain:\"sindonews.com\",dynamic: true};
            (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = \"https://certify-js.alexametrics.com/atrk.js\"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>
        <noscript><img src=\"https://certify.alexametrics.com/atrk.gif?account=p03zj1aEsk00MA\" style=\"display:none\" height=\"1\" width=\"1\" alt=\"alexametrics\"></noscript>";

    $google_analytics_code .= '
        <script>
            var _comscore = _comscore || [];
            _comscore.push({ c1: "2", c2: "9013027" });
            (function() {
              var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
              s.src = "https://sb.scorecardresearch.com/cs/9013027/beacon.js";
              el.parentNode.insertBefore(s,el);
            })();
        </script>
        <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=3.6.0&cj=1" alt="comscore tracking"></noscript>';

    return $google_analytics_code;
}

function ua_alexa_coms($google_account_id){
    $ci = & get_instance();
    $pathsource = '/' . $ci->uri->segment(1);
    $path = str_replace('readsimple','read',$pathsource);
    $geturl = parse_url(current_url());
    $host = $geturl['host'];
    $extract = explode('.',$host);
    $subdomain = $extract[0];

    $google_analytics_code = "
        <script async src=\"https://www.googletagmanager.com/gtag/js?id=" . $google_account_id . "\"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '" . $google_account_id . "', {'link_attribution': true});
            setTimeout(abr,30000);function abr(){gtag('event','" . $subdomain . " - " . $path . "',{event_category: '" . $subdomain . " pages',event_label: '" . $subdomain . " - " . $path . "',value: '" . $subdomain . " - " . $path . "'});}            
        </script>";

    $google_analytics_code .= "
        <script>
            _atrk_opts = { atrk_acct:\"p03zj1aEsk00MA\", domain:\"sindonews.com\",dynamic: true};
            (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = \"https://certify-js.alexametrics.com/atrk.js\"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>
        <noscript><img src=\"https://certify.alexametrics.com/atrk.gif?account=p03zj1aEsk00MA\" style=\"display:none\" height=\"1\" width=\"1\" alt=\"alexametrics\"></noscript>";

    $google_analytics_code .= '
        <script>
            var _comscore = _comscore || [];
            _comscore.push({ c1: "2", c2: "9013027" });
            (function() {
              var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
              s.src = "https://sb.scorecardresearch.com/cs/9013027/beacon.js";
              el.parentNode.insertBefore(s,el);
            })();
        </script>
        <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=3.6.0&cj=1" alt="comscore tracking"></noscript>';

    return $google_analytics_code;
}

function alexa_coms(){
    $google_analytics_code = "
        <script>
            _atrk_opts = { atrk_acct:\"p03zj1aEsk00MA\", domain:\"sindonews.com\",dynamic: true};
            (function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = \"https://certify-js.alexametrics.com/atrk.js\"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
        </script>
        <noscript><img src=\"https://certify.alexametrics.com/atrk.gif?account=p03zj1aEsk00MA\" style=\"display:none\" height=\"1\" width=\"1\" alt=\"alexametrics\"></noscript>";

    $google_analytics_code .= '
        <script>
            var _comscore = _comscore || [];
            _comscore.push({ c1: "2", c2: "9013027" });
            (function() {
              var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
              s.src = "https://sb.scorecardresearch.com/cs/9013027/beacon.js";
              el.parentNode.insertBefore(s,el);
            })();
        </script>
        <noscript><img src="https://sb.scorecardresearch.com/p?c1=2&c2=9013027&cv=3.6.0&cj=1" alt="comscore tracking"></noscript>';

    return $google_analytics_code;
}