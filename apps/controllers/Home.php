<?php

class Home extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helpers('gen');
        $this->load->model('news/mgonews', 'mgonews');
    }

    function index()
    {

        if ($this->mobiledevice->device_check()) {
            $this->desktop();
        } else {
            $this->mobile();
        }
    }

    function desktop()
    {
  
        $parent_id = $this->config->item('gensindo_id');
        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['title'] = 'Berita, Opini, Generasi Muda, Milenial - Gen SINDO';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Gen SINDO Berita Milenial Terkini menyajikan informasi seputar Anak Muda, Tren, Opini, Fashion dan Serba serbi Generasi Muda dan Milenial',
            'Slurp' => 'all',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $a['html']['metaname'] = metaname($metaname);
        $a['upVersion'] = $this->config->item('upVersioncssjs');  // for remove caching
        $a['html']['css'] = $this->load->view('destop2020/template/cssheader', $a, TRUE);

        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        // $a['html']['bottom_css'] .= m_add_css('/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/materialize.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/swiper.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/script.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/jquery.jscroll.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);

        $a['html']['js'] .= m_add_js('/js/loadingoverlay.min.js');

        // $a['html']['js'] .= add_external_js(base_url() . 'sn-v1.js');

        /*
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-app.js');
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-messaging.js');
        $a['html']['js'] .= add_external_js(base_url() . 'fcm.2020.js');
        */

        $a['html']['topjs'] = async_m_add_js('/js/prebid4.6.0-criteo.js');

        $a['template']['header'] = $this->load->view('destop2020/template/vheader', NULL, TRUE);
        $a['template']['minimenu'] = $this->load->view('destop2020/template/vminimenu', NULL, TRUE);
        $a['template']['minifooter'] = $this->load->view('destop2020/template/vfooter', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('destop2020/template/vnewcss', NULL, TRUE);
        $a['template']['promogen'] = $this->load->view('destop2020/template/vpromogen', NULL, TRUE);

        $t['event']['results'] = array();
        $a['template']['agenda'] = $this->load->view('template/vagenda', $t, TRUE);

        $t['trending'] = $this->mgonews->getKanalTrendingTopic($parent_id, 7);
        $a['template']['trending'] = $this->load->view('destop2020/template/vtrending', $t, TRUE);

        $t['trendingpopular'] = $this->mgonews->getListTitlePopular($parent_id, 5);
        $a['template']['popular'] = $this->load->view('destop2020/template/vpopular', $t, TRUE);

        $t['headline'] = $this->mgonews->getHeadline($parent_id, 1);
        // $t['headline'] = $this->mgonews->getListTitlePopular($parent_id, 1);
        $a['template']['headline'] = $this->load->view('destop2020/template/vheadline', $t, TRUE);
        //  echo "<pre>"; print_r($t['headline']); echo "</pre>"; die();


        $getArticleSticky = $this->mgonews->getArticleSticky();
        $t['articledata'] = $getArticleSticky;

        $t['latest'] = $this->mgonews->getListBreakingNews($parent_id, 10);

        $a['content']['latest'] = $this->load->view('destop2020/home/vlatest', $t, TRUE);

        /* Hits Token */
        $a['content']['parent_id'] = $parent_id;
        $t['site_url'] = base_url();
        $a['content']['site_url'] = $t['site_url'];


        $this->load->view('destop2020/pages/vhome', $a, FALSE);
    }


    function mobile()
    {
        //for css or js remoce caching
        $upVersion = "?v=02s99";

        $parent_id = $this->config->item('gensindo_id');
        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['title'] = 'Berita, Opini, Generasi Muda, Milenial - Gen SINDO';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Gen SINDO Berita Milenial Terkini menyajikan informasi seputar Anak Muda, Tren, Opini, Fashion dan Serba serbi Generasi Muda dan Milenial',
            'Slurp' => 'all',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $a['html']['metaname'] = metaname($metaname);

        $a['upVersion'] = $this->config->item('upVersioncssjs');  // for remove caching
        $a['html']['css'] = $this->load->view('mobile2020/template/cssheader', $a, TRUE);

        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');


        $a['html']['js'] = m_add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/materialize.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/jquery.jscroll.min.js'.$a['upVersion']);

        $a['html']['js'] .= m_add_js('/js/jquery.fancybox.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/swiper.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/script.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/loadingoverlay.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/jquery.jscroll.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);

        $a['html']['js'] .= add_external_js(base_url() . 'sn-v1.js'.$a['upVersion']);

        $a['template']['header'] = $this->load->view('mobile2020/template/vheader', NULL, TRUE);
        $a['template']['menuoverlay'] = $this->load->view('mobile2020/template/vmenuoverlay', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('mobile2020/template/vnewcss', NULL, TRUE);
        $a['template']['subkanal'] = $this->load->view('mobile2020/template/vsubkanalmenu', NULL, TRUE);
        $a['template']['footer'] = $this->load->view('mobile2020/template/vfooter', NULL, TRUE);

        $t['trending'] = $this->mgonews->getKanalTrendingTopic($parent_id, 7);
        $a['template']['trending'] = $this->load->view('mobile2020/template/vtrending', $t, TRUE);

        // $t['headine'] = $this->mgonews->getListTitlePopular($parent_id, 5);
        $t['headline'] = $this->mgonews->getHeadline($parent_id, 5);
        $a['template']['headine'] = $this->load->view('mobile2020/template/vheadline', $t, TRUE);


        $getArticleSticky = $this->mgonews->getArticleSticky();
        $t['articledata'] = $getArticleSticky;

        $t['latest'] = $this->mgonews->getListBreakingNews($parent_id, 10);
        $a['content']['latest'] = $this->load->view('mobile2020/template/vlatest', $t, TRUE);


        $this->load->view('mobile2020/pages/vhome', $a, FALSE);
        // echo "<pre>"; print_r($a); echo "</pre>"; die();
    }
}
