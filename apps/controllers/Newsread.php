<?php

class Newsread extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->config('site_channel');
        $this->load->config('site_domain');

        $this->load->library('goarticleamp');
        $this->load->model('news/mgonews');
    }

    function index()
    {
        $parent_id = $this->config->item('gensindo_id');

        $a = array();
        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['mcss'] = $this->load->view('amp/vampcss', NULL, TRUE);


        /* get article data */
        $getarticle = $this->goarticleamp->getNewArticle();

      
        $t['details'] = $getarticle['details'];
        $content_id = $getarticle['content_id'];

        $uriread = $this->goarticleamp->createPathUrl($t['details']);
       
        $getEditorData = $this->mgonews->getEditorByNews($content_id);
     

        $t['content_id'] = $content_id;
        $t['site_url'] = site_url('read') . '/' . $uriread;
        $t['share_url'] = $t['site_url'];
        $t['amp_url'] = site_url('newsread') . '/' . $uriread;
        $a['content']['site_url'] = $t['site_url'];
        $a['content']['share_url'] = $t['site_url'];
        $a['content']['amp_url'] = $t['amp_url'];
        $a['content']['content_id'] = $content_id;
        $a['content']['site_editor'] = ucwords($getEditorData[0]['fullname']);
        $a['content']['date_publish'] = date('Ymd', strtotime($t['details']['date_published']));
        $a['content']['time_publish'] = date('H:i', strtotime($t['details']['date_published']));
        $a['content']['site_label'] = textlimit($t['details']['title'], 40);
  
        $t['parent_id'] = $parent_id;

        $topic_id_list = '';
        $topic_keyword = array();
        $getRelated = array();
        $getbaca = array();
        $getTopicData = array();

        $getTopic = $this->mgonews->getTopicContent($content_id);
        if (count($getTopic) > 0) {
            $topic_id_list = $getTopic[0]['id_topik'];
            if ($topic_id_list != '') {
                $getRelated = $this->mgonews->getRelatedContent($topic_id_list,$parent_id,$content_id,10);
                //$getbaca = $this->mgonews->getRelatedContent($topic_id_list,$parent_id,$content_id,16);
                $getbaca = $getRelated;

                $topicData = explode(',', $topic_id_list);
                for ($i = 0; $i < count($topicData); $i++) {
                    $getTopicData[$i] = $this->mgonews->getTopicList($topicData[$i]);
                    $topic_keyword[$i] = $getTopicData[$i][0]['topik'];
                }
            }
        }
        

        $t['topiklist'] = $topic_id_list;
        $t['related'] = $getRelated;

        $a['content']['related'] = $this->load->view('amp/vamprelated', $t, true);

        $relatedLink = array();
        if (count($getRelated) > 0 && count($getRelated) > 3) {
            for ($i = 0; $i < 3; $i++) {
                $site_domain[$i] = $this->config->item('read_domain');
                $dtime[$i] = strtotime($getRelated[$i]['date_created']);
                $relatedLink[$i] = 'https://' . $site_domain[$i][$getRelated[$i]['id_subkanal']] . '/read/' . $getRelated[$i]['id_news'] . '/' . $getRelated[$i]['id_subkanal'] . '/' . slug($getRelated[$i]['title']) . '-' . $dtime[$i];
            }
        }
        $a['content']['relatedlink'] = $relatedLink;

        $suggest = $this->mgonews->getSuggest($topic_id_list, $parent_id, $content_id);
        $s_rand = rand(0, count($suggest) - 1);
        $t['suggest'] = $suggest[$s_rand];

        $t['populer'] = $this->mgonews->getListTitlePopular($parent_id, 5);
        $a['content']['populer'] = $this->load->view('amp/vamppopuler', $t, TRUE);

        $t['others'] = $this->mgonews->getListNews($parent_id, 10);
        $a['content']['others'] = $this->load->view('amp/vampothers', $t, true);

        $a['content']['rich'] = '';
        $contentrich = $t['others'];
        if (count($contentrich) > 0) {
            $snippet = array();
            for ($i = 0; $i < 3; $i++) {
                $snippet[$i]['@type'] = 'ListItem';
                $snippet[$i]['position'] = $i + 1;

                $dtime[$i] = strtotime($contentrich[$i]['date_created']);
                $urlsnippet[$i] = site_url('read') . '/' . $contentrich[$i]['content_id'] . '/' . $contentrich[$i]['channel_id'] . '/' . slug($contentrich[$i]['title']) . '-' . $dtime[$i];
                $snippet[$i]['url'] = $urlsnippet[$i];
            }

            $a['content']['rich'] = json_encode($snippet);
        }

        /* article pagination */
        $baca = array();
        if(count($getbaca) > 0 && count($getbaca) > 3){
            //$databaca = array_slice($getbaca,10,16);
            //$baca = randomData($databaca,6,2);
            $baca = randomData($getbaca,9,3);
        }


        $totalData = $this->goarticleamp->getArticleNumListPageBaca($t['details'], $baca);

        $start = $this->uri->segment(5, 0);
        if (isset($_GET['showpage']) && $_GET['showpage'] == 'all') {
            $offset = 100;
        } else {
            $defaultpage = 5;
            if ($totalData > ($defaultpage + 3)) {
                $offset = $defaultpage;
            } else {
                $offset = 100;
            }
        }

        $t['content'] = $this->goarticleamp->getArticleListPageBaca($start, $offset, $t['details'], $baca, array());
        $t['totalData'] = $totalData;
        $a['content']['totalData'] = $totalData;

        $t['per_page'] = $offset;
        $t['start'] = $start;

        $this->load->library('pagination');
        $this->config->load('pagination', FALSE, TRUE);
        $config = $this->config->item('artpaging');
        $config['base_url'] = site_url('newsread/' . $uriread);
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        $currentPage = floor(($start / $config['per_page']) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;
        if ($start == 0 || $start == '') {
            $pagetitle = '';
        }
        $t['current_page'] = $currentPage;
        $t['last_page'] = floor(($t['totalData'] / $config['per_page']) + 1);

        /* create canonical URL */
        $canonicalLimitPage = 10; /* parameter offset page at read.php */
        $totalParagraf = $t['totalData'] - 1; /* ada selisih 1 paragraf dengan halaman non-amp */
        $paging_status = 0;
        if ($totalParagraf > ($canonicalLimitPage + 3)) {
            $paging_status = 1;
        }

        $canonical = add_canonical('canonical',$t['site_url']);
        $a['html']['canonical'] = $canonical;
        

        /* create keyword tags */
        $content_keyword = trim(strtolower($t['details']['keyword']));
        if ($content_keyword == '') {
            $article_keyword = '';
        } else {
            $article_keyword = ',' . $content_keyword;
        }
        $get_topic_keyword = implode(',', $topic_keyword);
        $keyword = $this->goarticleamp->newsKeywords($get_topic_keyword . $article_keyword);
        $t['keyword'] = $keyword;
        $rich_keywords = explode(',', $keyword);
        $a['content']['keyword'] = $rich_keywords;
        $a['content']['site_tags'] = $keyword;

        /* checking image availability */
        if (isset($t['details']['photo'])) {
            $imgmeta = $this->config->item('images_uri') . '/dyn/620/pena/news/' . date('Y/m/d', strtotime($t['details']['date_created'])) . '/' . $t['details']['id_subkanal'] . '/' . $t['details']['id_news'] . '/' . $t['details']['photo'];
        } else {
            $imgmeta = '';
        }
        $a['content']['site_image'] = $imgmeta;

        if ($t['details']['author'] != '') {
            $reporter = ucwords($t['details']['author']);
        } else {
            $reporter = '';
        }

        $extend_desc = summaryMore($t['details']['content']);
        if (strlen($extend_desc) < 120) {
            $article_summary = $t['details']['summary'] . ' ' . $extend_desc;
        } else {
            $article_summary = $t['details']['summary'];
        }

        /* title & description filtering */
        //$metatitle = seo_title($t['details']['title']);
        $metatitle = cleanTitleArticle($t['details']['title']);
        $metadesc = seo_description($article_summary);
        $a['content']['site_title'] = $metatitle;
        $a['content']['site_summary'] = $metadesc;

        /* create html meta tags */
        $a['html']['title'] = $metatitle . $pagetitle;
        $metaname = array(
            'description' => $metadesc . $pagetitle,
            'title' => $metatitle . $pagetitle,
            'image_src' => $imgmeta,
            'keywords' => $keyword,
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        /* create facebook meta tags */
        $metafb = array(	
            'title' => cleanTitleArticle($t['details']['title']),	
            'type' => 'article',	
            'url' => $t['site_url'],	
            'image' => $imgmeta,	
            'image:type' => 'image/jpeg',	
            'image:width' => 620,	
            'image:height' => 413,	
            'description' => cleanSummary($metaname['description']),	
            'site_name' => 'SINDOnews.com'	
        );

        /* create twitter card tags */
        $metatwit = array(	
            'twitter:card' => 'summary_large_image',	
            'twitter:site' => '@SINDOnews',	
            'twitter:creator' => '@SINDOnews',	
            'twitter:title' => cleanTitleArticle($t['details']['title']),	
            'twitter:description' => cleanSummary($metaname['description']),	
            'twitter:image' => $imgmeta	
        );


        $metadable = array(
            'item_id' => $content_id,
            'title' => cleanWords($t['details']['title']),
            'image' => $imgmeta,
            'author' => $reporter
        );
        if(count($getTopicData) > 1){
            $section_a = trim($getTopicData[0][0]['topik']);
            $section_b = trim($getTopicData[1][0]['topik']);
            $metaArticle = array(
                'section' => 'seleb',
                'section2' => $section_a,
                'section3' => $section_b,
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }else{
            $metaArticle = array(
                'section' => 'generasi milenial',
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }
        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);
        $a['html']['metaname'] .= metaDable($metadable);
        $a['html']['metaname'] .= metaArticle($metaArticle);


        /* AMP Google */
        $a['content']['site_publish'] = date('Y-m-d\TH:i:sP', strtotime($t['details']['date_published']));
        $a['content']['site_reporter'] = $t['details']['author'];
        $a['content']['channel_id'] = $t['details']['id_kanal'];
        $a['content']['channel_name'] = $t['details']['subkanal'];

        /* GA Custom Dimension Start */
        $a['content']['cd_title'] = cleanTitleArticle($t['details']['title']) . $pagetitle;
        $a['content']['cd_subkanal'] = customDimensionString($a['content']['channel_name']);
        $a['content']['cd_author'] = ucwords($a['content']['site_reporter']);
        $a['content']['cd_editor'] = ucwords($a['content']['site_editor']);
        $a['content']['cd_tags'] = customDimensionTags(trim($a['content']['site_tags']));
        $a['content']['cd_publish'] = $t['details']['date_published'];
        
        $article_per_page = 10;
        if($t['totalData'] > $article_per_page){
            $currentPage = floor(($start / $article_per_page) + 1);
            $cd_page = $currentPage;
        }else{
            $cd_page = 0;
        }
        $a['content']['cd_page'] = $cd_page;
        /* GA Custom Dimension End */

        $a['content']['details'] = $this->load->view('amp/vampdetail', $t, true);

        $getVideoFrame = $this->mgonews->getLatestVideo(0, 15);
        $totalVideoFrame = count($getVideoFrame);
        $a['content']['totalvideoframe'] = $totalVideoFrame;
        if ($totalVideoFrame > 0) {
            if ($totalVideoFrame > 1) {
                $maxvid = ($totalVideoFrame - 1);
                $vid = rand(0, $maxvid);
            } else {
                $vid = 0;
            }

            if ($getVideoFrame[$vid]['photo']) {
                $pict = images_uri() . '/dyn/620/salsabila/video/' . date('Y/m/d', strtotime($getVideoFrame[$vid]['date_created'])) . '/' . $getVideoFrame[$vid]['id_subkanal'] . '/' . $getVideoFrame[$vid]['id_video'] . '/' . $getVideoFrame[$vid]['photo'];
            } else {
                $pict = 'https://cdn.sindonews.net/1x1.gif';
            }

            $a['content']['video_url'] = 'https://video.sindonews.com/play/' . $getVideoFrame[$vid]['id_video'] . '/' . slug($getVideoFrame[$vid]['title']);
            $a['content']['video_images'] = $pict;
            $a['content']['frame_title'] = addslashes(cleanWords($getVideoFrame[$vid]['title']));
            $a['content']['frame_url'] = 'https://video.sindonews.com/embedjw/' . $getVideoFrame[$vid]['id_video'] . '?ads=1';
        } else {
            $a['content']['video_url'] = '';
            $a['content']['video_images'] = '';
            $a['content']['frame_title'] = '';
            $a['content']['frame_url'] = '';
        }

        /* Hits Token */
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $token_encode = base64_encode($token);
        $a['content']['pixel'] = 'r=' . randomString(11) . '&z=' . $token_encode . '&c=' . $t['details']['id_news'] . '&ch=' . $t['details']['id_kanal'];

        $this->load->view('amp/vampread', $a, false);
    }
}
