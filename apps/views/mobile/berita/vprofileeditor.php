<?php

// if($details['id_group'] == '2'){
    // $img = $this->config->item('images_uri') . $this->config->item('dyn_120').'/gensindo/user/' . $details['id_user'] . '/' . $details['photo_user'];
    // $facebook = $details['facebook'];
    // $instagram = $details['instagram'];
    // $twitter = $details['twitter'];
    // $caption_user = $details['caption_user'];
    // $username = $details['username'];
// }else{
    // /* set for redaksi sindonews */
    // $img = $this->config->item('images_uri') . $this->config->item('dyn_120').'/gensindo/user/anonynus.png';
    // $facebook = 'https://www.facebook.com/sindonews';
    // $instagram = 'https://www.instagram.com/sindonews/';
    // $twitter = 'https://twitter.com/sindonews';
    // $caption_user = 'SINDOnews';
    // $username = 'Redaksi SINDOnews';
// }

$img = $this->config->item('images_uri') . $this->config->item('dyn_120').'/gensindo/user/anonynus.png';
$facebook = 'https://www.facebook.com/sindonews';
$instagram = 'https://www.instagram.com/sindonews/';
$twitter = 'https://twitter.com/sindonews';
$caption_user = 'SINDOnews';
$username = 'Redaksi SINDOnews';
	
$author = $details['author'];
$link_author = site_url('reporter') .'/'. $details['slug_author'];

echo '<div class="author-bio clearfix">
    <div class="image pull-left"><img src="'. $img .'" alt="'. $username .'"></div>
    <div class="profile pull-right">
        <div class="follow-author">
            <ul>
                <li class="head">Follow :</li>
                <li><a target="_blank" href="'. $facebook .'"><i class="fa fa-facebook-square"></i></a></li>
                <li><a target="_blank" href="'. $instagram .'"><i class="fa fa-instagram"></i></a></li>
                <li><a target="_blank" href="'. $twitter .'"><i class="fa fa-twitter-square"></i></a></li>
            </ul>
        </div>
        <div class="caption">'. $caption_user .'</div>
        <div class="more"><a href="'. $link_author .'">More articles from '. $author .'</a></div>
    </div>
</div>';