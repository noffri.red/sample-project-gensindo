

<?php if(count($related)>0) {?>
<div class="head">
    <div class="title">Berita Terkait</div>
</div>
<div class="list-news">
<ul>

  <?php foreach ($related as $value){

        $site_domain = $this->config->item('read_domain');
        $dtime = strtotime($value['date_created']);
        $l= 'https://' . $site_domain[$value['id_subkanal']] . '/read/' . $value['id_content'] . '/' . $value['id_subkanal'] . '/' . slug($value['title']) . '-' . $dtime;
        $title = cleanWords($value['title']);

        $img = images_uri() . '/dyn/360/gensindo/content/' . date('Y/m/d',strtotime($value['date_created'])) . '/' . $value['id_subkanal'] . '/' . $value['id_content'] . '/' . $value['images'];
        $subkanal = $value['subkanal'];
        $ago = time_difference($value['date_published']);
        
        ?>
        
        <li>
        
            <div class="image"><a href="<?php echo $l ?>"><img class="lazyload" data-src="<?php echo $img; ?>" alt="<?php echo $title; ?>" width="360"></a></div>
            <div class="block-caption">
            <div class="title"><a href="<?php echo $l ?>"><?php echo $title; ?></a></div>
            <!-- <div class="category gen-news"><a href="#"><?php //echo $subkanal ?></a></div> -->
            <!-- <div class="time"><?php //echo $ago ?></div> -->
            </div>
        </li>
    <?php } ?>
</ul>
</div>
<?php } ?>