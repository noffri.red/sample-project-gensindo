<?php

// echo '<div id="sinnav">
// <div class="container">
// <div class="cl-effect-5">
// <a href="https://nasional.sindonews.com"><span data-hover="Nasional">Nasional</span></a>
// <a href="https://metro.sindonews.com"><span data-hover="Metro">Metro</span></a>
// <a href="https://daerah.sindonews.com"><span data-hover="Dareah">Daerah</span></a>
// <a href="https://ekbis.sindonews.com"><span data-hover="Ekbis">Ekbis</span></a>
// <a href="https://international.sindonews.com"><span data-hover="International">International</span></a>
// <a href="https://sports.sindonews.com"><span data-hover="Sports">Sports</span></a>
// <a href="https://autotekno.sindonews.com"><span data-hover="Autotekno">Autotekno</span></a>
// <a href="https://lifestyle.sindonews.com"><span data-hover="Lifestyle">Lifestyle</span></a>
// <a href="https://kalam.sindonews.com"><span data-hover="Kalam">Kalam</span></a>
// <a href="https://photo.sindonews.com"><span data-hover="Photo">Photo</span></a>
// <a href="https://video.sindonews.com"><span data-hover="Video">Video</span></a>
// </div>
// </div>
// </div>';
echo '<nav>
    <div class="container">
        <section class="slicknav">
            <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </section>
        <a href="' . base_url() . '" class="logo"><img width="273" height="40" src="' . template_uri() . '/images/logo.png" alt="GEN SINDO"></a>
        <ul class="sosmed pull-right">
            <li><a href="https://www.facebook.com/sindonews" target="_blank" title="facebook"><img src="' . template_uri() . '/images/icon_facebook.png" alt="Facebook"></a></li>
            <li><a href="https://www.instagram.com/sindonews/" target="_blank" title="Instagram"><img src="' . template_uri() . '/images/icon_instagram.png" alt="Instagram"></a></li>
            <li><a href="https://www.youtube.com/channel/UCCrfYLen-jXQAAJRo4oRbZQ" target="_blank" title="Youtube"><img src="' . template_uri() . '/images/icon_youtube.png" alt="Youtube"></a></li>
            <li><a href="https://twitter.com/sindonews" target="_blank" title="twitter"><img src="' . template_uri() . '/images/icon_twitter.png" alt="Twitter"></a></li>
        </ul>
    </div>
</nav>';
