<?php

class Uc extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->config('site_channel');
        $this->load->config('site_domain');

        $this->load->library('ucnews');
        $this->load->model('news/mgonews');
        $this->load->model('news/mes');
    }

    function index(){
        $a = array();
        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $getarticle = $this->ucnews->getNewArticle();
        $t['details'] = $getarticle['details'];
        $content_id = $getarticle['content_id'];

        $uriread = $this->ucnews->createPathUrl($t['details']);
        $t['site_url'] = site_url('read') . '/' . $uriread;
        $a['content']['site_url'] = site_url('read') . '/' . $uriread;

        /* checking image availability */
        if(isset($t['details']['photo'])){
            $imgmeta = $this->config->item('images_uri') . '/dyn/620/pena/news/' . date('Y/m/d',strtotime($t['details']['date_created'])) . '/' . $t['details']['id_subkanal'] . '/' . $t['details']['id_news'] . '/' . $t['details']['photo'];
        }else{
            $imgmeta = '';
        }
        $a['content']['site_image'] = $imgmeta;

        $extend_desc = summaryMore($t['details']['content']);
        if(strlen($extend_desc) < 120){
            $article_summary = $t['details']['summary'] . ' ' . $extend_desc;
        }else{
            $article_summary = $t['details']['summary'];
        }

        $metatitle = cleanWords($t['details']['title']);
        $metadesc = seo_description($article_summary);

        $a['html']['title'] = $metatitle;
        $metaname = array(
            'description' => $metadesc,
            'title' => $metatitle,
            'image_src' => $imgmeta,
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing')
        );

        /* create facebook meta tags */
        $metafb = array(
            'title' => cleanWords($t['details']['title']),
            'type' => 'article',
            'url' => $t['site_url'],
            'image' => $imgmeta,
            'description' => cleanWords($metaname['description']),
            'site_name' => 'SINDOnews.com'
        );

        /* create twitter card tags */
        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@SINDOnews',
            'twitter:creator' => '@SINDOnews',
            'twitter:url' => $t['site_url'],
            'twitter:domain' => 'sindonews.com',
            'twitter:title' => cleanWords($t['details']['title']),
            'twitter:description' => cleanWords($metaname['description']),
            'twitter:image:src' => $imgmeta
        );

        /* set all SEO meta tags */
        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);

        $parent_id = $this->config->item('gensindo_id');
        
        $topic_id_list = '';
        $topic_keyword = array();
        $getRelated = array();
        $getTopicData = array();

        $getTopic = $this->mgonews->getTopicContent($content_id);
        if(count($getTopic) > 0){
            $topic_id_list = $getTopic[0]['id_topik'];
            if($topic_id_list != ''){
                $getRelated = $this->mgonews->getRelatedContent($topic_id_list,$parent_id,$content_id,10);

                $topicData = explode(',',$topic_id_list);
                for($i = 0; $i < count($topicData); $i++){
                    $getTopicData[$i] = $this->mgonews->getTopicList($topicData[$i]);
                    $topic_keyword[$i] = $getTopicData[$i][0]['topik'];
                }
            }
        }
        
        $t['related'] = randomData($getRelated,10,2);

        $getVideoFrame = $this->mgonews->getLatestVideo(0,10);
        $totalVideoFrame = count($getVideoFrame);
        $t['totalvideoframe'] = $totalVideoFrame;
        if($totalVideoFrame > 0){
            if($totalVideoFrame > 1){
                $maxvid = ($totalVideoFrame - 1);
                $vid = rand(0,$maxvid);
            }else{
                $vid = 0;
            }
            
            if($getVideoFrame[$vid]['photo']){
                $pict = images_uri() . '/dyn/620/salsabila/video/' . date('Y/m/d',strtotime($getVideoFrame[$vid]['date_created'])) . '/' . $getVideoFrame[$vid]['id_subkanal'] . '/' . $getVideoFrame[$vid]['id_video'] . '/' . $getVideoFrame[$vid]['photo'];
            }else{
                $pict = 'https://cdn.sindonews.net/1x1.gif';
            }

            $t['video_url'] = 'https://video.sindonews.com/play/' . $getVideoFrame[$vid]['id_video'] . '/' . slug($getVideoFrame[$vid]['title']);
            $t['video_images'] = $pict;
            $t['frame_title'] = addslashes(cleanWords($getVideoFrame[$vid]['title']));
            $t['frame_url'] = 'https://video.sindonews.com/embed/' . $getVideoFrame[$vid]['id_video'] . '?ads=1';
        }else{
            $t['video_url'] = '';
            $t['video_images'] = '';
            $t['frame_title'] = '';
            $t['frame_url'] = '';
        }

        $a['content']['details'] = $this->load->view('uc/vucdetail',$t,true);

        /* Hits Token */
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $token_encode = base64_encode($token);
        $a['content']['pixel'] = 'r=' . randomString(11) . '&z=' . $token_encode . '&c=' . $t['details']['id_news'] . '&ch=' . $t['details']['id_kanal'];

        $this->load->view('uc/vuc',$a,false);
    }

}
