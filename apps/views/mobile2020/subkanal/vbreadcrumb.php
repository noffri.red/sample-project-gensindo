<div id="detail">

    <div class="breadcrumb">
        <ul>
            <li><a href="<?php echo base_url(); ?>">Home</a></li>
            <?php if (count($breaking['results']) > 0) { ?>
                <li><a href="#"><i class="fas fa-angle-right"></i></a></li>
                <?php if ($this->uri->segment(1) == 'samsung') { ?>
                    <li><a href="<?php echo current_url() ?>">Samsung</a></li>
                <?php } else { ?>
                    <li><a href="<?php echo current_url() ?>"><?php echo $breaking['results'][0]['subkanal'] ?></a></li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>

</div>