<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subkanal extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helpers("gen");
        $this->load->model('news/mgonews','mgonews');
    }

    function index(){
        if($this->mobiledevice->device_check()){
            $this->desktop();
        }else{
            $this->mobile();
        }
    }

    function desktop(){
        $parent_id = $this->config->item('gensindo_id');
        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= m_add_css('/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-1.10.2.min.js');
        $a['html']['js'] .= add_js('/js/materialize.min.js');
        $a['html']['js'] .= add_js('/js/jquery.jscroll.min.js');
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js');

        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['promoted'] = $this->load->view('template/vpromoted',NULL,TRUE);
        $a['template']['kategori'] = $this->load->view('template/vkategori',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('template/vminifooter',NULL,TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss',NULL,TRUE);
        $a['template']['promogen'] = $this->load->view('template/vpromogen',NULL,TRUE);

        // $t['event'] = $this->mgonews->getEvent();
        $t['event']['results'] = array();
        $a['template']['agenda'] = $this->load->view('template/vagenda',$t,TRUE);

        $t['trending'] = $this->mgonews->getKanalTrendingTopic($parent_id,7);
        $a['template']['trending'] = $this->load->view('template/vtrending',$t,TRUE);

        // $t['trendingpopular'] = $this->mgonews->getListTitlePopular($parent_id,10);
        $t['trendingpopular'] = array();
        $a['template']['popular'] = $this->load->view('template/vpopular',$t,TRUE);

        $t['breaking']['results'] = $this->mgonews->getListNews($id_subkanal,20);
        $a['content']['breaking'] = $this->load->view('subkanal/vbreaking',$t,TRUE);
        $a['content']['breadcrumb'] = $this->load->view('subkanal/vbreadcrumb',$t,TRUE);

        $a['html']['title'] = 'Berita Gen SINDO dan Generasi Milenial - Gen SINDO';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Berita Gen SINDO dan Generasi Milenial Terkini menyajikan informasi seputar Anak Muda, Tren, Serba serbi Generasi Muda',
            'Slurp' => 'all',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $a['html']['metaname'] = metaname($metaname);

        $this->load->view('pages/vsubkanal',$a,FALSE);
    }

    function mobile(){
        
    }

}
