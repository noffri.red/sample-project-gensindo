<?php

$total = count($popular);
if($total > 0){
    $data = $popular;

    echo '<ul>';
    $i = 0;
    if(!empty($data[$i]['id_content'])){
        $id_content[$i] = $data[$i]['id_content'];
        $img[$i] = images_uri().'/dyn/300/gensindo/content/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$data[$i]['id_subkanal'].'/'.$id_content[$i].'/'.$data[$i]['images'];
        $subakanal[$i] = $data[$i]['subkanal'];
		$slug_subkanal[$i] = $data[$i]['slug_subkanal'];
        $dtime[$i] = strtotime($data[$i]['date_created']);
        $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);
    }
    if(!empty($data[$i]['id_imaji'])){
        $id_content[$i] = $data[$i]['id_imaji'];
        $img[$i] = images_uri().'/dyn/300/gensindo/imaji/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$id_content[$i].'/'.$data[$i]['images'];
        $subakanal[$i] = 'Gen View';
		$slug_subkanal[$i] = 'imaji';
        $dtime[$i] = strtotime($data[$i]['date_created']);
        $link[$i] = site_url('view/' . $id_content[$i] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);
    }

    $title[$i] = cleanTitleArticle($data[$i]['title']);
    $summary[$i] = cleanWords($data[$i]['summary']);
    $ago[$i] = time_difference($data[$i]['date_published']);


    echo '<li>
            <div class="image"><a href="' . $link[$i] . '"><img data-src="' . $img[$i] . '" alt="'.cleanHTML($title[$i]) .'" width="300"></a></div>
            <div class="block-caption">
                <div class="title"><a href="' . $link[$i] . '">' . $title[$i] . '</a></div>
                <div class="category gen-news"><a href="#">'.ucwords($subakanal[$i]).'</a></div>
                <div class="time">' . $ago[$i] . '</div>
            </div>
        </li>';

    echo '</ul>';
    echo '<ul>';

    for($i=1; $i<$total; $i++){
        if(!empty($data[$i]['id_content'])){
            $id_content[$i] = $data[$i]['id_content'];
            $img[$i] = images_uri().'/dyn/300/gensindo/content/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$data[$i]['id_subkanal'].'/'.$id_content[$i].'/'.$data[$i]['images'];
            $subakanal[$i] = $data[$i]['subkanal'];
			$slug_subkanal[$i] = $data[$i]['slug_subkanal'];
            $dtime[$i] = strtotime($data[$i]['date_created']);
            $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);
        }
        if(!empty($data[$i]['id_imaji'])){
            $id_content[$i] = $data[$i]['id_imaji'];
            $img[$i] = images_uri().'/dyn/300/gensindo/imaji/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$id_content[$i].'/'.$data[$i]['images'];
            $subakanal[$i] = 'Gen View';
			$slug_subkanal[$i] = 'imaji';
            $dtime[$i] = strtotime($data[$i]['date_created']);
            $link[$i] = site_url('view/' . $id_content[$i] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);
        }

        $title[$i] = cleanWords($data[$i]['title']);
        $summary[$i] = cleanWords($data[$i]['summary']);
        $ago[$i] = time_difference($data[$i]['date_published']);

        echo '<li>
                <div class="image"><a href="' . $link[$i] . '"><img data-src="' . $img[$i] . '" alt="'.$title[$i].'"></a></div>
                <div class="block-caption">
                    <div class="title"><a href="' . $link[$i] . '">' . $title[$i] . '</a></div>
                    <div class="category gen-news"><a href="#">Gen News</a></div>
                    <div class="time">' . $ago[$i] . '</div>
                </div>
            </li>';
    }

    echo '</ul>';

    // echo '<a href="#" class="btn-more">View More</a>';
}
