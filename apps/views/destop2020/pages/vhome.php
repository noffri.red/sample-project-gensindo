<!doctype html>
<html lang="id-ID">

<head>
  <!-- Meta SEO -->
  <title><?php echo $html['title']; ?></title>
  <meta charset="utf-8">
  <meta http-equiv="x-dns-prefetch-control" content="on">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
  <meta name="theme-color" content="#499AE8">
  <?php echo $html['metaname']; ?>
  <!-- Icon -->
  <link rel="shortcut icon" href="<?php echo template_uri(); ?>/image/favicon.ico">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-160x160.png" sizes="160x160">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-192x192.png" sizes="192x192">
  <!-- Font -->
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;900&display=swap" rel="stylesheet"> <!-- Nunito -->
  <!-- CSS -->
  <!-- Development -->
  <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css"> -->
  <!-- Bootstrap -->
  
  <link rel="canonical" href="<?php echo base_url(); ?>">
  <link rel="manifest" href="<?php echo base_url(); ?>manifest.json">
  <link rel="alternate" title="Gen SINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>

  <?php echo $html['css']; ?>
  <?php echo $template['newcss']; ?>

  <!-- Production -->
  <!-- <link rel="stylesheet" href="assets/css/styles.min.css"> -->
  <!-- Style -->
  <!-- Mediaquery -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-HDPI.min.css" media="screen and (min-width: 1083px) and (max-width: 1290px)"> <!-- HDPI Screen -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-MDPI.min.css" media="screen and (min-width: 993px) and (max-width: 1082px)"> <!-- MDPI Screen -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-SDPI.min.css" media="screen and (min-width: 769px) and (max-width: 992px)"> <!-- SDPI Screen -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-mobile.min.css" media="screen and (max-width: 768px)"> <!-- Mobile Screen -->


  <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
  <script>
    window.googletag = window.googletag || {
      cmd: []
    };
    
    var wv = navigator.userAgent.toLowerCase();
    var aggr = "0";
    if(wv.includes('topbuzz') || wv.includes('babe') || wv.includes('bacaberita')) {
        aggr = "topbuzz";
    } else if (wv.includes('babe')){
        aggr = "babe";
    } else if (wv.includes('bacaberita')){
        aggr = "babe";
    } else if (wv.includes('ucbrowser')){
        aggr = "ucbrowser";
    } else if (wv.includes('kurio')){
        aggr = "kurio";
    } else {
        aggr = "other";
    }

    googletag.cmd.push(function() {
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [970, 250],
        [970, 90],
        [728, 90],
        [1, 1]
      ], 'div-gpt-ad-billboard').setTargeting('pos', ['billboard']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [300, 250],
        [300, 600]
      ], 'div-gpt-ad-right_sidebar_1').setTargeting('pos', ['right_sidebar_1']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [300, 250]
      ], 'div-gpt-ad-right_sidebar_2').setTargeting('pos', ['right_sidebar_2']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [300, 250]
      ], 'div-gpt-ad-right_sidebar_3').setTargeting('pos', ['right_sidebar_3']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [300, 250]
      ], 'div-gpt-ad-right_sidebar_4').setTargeting('pos', ['right_sidebar_4']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [120, 600]
      ], 'div-gpt-ad-left_skinads').setTargeting('pos', ['left_skinads']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [[336,280],[300,250],[1,1]], 'div-gpt-ad-middle_1').setTargeting('pos', ['middle_1']).addService(googletag.pubads());

      googletag.defineOutOfPageSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', 'div-gpt-ad-outstream').setTargeting('pos', ['outstream']).addService(googletag.pubads());
      googletag.defineOutOfPageSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', 'div-gpt-ad-abm').setTargeting('partner', ['abm']).addService(googletag.pubads());

      

      googletag.pubads().setTargeting('page_type', ['kanalpage']);
      googletag.pubads().setTargeting('aggregator', aggr);
      googletag.pubads().enableSingleRequest();
      googletag.pubads().collapseEmptyDivs();
      googletag.enableServices();
      /*
      googletag.pubads().addEventListener('slotRenderEnded', function(event) {
          var size = event.size;
          if(size === null) return;
          
          var slot = event.slot;
          var slotDiv = document.getElementById(slot.getSlotElementId());
          if(!event.isEmpty){
              slotDiv.style.height = size[1] + 'px';
          }
      });
      */

    });

    function bannerCheck(unitAd) {
      googletag.pubads().addEventListener('slotRenderEnded', function(event) {
        if (event.slot.getSlotElementId() === unitAd) {
          if (event.isEmpty) {
            var eAd = document.getElementById('me-' + unitAd);
            eAd.classList.add('adnone');
          }
        }
      });
    }
  </script>

<script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
  <script>
    window.OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
      OneSignal.init({
        appId: '<?php echo $this->config->item('onesignal_appid'); ?>',
        autoResubscribe: true,
        notifyButton: {
          enable: false
        },
        promptOptions: {
          slidedown: {
            enabled: true,
            autoPrompt: true,
            actionMessage: "Dapatkan update berita SINDOnews melalui notifikasi browser Anda.",
            acceptButtonText: "SUBSCRIBE",
            cancelButtonText: "TIDAK"
          }
        }
      });
    });
  </script> 

  <?php echo $html['bottom_js']; ?>
</head>

<body>
  <div id="app">


    <!-- Header -->
    <header>
      <div class="header-top">
        <?php echo $template['header']; ?>
      </div>
      <div class="header-middle">
        <div class="container">
          <!-- Logo -->
          <div id="logo">
            <a href="<?php echo base_url(); ?>"><img src="<?php echo template_uri(); ?>/image/logo.png" alt="Gensindo" width="300" height="45"></a>
          </div>
          <!-- Search -->
          <div id="search">
            <form class="search-field sample two" name="myForm" method="get" action="" onsubmit="return validateForm(this)">
              <input type="text" name="search" id="search-input" placeholder="Cari Berita" autocomplete="off">
              <button type="submit" class="btn btn-search fa fa-search"></button>
              <button onfocus="document.getElementById('search-input').value = ''" type="reset" class="btn btn-reset fa fa-times"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="header-bottom">
        <?php echo $template['minimenu']; ?>
      </div>
    </header>
    <!-- <div class="mask-header">Mask</div> -->

    <!-- Main -->
    <main>


      <!-- Headline -->
      <div id="headline">

        <div class="billboard">
          <div id="div-gpt-ad-billboard">
            <script>
              googletag.cmd.push(function() {
                googletag.display('div-gpt-ad-billboard');
              });
            </script>
          </div>
        </div>

        <!-- headline -->
        <div class="container">

          <?php echo $template['headline']; ?>


        </div>

      </div>
      <div class="container">
        <!-- View -->
        <div id="view">
          <!-- Latest News -->
          <div id="latest">
            <div class="head">
              <div class="title">Latest News</div>
              <!-- <div class="view-all"><a href="#">View All</a></div> -->
            </div>
            <div class="list-news">


              <?php echo $content['latest']; ?>
            </div>
          </div>
        </div>
        <!-- Sidebar -->
        <div id="sidebar">

        
          <!-- Trending Topic -->
          <div id="trending-topic">
            <?php echo $template['trending'] ?>
          </div>

          <div id="trending-topic">

            <div class="r-side mb20 medium-banner">
              <div id="div-gpt-ad-right_sidebar_1">
                <script>
                  googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-right_sidebar_1');
                  });
                </script>
              </div>
            </div>

          </div>
          


          <!-- Announcement -->
          <div id="announcement">
            <?php echo $template['promogen'] ?>
          </div>

         
          <!-- Terpopuler -->
          <div id="terpopuler">
            <?php echo $template['popular'] ?>
          </div>

          

          <div class="r-side mb20 mt20important medium-banner">
            <div id="div-gpt-ad-right_sidebar_2">
              <script>
                googletag.cmd.push(function() {
                  googletag.display('div-gpt-ad-right_sidebar_2');
                });
              </script>
            </div>
          </div>

          <div>
            <div id="div-gpt-ad-abm"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-abm');});</script></div>
            <div id="div-gpt-ad-outstream"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-outstream');});</script></div>
        </div>
          
        </div>
      </div>
    </main>
    
    
    <div class="mask-footer">Mask</div>


    <!-- Footer -->
    <footer>
      <?php echo $template['minifooter'] ?>
    </footer>
  </div>
    
  <!-- Javascript -->
  <?php echo $html['bottom_css']; ?>
  <?php echo $html['js']; ?>

  <script>


    $(function() {

      var i = 10;
      var countPaging = 0;
      var totalPaging = 0;
      var imagesLoad = new LazyLoad({
        elements_selector: '.lazyload'
      });

      $('#subkanal-navigation').find('#latest-menu').addClass('active')
      
      $('.scroll').jscroll({
          loadingHtml: '<div class="news-load"><img src="<?php echo m_template_uri(); ?>/image/ajax-loader.gif" alt="loading image"></div>',
          nextSelector: '.news-more a',
          autoTrigger: true,
          autoTriggerUntil : 2,
          callback: function(){
              imagesLoad.update();
             
       
          }
      });

     


       /*
        $(document).scroll(function(){var y = $(this).scrollTop();if(y <= 800){$('.notif-box').removeClass('notif-show').addClass('notif-hide');}else{if($('.notif-box').hasClass('kill-me')){$('.notif-box').removeClass('notif-show').addClass('notif-hide');}else{$('.notif-box').removeClass('notif-hide').addClass('notif-show');}}});                   
        var subdomain='<?php echo $this->config->item('domain'); ?>';var cookiename='__gensindo_push_d';var getme = getCookie(cookiename);if(getme != ''){dismissPush();}
        $('.notif-yes').click(function(){dismissPush();});
        $('.notif-no').click(function(){setCookie(cookiename,randomuv(),30,subdomain);dismissPush();});
        if(Notification.permission==='granted' || Notification.permission==='denied'){setCookie(cookiename,randomuv(),30,subdomain);}
        
        function dismissPush(){$('.notif-box').removeClass('notif-show').addClass('notif-hide').addClass('kill-me');}
        */


    });

    // search action
    function validateForm() {
      var x = document.forms["myForm"]["search"].value;
      x = x.replace(' ', '+')

      if (x != "") {
        var url = 'https://search.sindonews.com/gokanal?type=artikel&q=' + x + '&pid=600';

        document.location.href = url, true;
        return false;
      }

    }
  </script>
</body>

</html>