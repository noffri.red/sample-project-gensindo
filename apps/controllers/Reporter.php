<?php

class Reporter extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('blog/mredisdbblog','mdbblog');
        $this->load->model('news/mredisnews','mredisnews');
    }

    function index($id_subkanal){
        $a = array();

        $uri = $this->uri->segment(2,0);
        $ex_slug = explode('-',$uri);

        $slug_url = '';
        for($i = 0; $i < count($ex_slug); $i++){
            $slug_url .= '-' . $ex_slug[$i];
        }
        $slug = substr($slug_url,1);
        $cekauthor = $this->mdbblog->cekAuthor($slug);
        if(empty($cekauthor['results'])){
            redirect(base_url(),'location',301);
        }
        $id_author = $cekauthor['results'][0]['id_author'];
        $author = $cekauthor['results'][0]['author'];
        $slug_author = $cekauthor['results'][0]['slug_author'];

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= add_external_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-1.10.2.min.js');
        $a['html']['js'] .= add_js('/js/materialize.min.js');

        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['promoted'] = $this->load->view('template/vpromoted',NULL,TRUE);
        $a['template']['kategori'] = $this->load->view('template/vkategori',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('template/vminifooter',NULL,TRUE);
        $a['template']['promogen'] = $this->load->view('template/vpromogen',NULL,TRUE);

        $t['event'] = $this->mredisnews->getEvent();
        $a['template']['agenda'] = $this->load->view('template/vagenda',$t,TRUE);

        $t['trending'] = $this->mredisnews->getKanalTrendingTopic();
        $a['template']['trending'] = $this->load->view('template/vtrending',$t,TRUE);

        $offset = 10;
        $start = $this->uri->segment(3,0);

        $param['id_author'] = $id_author;
        $getBlog = $this->mdbblog->getBlog($param,$offset,$start,TRUE);
        $t['blog'] = $getBlog['results'];
        $total = $getBlog['total_results'];

        $t['totalData'] = $total;
        $t['per_page'] = $offset;
        $t['page'] = $start;

        $this->load->library('pagination');
        $this->config->load('mpagination',FALSE,TRUE);
        $config = $this->config->item('mpaging');
        $page_uri = 'reporter/' . $slug_author;
        $config['base_url'] = site_url($page_uri);
        $config['total_rows'] = $total;
        $config['per_page'] = $offset;
        $config['uri_segment'] = 3;

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        $t['author'] = $author;
        $a['content']['blog'] = $this->load->view('blog/vblog',$t,TRUE);

        $strMetaDesc = '';
        if(count($t['blog']) > 5){
            $loop = "5";
        }else{
            $loop = count($t['blog']);
        }
        for($i = 0; $i < $loop; $i++){
            if($i == ($loop - 1)){
                $strMetaDesc .= cleanHTML($t['blog'][$i]['title']);
            }else{
                $strMetaDesc .= cleanHTML($t['blog'][$i]['title']) . '. ';
            }
        }

        $currentPage = floor(($start / $offset) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;
        if($start == 0 || $start == ''){
            $pagetitle = '';
        }

        $a['html']['title'] = ucwords($author) . ' Reportase - Gensindo' . $pagetitle;
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => ucwords($author) . ' Reportase Gensindo - ' . $currentPage . ', ' . climiter($strMetaDesc,80),
            'alexaVerifyID' => $this->config->item('alexaVerifyID'),
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );
        $a['html']['metaname'] = metaname($metaname);

        $canonical = '';
        if($start == 0){
            $nnext = ($start + $offset);
            $canonical .= add_canonical('next',site_url($page_uri) . '/' . $nnext);
        }else{
            $nprev = ($start - $offset);
            if($nprev == 0){
                $canonical = add_canonical('prev',site_url($page_uri));
            }else{
                $canonical = add_canonical('prev',site_url($page_uri) . '/' . $nprev);
            }

            $nnext = ($start + $offset);
            if($nnext < $t['totalData']){
                $canonical .= add_canonical('next',site_url($page_uri) . '/' . $nnext);
            }else{
                $canonical .= '';
            }
        }

        $canonical .= add_canonical('canonical',site_url($page_uri));
        $a['html']['canonical'] = $canonical;

        $this->load->view('pages/vblog',$a,FALSE);
    }

}
