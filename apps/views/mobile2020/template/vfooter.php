<footer>
      <div id="social-media-footer">
        <div class="head">Visit Our Page</div>
        <ul>
          <li><a target="_blank" href="https://www.facebook.com/sindonews"><i class="fab fa-facebook-square"></i></a></li>
          <li><a target="_blank" href="https://twitter.com/sindonews"><i class="fab fa-twitter-square"></i></a></li>
          <li><a target="_blank" href="<?php echo site_url('feed'); ?>"><i class="fas fa-rss-square"></i></a></li>
        </ul>
      </div>
      <div id="sitemap">
        <ul>
          <li><a  href="https://index.sindonews.com/about/">About</a></li>
          <li><a  href="https://index.sindonews.com/about/privacy-policy">Privacy</a></li>
          <li><a  href="https://index.sindonews.com/about/term-of-service">Terms</a></li>
          <li><a  href="https://index.sindonews.com/about/?#kontak-kami">Advertising</a></li>
          <li><a  href="https://index.sindonews.com/about/?#kontak-kami">Contact</a></li>
        </ul>
      </div>
      <div id="mnc-logo"><a target="_blank" href="https://mnc.co.id/id"><img src="<?php echo $this->config->item('tpl_uri'); ?>/dyn/75/gensindo/2020-mobile/image/logo_mnc.png" alt="MNC Media" width="75" height="34"></a></div>
      <div id="ariflutfhansah">
        <div id="copyright">&copy; <?php echo date('Y'); ?> SINDOnews.com. All right reserved</div>
        <div id="render-time">/<?php $this->uri->segment(1)?> rendering {elapsed_time}s (<?php echo infoServer(); ?>)</div>
      </div>
      <div class="to-top"><i class="fas fa-arrow-up"></i></div>

    </footer>