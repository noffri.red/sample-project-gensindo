<?php

function td_subkanal($id_subkanal){
    if($id_subkanal == "1"){
        /* News */
        $data['title'] = "GEN SINDO | Berita Gen SINDO Terkini";
        $data['description'] = "Berita Gen SINDO Terkini - ";
    }elseif($id_subkanal == "2"){
        /* YOUTH */
        $data['title'] = "GEN SINDO | Berita Opini Gen SINDO Terkini";
        $data['description'] = "Berita Opini Gen SINDO Terkini - ";
    }else{
        $data['title'] = "GEN SINDO | Berita, Opini, Generasi Muda, Milenial";
        $data['description'] = "Berita Gen SINDO Terkini menyajikan informasi seputar Tren, Opini, Fashion dan Serba serbi Generasi Muda dan Milenial";
    }
    return $data;
}

function array_sort_by_column(&$array,$column,$direction = SORT_DESC){
    $reference_array = array();
    foreach($array as $key => $row){
        $reference_array[$key] = $row[$column];
    }
    return array_multisort($reference_array,$direction,$array);
}

function generateRandomString($length = 10){
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for($i = 0; $i < $length; $i++){
        $randomString .= $characters[rand(0,$charactersLength - 1)];
    }
    return $randomString;
}
