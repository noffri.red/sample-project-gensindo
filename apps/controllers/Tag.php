<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helpers("gen");
        $this->load->library('pagination');
        $this->load->library('article');
        $this->load->config('site_channel');
        $this->load->model('news/mredisnews','mredisnews');
        $this->load->model('topic/mdbtopic','mdbtopic');
    }

	function index(){
        $a = array();

        $offset = 13;
        $id_topic = $this->uri->segment(2,0);
        $slug_topic = $this->uri->segment(3,0);
        $start = $this->uri->segment(4,0);

        $t['data_topic'] = $this->mdbtopic->getTopicData($id_topic);
        if(empty($t['data_topic'])){
            redirect(base_url(),'location',301);
        }else{
            /* fix slug topic */
            if($slug_topic != $t['data_topic']['results'][0]['slug_topic']){
                redirect(base_url().'tag/'. $t['data_topic']['results'][0]['id_topic'] .'/'. $t['data_topic']['results'][0]['slug_topic']);
            }
        }

        $t['topic'] = $this->mdbtopic->getTopicContent($id_topic,$offset,$start, TRUE);
        if(empty($t['topic']['results'])){
            redirect(base_url(),'location',301);
        }

        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('paging');
        $page_uri = 'tag/' . $id_topic . '/' . $slug_topic;
        $config['base_url'] = site_url($page_uri);
        $config['total_rows'] = $t['topic']['total_results'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();
        $t['per_page'] = $offset;

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= add_external_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-1.10.2.min.js');
        $a['html']['js'] .= add_js('/js/materialize.min.js');

        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['promoted'] = $this->load->view('template/vpromoted',NULL,TRUE);
        $a['template']['kategori'] = $this->load->view('template/vkategori',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('template/vminifooter',NULL,TRUE);
        $a['template']['promogen'] = $this->load->view('template/vpromogen',NULL,TRUE);

        $t['event'] = $this->mredisnews->getEvent();
        $a['template']['agenda'] = $this->load->view('template/vagenda',$t,TRUE);

        $t['trending'] = $this->mredisnews->getKanalTrendingTopic();
        $a['template']['trending'] = $this->load->view('template/vtrending',$t,TRUE);

        $strMetaDesc = '';
        if(count($t['topic']['results']) > 5){
            $loop = "5";
        }else{
            $loop = count($t['topic']['results']);
        }
        for($i = 0; $i < $loop; $i++){
            if($i ==  ($loop - 1) ){
                $strMetaDesc .= cleanHTML($t['topic']['results'][$i]['title']);
            }else{
                $strMetaDesc .= cleanHTML($t['topic']['results'][$i]['title']) . '. ';
            }
        }

        $currentPage = floor(($start / $config['per_page']) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;
        $n = "9";
        if($start == 0 || $start == ''){
            $pagetitle = '';
            $n = "20";
        }

        $title_topic = seo_tag(ucwords($t['data_topic']['results'][0]['topic']), $n);
        $a['html']['title'] = 'Berita ' . $title_topic . ' Terbaru - SINDOnews' . $pagetitle;
        if(strlen($a['html']['title']) > 50){
            $a['html']['title'] = 'Berita ' . $title_topic . ' Terbaru - SINDOnews' . $pagetitle;
        }

        $metaname = array(
            'title' => 'Berita ' . $title_topic . ' Terbaru - SINDOnews' . $pagetitle,
            'description' => 'Kumpulan Berita ' . $title_topic . ' Terbaru - ' . $strMetaDesc,
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $url_current = site_url('tag') . '/' . $id_topic. '/'. $slug_topic;
        $a['html']['canonical'] = $this->article->pagingCanonical($url_current,$start,$offset,$t['topic']['total_results']);
        $imgmeta = $this->config->item('images_uri')
            . 'news/'
            . $t['topic']['results'][0]['year'] . '/'
            . $t['topic']['results'][0]['month'] . '/'
            . $t['topic']['results'][0]['day'] . '/'
            . $t['topic']['results'][0]['id_subkanal'] . '/'
            . $t['topic']['results'][0]['id_content'] . '/'
            . $t['topic']['results'][0]['images'];
        $metafb = array(
            'title' => trim($metaname['title']),
            'type' => 'article',
            'url' => $url_current,
            'image' => $imgmeta,
            'description' => $metaname['description'],
            'site_name' => 'SINDOnews'
        );

        $metatwit = array(
            'twitter:card' => 'summary',
            'twitter:site' => '@SINDOnews',
            'twitter:url' => $url_current,
            'twitter:domain' => 'sindonews.com',
            'twitter:title' => $metaname['title'],
            'twitter:description' => $metaname['description'],
            'twitter:image:src' => $imgmeta
        );

        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);

        $a['content']['breadcrumb'] = $this->load->view('topic/vbreadcrumb',$t,TRUE);
        $a['content']['topic'] = $this->load->view('topic/vtopic',$t,TRUE);

        $this->load->view('pages/vtopic',$a,FALSE);
    }
}
