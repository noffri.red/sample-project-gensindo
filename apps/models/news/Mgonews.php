<?php

class Mgonews extends CI_Model {

    var $salsabila;
    var $gensindo;

    function __construct(){
        parent::__construct();
        $this->load->database();
        $this->gensindo = $this->load->database('gensindo',TRUE);
        $this->salsabila = $this->load->database('salsabila',TRUE);
    }

    function getChannelXML($channel_id){
        $data = array();

        $jsonFile = $this->config->item('json_data') . '/channel/channelinfo_' . $channel_id . '.json';
        if(is_file($jsonFile)){
            $getJson = file_get_contents($jsonFile);
            $channel = json_decode(trim($getJson),TRUE);
            $total = count($channel);

            for($i = 0; $i < $total; $i++){
                $data[$i]['channel_id'] = $channel[$i]['channel_id'];
                $data[$i]['channel_name'] = $channel[$i]['channel_name'];
                $data[$i]['parent_id'] = $channel[$i]['parent_id'];
                $data[$i]['parent_name'] = $channel[$i]['parent_name'];
                $data[$i]['path'] = $channel[$i]['path'];
            }
        }

        return $data;
    }


    function getHeadline($idchannel,$howMuch){
        $key = 'pena:headline:list_' . $idchannel;
        $getredis = $this->textcache->redisDbGet($key);

        $data = array();
        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['content_id'] = $datanews[$i]['content_id'];
                $data[$i]['news_id'] = $data[$i]['content_id'];
                $data[$i]['parent_id'] = $datanews[$i]['parent_id'];
                $data[$i]['parent_name'] = $datanews[$i]['parent_name'];
                $data[$i]['channel_id'] = $datanews[$i]['channel_id'];
                $data[$i]['channel_name'] = $datanews[$i]['channel_name'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                $data[$i]['published'] = parseDateTime($datanews[$i]['publish']);
                $data[$i]['publish'] = $datanews[$i]['publish'];
                $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                $data[$i]['subtitle'] = $datanews[$i]['subtitle'];
                $data[$i]['summary'] = stripslashes($datanews[$i]['summary']);

                $thumb[$i] = $datanews[$i]['image'];
                if($thumb[$i] != ''){
                    $data[$i]['image'] = $thumb[$i];
                    $data[$i]['thumb'] = $thumb[$i];
                }else{
                    $data[$i]['image'] = '';
                    $data[$i]['thumb'] = '';
                }
            }

            $result = $data;
        }else{
            $jsonFile = $this->config->item('json_data') . '/headline/' . $idchannel . '/headline-' . $idchannel . '.json';
            if(is_file($jsonFile)){
                $getJson = file_get_contents($jsonFile);

                $datanews = json_decode(trim($getJson),TRUE);
                $total = count($datanews);

                if($total > $howMuch){
                    $totalshow = $howMuch;
                }else{
                    $totalshow = $total;
                }

                for($i = 0; $i < $totalshow; $i++){
                    $data[$i]['content_id'] = $datanews[$i]['content_id'];
                    $data[$i]['news_id'] = $data[$i]['content_id'];
                    $data[$i]['parent_id'] = $datanews[$i]['parent_id'];
                    $data[$i]['parent_name'] = $datanews[$i]['parent_name'];
                    $data[$i]['channel_id'] = $datanews[$i]['channel_id'];
                    $data[$i]['channel_name'] = $datanews[$i]['channel_name'];
                    $data[$i]['date_created'] = $datanews[$i]['date_created'];
                    $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                    $data[$i]['published'] = parseDateTime($datanews[$i]['publish']);
                    $data[$i]['publish'] = $datanews[$i]['publish'];
                    $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                    $data[$i]['subtitle'] = $datanews[$i]['subtitle'];
                    $data[$i]['summary'] = stripslashes($datanews[$i]['summary']);

                    $thumb[$i] = $datanews[$i]['image'];
                    if($thumb[$i] != ''){
                        $data[$i]['image'] = $thumb[$i];
                        $data[$i]['thumb'] = $thumb[$i];
                    }else{
                        $data[$i]['image'] = '';
                        $data[$i]['thumb'] = '';
                    }
                }
            }

            $result = $data;
        }

        return $result;
    }

    function getListBreakingNews($idchannel,$howMuch){
        $key = 'pena:breaking:list_' . $idchannel;
        $getredis = $this->textcache->redisDbGet($key);

        $data = array();
        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['content_id'] = $datanews[$i]['content_id'];
                $data[$i]['news_id'] = $data[$i]['content_id'];
                $data[$i]['parent_id'] = $datanews[$i]['parent_id'];
                $data[$i]['parent_name'] = $datanews[$i]['parent_name'];
                $data[$i]['channel_id'] = $datanews[$i]['channel_id'];
                $data[$i]['channel_name'] = $datanews[$i]['channel_name'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                $data[$i]['published'] = parseDateTime($datanews[$i]['publish']);
                $data[$i]['publish'] = $datanews[$i]['publish'];
                $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                $data[$i]['subtitle'] = $datanews[$i]['subtitle'];
                $data[$i]['summary'] = stripslashes($datanews[$i]['summary']);
                $data[$i]['reporter'] = $datanews[$i]['reporter_name'];

                $thumb[$i] = $datanews[$i]['image'];
                if($thumb[$i] != ''){
                    $data[$i]['image'] = $thumb[$i];
                    $data[$i]['thumb'] = $thumb[$i];
                }else{
                    $data[$i]['image'] = '';
                    $data[$i]['thumb'] = '';
                }
            }

            $result = $data;
        }else{
            $jsonFile = $this->config->item('json_data') . '/headline/' . $idchannel . '/breaking-' . $idchannel . '.json';
            if(is_file($jsonFile)){
                $getJson = file_get_contents($jsonFile);

                $datanews = json_decode(trim($getJson),TRUE);
                $total = count($datanews);

                if($total > $howMuch){
                    $totalshow = $howMuch;
                }else{
                    $totalshow = $total;
                }

                for($i = 0; $i < $totalshow; $i++){
                    $data[$i]['content_id'] = $datanews[$i]['content_id'];
                    $data[$i]['news_id'] = $data[$i]['content_id'];
                    $data[$i]['parent_id'] = $datanews[$i]['parent_id'];
                    $data[$i]['parent_name'] = $datanews[$i]['parent_name'];
                    $data[$i]['channel_id'] = $datanews[$i]['channel_id'];
                    $data[$i]['channel_name'] = $datanews[$i]['channel_name'];
                    $data[$i]['date_created'] = $datanews[$i]['date_created'];
                    $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                    $data[$i]['published'] = parseDateTime($datanews[$i]['publish']);
                    $data[$i]['publish'] = $datanews[$i]['publish'];
                    $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                    $data[$i]['subtitle'] = $datanews[$i]['subtitle'];
                    $data[$i]['summary'] = stripslashes($datanews[$i]['summary']);
                    $data[$i]['reporter'] = $datanews[$i]['reporter_name'];

                    $thumb[$i] = $datanews[$i]['image'];
                    if($thumb[$i] != ''){
                        $data[$i]['image'] = $thumb[$i];
                        $data[$i]['thumb'] = $thumb[$i];
                    }else{
                        $data[$i]['image'] = '';
                        $data[$i]['thumb'] = '';
                    }
                }
            }

            $result = $data;
        }

        return $result;
    }

    function getListNews($idchannel,$howMuch){
        $key = 'pena:latest:list_' . $idchannel;
        $getredis = $this->textcache->redisDbGet($key);

        $data = array();
        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['content_id'] = $datanews[$i]['content_id'];
                $data[$i]['news_id'] = $datanews[$i]['content_id'];
                $data[$i]['parent_id'] = $datanews[$i]['parent_id'];
                $data[$i]['parent_name'] = $datanews[$i]['parent_name'];
                $data[$i]['channel_id'] = $datanews[$i]['channel_id'];
                $data[$i]['channel_name'] = $datanews[$i]['channel_name'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                $data[$i]['published'] = parseDateTime($datanews[$i]['date_publish']);
                $data[$i]['date_publish'] = $datanews[$i]['date_publish'];
                $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                $data[$i]['subtitle'] = $datanews[$i]['subtitle'];
                $data[$i]['location'] = $datanews[$i]['location'];
                $data[$i]['reporter_name'] = $datanews[$i]['reporter_name'];
                $data[$i]['summary'] = stripslashes($datanews[$i]['summary']);

                $thumb[$i] = $datanews[$i]['image'];
                if($thumb[$i] != ''){
                    $data[$i]['image'] = $thumb[$i];
                    $data[$i]['thumb'] = $thumb[$i];
                }else{
                    $data[$i]['image'] = '';
                    $data[$i]['thumb'] = '';
                }
            }

            $result = $data;
        }else{
            $jsonFile = $this->config->item('json_data') . '/headline/' . $idchannel . '/news-' . $idchannel . '.json';
            if(is_file($jsonFile)){
                $getJson = file_get_contents($jsonFile);

                $datanews = json_decode(trim($getJson),TRUE);
                $total = count($datanews);

                if($total > $howMuch){
                    $totalshow = $howMuch;
                }else{
                    $totalshow = $total;
                }

                for($i = 0; $i < $totalshow; $i++){
                    $data[$i]['content_id'] = $datanews[$i]['content_id'];
                    $data[$i]['news_id'] = $datanews[$i]['content_id'];
                    $data[$i]['parent_id'] = $datanews[$i]['parent_id'];
                    $data[$i]['parent_name'] = $datanews[$i]['parent_name'];
                    $data[$i]['channel_id'] = $datanews[$i]['channel_id'];
                    $data[$i]['channel_name'] = $datanews[$i]['channel_name'];
                    $data[$i]['date_created'] = $datanews[$i]['date_created'];
                    $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                    $data[$i]['published'] = parseDateTime($datanews[$i]['date_publish']);
                    $data[$i]['date_publish'] = $datanews[$i]['date_publish'];
                    $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                    $data[$i]['subtitle'] = $datanews[$i]['subtitle'];
                    $data[$i]['location'] = $datanews[$i]['location'];
                    $data[$i]['reporter_name'] = $datanews[$i]['reporter_name'];
                    $data[$i]['summary'] = stripslashes($datanews[$i]['summary']);

                    $thumb[$i] = $datanews[$i]['image'];
                    if($thumb[$i] != ''){
                        $data[$i]['image'] = $thumb[$i];
                        $data[$i]['thumb'] = $thumb[$i];
                    }else{
                        $data[$i]['image'] = '';
                        $data[$i]['thumb'] = '';
                    }
                }
            }

            $result = $data;
        }

        return $result;
    }

    function getDbListNews($id_kanal,$start,$offset){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "
            SELECT a.id_news,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.title,a.summary,a.url_title,a.photo,a.date_created,a.date_published 
            FROM t_news AS a
            LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal 
            LEFT JOIN t_subkanal AS c ON a.id_subkanal = c.id_subkanal 
            WHERE a.status = 0 AND a.id_kanal = " . $id_kanal . "
            ORDER BY a.date_published DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function getNumListNews($id_kanal){
        $sql = "SELECT id_news FROM t_news WHERE status = 0 AND id_kanal = " . $id_kanal . "";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }
    
    function getDbListNewsSubkanal($id_subkanal,$start,$offset){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "
            SELECT a.id_news,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.title,a.summary,a.url_title,a.photo,a.date_created,a.date_published 
            FROM t_news AS a
            LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal 
            LEFT JOIN t_subkanal AS c ON a.id_subkanal = c.id_subkanal 
            WHERE a.status = 0 AND a.id_subkanal = " . $id_subkanal . "
            ORDER BY a.date_published DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function getNumListNewsSubkanal($id_subkanal){
        $sql = "SELECT id_news FROM t_news WHERE status = 0 AND id_subkanal = " . $id_subkanal . "";

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function getListTitlePilihan($idchannel,$howMuch){
        if($idchannel == 0){
            $kanal = '';
        }else{
            $kanal = 'kanal/';
        }
        $xmlFile = $this->config->item('xml_data') . '/pilihan/' . $kanal . $idchannel . "/select-" . $idchannel . ".xml";
        $xmlObj = simplexml_load_file($xmlFile);

        $data = array();
        $totalxml = (int) count($xmlObj->news);
        if($totalxml >= $howMuch){
            $toLoad = (($howMuch != NULL)) ? $howMuch : (int) count($xmlObj->news);
        }else{
            $toLoad = (int) count($xmlObj->news);
        }

        for($i = 0; $i < $toLoad; $i++){
            /* kebutuhan gen */
            $data[$i]['id_content'] = $datanews[$i]['id_news'];
            $data[$i]['slug_title'] = $datanews[$i]['url_title'];
            $data[$i]['images'] = $datanews[$i]['photo'];

            /* kebutuhan biro */
            $year[$i] = date('Y',strtotime($datanews[$i]['date_created']));
            $month[$i] = date('m',strtotime($datanews[$i]['date_created']));
            $day[$i] = date('d',strtotime($datanews[$i]['date_created']));
            $data[$i]['id_news'] = $datanews[$i]['id_news'];
            $data[$i]['id_kanal'] = $datanews[$i]['id_kanal'];
            $data[$i]['kanal'] = $datanews[$i]['kanal'];
            $data[$i]['id_subkanal'] = $datanews[$i]['id_subkanal'];
            $data[$i]['subkanal'] = $datanews[$i]['subkanal'];
            $data[$i]['title'] = filterText($datanews[$i]['title']);
            $data[$i]['summary'] = $datanews[$i]['summary'];
            $data[$i]['url_title'] = $datanews[$i]['url_title'];
            $data[$i]['photo'] = $datanews[$i]['photo'];
            $data[$i]['date_published'] = $datanews[$i]['date_published'];
            $data[$i]['date_created'] = $datanews[$i]['date_created'];
            $data[$i]['year'] = $year[$i];
            $data[$i]['month'] = $month[$i];
            $data[$i]['day'] = $day[$i];
            $data[$i]['url_subkanal'] = $datanews[$i]['url_subkanal'];
            $data[$i]['sub_title'] = $datanews[$i]['sub_title'];

            $data[$i]['news_id'] = (String) $xmlObj->news[$i]->attributes()->id;
            $data[$i]['channel_id'] = (String) $xmlObj->news[$i]->channel->attributes()->id;
            $data[$i]['channel_name'] = (String) $xmlObj->news[$i]->channel;
            $data[$i]['created'] = parseDateTime((String) $xmlObj->news[$i]->created);
            $data[$i]['date_created'] = (String) $xmlObj->news[$i]->created;
            $data[$i]['published'] = parseDateTime((String) $xmlObj->news[$i]->published);
            $data[$i]['title'] = stripslashes(utf8_decode((String) $xmlObj->news[$i]->title));
            $data[$i]['subtitle'] = stripslashes(utf8_decode((String) $xmlObj->news[$i]->subtitle));
            $data[$i]['summary'] = stripslashes(utf8_decode((String) $xmlObj->news[$i]->summary));
            $data[$i]['thumb'] = (String) $xmlObj->news[$i]->attributes->images->thumb;
            $data[$i]['image'] = (String) $xmlObj->news[$i]->attributes->images->thumb;
            $data[$i]['publish'] = (String) $xmlObj->news[$i]->published;
        }

        return $data;
    }
    
    function getListNonKanal($idchannel,$howMuch){
        $key = 'latest:nonkanal_' . $idchannel;
        $getredis = $this->textcache->redisDbGet($key);

        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            $data = array();
            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['content_id'] = $datanews[$i]['id_news'];
                $data[$i]['news_id'] = $datanews[$i]['id_news'];
                $data[$i]['parent_id'] = $datanews[$i]['id_kanal'];
                $data[$i]['parent_name'] = $datanews[$i]['kanal'];
                $data[$i]['channel_id'] = $datanews[$i]['id_subkanal'];
                $data[$i]['channel_name'] = $datanews[$i]['subkanal'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                $data[$i]['published'] = parseDateTime($datanews[$i]['date_published']);
                $data[$i]['date_publish'] = $datanews[$i]['date_published'];
                $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                $data[$i]['subtitle'] = $datanews[$i]['subtitle'];
                $data[$i]['location'] = '';
                $data[$i]['reporter_name'] = $datanews[$i]['author'];
                $data[$i]['summary'] = stripslashes(base64_decode($datanews[$i]['summary']));

                $thumb[$i] = $datanews[$i]['photo'];
                if($thumb[$i] != ''){
                    $data[$i]['image'] = $thumb[$i];
                    $data[$i]['thumb'] = $thumb[$i];
                }else{
                    $data[$i]['image'] = '';
                    $data[$i]['thumb'] = '';
                }
            }

            $result = $data;
        }else{
            $jsonFile = $this->config->item('json_data') . '/headline/' . $idchannel . '/nonkanal_' . $idchannel . '.json';
            if(is_file($jsonFile)){
                $getJson = file_get_contents($jsonFile);

                $datanews = json_decode(trim($getJson),TRUE);
                $total = count($datanews);

                if($total > $howMuch){
                    $totalshow = $howMuch;
                }else{
                    $totalshow = $total;
                }

                $data = array();
                for($i = 0; $i < $totalshow; $i++){
                    $data[$i]['content_id'] = $datanews[$i]['id_news'];
                    $data[$i]['news_id'] = $data[$i]['id_news'];
                    $data[$i]['parent_id'] = $datanews[$i]['id_kanal'];
                    $data[$i]['parent_name'] = $datanews[$i]['kanal'];
                    $data[$i]['channel_id'] = $datanews[$i]['id_subkanal'];
                    $data[$i]['channel_name'] = $datanews[$i]['subkanal'];
                    $data[$i]['date_created'] = $datanews[$i]['date_created'];
                    $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                    $data[$i]['published'] = parseDateTime($datanews[$i]['date_published']);
                    $data[$i]['date_publish'] = $datanews[$i]['date_published'];
                    $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                    $data[$i]['subtitle'] = $datanews[$i]['subtitle'];
                    $data[$i]['location'] = '';
                    $data[$i]['reporter_name'] = $datanews[$i]['author'];
                    $data[$i]['summary'] = stripslashes(base64_decode($datanews[$i]['summary']));

                    $thumb[$i] = $datanews[$i]['photo'];
                    if($thumb[$i] != ''){
                        $data[$i]['image'] = $thumb[$i];
                        $data[$i]['thumb'] = $thumb[$i];
                    }else{
                        $data[$i]['image'] = '';
                        $data[$i]['thumb'] = '';
                    }
                }
            }

            $result = $data;
        }

        return $result;
    }

    function getReadAgain($idchannel,$howMuch){
        $key = 'popular:readagain_' . $idchannel;
        $getredis = $this->textcache->redisDbGet($key);

        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            $data = array();
            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['content_id'] = $datanews[$i]['id_news'];
                $data[$i]['news_id'] = $data[$i]['content_id'];
                $data[$i]['parent_id'] = $datanews[$i]['id_kanal'];
                $data[$i]['parent_name'] = $datanews[$i]['kanal'];
                $data[$i]['channel_id'] = $datanews[$i]['id_subkanal'];
                $data[$i]['channel_name'] = $datanews[$i]['subkanal'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                $data[$i]['published'] = parseDateTime($datanews[$i]['date_published']);
                $data[$i]['date_publish'] = $datanews[$i]['date_published'];
                $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                $data[$i]['summary'] = stripslashes(base64_decode($datanews[$i]['summary']));
                $data[$i]['hit'] = $datanews[$i]['hit'];

                $thumb[$i] = $datanews[$i]['photo'];
                if($thumb[$i] != ''){
                    $data[$i]['image'] = $thumb[$i];
                    $data[$i]['thumb'] = $thumb[$i];
                }else{
                    $data[$i]['image'] = '';
                    $data[$i]['thumb'] = '';
                }
            }

            $result = $data;
        }else{
            $jsonFile = $this->config->item('json_path') . '/mostpopular/readagain_' . $idchannel . '.json';
            if(is_file($jsonFile)){
                $getJson = file_get_contents($jsonFile);

                $datanews = json_decode(trim($getJson),TRUE);
                $total = count($datanews);

                if($total > $howMuch){
                    $totalshow = $howMuch;
                }else{
                    $totalshow = $total;
                }

                for($i = 0; $i < $totalshow; $i++){
                    $data[$i]['content_id'] = $datanews[$i]['id_news'];
                    $data[$i]['news_id'] = $data[$i]['content_id'];
                    $data[$i]['parent_id'] = $datanews[$i]['id_kanal'];
                    $data[$i]['parent_name'] = $datanews[$i]['kanal'];
                    $data[$i]['channel_id'] = $datanews[$i]['id_subkanal'];
                    $data[$i]['channel_name'] = $datanews[$i]['subkanal'];
                    $data[$i]['date_created'] = $datanews[$i]['date_created'];
                    $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                    $data[$i]['published'] = parseDateTime($datanews[$i]['date_published']);
                    $data[$i]['date_publish'] = $datanews[$i]['date_published'];
                    $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                    $data[$i]['summary'] = stripslashes(base64_decode($datanews[$i]['summary']));
                    $data[$i]['hit'] = $datanews[$i]['hit'];

                    $thumb[$i] = $datanews[$i]['photo'];
                    if($thumb[$i] != ''){
                        $data[$i]['image'] = $thumb[$i];
                        $data[$i]['thumb'] = $thumb[$i];
                    }else{
                        $data[$i]['image'] = '';
                        $data[$i]['thumb'] = '';
                    }
                }
            }

            $result = $data;
        }

        return $result;
    }

    function getListTitlePopular($idchannel,$howMuch){
        $key = "popular:most_" . $idchannel;
        $getredis = $this->textcache->redisDbGet($key);

        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            $data = array();
            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['content_id'] = $datanews[$i]['id_news'];
                $data[$i]['news_id'] = $data[$i]['content_id'];
                $data[$i]['parent_id'] = $datanews[$i]['id_kanal'];
                $data[$i]['parent_name'] = $datanews[$i]['kanal'];
                $data[$i]['channel_id'] = $datanews[$i]['id_subkanal'];
                $data[$i]['channel_name'] = $datanews[$i]['subkanal'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                $data[$i]['published'] = parseDateTime($datanews[$i]['date_published']);
                $data[$i]['date_publish'] = $datanews[$i]['date_published'];
                $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                $data[$i]['summary'] = stripslashes($datanews[$i]['summary']);
                $data[$i]['hit'] = $datanews[$i]['hit'];

                $thumb[$i] = $datanews[$i]['photo'];
                if($thumb[$i] != ''){
                    $data[$i]['image'] = $thumb[$i];
                    $data[$i]['thumb'] = $thumb[$i];
                }else{
                    $data[$i]['image'] = '';
                    $data[$i]['thumb'] = '';
                }
            }

            $result = $data;
        }else{
            $jsonFile = $this->config->item('json_path') . "/mostpopular/mostpopular_" . $idchannel . ".json";

            $getJson = file_get_contents(trim($jsonFile));
            $datanews = json_decode(utf8_encode($getJson),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            $data = array();
            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['content_id'] = $datanews[$i]['id_news'];
                $data[$i]['news_id'] = $data[$i]['content_id'];
                $data[$i]['parent_id'] = $datanews[$i]['id_kanal'];
                $data[$i]['parent_name'] = $datanews[$i]['kanal'];
                $data[$i]['channel_id'] = $datanews[$i]['id_subkanal'];
                $data[$i]['channel_name'] = $datanews[$i]['subkanal'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                $data[$i]['published'] = parseDateTime($datanews[$i]['date_published']);
                $data[$i]['date_publish'] = $datanews[$i]['date_published'];
                $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                $data[$i]['summary'] = stripslashes($datanews[$i]['summary']);
                $data[$i]['hit'] = $datanews[$i]['hit'];

                $thumb[$i] = $datanews[$i]['photo'];
                if($thumb[$i] != ''){
                    $data[$i]['image'] = $thumb[$i];
                    $data[$i]['thumb'] = $thumb[$i];
                }else{
                    $data[$i]['image'] = '';
                    $data[$i]['thumb'] = '';
                }
            }

            $result = $data;
        }

        return $result;
    }

    function getKanalTrendingTopic($channel_id,$howMuch){
        $key = "trending:topic_" . $channel_id;
        $getredis = $this->textcache->redisDbGet($key);
        $data = array();
        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }
           
            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['topic_id'] = $datanews[$i]['topic_id'];
                $data[$i]['channel_id'] = $datanews[$i]['channel_id'];
                $data[$i]['topic'] = $datanews[$i]['topic'];
                $data[$i]['channel_name'] = $datanews[$i]['channel_name'];
                $data[$i]['hit'] = $datanews[$i]['hits'];
                $data[$i]['date_publish'] = $datanews[$i]['date_publish'];
            }

            $result = $data;
        }else{
            $jsonFile = $this->config->item('json_path') . '/topic/trending_kanal_' . $channel_id . '.json';
            if(is_file($jsonFile)){
                $getJson = file_get_contents(trim($jsonFile));
                $datanews = json_decode(utf8_encode($getJson),TRUE);
                $total = count($datanews);

                if($total > $howMuch){
                    $totalshow = $howMuch;
                }else{
                    $totalshow = $total;
                }

                $data = array();
                for($i = 0; $i < $totalshow; $i++){
                    $data[$i]['topic_id'] = $datanews[$i]['topic_id'];
                    $data[$i]['channel_id'] = $datanews[$i]['channel_id'];
                    $data[$i]['topic'] = $datanews[$i]['topic'];
                    $data[$i]['channel_name'] = $datanews[$i]['channel_name'];
                    $data[$i]['hit'] = $datanews[$i]['hits'];
                    $data[$i]['date_publish'] = $datanews[$i]['date_publish'];
                }
            }
            $result = $data;
        }

        return $result;
    }
    
    function getLatestPhoto($channel_id,$howMuch){
        $key = 'salsabila:photo:latest:list_' . $channel_id;
        $getredis = $this->textcache->redisDbGet($key);

        $data = array();
        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['id_photo'] = $datanews[$i]['id_photo'];
                $data[$i]['id_kanal'] = $datanews[$i]['id_kanal'];
                $data[$i]['kanal'] = $datanews[$i]['kanal'];
                $data[$i]['id_subkanal'] = $datanews[$i]['id_subkanal'];
                $data[$i]['subkanal'] = $datanews[$i]['subkanal'];
                $data[$i]['title'] = $datanews[$i]['title'];
                $data[$i]['summary'] = $datanews[$i]['summary'];
                $data[$i]['lokasi'] = $datanews[$i]['lokasi'];
                $data[$i]['url_title'] = $datanews[$i]['url_title'];
                $data[$i]['photo'] = $datanews[$i]['photo'];
                $data[$i]['date_published'] = $datanews[$i]['date_published'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['author'] = $datanews[$i]['reporter_name'];
            }

            $result = $data;
        }else{
            $jsonFile = $this->config->item('json_path') . '/salsabila/photo/headline/' . $channel_id . '/news-' . $channel_id . '.json';
            if(is_file($jsonFile)){
                $getJson = file_get_contents(trim($jsonFile));
                $datanews = json_decode(utf8_encode($getJson),TRUE);
                $total = count($datanews);

                if($total > $howMuch){
                    $totalshow = $howMuch;
                }else{
                    $totalshow = $total;
                }

                for($i = 0; $i < $totalshow; $i++){
                    $data[$i]['id_photo'] = $datanews[$i]['id_photo'];
                    $data[$i]['id_kanal'] = $datanews[$i]['id_kanal'];
                    $data[$i]['kanal'] = $datanews[$i]['kanal'];
                    $data[$i]['id_subkanal'] = $datanews[$i]['id_subkanal'];
                    $data[$i]['subkanal'] = $datanews[$i]['subkanal'];
                    $data[$i]['title'] = $datanews[$i]['title'];
                    $data[$i]['summary'] = $datanews[$i]['summary'];
                    $data[$i]['lokasi'] = $datanews[$i]['lokasi'];
                    $data[$i]['url_title'] = $datanews[$i]['url_title'];
                    $data[$i]['photo'] = $datanews[$i]['photo'];
                    $data[$i]['date_published'] = $datanews[$i]['date_published'];
                    $data[$i]['date_created'] = $datanews[$i]['date_created'];
                    $data[$i]['author'] = $datanews[$i]['reporter_name'];
                }
            }

            $result = $data;
        }

        return $result;
    }

    function getLatestVideo($id_subkanal,$howMuch){
        $key = 'salsabila:video:latest:list_' . $id_subkanal;
        $getredis = $this->textcache->redisDbGet($key);

        $data = array();
        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['id_video'] = $datanews[$i]['id_video'];
                $data[$i]['id_kanal'] = $datanews[$i]['id_kanal'];
                $data[$i]['kanal'] = $datanews[$i]['kanal'];
                $data[$i]['id_subkanal'] = $datanews[$i]['id_subkanal'];
                $data[$i]['subkanal'] = $datanews[$i]['subkanal'];
                $data[$i]['title'] = $datanews[$i]['title'];
                $data[$i]['summary'] = $datanews[$i]['summary'];
                $data[$i]['lokasi'] = $datanews[$i]['lokasi'];
                $data[$i]['url_title'] = $datanews[$i]['url_title'];
                $data[$i]['photo'] = $datanews[$i]['photo'];
                $data[$i]['date_published'] = $datanews[$i]['date_published'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['author'] = $datanews[$i]['reporter_name'];
            }

            $result = $data;
        }else{
            $jsonFile = $this->config->item('json_path') . '/salsabila/video/headline/' . $id_subkanal . '/news-' . $id_subkanal . '.json';
            if(is_file($jsonFile)){
                $getJson = file_get_contents(trim($jsonFile));
                $datanews = json_decode(utf8_encode($getJson),TRUE);
                $total = count($datanews);

                if($total > $howMuch){
                    $totalshow = $howMuch;
                }else{
                    $totalshow = $total;
                }


                for($i = 0; $i < $totalshow; $i++){
                    $data[$i]['id_video'] = $datanews[$i]['id_video'];
                    $data[$i]['id_kanal'] = $datanews[$i]['id_kanal'];
                    $data[$i]['kanal'] = $datanews[$i]['kanal'];
                    $data[$i]['id_subkanal'] = $datanews[$i]['id_subkanal'];
                    $data[$i]['subkanal'] = $datanews[$i]['subkanal'];
                    $data[$i]['title'] = $datanews[$i]['title'];
                    $data[$i]['summary'] = $datanews[$i]['summary'];
                    $data[$i]['lokasi'] = $datanews[$i]['lokasi'];
                    $data[$i]['url_title'] = $datanews[$i]['url_title'];
                    $data[$i]['photo'] = $datanews[$i]['photo'];
                    $data[$i]['date_published'] = $datanews[$i]['date_published'];
                    $data[$i]['date_created'] = $datanews[$i]['date_created'];
                    $data[$i]['author'] = $datanews[$i]['reporter_name'];
                }
            }

            $result = $data;
        }

        return $result;
    }

    function getPhotoByKanal($channel_id,$howMuch){
        $jsonFile = $this->config->item('json_data') . '/photo/photo_channel_' . $channel_id . '.json';
        $getJson = file_get_contents($jsonFile);

        $news = json_decode(trim($getJson),TRUE);
        $total = count($news);

        if($total > $howMuch){
            $totalnews = $howMuch;
        }else{
            $totalnews = $total;
        }

        for($i = 0; $i < $totalnews; $i++){
            $data[$i]['photo_id'] = $news[$i]['photo_id'];
            $data[$i]['channelname'] = $news[$i]['channelname'];
            $data[$i]['channel_id'] = $news[$i]['channel_id'];
            $data[$i]['photographer_id'] = $news[$i]['photographer_id'];
            $data[$i]['photographer'] = ucwords($news[$i]['photographer']);
            $data[$i]['photographer_code'] = strtolower($news[$i]['photographer_code']);
            $data[$i]['date_created'] = $news[$i]['date_created'];
            $data[$i]['created'] = parseDateTime($news[$i]['date_created']);
            $data[$i]['date_modified'] = $news[$i]['date_modified'];
            $data[$i]['modified'] = parseDateTime($news[$i]['date_modified']);
            $data[$i]['title'] = filterTitle($news[$i]['title']);
            $data[$i]['summary'] = base64_decode(filterSummary($news[$i]['summary']));

            $thumb[$i] = $news[$i]['thumb'];
            if($thumb[$i] != ''){
                $images[$i] = str_replace('small','large',$thumb[$i]);
                $data[$i]['path_image'] = '/photos/' . date('Y/m/d',strtotime($news[$i]['date_created'])) . '/' . $news[$i]['photo_id'];
                $data[$i]['thumb'] = images_uri() . $data[$i]['path_image'] . '/' . $images[$i];
                $data[$i]['image'] = $images[$i];
            }else{
                $data[$i]['path_image'] = '';
                $data[$i]['thumb'] = '';
                $data[$i]['image'] = '';
            }
        }

        return $data;
    }

    function getVideoDetail($video_id){
        $key = "video:play:db_" . $video_id;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "
                SELECT a.id_video,a.id_type,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.id_author,d.author,a.title,a.url_title,a.summary,a.keyword,a.lokasi,a.content,a.date_created,a.date_published,a.photo,a.caption_photo,.a.video,a.editor_id,e.kode_user 
                FROM t_video AS a 
                LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal
                LEFT JOIN t_subkanal AS c ON a.id_subkanal = c.id_subkanal 
                LEFT JOIN t_author AS d ON a.id_author = d.id_author 
                LEFT JOIN t_user AS e ON a.editor_id = e.id_user 
                WHERE a.status = 0 AND a.id_video = " . $video_id;

            $query = $this->salsabila->query($sql);
            $data = $query->result_array();
            $this->salsabila->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,1200,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getVideoByKanal($channel_id,$howMuch){
        $data = array();

        $jsonFile = $this->config->item('json_data') . '/video/video_channel_' . $channel_id . '.json';
        $getJson = file_get_contents($jsonFile);

        $news = json_decode(trim($getJson),TRUE);
        $total = count($news);

        if($total > $howMuch){
            $totalnews = $howMuch;
        }else{
            $totalnews = $total;
        }

        for($i = 0; $i < $totalnews; $i++){
            $data[$i]['video_id'] = $news[$i]['video_id'];
            $data[$i]['created'] = parseDateTime($news[$i]['date_created']);
            $data[$i]['date_created'] = $news[$i]['date_created'];
            $data[$i]['publish'] = $news[$i]['date_published'];
            $data[$i]['published'] = parseDateTime($news[$i]['date_published']);
            $data[$i]['channel_id'] = $news[$i]['channel_id'];
            $data[$i]['channel_name'] = $news[$i]['channel_name'];
            $data[$i]['cameramen_id'] = $news[$i]['cameramen_id'];
            $data[$i]['cameramen_name'] = $news[$i]['cameramen_name'];
            $data[$i]['user_code'] = strtolower($news[$i]['user_code']);
            $data[$i]['hits'] = $news[$i]['hits'];
            $data[$i]['title'] = stripslashes(filterTitle($news[$i]['title']));
            $data[$i]['summary'] = base64_decode(filterSummary($news[$i]['summary']));

            $thumb[$i] = $news[$i]['thumbnail'];
            if($thumb[$i] != ''){
                $images[$i] = str_replace('small','large',$news[$i]['thumbnail']);
                $imgPath[$i] = '/videos/' . date('Y/m/d',strtotime($news[$i]['date_created'])) . '/' . $news[$i]['channel_id'] . '/' . $news[$i]['video_id'];
                $data[$i]['thumbnail'] = $imgPath[$i] . '/' . $images[$i];
                $data[$i]['image'] = $images[$i];
            }else{
                $data[$i]['thumbnail'] = '';
                $data[$i]['image'] = '';
            }
        }

        return $data;
    }

    function getJsonDetail($params){
        $out = array();

        $key = "article:json_" . slug($params['date_created']) . ":" . $params['id_subkanal'] . "_" . $params['id_news'];
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $path_file = $this->config->item('json_data') . '/content/' . $params['date_created'] . '/' . $params['id_subkanal'] . '/' . $params['id_news'] . '.json';
            if(is_file($path_file)){
                $json_file = file_get_contents($path_file);
                $decode = json_decode($json_file,TRUE);
                $out = $decode;
            }

            $redisdata = json_encode($out);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $out;
        }

        return $result;
    }

    function getNewsSimple($id_news,$id_subkanal){
        $key = "article:db:simple_" . $id_news;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "SELECT id_news,id_kanal,id_subkanal,id_topik,title,date_created FROM t_news WHERE id_news = " . $id_news . " AND id_subkanal = " . $id_subkanal;

            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getDbDetail($id_news){
        $key = "article:db:detail_" . $id_news;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "
                SELECT a.id_news,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.id_topik,a.id_author,d.author,e.kode_user,a.caption_photo,a.keyword,a.lokasi,a.title,f.subtitle AS sub_title,a.summary,a.content,a.url_title,a.photo,a.date_published,a.date_created 
                FROM t_news AS a 
                LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal 
                LEFT JOIN t_subkanal AS c ON a.id_subkanal = c.id_subkanal 
                LEFT JOIN t_author AS d ON a.id_author = d.id_author 
                LEFT JOIN t_user AS e ON a.editor_id = e.id_user 
                LEFT JOIN t_subtitle AS f ON a.id_subtitle = f.id_subtitle 
                WHERE id_news = " . $id_news;

            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $data;
        }

        return $result;
    }
    
    function getEditorByNews($id_news){
        $key = "article:db:editorByNews_" . $id_news;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "
                SELECT a.id_news,a.editor_id,b.fullname,b.kode_user 
                FROM t_news AS a 
                LEFT JOIN t_user AS b ON a.editor_id = b.id_user 
                WHERE a.id_news = " . $id_news;

            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,1200,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getAllKanal($channel_id,$start,$offset){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "
            SELECT a.id_news,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.title,a.summary,a.url_title,a.photo,a.date_created,a.date_published 
            FROM t_news AS a
            LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal 
            LEFT JOIN t_subkanal AS c ON a.id_subkanal = c.id_subkanal 
            WHERE a.status = 0 AND a.id_kanal = " . $channel_id . " 
            ORDER BY a.date_published DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function getTotalAllKanal($channel_id){
        $sql = "SELECT id_news FROM t_news WHERE status = 0 AND id_kanal = " . $channel_id;

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function getAllSubKanal($channel_id,$start,$offset){
        if($offset == NULL){
            $limit = "";
        }else{
            $limit = " LIMIT " . $start . "," . $offset;
        }

        $sql = "
            SELECT a.id_news,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.title,a.summary,a.url_title,a.photo,a.date_created,a.date_published 
            FROM t_news AS a
            LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal 
            LEFT JOIN t_subkanal AS c ON a.id_subkanal = c.id_subkanal 
            WHERE a.status = 0 AND a.id_subkanal = " . $channel_id . " 
            ORDER BY a.date_published DESC" . $limit;

        $query = $this->db->query($sql);
        $result = $query->result_array();
        $this->db->close();

        return $result;
    }

    function getTotalAllSubKanal($channel_id){
        $sql = "SELECT id_news FROM t_news WHERE status = 0 AND id_subkanal = " . $channel_id;

        $query = $this->db->query($sql);
        $result = $query->num_rows();
        $this->db->close();

        return $result;
    }

    function getAllContent($channel_id,$start,$offset){
        if($offset == NULL){
            $limit = '';
        }else{
            $limit = ' LIMIT ' . $start . ',' . $offset;
        }

        $checkparent = $this->getChannelXML($channel_id);
        if($checkparent[0]['parent_id'] == 0){
            $sql = "
                SELECT 
                    a.content_id,a.title,a.subtitle,a.summary,a.image_thumb,a.date_created,a.date_publish,
                    b.parent_id,a.channel_id,b.channel_name 
                FROM td_content AS a 
                LEFT JOIN td_channel AS b ON a.channel_id = b.channel_id 
                WHERE a.active_status = 4 AND b.parent_id = " . $channel_id . " 
                ORDER BY a.date_publish DESC" . $limit;
        }else{
            $sql = "
                SELECT 
                    a.content_id,a.title,a.subtitle,a.summary,a.image_thumb,a.date_created,a.date_publish,
                    b.parent_id,a.channel_id,b.channel_name 
                FROM td_content AS a 
                LEFT JOIN td_channel AS b ON a.channel_id = b.channel_id 
                WHERE a.active_status = 4 AND a.channel_id = " . $channel_id . " 
                ORDER BY a.date_publish DESC" . $limit;
        }

        $query = $this->atmaja->query($sql);
        $result = $query->result_array();
        $this->atmaja->close();

        return $result;
    }


    function getKanalById($channel_id){
        $key = "channel:allcms:db_" . $channel_id;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "
                SELECT a.id_subkanal,a.id_kanal,b.kanal,a.subkanal 
                FROM t_subkanal AS a 
                LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal 
                WHERE a.id_subkanal = " . $channel_id;

            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getTopicContent($id_news){
        $key = "topic:content_" . $id_news;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "SELECT id_topik FROM t_news WHERE id_news = '" . $id_news . "'";
            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getTopicList($id_topik){
        $key = "topic:info_" . $id_topik;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "SELECT id_topik,topik,url_topik FROM t_topik WHERE id_topik = '" . $id_topik . "'";
            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }
        return $result;
    }
    
    function getSlugTopic($slug){
        $key = "topic:slug_" . $slug;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "SELECT id_topik,topik,url_topik FROM t_topik WHERE url_topik = '" . $slug . "'";
            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,3600,$redisdata);
            $result = $data;
        }
        return $result;
    }

    function getRelatedContent($id_topic,$id_kanal,$id_news,$num){
        $key = "topic:related_" . $id_kanal . ":" . $id_topic . "_" . $id_news . "_" . $num;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            if($num == NULL){
                $limit = "";
            }else{
                $limit = " LIMIT 0," . $num;
            }

            $getIdTopic = explode(',',$id_topic);
            $totalId = count($getIdTopic);

            if($totalId == 1){
                $extra = " AND FIND_IN_SET(" . $id_topic . ",a.id_topik)";
            }else{
                $extra = " AND (FIND_IN_SET(" . $getIdTopic[0] . ",a.id_topik)";
                for($i = 1; $i < count($getIdTopic); $i++){
                    $extra .= " OR FIND_IN_SET(" . $getIdTopic[$i] . ",a.id_topik)";
                }
                $extra .= ")";
            }

            $sql = "
                SELECT a.id_news,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.title,a.summary,a.url_title,a.photo,a.date_created,a.date_published 
                FROM t_news AS a
                LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal 
                LEFT JOIN t_subkanal AS c ON a.id_subkanal = c.id_subkanal 
                WHERE a.status = 0 AND a.id_kanal = " . $id_kanal . " AND a.id_news != " . $id_news . $extra . " 
                ORDER BY a.date_published DESC" . $limit;

            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,600,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getRelatedContentNonKanal($id_topic,$id_kanal,$id_news,$num){
        $key = "topic:related_non_" . $id_kanal . ":" . $id_topic . "_" . $id_news . "_" . $num;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            if($num == NULL){
                $limit = "";
            }else{
                $limit = " LIMIT 0," . $num;
            }

            $getIdTopic = explode(',',$id_topic);
            $totalId = count($getIdTopic);

            if($totalId == 1){
                $extra = " AND FIND_IN_SET(" . $id_topic . ",a.id_topik)";
            }else{
                $extra = " AND (FIND_IN_SET(" . $getIdTopic[0] . ",a.id_topik)";
                for($i = 1; $i < count($getIdTopic); $i++){
                    $extra .= " OR FIND_IN_SET(" . $getIdTopic[$i] . ",a.id_topik)";
                }
                $extra .= ")";
            }

            $sql = "
                SELECT a.id_news,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.title,a.summary,a.url_title,a.photo,a.date_created,a.date_published 
                FROM t_news AS a
                LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal 
                LEFT JOIN t_subkanal AS c ON a.id_subkanal = c.id_subkanal 
                WHERE a.status = 0 AND a.id_kanal != " . $id_kanal . " AND a.id_news != " . $id_news . $extra . " 
                ORDER BY a.date_published DESC" . $limit;

            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,600,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getArticleSticky(){
        $output = array();

        $file = "/mainData/data/json/dfp/articlestickygensindo.json";
        if(is_file($file)){
            $list = file_get_contents($file);
            $datajson = json_decode($list,true);

            $data = $datajson['data'];

            if($datajson['data'] == ''){
                $total = 0;
            }else{
                $total = count($datajson['data']);
            }

            if($total > 0){
                for($i = 0; $i < $total; $i++){
                    if($data[$i]['type'] == 2){
                        $output[$i]['article_sticky_id'] = $data[$i]['article_sticky_id'];
                        $output[$i]['title'] = $data[$i]['title'];
                        $output[$i]['type'] = $data[$i]['type'];
                        $output[$i]['kanal_id'] = $data[$i]['kanal_id'];
                        $output[$i]['kanal'] = $data[$i]['kanal'];
                        $output[$i]['banner'] = $data[$i]['banner'];
                        $output[$i]['banner_webp'] = $data[$i]['banner_webp'];
                        $output[$i]['thumb'] = $data[$i]['thumb'];
                        $output[$i]['thumb_webp'] = $data[$i]['thumb_webp'];
                        $output[$i]['image_path'] = $data[$i]['image_path'];
                        $output[$i]['created'] = $data[$i]['created'];
                        $output[$i]['date_start'] = $data[$i]['date_start'];
                        $output[$i]['date_end'] = $data[$i]['date_end'];
                        $output[$i]['status'] = $data[$i]['status'];
                        $output[$i]['username'] = $data[$i]['username'];
                        $output[$i]['url'] = $data[$i]['url'];
                        $output[$i]['scheduled'] = $data[$i]['scheduled'];
                        $output[$i]['summary'] = $data[$i]['summary'];
                        $output[$i]['model'] = $data[$i]['model'];
                    }
                }
                $output = array_values($output);
            }
        }

        return $output;
    }

    function getRelatedContentWP($id_topic,$num){
        $key = "topic:related_wp:" . $id_topic . "_" . $num;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            if($num == NULL){
                $limit = "";
            }else{
                $limit = " LIMIT 0," . $num;
            }

            $getIdTopic = explode(',',$id_topic);
            $totalId = count($getIdTopic);

            if($totalId == 1){
                $extra = " AND FIND_IN_SET(" . $id_topic . ",a.id_topik)";
            }else{
                $extra = " AND (FIND_IN_SET(" . $getIdTopic[0] . ",a.id_topik)";
                for($i = 1; $i < count($getIdTopic); $i++){
                    $extra .= " OR FIND_IN_SET(" . $getIdTopic[$i] . ",a.id_topik)";
                }
                $extra .= ")";
            }

            $sql = "
                SELECT a.id_news,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.title,a.summary,a.url_title,a.photo,a.date_created,a.date_published 
                FROM t_news AS a
                LEFT JOIN t_kanal AS b ON a.id_kanal = b.id_kanal 
                LEFT JOIN t_subkanal AS c ON a.id_subkanal = c.id_subkanal 
                WHERE a.status = 0" . $extra . " 
                ORDER BY a.date_published DESC" . $limit;

            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }

        return $result;
    }

    /* S Data Lama */

    function getSimpleGen($content_id,$channel_id){
        $key = "biro:simple:gensindo:db_" . $channel_id . "_" . $content_id;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "SELECT id_content,title,date_created,id_subkanal FROM td_content WHERE id_content = " . $content_id . " AND id_subkanal = " . $channel_id;
            $query = $this->gensindo->query($sql);
            $data = $query->result_array();
            $this->gensindo->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,600,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getGen($content_id){
        $data = array();

        $getarticle = $this->getArticleGen($content_id);
        foreach($getarticle as $article){
            $data['id_news'] = $article['id_content'];
            $data['id_content'] = $article['id_content'];
            $data['id_subkanal'] = $article['id_subkanal'];
            $data['subkanal'] = $article['subkanal'];

            $data['id_kanal'] = $article['id_kanal'];
            $data['kanal'] = $article['kanal'];

            $data['summary'] = stripslashes($article['summary']);
            $data['title'] = stripslashes($article['title']);
            $data['url_title'] = stripslashes($article['slug_title']);
            $data['slug_title'] = stripslashes($article['slug_title']);
            $data['sub_title'] = $article['sub_title'];
            $data['lokasi'] = $article['lokasi'];
            $data['content'] = stripslashes($article['content']);
            $data['create'] = $article['date_created'];
            $data['date_created'] = $article['date_created'];
            $data['year'] = date('Y',strtotime($article['date_created']));
            $data['month'] = date('m',strtotime($article['date_created']));
            $data['day'] = date('d',strtotime($article['date_created']));
            $data['publish'] = $article['date_published'];
            $data['date_published'] = $article['date_published'];

            $data['photo'] = $article['images'];
            $data['caption_photo'] = $article['caption_images'];

            $data['images'] = $article['images'];
            $data['caption_images'] = $article['caption_images'];

            $data['id_author'] = $article['id_author'];
            $data['author'] = $article['author'];
            $data['url_author'] = $article['slug_author'];
            $data['slug_author'] = $article['slug_author'];
            $data['kode_user'] = $article['kode_user'];
            $data['editor_name'] = $article['fullname'];

            $publishedDate = parseDateTime($data['date_published']);
            $createdDate = parseDateTime($data['date_created']);
            $datePath = $createdDate['year'] . '/' . $createdDate['month'] . '/' . $createdDate['day'] . '/' . $data['id_subkanal'] . '/' . $data['id_content'];

            //$data['images']['thumb'] = '/gensindo/' . $datePath . '/' . $article['images'];
        }

        return $data;
    }

    function getArticleGen($content_id){
        $key = "biro:article:gensindo:db_" . $content_id;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "
                SELECT 
                    a.id_content, a.id_kanal, a.id_subkanal, a.id_author, a.id_topic, a.id_topic AS id_topik, 
                    a.lokasi, a.title, a.summary, a.slug_title, a.caption_images, a.images, a.date_published, a.date_created, 
                    a.content,
                    b.kanal,
                    c.subkanal, c.slug_subkanal,
                    d.slug_author, d.author,
                    e.kode_user,e.fullname,
                    f.subtitle AS sub_title
                FROM td_content a
                LEFT JOIN td_kanal AS b ON b.id_kanal = a.id_kanal
                LEFT JOIN td_subkanal AS c ON c.id_subkanal = a.id_subkanal
                LEFT JOIN td_author AS d ON d.id_author = a.id_author
                LEFT JOIN td_user AS e ON e.id_user = a.id_editor
                LEFT JOIN td_subtitle AS f ON f.id_subtitle = a.id_subtitle
                WHERE a.status = 0
                AND a.id_content = '" . $content_id . "'
                AND c.status = 0";

            $query = $this->gensindo->query($sql);
            $data = $query->result_array();
            $this->gensindo->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getTopicGensindo($id_news){
        $key = "gensindo:topic:content_" . $id_news;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "SELECT id_topic FROM td_content WHERE id_content = '" . $id_news . "'";
            $query = $this->gensindo->query($sql);
            $data = $query->result_array();
            $this->gensindo->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }

        return $result;
    }

    function getTopicListGensindo($id_topik){
        $key = "gensindo:topic:info_" . $id_topik;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            $sql = "SELECT id_topic,topic,slug_topic FROM td_topic WHERE id_topic = '" . $id_topik . "'";
            $query = $this->gensindo->query($sql);
            $data = $query->result_array();
            $this->gensindo->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }
        return $result;
    }

    function getRelatedGensindo($id_topic,$id_kanal,$num){
        $key = "gensindo:topic:related_" . $id_kanal . ":" . $id_topic . "_" . $num;
        $getredis = $this->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            if($num == NULL){
                $limit = "";
            }else{
                $limit = " LIMIT 0," . $num;
            }

            $getIdTopic = explode(',',$id_topic);
            $totalId = count($getIdTopic);

            if($totalId == 1){
                $extra = " AND FIND_IN_SET(" . $id_topic . ",a.id_topic)";
            }else{
                $extra = " AND (FIND_IN_SET(" . $getIdTopic[0] . ",a.id_topic)";
                for($i = 1; $i < count($getIdTopic); $i++){
                    $extra .= " OR FIND_IN_SET(" . $getIdTopic[$i] . ",a.id_topic)";
                }
                $extra .= ")";
            }

            $sql = "
                SELECT a.id_content,a.id_kanal,b.kanal,a.id_subkanal,c.subkanal,a.title,a.summary,a.slug_title,a.images,a.date_created,a.date_published 
                FROM td_content AS a
                LEFT JOIN td_kanal AS b ON a.id_kanal = b.id_kanal 
                LEFT JOIN td_subkanal AS c ON a.id_subkanal = c.id_subkanal 
                WHERE a.status = 0 AND a.id_kanal = " . $id_kanal . $extra . " 
                ORDER BY a.date_published DESC" . $limit;

            $query = $this->gensindo->query($sql);
            $data = $query->result_array();
            $this->gensindo->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,600,$redisdata);
            $result = $data;
        }

        return $result;
    }
    
    function getTopicURL($slug){
        $topic = $this->getSlugTopic($slug);
        if(count($topic) > 0){
            $url = 'https://www.sindonews.com/topic/' . $topic[0]['id_topik'] . '/' . $topic[0]['url_topik'];
        } else {
            $url = 'https://www.sindonews.com/terkait/' . $slug;
        }
        
        return $url;
    }

    /* E Data Lama */
    
    function getSuggest($topic_id_list,$parent_id,$content_id){
        $relatedNews = array();
        if($topic_id_list != ''){
            $relatedNews = $this->getRelatedContent($topic_id_list,$parent_id,$content_id,3);
        }
        
        $dataRelated = array();
        if(count($relatedNews) > 0){
            for($i = 0; $i < count($relatedNews); $i++){
                $dataRelated[$i]['content_id'] = $relatedNews[$i]['id_news'];
                $dataRelated[$i]['title'] = $relatedNews[$i]['title'];
                $dataRelated[$i]['parent_id'] = $relatedNews[$i]['id_kanal'];
                $dataRelated[$i]['parent_name'] = $relatedNews[$i]['kanal'];
                $dataRelated[$i]['channel_id'] = $relatedNews[$i]['id_subkanal'];
                $dataRelated[$i]['channel_name'] = $relatedNews[$i]['subkanal'];
                $dataRelated[$i]['date_created'] = $relatedNews[$i]['date_created'];
                $dataRelated[$i]['date_publish'] = $relatedNews[$i]['date_published'];
                $dataRelated[$i]['image'] = $relatedNews[$i]['photo'];
            }
        }
        $readagain = $this->getReadAgain($parent_id,3);
        $popular = $this->getListTitlePopular($parent_id,3);
        $latest = $this->getListNews($parent_id,3);

        $news = array_filter(array_merge($dataRelated,$readagain,$popular,$latest));
        
        return $news;
    }
}
