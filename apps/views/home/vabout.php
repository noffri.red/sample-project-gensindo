<?php
echo '<div id="tab3" class="tabs_item">';
$total = count($about);
if($total > 0) {
    $data = $about[0];
    
    $id_about = $data['id_about'];
    $img = images_uri().$this->config->item('dyn_300').'/gensindo/about/'.$data['year'].'/'.$data['month'].'/'.$data['day'].'/'.$id_about.'/'.$data['images'];
    $title = cleanWords($data['title']);
    $summary = cleanWords($data['summary']);
    $content = cleanWords($data['content']);

    echo '<div class="article-news">
        <div class="inner">
            <div class="title">'. $title .'</div>
            <div class="img mt20"><img src="'. $img .'" alt="'. $title .'"></div>
            <div class="caption mt20">'. $content .'</div>
        </div>
    </div>';
}
echo '</div>';