<!DOCTYPE html>
<html lang="id-ID">
    <head>
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#004B8F">
        <?php echo $html['metaname']; ?>

        <link rel="shortcut icon" href="<?php echo template_uri(); ?>/images/favicon.ico">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-192x192.png" sizes="192x192">

        <link rel="canonical" href="<?php echo $content['site_url']; ?>">
        <link rel="alternate" title="SINDOnews | RSS Feed Berita GEN SINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>

        <?php echo $html['css']; ?>
        <?php echo $template['newcss']; ?>
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-HDPI.css" media="screen and (min-width: 1083px) and (max-width: 1290px)">
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-MDPI.css" media="screen and (min-width: 993px) and (max-width: 1082px)">
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-SDPI.css" media="screen and (min-width: 769px) and (max-width: 992px)">
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-mobile.css" media="screen and (max-width: 768px)">

        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
            window.googletag = window.googletag || {cmd: []};
            
            googletag.cmd.push(function() {
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[970,250],[970,90],[728,90],[1,1]], 'div-gpt-ad-billboard').setTargeting('pos', ['billboard']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[300,250],[300,600]], 'div-gpt-ad-right_sidebar_1').setTargeting('pos', ['right_sidebar_1']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[300,250]], 'div-gpt-ad-right_sidebar_2').setTargeting('pos', ['right_sidebar_2']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[300,250]], 'div-gpt-ad-right_sidebar_3').setTargeting('pos', ['right_sidebar_3']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[300,250]], 'div-gpt-ad-right_sidebar_4').setTargeting('pos', ['right_sidebar_4']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[120,600]], 'div-gpt-ad-left_skinads').setTargeting('pos', ['left_skinads']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[1,1]], 'div-gpt-ad-abm').setTargeting('partner', ['abm']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[1,1]], 'div-gpt-ad-outstream').setTargeting('pos', ['outstream']).addService(googletag.pubads());
                
                googletag.pubads().setTargeting('page_type', ['kanalpage']);
                googletag.pubads().enableSingleRequest();
                googletag.pubads().collapseEmptyDivs();
                googletag.enableServices();
            });
            function bannerCheck(unitAd){googletag.pubads().addEventListener('slotRenderEnded',function(event){if(event.slot.getSlotElementId() === unitAd){if(event.isEmpty){var eAd = document.getElementById('me-'+unitAd);eAd.classList.add('adnone');}}});}
        </script>
        
        <?php echo $html['bottom_js']; ?>
    </head>
    
    <body>
        <header>
            <?php echo $template['header']; ?>
        </header>
        <section class="slidenav">
            <ul id="slide-out" class="sidenav">
                <li>
                    <div class="user-view">
                        <a href="#user"><img class="circle" src="<?php echo template_uri(); ?>/images/logo.png"></a>
                    </div>
                </li>
                <li class="gnews"><a href="<?php echo site_url('gnews'); ?>">Gen News</a></li>
                <li class="youth"><a href="<?php echo site_url('samsung'); ?>">Samsung</a></li>
            </ul>
        </section>
        <main>
            <div class="container clearfix">
                <div class="billboard">
                    <div id="div-gpt-ad-billboard"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-billboard');});</script></div>
                </div>
                
                <div class="column-big pull-left clearfix">
                    <div class="center pull-right">
                        <section class="detail">
                            <?php echo $content['breadcrumb']; ?>
                            <div class="tab_content">
                                <?php echo $content['breaking']; ?>
                            </div>
                        </section>
                    </div>
                    <div class="leftbar pull-left">
                        <?php echo $template['kategori']; ?>
                        <?php echo $template['trending']; ?>
                        <div class="r-side sky-banner">
                            <div id="div-gpt-ad-left_skinads"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-left_skinads');});</script></div>
                        </div>
                    </div>
                </div>
                <div class="rightbar pull-right">
                    <?php echo $template['promogen']; ?>
                    <div class="r-side mb20 medium-banner">
                        <div id="div-gpt-ad-right_sidebar_1"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-right_sidebar_1');});</script></div>
                    </div>
                    <?php echo $template['popular']; ?>
                    <div class="r-side mb20 medium-banner">
                        <div id="div-gpt-ad-right_sidebar_2"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-right_sidebar_2');});</script></div>
                    </div>
                    <?php //echo $template['agenda']; ?>
                    <?php //echo $template['promoted']; ?>
                    <?php echo $template['minifooter']; ?>
                </div>
            </div>
        </main>
        
        <?php echo $html['bottom_css']; ?>
        <?php echo $html['js']; ?>
        <script>
            $(function () {
                var imagesLoad = new LazyLoad({
                    elements_selector:'.lazyload'
                });
                
                if ($(window).width() > 770) {
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 1495) {
                            $('.leftbar').addClass("sticky");
                            $(".leftbar").animate({top: "30px"}, 0);
                            $('.leftbar .r-side').addClass("none");
                        } else {
                            $('.leftbar').removeClass("sticky");
                            $('.leftbar .r-side').removeClass("none");
                        }
                        if ($(this).scrollTop() > 1262) {
                            $('.rightbar').addClass("sticky");
                            $(".rightbar").animate({top: "30px"}, 0);
                            $('.rightbar .r-side').addClass("none");
                            $('.agenda').addClass("none");
                        } else {
                            $('.leftbar').removeClass("sticky");
                            $('.rightbar').removeClass("sticky");
                            $('.rightbar .r-side').removeClass("none");
                            $('.agenda').removeClass("none");
                        }
                    });
                }

                $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
                $('.tab ul.tabs li a').click(function (g) {
                    var tab = $(this).closest('.tab'), index = $(this).closest('li').index();
                    tab.find('ul.tabs > li').removeClass('current');
                    $(this).closest('li').addClass('current');
                    tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
                    tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();
                    g.preventDefault();
                });
                $('.sidenav').sidenav();
                
                $('.scroll').jscroll({
                    loadingHtml: '<div class="news-load"><img src="<?php echo m_template_uri(); ?>/images/ajax-loader.gif" alt="loading image"></div>',
                    nextSelector: '.news-more a',
                    autoTrigger: false,
                    callback: function(){
                        imagesLoad.update();
                    }
                });
            });
        </script>
    </body>
</html>