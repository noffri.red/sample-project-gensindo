<?php

if(count($details) > 0){
    if(!empty($details['subtitle'])){
        echo '<div class="subtitle">' . $details['subtitle'] . '</div>';
    }

    echo '<h1>' . $details['title'] . '</h1>';
    if(!empty($details['author'])){
        echo '<div class="reporter">' . ucfirst($details['author']) . '</div>';
    }

    $datepublish = parseDateTime((string) $details['date_published']);
    $date_published = $datepublish['day_ind_name'] . ", " . $datepublish['day'] . " " . $datepublish['month_ind_name'] . " " . $datepublish['year'];
    $date_published .= ' - ' . $datepublish['hour'] . ":" . $datepublish['minute'] . " WIB";
    echo '<time>' . $date_published . '</time>';

    $showimg = '';
    if($details['photo'] != ''){
        $img = $this->config->item('images_uri') . '/dyn/620/pena/news/' . date('Y/m/d',strtotime($details['date_created'])) . '/' . $details['id_subkanal'] . '/' . $details['id_news'] . '/' . $details['photo'];

        if($details['caption_photo'] != ''){
            $caption = cleanWords($details['caption_photo']);
        }else{
            $caption = cleanWords($details['title']);
        }

        $showimg = '
            <figure>
                <img width="620" src="' . $img . '" alt="' . cleanHTML($details['title']) . '">
                <figcaption>' . $caption . '</figcaption>
            </figure>';
    }

    $lasturl = (($per_page * $last_page) - $per_page);
    if($totalData > $per_page){
        if($start == $lasturl){
            $editor = ' <div class="editor">(' . strtolower($details['kode_user']) . ')</div>';
        }else{
            $editor = '';
        }
    }else{
        $editor = '<div class="editor">(' . strtolower($details['kode_user']) . ')</div>';
    }

    if(!empty($details['lokasi'])){
        if($start == 0 || $start == ''){
            $showcontent = '<strong>' . strtoupper($details['lokasi']) . '</strong> - ' . $content;
        }else{
            $showcontent = $content;
        }
    }else{
        $showcontent = $content;
    }

    if($start == 0 || $start == ''){
        echo $showimg;
    }

    echo '<div class="content">' . strip_tags($showcontent,'<strong><em><br>') . $editor . '</div>';

    if($totalData > $per_page){
        echo '<div class="note-paging">halaman ke-' . $current_page . ' dari ' . $last_page . '</div>';
        echo '<div class="next-page"><a href="' . $site_url . '">halaman selanjutnya</a></div>';
    }
}
