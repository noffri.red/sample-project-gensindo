<?php

if(count($details) > 0){
    $datepublish = parseDateTime((string) $details['date_published']);
    $date_published = $datepublish['day_ind_name'] . ", " . $datepublish['day'] . " " . $datepublish['month_ind_name'] . " " . $datepublish['year'];
    $date_published .= ' - ' . $datepublish['hour'] . ":" . $datepublish['minute'] . " WIB";

    echo '
        <div class="bread">
            <a href="'. base_url() .'">home</a> 
			<i class="fa fa-angle-double-right"></i> 
			<a href="' . site_url('loadmore/' . $details['id_subkanal']) . '">' . strtolower($details['subkanal']) . '</a>
        </div>';

    echo '<h1>' . $details['title'] . '</h1>';


    if(!empty($details['author'])){
        $dtime_author = strtotime($details['author_date']);
        echo '
                <div class="reporter">
                    <a rel="author" href="' .site_url('reporter') .'/'.  $details['url_author'] . '-' . $dtime_author . '">' . ucfirst($details['author']) . '</a>
                </div>';
    }

    echo '<div class="time">' . $date_published . '</div>';

    if($details['photo'] != ''){
        $img = $this->config->item('images_uri') .  $this->config->item('dyn_620').'/news/' . $details['year'] . '/' . $details['month'] . '/' . $details['day'] . '/' . $details['id_subkanal'] . '/' . $details['id_news'] . '/' . $details['photo'];
        $imgctn = '
            <div class="ximg">
                <amp-img src="' . $img . '" layout="responsive" width="620" height="413" alt="' . cleanHTML($details['title']) . '"></amp-img>          
                <div class="caption">' . $details['caption_photo'] . '</div>
            </div>';
    }

    $ampcontent = trim($content);
    if(!empty($details['lokasi'])){
        $location = '<span class="location">' . strtoupper($details['lokasi']) . '</span>';
        $location .= ' - ';
    }else{
        $location = '';
    }

    echo '<meta itemprop="description" content="' . cleanWords($details['summary']) . '">';
    /*if($totalData == $current_page){*/
        $editor = '<div class="editor">(' . strtolower($details['kode_user']) . ')</div>';
    /*}else{
        $editor = '';
    }*/

    if(!empty($details['lokasi'])){
        if($start == 0 || $start == ''){
            $showcontent = '<span class="location">' . strtoupper($details['lokasi']) . '</span> - ' . $ampcontent;
        }else{
            $showcontent = $ampcontent;
        }
    }else{
        $showcontent = $ampcontent;
    }

    if($start == 0 || $start == ''){
        echo $imgctn;
    }

    $gradient = '';
    $pagination = '';
    if($totalData > $per_page){
        $gradient = '<div class="content-line"></div>';
        $pagination = '
            <div class="ctn-next-title">halaman ke-' . $current_page . ' dari ' . $last_page . '</div>
            <div class="ctn-next">
                <a href="' . $site_url . '">Baca Selanjutnya</a>
            </div>';
    }

    echo '<div class="content">' . $showcontent . $gradient . '</div>' . $editor;
    echo $pagination;

    $title_share = strip_tags(cleanWords($details['title']));

    echo '
        <div class="social-share mt10">
            <div class="xtitle">Bagikan artikel ini:</div>
            <div class="xbutton clearfix">
                <amp-social-share type="facebook" data-param-href="' . $share_url . '" data-param-text="' . $title_share . '" data-param-app_id="325657497499535"></amp-social-share>
                <amp-social-share type="twitter" data-param-url="' . $share_url . '" data-param-text="' . $title_share . '"></amp-social-share>
                <amp-social-share type="gplus" data-param-url="' . $share_url . '"></amp-social-share>
                <amp-social-share type="whatsapp" data-share-endpoint="whatsapp://send" data-param-text="' . $title_share . ' - ' . $share_url . '"></amp-social-share>
            </div>
        </div>';
}