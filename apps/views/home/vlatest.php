<?php

echo '<div id="tab1" class="tabs_item">';
$total = count($latest);
if($total > 0){
    $data = $latest;

    /* Article Sticky Native Normal */
    if(count($articledata) > 0){
        $i = 0;
        $title[$i] = cleanWords($articledata[$i]['title']);
        $channel_name[$i] = $articledata[$i]['kanal'];
        $icon[$i] = '';
        $l[$i] = $articledata[$i]['url'];
        $data_summary[$i] = trim(base64url_decode(strip_tags($articledata[$i]['summary'])));
        $summ[$i] = $data_summary[$i];
        $img[$i] = $articledata[$i]['thumb'];

        $time_date_start[$i] = parseDateTime($articledata[$i]['date_start']);
        $time[$i] = $time_date_start[$i]['day_ind_name'] . ", " . $time_date_start[$i]['day'] . " " . $time_date_start[$i]['month_ind_name'] . " " . $time_date_start[$i]['year'];
        $time[$i] .= ' - ' . $time_date_start[$i]['hour'] . ":" . $time_date_start[$i]['minute'] . " WIB";

        $ago[$i] = time_difference($articledata[$i]['date_start']);

        echo '
            <div class="article-news">
                <div class="category-links gnews"><a href="' . site_url('gnews') . '" title="Gen News">Gen News</a></div>
                <div class="inner">
                    <div class="title"><a href="' . $l[$i] . '">' . $title[$i] . '</a></div>
                    <time>' . $ago[$i] . '</time>
                    <div class="caption">' . $summ[$i] . '</div>
                    <div class="img"><a href="' . $l[$i] . '"><img src="' . $img[$i] . '" alt="' . $title[$i] . '"></a></div>
                </div>
            </div>';
    }
    if(count($articledata) > 1){
        $i = 1;
        $title[$i] = cleanWords($articledata[$i]['title']);
        $channel_name[$i] = $articledata[$i]['kanal'];
        $icon[$i] = '';
        $l[$i] = $articledata[$i]['url'];
        $data_summary[$i] = trim(base64url_decode(strip_tags($articledata[$i]['summary'])));
        $summ[$i] = $data_summary[$i];
        $img[$i] = $articledata[$i]['thumb'];

        $time_date_start[$i] = parseDateTime($articledata[$i]['date_start']);
        $time[$i] = $time_date_start[$i]['day_ind_name'] . ", " . $time_date_start[$i]['day'] . " " . $time_date_start[$i]['month_ind_name'] . " " . $time_date_start[$i]['year'];
        $time[$i] .= ' - ' . $time_date_start[$i]['hour'] . ":" . $time_date_start[$i]['minute'] . " WIB";

        $ago[$i] = time_difference($articledata[$i]['date_start']);

        echo '
            <div class="article-news">
                <div class="category-links gnews"><a href="' . site_url('gnews') . '" title="Gen News">Gen News</a></div>
                <div class="inner">
                    <div class="title"><a href="' . $l[$i] . '">' . $title[$i] . '</a></div>
                    <time>' . $ago[$i] . '</time>
                    <div class="caption">' . $summ[$i] . '</div>
                    <div class="img"><a href="' . $l[$i] . '"><img src="' . $img[$i] . '" alt="' . $title[$i] . '"></a></div>
                </div>
            </div>';
    }
    /* END Article Sticky Native Normal */

    $i = 0;
    $id_content[$i] = $data[$i]['id_content'];
    $img[$i] = images_uri() . $this->config->item('dyn_620') . '/pena/news/' . date('Y/m/d/',strtotime($data[$i]['date_created'])) . '/' . $data[$i]['id_subkanal'] . '/' . $id_content[$i] . '/' . $data[$i]['images'];
    $subakanal[$i] = $data[$i]['subkanal'];
    $dtime[$i] = strtotime($data[$i]['date_created']);
    $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);

    $title[$i] = cleanWords($data[$i]['title']);
    $summary[$i] = cleanWords($data[$i]['summary']);
    $ago[$i] = time_difference($data[$i]['date_published']);

    echo '
        <div class="article-news">
            <div class="category-links gnews"><a href="' . site_url('gnews') . '" title="' . $subakanal[$i] . '">' . $subakanal[$i] . '</a></div>
            <div class="inner">
                <div class="title"><a href="' . $link[$i] . '">' . $title[$i] . '</a></div>
                <time>' . $ago[$i] . '</time>
                <div class="caption">' . $summary[$i] . '</div>
                <div class="img"><a href="' . $link[$i] . '"><img src="' . $img[$i] . '" alt="' . $title[$i] . '"></a></div>
            </div>
        </div>';

    $total_trendingtopic = count($trendingtopic);
    if($total_trendingtopic >= 3){
        echo '<section class="topic">
                <div class="head"><span>Topic : </span>' . $topic . '</div>
                <ul>';
        for($i = 0; $i < 3; $i++){
            $id_content[$i] = $trendingtopic[$i]['id_content'];
            $img[$i] = images_uri() . $this->config->item('dyn_200') . '/pena/news/' . $trendingtopic[$i]['year'] . '/' . $trendingtopic[$i]['month'] . '/' . $trendingtopic[$i]['day'] . '/' . $trendingtopic[$i]['id_subkanal'] . '/' . $id_content[$i] . '/' . $trendingtopic[$i]['images'];
            $subakanal[$i] = $trendingtopic[$i]['subkanal'];
            $dtime[$i] = strtotime($trendingtopic[$i]['date_created']);
            $link[$i] = site_url('read/' . $id_content[$i] . '/' . $trendingtopic[$i]['id_subkanal'] . '/' . $trendingtopic[$i]['slug_title'] . '-' . $dtime[$i]);
            $title[$i] = cleanWords($trendingtopic[$i]['title']);

            echo '
                <li>
                    <a href="' . $link[$i] . '" class="image"><img src="' . $img[$i] . '" alt="' . $title[$i] . '"></a>
                    <div class="category-text"><a href="' . $link[$i] . '">' . $subakanal[$i] . '</a></div>
                    <div class="title"><a href="' . $link[$i] . '">' . $title[$i] . '</a></div>
                </li>';
        }
        echo '</ul>
            </section>';
    }

    echo '<section class="article scroll">';

    for($i = 1; $i < $total; $i++){
        $id_content[$i] = $data[$i]['id_content'];
        $img[$i] = images_uri() . $this->config->item('dyn_620') . '/pena/news/' . date('Y/m/d',strtotime($data[$i]['date_created'])) . '/' . $data[$i]['id_subkanal'] . '/' . $id_content[$i] . '/' . $data[$i]['images'];
        $subakanal[$i] = $data[$i]['subkanal'];
        $dtime[$i] = strtotime($data[$i]['date_created']);
        $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);

        $title[$i] = cleanWords($data[$i]['title']);
        $summary[$i] = cleanWords($data[$i]['summary']);
        $ago[$i] = time_difference($data[$i]['date_published']);

        echo '
            <div class="article-news">
                <div class="category-links gnews"><a href="' . site_url('gnews') . '" title="' . $subakanal[$i] . '">' . $subakanal[$i] . '</a></div>
                <div class="inner">
                    <div class="title"><a href="' . $link[$i] . '" title="' . $title[$i] . '">' . $title[$i] . '</a></div>
                    <time>' . $ago[$i] . '</time>
                    <div class="caption">' . $summary[$i] . '</div>
                    <div class="img"><a href="' . $link[$i] . '"><img class="lazyload" data-src="' . $img[$i] . '" alt="' . $title[$i] . '"></a></div>
                </div>
            </div>';
    }

    echo '
        <div class="news-link-more">
            <div class="news-more">
                <a href="'. site_url('go/10').'">Load More</a>
            </div>
        </div>';
    
    echo '</section>';

    //echo '<a href="https://index.sindonews.com/index/600" class="btn-more">Indeks Berita</a>';
}
echo '</div>';
