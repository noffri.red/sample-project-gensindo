<?php

class Textcache {

    var $ip_gcp;
    var $redisCache;
    var $redisCacheStatus;
    var $redisDb;
    var $redisDbStatus;

    function __construct(){
        $this->CI = & get_instance();

        $this->ip_gcp = array(
            '10.184.15.219',/* staging */
            '10.184.0.25',/* ws1 */
            '10.184.0.58',/* ws2 */
            '10.184.0.59',/* ws3 */
            '10.184.0.60' /* ws3 */
        );

        $this->redisDb = new Redis();
        $this->redisCache = new Redis();
        $this->initRedis();
    }

    /* memstore cache */

    function initRedis(){
        if(isset($_SERVER['SERVER_ADDR']) && in_array($_SERVER['SERVER_ADDR'],$this->ip_gcp)){
            $redisip_lb = array(
                'server' => '10.11.9.11',
                'port' => 7011
            );

            try{
                /* redis ip load balancer */
                $this->redisCache->connect($redisip_lb['server'],$redisip_lb['port']);
                $this->redisCacheStatus = TRUE;
            }catch(Exception $e){
                $this->redisCacheStatus = FALSE;
                $msg = strtolower($e->getMessage());
                //xDebug($msg);
            }
        }elseif(isset($_SERVER['SERVER_ADDR']) && ($_SERVER['SERVER_ADDR'] == '10.1.7.212' || $_SERVER['SERVER_ADDR'] == '103.144.191.212')){
            $redisip = array(
                'server' => '10.1.9.177',
                'port' => 6379
            );

            try{
                $this->redisCache->connect($redisip['server'],$redisip['port']);
                $this->redisCacheStatus = TRUE;
            }catch(Exception $e){
                $this->redisCacheStatus = FALSE;
                $msg = strtolower($e->getMessage());

                //xDebug($msg);
            }
        }elseif(isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] == '10.1.7.175'){
            $redisip = array(
                'server' => '10.1.7.175',
                'port' => 6379
            );

            try{
                $this->redisCache->connect($redisip['server'],$redisip['port']);
                $this->redisCacheStatus = TRUE;
            }catch(Exception $e){
                $this->redisCacheStatus = FALSE;
                $msg = strtolower($e->getMessage());

                //xDebug($msg);
            }
        }else{
            $redisip_lb = array(
                array(
                    'server' => '10.1.9.112',
                    'port' => 7112
                ),
                array(
                    'server' => '10.1.9.113',
                    'port' => 7113
                )
            );
            $r_lb = rand(0,1);

            try{
                /* randop redis ip load balancer */
                $this->redisCache->connect($redisip_lb[$r_lb]['server'],$redisip_lb[$r_lb]['port']);
                $this->redisCacheStatus = TRUE;
            }catch(Exception $e){
                $this->redisCacheStatus = FALSE;
                $msg = strtolower($e->getMessage());

                //xDebug($msg);
            }
        }
    }

    function redisGet($key){
        if($this->redisCacheStatus){
            $cache = $this->redisCache->get($key);
            $this->redisCache->close();
        }else{
            $cache = '';
        }

        return $cache;
    }

    function redisSet($key,$ttl,$data){
        if($this->redisCacheStatus){
            $this->redisCache->setex($key,$ttl,$data);
            $this->redisCache->close();
        }
    }

    /* memstore main */

    function dbredis(){
        if(isset($_SERVER['SERVER_ADDR']) && in_array($_SERVER['SERVER_ADDR'],$this->ip_gcp)){
            $redisip_lb = array(
                'server' => '10.11.9.12',
                'port' => 7012
            );

            try{
                $this->redisDb->connect($redisip_lb['server'],$redisip_lb['port']);
                $this->redisDbStatus = TRUE;
            }catch(Exception $e){
                $this->redisDbStatus = FALSE;
                $msg = strtolower($e->getMessage());
                //xDebug($msg);
            }
        }elseif(isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] == '10.1.7.175'){
            $redisip = array(
                'server' => '10.1.7.175',
                'port' => 6379
            );

            try{
                $this->redisDb->connect($redisip['server'],$redisip['port']);
                $this->redisDbStatus = TRUE;
            }catch(Exception $e){
                $this->redisDbStatus = FALSE;
                $msg = strtolower($e->getMessage());
                //xDebug($msg);
            }
        }else{
            $redisip_lb = array(
                array(
                    'server' => '10.1.9.110',
                    'port' => 7110
                ),
                array(
                    'server' => '10.1.9.111',
                    'port' => 7111
                )
            );
            $r_lb = rand(0,1);

            try{
                $this->redisDb->connect($redisip_lb[$r_lb]['server'],$redisip_lb[$r_lb]['port']);
                $this->redisDbStatus = TRUE;
            }catch(Exception $e){
                $this->redisDbStatus = FALSE;
                $msg = strtolower($e->getMessage());
                //xDebug($msg);
            }
        }
    }

    function redisDbGet($key){
        $this->dbredis();
        if($this->redisDbStatus){
            $dbget = $this->redisDb->get($key);
            $this->redisDb->close();
        }else{
            $dbget = '';
        }

        return $dbget;
    }

}
