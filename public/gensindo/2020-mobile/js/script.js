
$(document).ready(function () {
  /* -- Menu -- */
  $(function() {
      $(".menu-link").click(function(e) {
        e.preventDefault();
        $("#menu-overlay").toggleClass("open");
        $("#menu").toggleClass("open");
        $("body").toggleClass("fixed");
        $(".menu-kanal").toggleClass("open");
      });
      /* -- Open Subkanal -- */
      var array = ["nasional", "metronews",
        "daerah", "bisnis", "internasional", "sports",
        "autotekno", "lifestyle", "kalam", "tv", "radio"];
      var i;
      var subkanal = "";
      var kanal = "";
      for (i = 0; i < array.length; i++) {
        subkanal = ".subkanal." + array[i];
        expand = ".expand." + array[i];
        ExpandSubkanal(expand,subkanal);
      }
      function ExpandSubkanal(expand,subkanal){
        $(expand).click(function() {   
          $(subkanal).slideToggle("slow");
          $(expand).toggleClass("fa-angle-right fa-angle-down");
        });
      }
  });
  /* -- Sosmed Tooltip -- */
  $("#social-media-header").click(function(e) {
      $("#social-media-header .list").toggleClass("fade-in");
  });
  /*-- Menu Dropdown --*/
  $('#menu-overlay .icon-dropdown').on('click', function () {
      $(this).parent().parent().children('ul').slideToggle('slow');
      $(this).toggleClass("fa-angle-right fa-angle-down");
  });
  /* -- Slider Mainvisual -- */
  var swiper = new Swiper('.slider', {
    effect: 'fade',
    slidesPerView: 'auto',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
    },
  });
  /*-- To Top Button --*/
  $('.to-top').click(function () {
    window.scrollTo({top: 0, behavior: 'smooth'});
  });
  $(window).scroll(function() {
      if ($(this).scrollTop() > 600){  
          $('.to-top').fadeIn();
      }
      else{
          $('.to-top').fadeOut();
      }
  });
});