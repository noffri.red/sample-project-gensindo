<?php

$total = count($related);
if($total > 0){
    echo '<div class="link-article mt20">';
    echo '<div class="head">Artikel Terkait</div>';
    echo '<ul class="clearfix">';
    for($i = 0; $i < $total; $i++){
        $dtime[$i] = strtotime($related[$i]['date_created']);
        $l[$i] = site_url('berita') . '/' . $related[$i]['id_content'] . '/' . $related[$i]['id_subkanal'] . '/' . slug($related[$i]['title']) . '-' . $dtime[$i];
        $title[$i] = cleanWords($related[$i]['title']);

        $subkanal[$i] = $related[$i]['subkanal'];

        if($related[$i]['images'] != ''){
            $img[$i] = images_uri() . '/dyn/360/gensindo/content/' . date('Y/m/d',strtotime($related[$i]['date_created'])) . '/' . $related[$i]['id_subkanal'] . '/' . $related[$i]['id_content'] . '/' . $related[$i]['images'];
            $showimg[$i] = '<a href="' . $l[$i] . '" class="image"><img class="lazyload" data-src="' . $img[$i] . '" alt="' . $title[$i] . '"></a>';
        }else{
            $showimg[$i] = '';
        }

        echo '
            <li>
                ' . $showimg[$i] . '
                <div class="title"><a href="' . $l[$i] . '">' . $title[$i] . '</a></div>
            </li>';
    }

    echo '</ul>';
    echo '</div>';
}
