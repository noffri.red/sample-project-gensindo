 

 <?php if(count($latest) > 0){ 
   ?>
  <div id="arsip">
      <div class="head">
        <div class="title">Latest News</div>
      </div>
      <div class="list-news scroll">
        <ul>
          <?php foreach ($latest as $value) {
          
            $id_content= $value['content_id'];
            $img = images_uri() .'/dyn/360/pena/news/' . date('Y/m/d',strtotime($value['date_created'])) . '/' . $value['channel_id'] . '/' . $id_content . '/' . $value['image'];
            $subakanal = $value['channel_name'];
            $dtime= strtotime($value['date_created']);
            $link= site_url('read/' . $id_content . '/' . $value['channel_id'] . '/' . slug($value['title']) . '-' . $dtime);

            $title = cleanTitleArticle($value['title']);
            $summary = cleanWords($value['summary']);
            $ago = time_difference($value['publish']);
            $subkanal = ucwords($value['channel_name']);
          
            ?>
          <li>
            <div class="image"><a href="<?php echo $link; ?>"><img class="lazyload" data-src="<?php echo $img; ?>" alt="<?php echo cleanHTML($title); ?>" width="360"></a></div>
            <div class="block-caption">
              <div class="title"><a href="<?php echo $link; ?>"><?php echo $title ; ?></a></div>
              <div class="category gen-news"><a href="#"><?php echo ucwords($subkanal)?></a></div>
              <div class="time"><?php echo $ago ?></div>
            </div>
          </li>
        
          <?php } ?>

          <div class="ads300 mt20 mb20">
            <div id="div-gpt-ad-middle_2"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-middle_2');});</script></div>
           </div>
          
          <!-- <a href="#" class="btn-center">Indeks Berita</a> -->
          <div class="news-link-more">
            <div class="news-more">
                <a href="<?php echo site_url('mgo/10');  ?>">Load More</a>
            </div>
         </div>


        </ul>
      </div>
    </div>
  <?php } ?>