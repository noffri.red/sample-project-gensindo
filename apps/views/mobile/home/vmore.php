<?php

$total = count($latest);
if($total > 0){
    $adsID = $this->uri->segment(2,0);
    if($adsID > 600){
        $adsID = $this->uri->segment(3,0);
    }
    
    echo '<div class="ads300 mt20 mb20" id="div-gpt-ad-middle_3-' . $adsID . '"></div>';
    
    for($i = 0; $i < $total; $i++){
        $d[$i] = date('Y/m/d',strtotime($latest[$i]['date_created']));
        $id_content[$i] = $latest[$i]['id_news'];
        $img[$i] = images_uri() . '/dyn/360/pena/news/' . $d[$i] . '/' . $latest[$i]['id_subkanal'] . '/' . $latest[$i]['id_news'] . '/' . $latest[$i]['photo'];
        $subakanal[$i] = $latest[$i]['subkanal'];
        $dtime[$i] = strtotime($latest[$i]['date_created']);
        $link[$i] = site_url('read') . '/' . $id_content[$i] . '/' . $latest[$i]['id_subkanal'] . '/' . slug($latest[$i]['title']) . '-' . $dtime[$i];

        $title[$i] = cleanWords($latest[$i]['title']);
        $summary[$i] = cleanWords($latest[$i]['summary']);
        $ago[$i] = time_difference($latest[$i]['date_published']);

        echo '
            <div class="article-news">
                <div class="category-links gnews"><a href="' . site_url('gnews') . '" title="Gen News">Gen News</a></div>
                <div class="inner">
                    <div class="title"><a href="' . $link[$i] . '" title="' . $title[$i] . '">' . $title[$i] . '</a></div>
                    <time>' . $ago[$i] . '</time>
                    <div class="caption">' . $summary[$i] . '</div>
                    <div class="img"><a href="' . $link[$i] . '"><img class="lazyload" data-src="' . $img[$i] . '" alt="' . $title[$i] . '"></a></div>
                </div>
            </div>';
    }


    if($totalData > $per_page){
        echo '<div class="news-link-more">';
        echo $pagination;
        echo '</div>';
    }
} 