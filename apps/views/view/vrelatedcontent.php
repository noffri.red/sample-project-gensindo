<?php
$total_related = count($related);
if($total_related > 0) {
    $data_related = $related;

    echo '<div class="link-article">
            <div class="head">Artikel Terkait</div>
            <ul class="clearfix">';

    for($i=0; $i < $total_related; $i++) {
        $dtime[$i] = strtotime($data_related[$i]['date_created']);
        $link[$i] = site_url('read/' . $data_related[$i]['id_content'] . '/' . $data_related[$i]['id_subkanal'] . '/' . $data_related[$i]['slug_title'] . '-' . $dtime[$i]);
        $title[$i] = cleanWords($data_related[$i]['title']);
        $img[$i] = $this->config->item('images_uri') . $this->config->item('dyn_200').'/gensindo/content/' . $data_related[$i]['year'] . '/' . $data_related[$i]['month'] . '/' . $data_related[$i]['day'] . '/' . $data_related[$i]['id_subkanal'] . '/' . $data_related[$i]['id_content'] . '/' . $data_related[$i]['images'];
        $subkanal[$i] = $data_related[$i]['subkanal'];

        echo '<li>
            <a href="'. $link[$i] .'"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></a>
            <div class="category-text"><a href="'. $link[$i] .'">'. $subkanal[$i] .'</a></div>
            <div class="title"><a href="'. $link[$i] .'">'. $title[$i] .'</a></div>
        </li>';
    }
    echo '</ul>
        <a href="#" class="btn-more">View More</a>
    </div>';
}
