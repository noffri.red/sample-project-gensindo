<ul>
    <li><a href="<?php echo base_url();?>">Home</a></li>
    <li><a href="<?php echo site_url('gnews');?>">G-news</a></li>
    <li><a href="<?php echo site_url('youth');?>">Youth</a></li>
    <li><a href="<?php echo site_url('imaji');?>">Imaji</a></li>
</ul>

<div class="foot-box">
    <p>Copyright &copy; <?php echo date('Y'); ?> SINDOnews.com, All Rights Reserved</p>
    <p class="rendering"><?php echo $this->uri->segment(1); ?>/ rendering in {elapsed_time} seconds (<?php echo infoServer(); ?>)</p>        
</div>