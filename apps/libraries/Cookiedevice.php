<?php

class Cookiedevice {

    var $CI;
    var $desktopUA;
    var $mobileUA;
    var $device;
    var $domain;
    var $age;

    function __construct(){
        $this->CI = & get_instance();
        $this->domain = $this->CI->config->item('domain');
        $this->desktopUA = $this->CI->config->item('desktop-ua');
        $this->mobileUA = $this->CI->config->item('mobile-ua');
        $this->age = 120;

        if($this->CI->mobiledevice->device_check()){
            $this->device = $this->desktopUA;
        }else{
            $this->device = $this->mobileUA;
        }

        $this->init($this->device);
    }

    function init($device){
        $utcDateTime = date('D, d-M-Y H:i:s',time() + $this->age);
        $expires = convertDateTime($utcDateTime) . ' GMT+7';
        $dataCookie = 'sinua=' . $device;

        $this->CI->output->set_header('Set-Cookie: ' . $dataCookie . '; expires=' . $expires . '; Max-Age=' . $this->age . '; path=/; domain=' . $this->domain . '; secure; samesite=none');
    }


}
