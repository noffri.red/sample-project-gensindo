<?php

class Mdbblog extends CI_Model {

    function getBlog($param=array(), $howmuch = 3, $start = 0, $total_results = FALSE){
        $id_news = '';
        if(!empty($param['id_news'])){
            $id_news = "AND a.id_news != '". $param['id_news'] ."'";
        }

        $qString = "SELECT 
                        a.id_news, a.id_subkanal, a.title, a.url_title, a.photo, a.date_created, a.date_published, 
                        DATE_FORMAT(a.date_created, '%Y') AS year, 
                        DATE_FORMAT(a.date_created, '%m') AS month,  
                        DATE_FORMAT(a.date_created, '%d') AS day,  
                        b.kanal,
                        c.subkanal, c.url_subkanal
                    FROM t_news a";
        $qJoin = " LEFT JOIN t_kanal AS b ON b.id_kanal = a.id_kanal
                  LEFT JOIN t_subkanal AS c ON c.id_subkanal = a.id_subkanal";
        $qCondition = " WHERE 
                            a.status = '0'
                            AND a.id_author = '". $param['id_author'] ."'
                            $id_news ";
        $qOrderBy = " ORDER BY a.date_published DESC";
        $qLimit = " LIMIT ".$start.",".$howmuch;

        $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
        $response_data = $this->db->query($qString);
        $data['results'] = $response_data->result_array();

        if($total_results == TRUE){
            $qStringTotal = "SELECT 
                            COUNT(*) AS total 
                          FROM t_news a";

            $qStringTotal .= $qJoin.$qCondition;
            $response_total = $this->db->query($qStringTotal);
            $total = $response_total->result_array();
            $data['total_results'] = $total[0]['total'];
        }
        $this->db->close();

        return $data;
    }
    function cekAuthor($url_author){
        $qString = "SELECT 
                        id_author, url_author, author, SUBSTRING_INDEX(date_modified, ',', 1) AS author_date
                    FROM t_author";
        $qCondition = " WHERE 
                            url_author = '". $url_author ."'";

        $qString .= $qCondition;
        $response_data = $this->db->query($qString);
        $data['results'] = $response_data->result_array();
        $this->db->close();
        return $data;
    }
}