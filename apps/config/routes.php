<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* komponen read */
$route['read/(.*)'] = "read/index/$1";
$route['newsread/(.*)'] = "newsread/index/$1";
$route['berita/(.*)'] = "berita/index/$1";
$route['beritaamp/(.*)'] = "beritaamp/index/$1";
$route['readsimple/(.*)'] = "readsimple/index/$1";
$route['archive/(.*)'] = "archive/index/$1";
$route['uc/(.*)'] = "uc/index/$1";
$route['forum/(.*)'] = "forum/index/$1";
$route['mobile-forum/(.*)'] = "forum/mobileforum/$1";
$route['desktop-forum/(.*)'] = "forum/desktopforum/$1";
$route['disqus/(.*)'] = "forum/disqus/$1";
$route['mdisqus/(.*)'] = "forum/mdisqus/$1";
$route['simple'] = "forum/simple";

/* imaji */
$route['view/(.*)'] = "view/index/$1";

/* subkanal */
$route['gnews'] = "gennews/index";
//$route['youth'] = "subkanal/gohome";
$route['loadmore/(.*)'] = "loadmore/index/$1";
//$route['samsung'] = "subkanal/gohome";

$route['imaji'] = "imaji/index";

/* topic */
$route['tag/(.*)'] = "tag/index/$1";

/* reporter */
$route['reporter'] = 'reporter/index/$1';
$route['reporter/(:any)'] = 'reporter/index/$1';
$route['reporter/(:any)/(:any)'] = 'reporter/index/$1';
$route['reporter/(:any)/(:any)/(:any)'] = 'reporter/index/$1';

/* AJAX */
$route['go/(.*)'] = "ajax/breakinghome/$1";
$route['gosub/(.*)'] = "ajax/subkanal/$1";
$route['mgo/(.*)'] = "ajax/mbreakinghome/$1";
$route['mgosub/(.*)'] = "ajax/msubkanal/$1";

/* API */
$route['scream'] = "api/scream";
$route['xyz'] = "api/xyz";
$route['blackclouds'] = "api/blackclouds";
$route['nature'] = "api/pushtopic";

/* Tracker */
$route['freshmeat'] = "gamp/index";
$route['dontrun'] = "gamp/emptyspace";
$route['artsindo'] = "gamp/garec";
$route['artcode'] = "gamp/gapop";
$route['silence/(.*)'] = "gamp/generator/$1";
$route['aimatrix/(.*)'] = "gamp/aimatrix/$1";
$route['reckoning/(.*)'] = "gamp/reckoning/$1";
$route['ftrack'] = "gamp/framesimple";
$route['adtrack'] = "gamp/adtrack";

/* sitemap */
$route['sitemap.xml'] = 'feed/sitemap';
$route['sitemap-channel.xml'] = 'feed/channel';
$route['sitemap-tag.xml'] = 'feed/tag';
$route['sitemap-web.xml'] = 'feed/web';
$route['sitemap-tags/(.*).xml'] = "feed/indextopic/$1";
$route['sitemap-indextags.xml'] = "feed/indextags";
