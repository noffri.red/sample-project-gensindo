<div class="container">
    <!-- Menu Kanal -->
    <div id="menu-kanal">
        <ul>
        <li><a href="https://www.sindonews.com"><span data-hover="Beranda">Beranda</span></a></li>
              <li><a href="https://nasional.sindonews.com/"><span data-hover="Nasional">Nasional</span></a></li>
              <li><a href="https://metro.sindonews.com/"><span data-hover="Metro">Metro</span></a></li>
              <li><a href="https://daerah.sindonews.com/"><span data-hover="Dareah">Daerah</span></a></li>
              <li><a href="https://ekbis.sindonews.com/"><span data-hover="Ekbis">Ekbis</span></a></li>
              <li><a href="https://international.sindonews.com/"><span data-hover="International">International</span></a></li>
              <li><a href="https://sports.sindonews.com/"><span data-hover="Sports">Sports</span></a></li>
              <li><a href="https://autotekno.sindonews.com/"><span data-hover="Autotekno">Autotekno</span></a></li>
              <li><a href="https://lifestyle.sindonews.com/"><span data-hover="Lifestyle">Lifestyle</span></a></li>
              <li class="active"><a href="https://gensindo.sindonews.com/"><span data-hover="Gen&nbsp;SINDO">Gen&nbsp;SINDO</span></a></li>
              <li class="kanal-lain">
                <a href="#">Kanal Lain<i class="fas fa-angle-right"></i></a>
                <div class="kanal-expand">
                  <ul>
                    <li><a href="https://kalam.sindonews.com/">Kalam</a></li>
                    <!-- <li><a href="#">Quiz</a></li> -->
                    <li><a href="https://infografis.sindonews.com/">Infografis</a></li>
                    <!-- <li><a href="#">Live TV</a></li> -->
                    <!-- <li><a href="#">Otomotif</a></li> -->
                    <!-- <li><a href="#">Tekno</a></li> -->
                    <li><a href="https://makassar.sindonews.com/">Makassar</a></li>
                    <li><a href="https://photo.sindonews.com/">Photo</a></li>
                    <li><a href="https://video.sindonews.com/">Video</a></li>
                  </ul>
                </div>
              </li>
        </ul>
    </div>
    <!-- Social Media Header -->
    <div id="social-media-header">
        <ul>
            <li><a target="_blank" href="https://www.facebook.com/sindonews"><i class="fab fa-facebook-f"></i></a></li>
            <li><a target="_blank" href="https://twitter.com/sindonews"><i class="fab fa-twitter"></i></a></li>
            <li><a  href="<?php echo site_url('feed'); ?>"><i class="fas fa-rss"></i></a></li>
        </ul>
    </div>
</div>