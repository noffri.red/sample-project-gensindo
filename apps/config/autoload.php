<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('curl','session','mobile_detect','mobiledevice','textcache','cookiedevice');
$autoload['drivers'] = array();
$autoload['helper'] = array('url','google_analytics','weburi','header','textlimiter','stringurl','string','contentdate','gen');
$autoload['config'] = array('webconfig','site_domain');
$autoload['language'] = array();
$autoload['model'] = array();