<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imaji extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helpers("gen");
        $this->load->model('news/mredisnews','mredisnews');
    }

	function index(){
        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= add_external_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-1.10.2.min.js');
        $a['html']['js'] .= add_js('/js/materialize.min.js');

        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['promoted'] = $this->load->view('template/vpromoted',NULL,TRUE);
        $a['template']['kategori'] = $this->load->view('template/vkategori',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('template/vminifooter',NULL,TRUE);
        $a['template']['promogen'] = $this->load->view('template/vpromogen',NULL,TRUE);

        $t['event'] = $this->mredisnews->getEvent();
        $a['template']['agenda'] = $this->load->view('template/vagenda',$t,TRUE);

        $t['trending'] = $this->mredisnews->getKanalTrendingTopic();
        $a['template']['trending'] = $this->load->view('template/vtrending',$t,TRUE);

        $t['imaji'] = $this->mredisnews->getListImaji();
        $a['content']['imaji'] = $this->load->view('imaji/vimaji',$t,TRUE);

        $strMetaDesc = '';
        if(count($t['imaji']['results']) > 5){
            $loop = "5";
        }else{
            $loop = count($t['imaji']['results']);
        }
        for($i = 0; $i < $loop; $i++){
            if($i ==  ($loop - 1) ){
                $strMetaDesc .= cleanHTML($t['imaji']['results'][$i]['title']);
            }else{
                $strMetaDesc .= cleanHTML($t['imaji']['results'][$i]['title']) . '. ';
            }
        }

        $a['html']['title'] = "GEN SINDO | Berita imaji";
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => "Berita imaji - " . climiter($strMetaDesc,95),
            'alexaVerifyID' => $this->config->item('alexaVerifyID'),
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );
        $a['html']['metaname'] = metaname($metaname);

        $this->load->view('pages/vimaji',$a,FALSE);
    }
}
