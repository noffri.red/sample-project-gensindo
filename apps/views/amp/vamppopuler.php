<?php

$total = count($populer);

if($total > 0){
    echo '<div class="popular mt20">';
    echo '<div class="popular-heading">Berita Populer</div>';
    echo '<ul>';
    for($i = 0; $i < $total; $i++){
        $dtime[$i] = strtotime($populer[$i]['date_created']);
        $date[$i] = date('Y/m/d',strtotime($populer[$i]['date_created']));
        $l[$i] = site_url('read/' . $populer[$i]['content_id'] . '/' . $populer[$i]['channel_id'] . '/' . slug($populer[$i]['title']) . '-' . $dtime[$i]);
        $title[$i] = cleanWords($populer[$i]['title']);

        echo '
            <li>
                <a href="' . $l[$i] . '">' . $title[$i] . '</a>
            </li>';
    }
    echo '</ul>';
    echo '</div>';
}