<?php
$total = count($sindonews_terkini);
if($total > 0){
    echo '<div class="link-article">
		<div class="head">Baca Juga</div>
		<ul class="clearfix">';

			for($i=0; $i < $total; $i++) {
				$dtime[$i] = strtotime($sindonews_terkini[$i]['date_created']);
				$d[$i] = date('Y/m/d',strtotime($sindonews_terkini[$i]['date_created']));
				$subtitle[$i] = cleanWords($sindonews_terkini[$i]['subtitle']);
				$title[$i] = cleanWords($sindonews_terkini[$i]['title']);
				$img[$i] = $this->config->item('images_uri') . '/dyn/200/content/' . $d[$i] . '/' . $sindonews_terkini[$i]['channel_id'] . '/' . $sindonews_terkini[$i]['news_id'] . '/' . $sindonews_terkini[$i]['thumb'];
				$channel_name[$i] = ucwords($sindonews_terkini[$i]['channel_name']);
				$ago[$i] = time_difference($sindonews_terkini[$i]['date_publish']);

				$channel_id[$i] = $sindonews_terkini[$i]['channel_id'];
				$site_domain[$i] = $this->config->item('read_domain');
				if($channel_id[$i] == ''){
					$l[$i] = 'https://www.sindonews.com/read/' . $sindonews_terkini[$i]['news_id'] . '/' . $sindonews_terkini[$i]['channel_id'] . '/' . slug($sindonews_terkini[$i]['title']) . '-' . $dtime[$i];
				}else{
					$l[$i] = 'http://' . $site_domain[$i][$channel_id[$i]] . '/read/' . $sindonews_terkini[$i]['news_id'] . '/' . $sindonews_terkini[$i]['channel_id'] . '/' . slug($sindonews_terkini[$i]['title']) . '-' . $dtime[$i];
				}
				 
				if($sindonews_terkini[$i]['thumb'] != ''){
					$showimg[$i] = '<a href="'. $l[$i] .'"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></a>';
				}else{
					$showimg[$i] = '';
				}
		
				echo '<li>
					' . $showimg[$i] . '
					<div class="category-text"><a href="'. $l[$i] .'">'. $channel_name[$i] .'</a></div>
					<div class="title"><a href="'. $l[$i] .'">'. $title[$i] .'</a></div>
				</li>';
			}
			
		echo '</ul>';
    echo '</div>';
}
