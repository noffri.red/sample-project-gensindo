<?php if (count($related) > 0) {
        echo '<div class="head">
                <div class="title">Berita Terkait</div>
              </div>
            <div class="list-news">';

        echo '<ul>';

        foreach ($related as $value) {
         
            $site_domain = $this->config->item('read_domain');
            $dtime = strtotime($value['date_created']);

            $l = site_url('berita') . '/' . $value['id_content'] . '/' . $value['id_subkanal'] . '/' . slug($value['title']) . '-' . $dtime;

            $title = cleanWords($value['title']);
            $subkanal = $value['subkanal'];

            $ago = time_difference($value['date_published']);

            $img = '';
            if ($value['images'] != '') {
                $img = images_uri() . '/dyn/300/gensindo/content/' . date('Y/m/d',strtotime($value['date_created'])) . '/' . $value['id_subkanal'] . '/' . $value['id_content'] . '/' . $value['images'];

                $imgHtml =  '<a href="'.$l.'"><img src="'.$img.'" alt="'.cleanHTML($title).'"></a>';
            }else{
                $imgHtml = '';
            }

    ?>

           <li>
               <div class="image"><?php echo $imgHtml ?></div>
               <div class="block-caption">
                   <div class="title"><a href="<?php echo $l; ?>"><?php echo cleanHTML($title) ?></a></div>
                   <!-- <div class="category gen-news"><a href="#"><?php //echo ucwords($subkanal) ?></a></div> -->
                   <!-- <div class="time"><?php //echo $ago ?></div> -->
               </div>
           </li>
           </ul>

   <?php  }
        echo '</ul>
        </div>';
    } ?>