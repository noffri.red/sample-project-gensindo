<?php

$total = count($breaking);
if($total > 0){
    $data = $breaking;

    echo '<div id="arsip">';

    $i = 0;
    $id_content[$i]= $data[$i]['content_id'];
    $img[$i] = images_uri() .'/dyn/360/pena/news/' . date('Y/m/d',strtotime($data[$i]['date_created'])) . '/' . $data[$i]['channel_id'] . '/' . $id_content[$i] . '/' . $data[$i]['image'];
    $subakanal[$i] = $data[$i]['channel_name'];
    $dtime[$i]= strtotime($data[$i]['date_created']);
    $link[$i]= site_url('read/' . $id_content[$i] . '/' . $data[$i]['channel_id'] . '/' . slug($data[$i]['title']) . '-' . $dtime[$i]);

    $title[$i] = cleanTitleArticle($data[$i]['title']);
    $summary[$i] = cleanWords($data[$i]['summary']);
    $ago[$i] = time_difference($data[$i]['publish']);
    $subkanal[$i] = ucwords($data[$i]['channel_name']);
    
    echo '<div class="list-news">';
      echo ' <ul>';
         
          echo ' <li>
                    <div class="image"><a href="' . site_url('gnews') . '"><img class="lazyload" data-src="' . $img[$i] . '" alt="'.$title[$i].'" width="360"></a></div>

                    <div class="block-caption">
                      <div class="title"><a href="' . $link[$i] . '">' . $title[$i] . '</a></div>
                      <div class="category gen-news"><a href="#">' . ucwords($subakanal[$i]) . '</a></div>
                      <div class="time">' . $ago[$i] . '</div>
                    </div>
                  </li>';
        

    echo '</ul>';
    echo '</div>';

    echo '<div class="ads300 mt20 mb20">
          <div id="div-gpt-ad-middle_1"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_1\');});</script></div>
      </div>';

    echo '<div class="list-news scroll">';
    echo '<ul>';
    
    // for($i = 1; $i < $total; $i++){
    $j = 0 ;
    foreach ($data as $value){

      if($j >= 1){
        
        $id_content= $value['content_id'];
        $img = images_uri() .'/dyn/360/pena/news/' . date('Y/m/d',strtotime($value['date_created'])) . '/' . $value['channel_id'] . '/' . $id_content . '/' . $value['image'];
        $subakanal = $value['channel_name'];
        $dtime= strtotime($value['date_created']);
        $link= site_url('read/' . $id_content . '/' . $value['channel_id'] . '/' . slug($value['title']) . '-' . $dtime);
    
        $title = cleanTitleArticle($value['title']);
        $summary = cleanWords($value['summary']);
        $ago = time_difference($value['publish']);
        $subkanal = ucwords($value['channel_name']);
  
  
          echo ' <li>
                  <div class="image"><a href="' . $link . '"><img class="lazyload" data-src="' . $img . '" alt="'.cleanHTML($title).'" width="360"></a></div>
  
                  <div class="block-caption">
                    <div class="title"><a href="' . $link . '">' . $title . '</a></div>
                    <div class="category gen-news"><a href="#">' . ucwords($subakanal) . '</a></div>
                    <div class="time">' . $ago . '</div>
                  </div>
                </li>';

      }
      
      $j++;
    }

    echo '
         <div class="news-link-more" hidden>
            <div class="news-more">
                <a href="' . site_url('mgosub') . '/' . $data[0]['channel_id'] . '/10">Load More</a>
            </div>
         </div>';

     echo '</ul>';
    
     echo '</div>';
     echo '</div>';
}