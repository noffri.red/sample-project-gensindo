<?php

class Ajaxpage extends CI_Controller {

    function __construct(){
        parent::__construct(); 
        $this->load->model('news/mgonews');
    }

    function index(){
        $result = array('success' => 0,'error' => 1,'data' => array('message' => 'are u missing something?'));
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    } 

    function simplenews(){
        $kanal_id = $this->input->post('pid');

        $newsdata = $this->mgonews->getListNews($kanal_id,9);
        $newarticle = randomData($newsdata,9,3);

        $urlframe = array();
        for($z = 0; $z < count($newarticle); $z++){
            $ntime[$z] = strtotime($newarticle[$z]['date_created']);
            $nsiteurl[$z] = site_url('archive') . '/' . $newarticle[$z]['content_id'] . '/' . $newarticle[$z]['channel_id'] . '/' . slug($newarticle[$z]['title'] . '-' . $ntime[$z]);
            $urlframe[$z] = $nsiteurl[$z];
        }

        $datapost = base64url_encode(json_encode($urlframe));
        $gaother = "$.ajax({url:'" . site_url('simple') . '?code=sindoapi&v=11.0&source=sindocloud' . rand(0,11) . '&z=' . time() . "',method:'POST',data:{fu:'" . $datapost . "'},success:function(result){\$('.sindoframe').html(result);}});";

        if($kanal_id != ''){
            echo base64_encode($gaother);
        }
    }

    function related(){
        $topic_list = base64_decode($this->input->post('tlist'));
        $id_news = $this->input->post('cid');

        $garelated = '';
        if($topic_list != ''){
            $newsdata = $this->mgonews->getRelatedContent($topic_list,$parent_id,0,15);
            $news = randomData($newsdata['results'],15,3);
            $total = count($news);
            if($total > 0){
                for($i = 0; $i < $total; $i++){
                    $dtime[$i] = strtotime($news[$i]['date_created']);
                    $pathurl[$i] = base64_encode('/read/' . $news[$i]['id_news'] . '/' . $news[$i]['id_subkanal'] . '/' . $news[$i]['url_title'] . '-' . $dtime[$i]);
                    $siteurl[$i] = base64_encode(site_url('read') . '/' . $news[$i]['id_news'] . '/' . $news[$i]['id_subkanal'] . '/' . $news[$i]['url_title'] . '-' . $dtime[$i]);
                    $title[$i] = base64_encode(seo_title($news[$i]['title']));

                    $garelated .= "txcors('" . $siteurl[$i] . "','GET','" . $pathurl[$i] . "','" . $title[$i] . "');";
                }
            }
        }

        if($id_news != '' && $topic_list != ''){
            echo base64_encode($garelated);
        }
    }

    function latestnews(){
        $kanal_id = $this->input->post('pid');

        $newsdata = $this->mgonews->getListNews($kanal_id,8);
        $newarticle = randomData($newsdata,8,2);
        $total = count($newarticle);
        $gaother = "";
        if($total > 0){
            for($i = 0; $i < $total; $i++){
                $dtime[$i] = strtotime($newarticle[$i]['date_created']);
                $siteurl[$i] = base64_encode(site_url('read') . '/' . $newarticle[$i]['content_id'] . '/' . $newarticle[$i]['channel_id'] . '/' . slug($newarticle[$i]['title']) . '-' . $dtime[$i]);
                $title[$i] = base64_encode(seo_title($newarticle[$i]['title']));

                $gaother .= "txcorslocation('" . $siteurl[$i] . "','GET','" . $siteurl[$i] . "','" . $title[$i] . "');";
            }
        }

        if($kanal_id != ''){
            echo base64_encode($gaother);
        }
    } 

}
