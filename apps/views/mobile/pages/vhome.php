<!DOCTYPE html>
<html lang="id-ID">
    <head>
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#004B8F">
        <?php echo $html['metaname']; ?>

        <link rel="shortcut icon" href="<?php echo template_uri(); ?>/images/favicon.ico">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-192x192.png" sizes="192x192">

        <link rel="canonical" href="<?php echo base_url(); ?>">
        <link rel="alternate" title="SINDOnews | RSS Feed Berita GEN SINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>

        <?php echo $html['css']; ?>
        <?php echo $template['newcss']; ?>
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-HDPI.css" media="screen and (min-width: 1083px) and (max-width: 1290px)">
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-MDPI.css" media="screen and (min-width: 993px) and (max-width: 1082px)">
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-SDPI.css" media="screen and (min-width: 769px) and (max-width: 992px)">
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-mobile.css" media="screen and (max-width: 768px)">

        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <script>
            window.googletag = window.googletag || {cmd: []};
            
            var adslot0;
            
            googletag.cmd.push(function() {
                googletag.defineSlot('/105246217/sindonews_mobile/gensindo', [[320,100],[320,50],[1,1]], 'div-gpt-ad-billboard').setTargeting('pos', ['billboard']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_mobile/gensindo', [[300,600],[300,250],[1,1]], 'div-gpt-ad-middle_1').setTargeting('pos', ['middle_1']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_mobile/gensindo', [[300,250],[1,1]], 'div-gpt-ad-middle_2').setTargeting('pos', ['middle_2']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_mobile/gensindo', [[320,50],[1,1]], 'div-gpt-ad-top-sticky').setTargeting('pos', ['top_sticky']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_mobile/gensindo', [[320,50],[1,1]], 'div-gpt-ad-bottom-sticky').setTargeting('pos', ['bottom_sticky']).addService(googletag.pubads());
                
                googletag.defineOutOfPageSlot('/105246217/sindonews_mobile/gensindo', 'div-gpt-ad-outstream').setTargeting('pos', ['outstream']).addService(googletag.pubads());
                googletag.defineOutOfPageSlot('/105246217/ssindonews_mobile/gensindo', 'div-gpt-ad-abm').setTargeting('partner', ['abm']).addService(googletag.pubads());
                
                adslot0 = googletag.defineSlot('/105246217/sindonews_mobile/gensindo', [[300,600],[300,250],[1,1]], 'div-gpt-ad-middle_3').setTargeting('pos', ['middle_3']).addService(googletag.pubads());
                
                googletag.pubads().setTargeting('page_type', ['kanalpage']);
                googletag.pubads().enableSingleRequest();
                googletag.pubads().collapseEmptyDivs();
                googletag.enableServices();
            });
            function bannerCheck(unitAd){googletag.pubads().addEventListener('slotRenderEnded',function(event){if(event.slot.getSlotElementId() === unitAd){if(event.isEmpty){var eAd = document.getElementById('me-'+unitAd);eAd.classList.add('adnone');}}});}
        </script>
        
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
        <script>
            window.OneSignal = window.OneSignal || [];
            OneSignal.push(function () {
                OneSignal.init({
                    appId: '<?php echo $this->config->item('onesignal_appid'); ?>',
                    autoResubscribe: false,
                    notifyButton: {
                        enable: false
                    },
                    promptOptions: {
                        slidedown: {
                            enabled: true,
                            autoPrompt: true,
                            actionMessage: "Dapatkan update berita SINDOnews melalui notifikasi browser Anda.",
                            acceptButtonText: "SUBSCRIBE",
                            cancelButtonText: "TIDAK"
                        }
                    }
                });
            });
        </script>
        
        <?php echo $html['bottom_js']; ?>
    </head>
    <body>
        <header>
            <?php echo $template['header']; ?>
        </header>
        <section class="slidenav">
            <ul id="slide-out" class="sidenav">
                <li>
                    <div class="user-view">
                        <a href="#user"><img class="circle" src="<?php echo template_uri(); ?>/images/logo.png"></a>
                    </div>
                </li>
                <li><a href="<?php echo base_url(); ?>">Home</a></li>
                <li class="gnews"><a href="<?php echo site_url('gnews'); ?>">Gen News</a></li>
                <!--<li class="gnews"><a href="<?php echo site_url('samsung'); ?>">Samsung</a></li>-->
                <li><a href="https://nasional.sindonews.com/">Nasional</a></li>
                <li><a href="https://metro.sindonews.com/">Metronews</a></li>
                <li><a href="https://daerah.sindonews.com/">Daerah</a></li>
                <li><a href="https://ekbis.sindonews.com/">Ekonomi Bisnis</a></li>
                <li><a href="https://international.sindonews.com/">International</a></li>
                <li><a href="https://sports.sindonews.com/">Sports</a></li>
                <li><a href="https://autotekno.sindonews.com/">Autotekno</a></li>
                <li><a href="https://lifestyle.sindonews.com/">Lifestyle</a></li>
                <li><a href="https://edukasi.sindonews.com/">Edukasi</a></li>
                <li><a href="https://kalam.sindonews.com/">Kalam</a></li> 
                <li><a href="https://infografis.sindonews.com/">Infografis</a></li>
                <li><a href="https://photo.sindonews.com/">Photo</a></li>
                <li><a href="https://video.sindonews.com/">Video</a>
            </ul>
        </section>
        <main>
            <div class="container clearfix">
                <div class="ads320">
                    <div id="div-gpt-ad-billboard"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-billboard');});</script></div>
                </div>
                
                <div class="column-big pull-left clearfix">
                    <div class="center pull-right">
                        <section class="tab">
                            <!--<div class="tab-head">
                                <ul class="tabs">
                                    <li class="current"><a href="<?php echo base_url(); ?>">Latest</a></li>
                                    <li><a href="<?php echo site_url('trending'); ?>">Trending</a></li>
                                    <li><a href="<?php echo site_url('about'); ?>">About</a></li>
                                </ul>
                            </div>-->
                            <div class="tab_content">
                                <?php echo $content['latest']; ?>
                            </div>
                        </section>
                    </div>
                    <div class="leftbar pull-left">
                        <?php echo $template['popular'];?>
                        <?php echo $template['trending']; ?>
                    </div>
                </div>
                <div class="rightbar pull-right">
                    <?php echo $template['promogen']; ?>
                    <?php echo $template['minifooter']; ?>
                </div>
            </div>
        </main>
        
        <div class="ads1x1">
            <div id="div-gpt-ad-abm"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-abm');});</script></div>
            <div id="div-gpt-ad-outstream"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-outstream');});</script></div>
        </div>
        
        <div class="bottom-block" id="me-div-gpt-ad-bottom-sticky">
            <div class="bottom-float">                
                <div class="bottom-genie bottom-extra">
                    <div id="div-gpt-ad-bottom-sticky"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-bottom-sticky');});</script></div>
                </div>
            </div>
        </div>
        
        <?php echo $html['bottom_css']; ?>
        <?php echo $html['js']; ?>
        <script>
            $(function () {
                
                var imagesLoad = new LazyLoad({
                    elements_selector:'.lazyload'
                });

                if ($(window).width() > 770) {
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 1495) {
                            $('.leftbar').addClass("sticky");
                            $(".leftbar").animate({top: "30px"}, 0);
                            $('.leftbar .r-side').addClass("none");
                        } else {
                            $('.leftbar').removeClass("sticky");
                            $('.leftbar .r-side').removeClass("none");
                        }
                        if ($(this).scrollTop() > 1262) {
                            $('.rightbar').addClass("sticky");
                            $(".rightbar").animate({top: "30px"}, 0);
                            $('.rightbar .r-side').addClass("none");
                            $('.agenda').addClass("none");
                        } else {
                            $('.leftbar').removeClass("sticky");
                            $('.rightbar').removeClass("sticky");
                            $('.rightbar .r-side').removeClass("none");
                            $('.agenda').removeClass("none");
                        }
                    });
                }

                $('.sidenav').sidenav();
                
                var mycount = 10;
                $('.scroll').jscroll({
                    loadingHtml: '<div class="news-load"><img src="<?php echo m_template_uri(); ?>/images/ajax-loader.gif" alt="loading image"></div>',
                    nextSelector: '.news-more a',
                    autoTrigger: false,
                    callback: function(){
                        imagesLoad.update();
                        
                        var slot;
                        var slotName = generateNextSlotName();
                        var parent = document.getElementById('div-gpt-ad-middle_3-' + mycount);
                        var child = document.createElement('div');
                        child.setAttribute('id', slotName);
                        parent.appendChild(child);
                        
                        googletag.cmd.push(function(){
                            slot = googletag.defineSlot('/105246217/sindonews_mobile/gensindo', [[300,600],[300,250],[1,1]], slotName).setTargeting('pos', ['middle_3']).addService(googletag.pubads());
                            googletag.display(slotName);
                            googletag.pubads().refresh([slot]);
                        });
                        
                        mycount += 10;
                    }
                });

                /*
                function dismissPush(){$('.notif-box').addClass('notif-hide');}
                if(navigator.platform === 'iPhone'){dismissPush();}
                var subdomain='<?php echo $this->config->item('domain'); ?>';var cookiename='__gensindo_push_m';var getme = getCookie(cookiename);if(getme !== ''){dismissPush();}
                if(Notification.permission==='granted' || Notification.permission==='denied'){setCookie(cookiename,randomuv(),15,subdomain);}
                console.log(Notification.permission);
                $('.notif-yes').click(function(){
                    $('.notif-lead').text('Terima Kasih...');
                    $('.notif-yes').hide();
                    setTimeout(function(){
                        $('.notif-box').addClass('notif-hide');
                    },3000);
                });
                */
            });
            
            var nextSlotId = 1;
            function generateNextSlotName() {
                var id = nextSlotId++;
                return 'adslot' + id;
            }
        </script>
    </body>
</html>