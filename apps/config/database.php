<?php

$active_group = 'default';
$query_builder = TRUE;

$ip_gcp = array(
    '10.184.15.219',/* staging */
    '10.184.0.25',/* ws1 */
    '10.184.0.58',/* ws2 */
    '10.184.0.59',/* ws3 */
    '10.184.0.60' /* ws3 */
);

$ipdb = __IP_SLAVE;
$username = 'webusers';
$password = 's1nd0n3ws';

$port = 6033;
if(isset($_SERVER['SERVER_ADDR']) && in_array($_SERVER['SERVER_ADDR'],$ip_gcp)){
    $ipdb_arsip = '10.11.8.22';
}elseif(isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] == '10.1.7.175'){
    $ipdb_arsip = '10.1.8.67';
    $port = 3306;
}else{
    $ipdb_arsip = '10.1.8.17';
}

$db['default'] = array(
    'dsn' => '',
    'hostname' => $ipdb,
    'username' => $username,
    'password' => $password,
    'port' => $port,
    'database' => 'allcms',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['gensindo'] = array(
    'dsn' => '',
    'hostname' => $ipdb_arsip,
    'username' => $username,
    'password' => $password,
    'port' => $port,
    'database' => 'gensindo',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);

$db['salsabila'] = array(
    'dsn' => '',
    'hostname' => $ipdb,
    'username' => $username,
    'password' => $password,
    'port' => $port,
    'database' => 'salsabila',
    'dbdriver' => 'mysqli',
    'dbprefix' => '',
    'pconnect' => FALSE,
    'db_debug' => (ENVIRONMENT !== 'production'),
    'cache_on' => FALSE,
    'cachedir' => '',
    'char_set' => 'utf8',
    'dbcollat' => 'utf8_general_ci',
    'swap_pre' => '',
    'encrypt' => FALSE,
    'compress' => FALSE,
    'stricton' => FALSE,
    'failover' => array(),
    'save_queries' => TRUE
);
