<?php

echo '
    <nav>
        <div class="container">
            <section class="slicknav">
                <a href="#" data-target="slide-out" class="sidenav-trigger"><i class="fa fa-bars" aria-hidden="true"></i></a>
            </section>
            <div class="logo-header">
                <a href="' . base_url() . '" class="logo"><img width="220" height="32" src="https://sd.sindonews.net/dyn/220/gensindo/2019/images/logo.png" alt="GEN SINDO"></a>
            </div>
        </div>
    </nav>';
