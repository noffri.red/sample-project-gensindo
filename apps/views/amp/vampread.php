<!DOCTYPE html>
<html amp lang="id-ID">
    <head>
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
        <meta name="theme-color" content="#002445">

        <link rel="shortcut icon" href="<?php echo m_template_uri(); ?>/images/icon/favicon.ico">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-32x32.png" sizes="32x32">
        
        <?php echo $html['metaname']; ?>
        <?php echo $html['canonical']; ?>
        
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        
        <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <?php echo $html['mcss']; ?>
        
        <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "NewsArticle",
            "mainEntityOfPage": {
                "@type": "WebPage",
                "@id": "<?php echo $content['site_url']; ?>"
            },
            "headline": "<?php echo addcslashes($content['site_title'],'"\\/'); ?>",
            "image": {
                "@type": "ImageObject",
                "url": "<?php echo $content['site_image']; ?>",
                "height": 413,
                "width": 620
            },
            "datePublished": "<?php echo $content['site_publish']; ?>",
            "dateModified": "<?php echo $content['site_publish']; ?>",
            "author": {
                "@type": "Person",
                "name": "<?php echo $content['site_reporter']; ?>"
            },
            "publisher": {
                "@type": "Organization",
                "name": "SINDOnews.com",
                "logo": {
                    "@type": "ImageObject",
                    "url": "https://sm.sindonews.net/mobile/2016/images/sindonews-480.png",
                    "width": 600,
                    "height": 55
                }
            },
            "description": "<?php echo addcslashes($content['site_summary'],'"\\/'); ?>"
        }
        </script>
        <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "<?php echo base_url(); ?>",
                    "name": "home"
                }
            }, {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "<?php echo $content['site_url']; ?>",
                    "name": "<?php echo $content['site_label']; ?>"
                }
            }]
        }
    </script>
        <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebPage",
            "headline": "<?php echo addcslashes($content['site_title'],'"\\/'); ?>",
            "url": "<?php echo $content['site_url']; ?>",
            "datePublished": "<?php echo $content['site_publish']; ?>",
            "image": {
                "@type": "ImageObject",
                "url": "<?php echo $content['site_image']; ?>",
                "height": 413,
                "width": 620
            },
            "description": "<?php echo addcslashes($content['site_summary'],'"\\/'); ?>"
	}
	</script>        
        
        <script async custom-element="amp-font" src="https://cdn.ampproject.org/v0/amp-font-0.1.js"></script>
        <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        <script async custom-element="amp-ad" src="https://cdn.ampproject.org/v0/amp-ad-0.1.js"></script>
        <script async custom-element="amp-fx-flying-carpet" src="https://cdn.ampproject.org/v0/amp-fx-flying-carpet-0.1.js"></script>
        <script async custom-element="amp-sticky-ad" src="https://cdn.ampproject.org/v0/amp-sticky-ad-1.0.js"></script>
        
        <script async custom-element="amp-web-push" src="https://cdn.ampproject.org/v0/amp-web-push-0.1.js"></script>
        <script async custom-element="amp-addthis" src="https://cdn.ampproject.org/v0/amp-addthis-0.1.js"></script>
        <script async custom-element="amp-video-iframe" src="https://cdn.ampproject.org/v0/amp-video-iframe-0.1.js"></script>
        
        <script async src="https://cdn.ampproject.org/v0.js"></script>
    </head>

    <body>
        <amp-analytics config="https://www.googletagmanager.com/amp.json?id=GTM-P5LP5R2&gtm.url=SOURCE_URL" data-credentials="include">
            <script type="application/json">
                {
                    "vars":{
                        "kanal": "GenSINDO",
                        "subkanal": "<?php echo $content['cd_subkanal']; ?>",
                        "tags": "<?php echo addslashes(trim($content['cd_tags'])); ?>",
                        "author": "<?php echo addslashes($content['cd_author']); ?>",
                        "editor": "<?php echo addslashes($content['cd_editor']); ?>",
                        "publish_date": "<?php echo $content['date_publish']; ?>",
                        "publish_time": "<?php echo $content['time_publish']; ?>",
                        "publish_year": "<?php echo date('Y',strtotime($content['cd_publish'])); ?>",
                        "publish_month": "<?php echo date('m',strtotime($content['cd_publish'])); ?>",
                        "publish_day": "<?php echo date('d',strtotime($content['cd_publish'])); ?>",
                        "article_id": "<?php echo $content['content_id']; ?>",
                        "article_title": "<?php echo $content['cd_title']; ?>",
                        "content_type": "artikel",
                        "page_type": "article_page",
                        "data_source": "AMP",
                        "pagination": "page <?php echo $content['cd_page']; ?>"
                    }
                }
            </script>
        </amp-analytics>
    
        <amp-analytics type="comscore">
            <script type="application/json">
                {
                    "vars": {
                        "c2": "9013027"
                    },
                    "extraUrlParams": {
                        "comscorekw": "amp"
                    }
                }
            </script>
        </amp-analytics>
    
        <amp-analytics type="alexametrics">
            <script type="application/json">
                {"vars":{"atrk_acct":"p03zj1aEsk00MA","domain":"sindonews.com"}}
            </script>
        </amp-analytics>
    
        <amp-sidebar id="sidebar-left" layout="nodisplay" side="left">
            <ul class="nav-side">
                <li><a href="https://www.sindonews.com/">Home</a></li>
                <li><a href="https://nasional.sindonews.com/">Nasional</a></li>
                <li><a href="https://metro.sindonews.com/">Metronews</a></li>
                <li><a href="https://daerah.sindonews.com/">Daerah</a></li>
                <li><a href="https://ekbis.sindonews.com/">Ekonomi Bisnis</a></li>
                <li><a href="https://international.sindonews.com/">International</a></li>
                <li><a href="https://sports.sindonews.com/">Sports</a></li>
                <li><a href="https://autotekno.sindonews.com/">Autotekno</a></li>
                <li><a href="https://lifestyle.sindonews.com/">Lifestyle</a></li>
                <li><a href="https://kalam.sindonews.com/">Kalam</a></li>
                <li><a href="https://photo.sindonews.com/">Photo</a></li>
                <li><a href="https://video.sindonews.com/">Video</a></li>
                <li><a href="https://infografis.sindonews.com/">Infografis</a></li>
                <li><a href="https://gensindo.sindonews.com/">GenSINDO</a></li>
            </ul>
        </amp-sidebar>
        
        <header>
            <div class="btn-menu">
                <button on="tap:sidebar-left.toggle" class="ampstart-btn caps m2">
                    <amp-img width="24" height="24" src="<?php echo m_template_uri(); ?>/images/menu_ic.png"></amp-img>
                </button>
            </div>
            <div class="logo-box">
                <div class="header-logo-pict">
                    <a href="https://www.sindonews.com/">
                        <amp-img src="<?php echo m_template_uri(); ?>/images/sindonews-logo-2020-200-v2.png" width="200" height="25" alt="logo sindonews"></amp-img>
                    </a>
                </div>
                <div class="text">#BukanBeritaBiasa</div>
            </div>
            <div class="btn-search">
                <a href="https://search.sindonews.com/" target="_blank">
                    <amp-img width="24" height="24" src="<?php echo m_template_uri(); ?>/images/search_ic.png"></amp-img>
                </a>
            </div>
        </header>

        <article>
            <?php echo $content['details']; ?>
        </article>

        <section>
            <div class="notif-box">
                <amp-web-push-widget visibility="unsubscribed" layout="fixed" width="300" height="64">
                    <button class="subscribe" on="tap:amp-web-push.subscribe">
                        <amp-img class="subscribe-icon" width="24" height="24" layout="fixed" src="data:image/svg+xml;base64,PHN2ZyBjbGFzcz0ic3Vic2NyaWJlLWljb24iIHdpZHRoPSIyNCIgaGVpZ2h0PSIyNCIgdmlld0JveD0iMCAwIDI0IDI0IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0xMS44NCAxOS44ODdIMS4yMnMtLjk0Ny0uMDk0LS45NDctLjk5NWMwLS45LjgwNi0uOTQ4LjgwNi0uOTQ4czMuMTctMS41MTcgMy4xNy0yLjYwOGMwLTEuMDktLjUyLTEuODUtLjUyLTYuMzA1czIuODUtNy44NyA2LjI2LTcuODdjMCAwIC40NzMtMS4xMzQgMS44NS0xLjEzNCAxLjMyNSAwIDEuOCAxLjEzNyAxLjggMS4xMzcgMy40MTMgMCA2LjI2IDMuNDE4IDYuMjYgNy44NyAwIDQuNDYtLjQ3NyA1LjIyLS40NzcgNi4zMSAwIDEuMDkgMy4xNzYgMi42MDcgMy4xNzYgMi42MDdzLjgxLjA0Ni44MS45NDdjMCAuODUzLS45OTYuOTk1LS45OTYuOTk1SDExLjg0ek04IDIwLjk3N2g3LjExcy0uNDkgMi45ODctMy41MyAyLjk4N1M4IDIwLjk3OCA4IDIwLjk3OHoiIGZpbGw9IiNGRkYiLz48L3N2Zz4="></amp-img>
                        Subscribe Sekarang Juga! Dapatkan update berita SINDOnews
                    </button>
                </amp-web-push-widget>
            </div>
            <?php if($content['totalvideoframe'] > 0): ?>
            <div class="video-box">
                <div class="video-head">Saksikan juga video pilihan berikut :</div>
                <div class="video-frame">
                    <amp-video-iframe id="myVideo" src="<?php echo $content['frame_url']; ?>" width="640" height="360" layout="responsive" autoplay>
                        <amp-img placeholder src="<?php echo $content['video_url']; ?>" layout="fill"></amp-img>
                    </amp-video-iframe>
                </div>
                <div class="video-title"><a target="_blank" href="<?php echo $content['video_url']; ?>"><?php echo $content['frame_title']; ?></a></div>
            </div>
            <?php endif; ?>
            <?php echo $content['related']; ?>
            <div class="p16">
                <amp-embed type="dable" 
                    data-widget-id="Pl1qvelE" 
                    data-service-name="sindonews.com" 
                    data-item-id="<?php echo $content['content_id'] . $content['channel_id']; ?>" 
                    height="600">
                </amp-embed>
            </div>
            <?php echo $content['populer']; ?>
            <?php echo $content['others']; ?>
        </section>

        <footer>
            <div class="footer-nav"> 
                <ul>
                    <li><a href="https://www.sindonews.com/">Home</a></li>
                    <li><a href="https://nasional.sindonews.com/">Nasional</a></li>
                    <li><a href="https://metro.sindonews.com/">Metronews</a></li>
                    <li><a href="https://daerah.sindonews.com/">Daerah</a></li>
                    <li><a href="https://ekbis.sindonews.com/">Ekonomi Bisnis</a></li>
                    <li><a href="https://international.sindonews.com/">Autotekno</a></li>
                    <li><a href="https://sports.sindonews.com/">Sports</a></li>
                    <li><a href="https://autotekno.sindonews.com/">Autotekno</a></li>
                    <li><a href="https://lifestyle.sindonews.com/">Lifestyle</a></li>
                    <li><a href="https://kalam.sindonews.com/">Kalam</a></li>
                    <li><a href="https://photo.sindonews.com/">Photo</a></li>
                    <li><a href="https://video.sindonews.com/">Video</a></li>
                    <li><a href="https://infografis.sindonews.com/">Infografis</a></li>
                    <li><a href="https://gensindo.sindonews.com/">GenSINDO</a></li>
                </ul>
            </div>
            <div class="box-text">
                <p>Copyright &copy; <?php echo date('Y'); ?> SINDOnews.com, All Rights Reserved</p>
                <p class="rendering"><?php echo $this->uri->segment(1); ?>/ rendering in {elapsed_time} seconds (<?php echo infoServer(); ?>)</p>
            </div>
        </footer>
    
        <amp-web-push
            id="amp-web-push" 
            layout="nodisplay" 
            helper-iframe-url="<?php echo base_url(); ?>amp-helper-frame.html?appId=<?php echo $this->config->item('onesignal_appid'); ?>" 
            permission-dialog-url="<?php echo base_url(); ?>amp-permission-dialog.html?appId=<?php echo $this->config->item('onesignal_appid'); ?>" 
            service-worker-url="<?php echo base_url(); ?>OneSignalSDKWorker.js?appId=<?php echo $this->config->item('onesignal_appid'); ?>">
        </amp-web-push>
    
        <amp-sticky-ad layout="nodisplay">
            <amp-ad width="320"
                height="50"
                type="doubleclick"
                data-slot="<?php echo $this->config->item('amp_gam_id'); ?>" 
                json='{"targeting":{"pos":["bottom_sticky"]}}'>
            </amp-ad>
        </amp-sticky-ad>
    
        <amp-font
            layout="nodisplay"
            font-family="FontAwesome"
            timeout="30000">
        </amp-font>
    </body>
</html>