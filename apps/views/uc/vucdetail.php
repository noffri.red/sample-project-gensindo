<?php

$datepublish = parseDateTime((string) $details['date_published']);
$date_published = $datepublish['day_ind_name'] . ", " . $datepublish['day'] . " " . $datepublish['month_ind_name'] . " " . $datepublish['year'];
$date_published .= ' - ' . $datepublish['hour'] . ":" . $datepublish['minute'] . " WIB";
$publish = date('d M. Y',strtotime($details['date_published'])) . '&nbsp;&nbsp;' . date('H:i',strtotime($details['date_published']));

echo '
    <header>
        <h1 class="w-article-title">' . $details['title'] . '</h1>
        <div class="w-article-meta">
            <span class="w-article-source">SINDOnews</span>
            <span class="w-article-time">' . $publish . '</span>
        </div>
    </header>';

echo '<article>';
echo '<div class="w-article-summary">' . cleanWords($details['summary']) . '</div>';
echo '<div class="w-article-text">';
if($details['photo'] != ''){
    $img = $this->config->item('images_uri') . '/dyn/620/pena/news/' . date('Y/m/d',strtotime($details['date_created'])) . '/' . $details['id_subkanal'] . '/' . $details['id_news'] . '/' . $details['photo'];
    if($details['caption_photo'] != ''){
        $caption = cleanWords($details['caption_photo']);
    }else{
        $caption = cleanWords($details['title']);
    }

    echo '<res-image data-src="' . $img . '" data-width="620" data-height="413" data-title="' . $caption . '"></res-image>';
}

if(!empty($details['lokasi'])){
    $location = strtoupper($details['lokasi']) . ' - ';
}else{
    $location = '';
}

$track = '?utm_source=UC&utm_medium=ucnews&utm_campaign=UC%20Partnership';
$showcontent = ucFilter($details['content'],$details['title']);
$input = explode('<br><br>',$showcontent);
$output = '<p>' . $location . trim(implode('</p><p>',$input)) . '</p>';

echo $output;

$total_related = count($related);
if($total_related > 0){
    echo '<p><strong>Rekomendasi:</strong></p>';
    for($i = 0; $i < $total_related; $i++){
        $dtime[$i] = strtotime($related[$i]['date_created']);
        $urlrelated[$i] = site_url('read') . '/' . $related[$i]['id_news'] . '/' . $related[$i]['id_subkanal'] . '/' . slug($related[$i]['title']) . '-' . $dtime[$i];
        $urlrelated[$i] .= '?utm_source=uc&utm_medium=aggregator&utm_campaign=content_aggregator';
        echo '<p>- <a href="' . $urlrelated[$i] . '">' . cleanTitleArticle($related[$i]['title']) . '</a>';
    }
}

if($totalvideoframe > 0){
    echo '
        <p><strong>Video pilihan: <a href="' . $video_url . '">' . $frame_title . '</a></strong></p>
        <res-iframe type="video"
            data-src="' . $frame_url . '"
            data-title="' . $frame_title . '" 
            data-poster="' . $video_images . '" 
            data-width="640"
            data-height="360">
        </res-iframe>';
}

echo '</div>';

echo '
    <div class="w-article-original">
        <a href="' . $site_url . '?utm_source=uc&utm_medium=aggregator&utm_campaign=content_aggregator">READ SOURCE</a>
    </div>';

echo '</article>';
