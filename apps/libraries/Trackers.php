<?php

class Trackers {

    var $CI;

    function __construct(){
        $this->CI = & get_instance();
    }
    
    function topicTracker($topic_name,$url_topic,$urlgatopic){
        $titlegatopic = 'GEN SINDO | Kumpulan Berita ' . ucwords($topic_name) . ' Terbaru ';
        $gatopic = "$.ajax({url:'" . $url_topic . "',method:'GET',success:function(){ga('send',{'hitType':'pageview','page':'" . $urlgatopic . "','title':'" . $titlegatopic . "'});}});";
        return $gatopic;
    }

    function relatedTrack($id_content,$topic_id_list,$howmuch){
        $garelated = '';
        if($topic_id_list != ''){
            $nrelated = $this->CI->mredisnews->getRelatedNewsByTopic($id_content,$topic_id_list,$howmuch);
            $nrelated = $nrelated['results'];
            if(count($nrelated) > 0){
                for($r = 0; $r < count($nrelated); $r++){
                    $rel_time[$r] = strtotime($nrelated[$r]['date_created']);
                    $rel_url[$r] = '/read/' . $nrelated[$r]['id_content'] . '/' . $nrelated[$r]['id_subkanal'] . '/' . $nrelated[$r]['slug_title'] . '-' . $rel_time[$r];
                    $rel_siteurl[$r] = site_url('read') . '/' . $nrelated[$r]['id_content'] . '/' . $nrelated[$r]['id_subkanal'] . '/' . $nrelated[$r]['slug_title'] . '-' . $rel_time[$r];
                    $rel_title[$r] = cleanTrackTitle($nrelated[$r]['title']);
                    if($nrelated[$r]['id_content'] != $id_content){
                        $garelated .= "$.ajax({url:'" . $rel_siteurl[$r] . "',method:'GET',success:function(){ga('send',{'hitType':'pageview','page':'" . $rel_url[$r] . "','title':'" . $rel_title[$r] . "'});}});";
                    }
                }
            }
        }

        return $garelated;
    }

    function latestTracker($id_content, $howmuch){
        $newarticle = $this->CI->mredisnews->getListBreakingNews($howmuch);
        $newarticle = $newarticle['results'];
        $gaother = "";
        for($z = 0; $z < count($newarticle); $z++){
            $ntime[$z] = strtotime($newarticle[$z]['date_created']);
            $nurl[$z] = '/read/' . $newarticle[$z]['id_content'] . '/' . $newarticle[$z]['id_subkanal'] . '/' . $newarticle[$z]['slug_title'] . '-' . $ntime[$z];
            $nsiteurl[$z] = site_url('read') . '/' . $newarticle[$z]['id_content'] . '/' . $newarticle[$z]['id_subkanal'] . '/' . $newarticle[$z]['slug_title'] . '-' . $ntime[$z];
            $ntitle[$z] = cleanTrackTitle($newarticle[$z]['title']);
            if($newarticle[$z]['id_content'] != $id_content){
                $gaother .= "$.ajax({url:'" . $nsiteurl[$z] . "',method:'GET',success:function(){ga('send',{'hitType':'pageview','page':'" . $nurl[$z] . "','title':'" . $ntitle[$z] . "'});}});";
            }
        }

        return $gaother;
    }

    function articleFrameTracker($howmuch){
        $newarticle = $this->CI->mredisnews->getListBreakingNews($howmuch);
        $newarticle = $newarticle['results'];
        $urlframe = array();
        for($z = 0; $z < count($newarticle); $z++){
            $ntime[$z] = strtotime($newarticle[$z]['date_created']);
            $nsiteurl[$z] = site_url('readsimple') . '/' . $newarticle[$z]['id_content'] . '/' . $newarticle[$z]['id_subkanal'] . '/' . $newarticle[$z]['slug_title'] . '-' . $ntime[$z];
            $urlframe[$z] = $nsiteurl[$z];
        }
        $datapost = base64url_encode(json_encode($urlframe));
        $gaother = "$.ajax({url:'" . site_url('simple') . "',method:'POST',data:{fu:'" . $datapost . "'},success:function(result){\$('.sindoframe').html(result);}});";

        return $gaother;
    }
}
