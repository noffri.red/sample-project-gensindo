
$(document).ready(function () {
  /*-- Fixed Sidebar --*/
  // $(window).scroll(function() {
  //   if ($(this).scrollTop() > 1652){ 
  //       $('#sidebar').addClass("fixed");
  //   }
  //   else{
  //       $('#sidebar').removeClass("fixed");
  //   }
  // });
  /*-- To Top Button --*/
  $('.to-top').click(function () {
    window.scrollTo({top: 0, behavior: 'smooth'});
  });
  $(window).scroll(function() {
      if ($(this).scrollTop() > 600){  
          $('.to-top').fadeIn();
      }
      else{
          $('.to-top').fadeOut();
      }
  });
});