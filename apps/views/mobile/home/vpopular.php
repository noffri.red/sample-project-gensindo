<?php
echo '<div id="tab2" class="tabs_item">';
$total = count($popular);
if($total > 0){
    $data = $popular;


    $i = 0;
    if(!empty($data[$i]['id_content'])){
        $id_content[$i] = $data[$i]['id_content'];
        $img[$i] = images_uri().'/dyn/360/gensindo/content/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$data[$i]['id_subkanal'].'/'.$id_content[$i].'/'.$data[$i]['images'];
        $subakanal[$i] = $data[$i]['subkanal'];
		$slug_subkanal[$i] = $data[$i]['slug_subkanal'];
        $dtime[$i] = strtotime($data[$i]['date_created']);
        $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);
    }
    if(!empty($data[$i]['id_imaji'])){
        $id_content[$i] = $data[$i]['id_imaji'];
        $img[$i] = images_uri().'/dyn/360/gensindo/imaji/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$id_content[$i].'/'.$data[$i]['images'];
        $subakanal[$i] = 'Gen View';
		$slug_subkanal[$i] = 'imaji';
        $dtime[$i] = strtotime($data[$i]['date_created']);
        $link[$i] = site_url('view/' . $id_content[$i] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);
    }

    $title[$i] = cleanWords($data[$i]['title']);
    $summary[$i] = cleanWords($data[$i]['summary']);
    $ago[$i] = time_difference($data[$i]['date_published']);

    echo '<div class="article-news">
                                <div class="category-links '. strtolower($slug_subkanal[$i]) .'"><a href="'. base_url().$slug_subkanal[$i] .'" title="'. $subakanal[$i] .'">'. $subakanal[$i] .'</a></div>
                                <div class="inner">
                                    <div class="title"><a href="'. $link[$i] .'">'. $title[$i] .'</a></div>
                                    <time>'. $ago[$i] .'</time>
                                    <div class="caption">'. $summary[$i] .'</div>
                                    <div class="img"><a href="'. $link[$i] .'" title="'. $title[$i] .'"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></a></div>
                                </div>
                            </div>';

    echo '<section class="article">';

    for($i=1; $i<$total; $i++){
        if(!empty($data[$i]['id_content'])){
            $id_content[$i] = $data[$i]['id_content'];
            $img[$i] = images_uri().'/dyn/360/gensindo/content/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$data[$i]['id_subkanal'].'/'.$id_content[$i].'/'.$data[$i]['images'];
            $subakanal[$i] = $data[$i]['subkanal'];
			$slug_subkanal[$i] = $data[$i]['slug_subkanal'];
            $dtime[$i] = strtotime($data[$i]['date_created']);
            $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);
        }
        if(!empty($data[$i]['id_imaji'])){
            $id_content[$i] = $data[$i]['id_imaji'];
            $img[$i] = images_uri().'/dyn/360/gensindo/imaji/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$id_content[$i].'/'.$data[$i]['images'];
            $subakanal[$i] = 'Gen View';
			$slug_subkanal[$i] = 'imaji';
            $dtime[$i] = strtotime($data[$i]['date_created']);
            $link[$i] = site_url('view/' . $id_content[$i] . '/' . strtolower($data[$i]['slug_title']) . '-' . $dtime[$i]);
        }

        $title[$i] = cleanWords($data[$i]['title']);
        $summary[$i] = cleanWords($data[$i]['summary']);
        $ago[$i] = time_difference($data[$i]['date_published']);

        echo '<div class="article-news">
                                <div class="category-links '. strtolower($slug_subkanal[$i]) .'"><a href="'. base_url().$slug_subkanal[$i] .'" title="'. $subakanal[$i] .'">'. $subakanal[$i] .'</a></div>
                                <div class="inner">
                                    <div class="title"><a href="'. $link[$i] .'" title="'. $title[$i] .'">'. $title[$i] .'</a></div>
                                    <time>'. $ago[$i] .'</time>
                                    <div class="caption">'. $summary[$i] .'</div>
                                    <div class="img"><a href="'. $link[$i] .'" title="'. $title[$i] .'"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></a></div>
                                </div>
                            </div>';
    }

    echo '</section>';

    // echo '<a href="#" class="btn-more">View More</a>';
}
echo '</div>';