<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Loadmore extends CI_Controller {

    function __construct(){
        parent::__construct();
        /*
        $this->load->helpers("gen");
        $this->load->library('pagination');
        $this->load->library('article');
        $this->load->config('site_channel');
        $this->load->model('news/mredisnews','mredisnews');
         * 
         */
    }

    function index(){
        redirect(base_url(),'location',301);
        $a = array();

        if($this->uri->segment(2,0) == 0 || $this->uri->segment(2,0) == ''){
            $id_subkanal = "0";
        }else{
            $id_subkanal = $this->uri->segment(2,0);
        }

        $site_channel = $this->config->item('site_channel');

        $urlsite = $site_channel[$id_subkanal] . '/';
        $urlgo = '';
        if($urlsite != base_url()){
            echo 'kena';
            exit;
            $urlgo = base_url();
            redirect($urlgo,'location',301);
        }

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= add_external_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-1.10.2.min.js');
        $a['html']['js'] .= add_js('/js/materialize.min.js');

        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['promoted'] = $this->load->view('template/vpromoted',NULL,TRUE);
        $a['template']['kategori'] = $this->load->view('template/vkategori',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('template/vminifooter',NULL,TRUE);
        $a['template']['promogen'] = $this->load->view('template/vpromogen',NULL,TRUE);

        $t['event'] = $this->mredisnews->getEvent();
        $a['template']['agenda'] = $this->load->view('template/vagenda',$t,TRUE);

        $t['trending'] = $this->mredisnews->getKanalTrendingTopic();
        $a['template']['trending'] = $this->load->view('template/vtrending',$t,TRUE);

        $offset = 13;
        $start = $this->uri->segment(3,0);

        $t['per_page'] = $offset;
        if($id_subkanal == '0'){
            $t['breaking'] = $this->mredisnews->getListBreakingNews($offset,$start,TRUE);
        }else{
            $t['breaking'] = $this->mredisnews->getListBreakingNewsBySubkanal($id_subkanal,$offset,$start,TRUE);
        }
        if(empty($t['breaking']['results'])){
            redirect(base_url(),'location',301);
        }

        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('paging');
        $page_uri = 'loadmore/' . $id_subkanal;
        $config['base_url'] = site_url($page_uri);
        $config['total_rows'] = $t['breaking']['total_results'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);

        $t['pagination'] = $this->pagination->create_links();
        $a['content']['breadcrumb'] = $this->load->view('more/vbreadcrumb',$t,TRUE);
        $a['content']['breaking'] = $this->load->view('more/vbreaking',$t,TRUE);

        $strMetaDesc = '';
        if(count($t['breaking']['results']) > 5){
            $loop = "5";
        }else{
            $loop = count($t['breaking']['results']);
        }
        for($i = 0; $i < $loop; $i++){
            if($i == ($loop - 1)){
                $strMetaDesc .= cleanHTML($t['breaking']['results'][$i]['title']);
            }else{
                $strMetaDesc .= cleanHTML($t['breaking']['results'][$i]['title']) . '. ';
            }
            $title_subkanal = ucwords($t['breaking']['results'][0]['subkanal']);
        }
        if($id_subkanal == '0'){
            $title_subkanal = "";
        }

        $currentPage = floor(($start / $config['per_page']) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;
        if($start == 0 || $start == ''){
            $pagetitle = '';
        }

        $a['html']['title'] = 'Berita ' . ucwords($title_subkanal) . ' Terkini - SINDOnews' . $pagetitle;
        $metaname = array(
            'title' => 'Berita ' . ucwords($title_subkanal) . ' Terkini - SINDOnews' . $pagetitle,
            'description' => 'Berita ' . ucwords($title_subkanal) . ' Terkini - SINDOnews' . $pagetitle . ' - ' . climiter($strMetaDesc,140),
            'alexaVerifyID' => $this->config->item('alexaVerifyID'),
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );
        $a['html']['metaname'] = metaname($metaname);

        $url_current = site_url('loadmore') . '/' . $id_subkanal;
        $a['html']['canonical'] = $this->article->pagingCanonical($url_current,$start,$offset,$t['breaking']['total_results']);

        $this->load->view('pages/vmore',$a,FALSE);
    }

}
