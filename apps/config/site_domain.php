<?php

$dev = 'gensindo.sindonews.com';
if ($_SERVER['SERVER_ADDR'] == '127.0.0.1' && $_SERVER['SERVER_NAME'] == 'gensindo-staging.com') {
    $dev =  $_SERVER['SERVER_NAME'];
}elseif($_SERVER['SERVER_ADDR'] == '10.1.7.175'){
    $dev = $_SERVER['SERVER_NAME'];
}


$config['read_domain'] = array(
    /* Piala Eropa */
    1 => 'sports.sindonews.com',
    2 => 'sports.sindonews.com',
    3 => 'sports.sindonews.com',
    4 => 'sports.sindonews.com',
    200 => 'sports.sindonews.com',
    201 => 'sports.sindonews.com',
    702 => 'sports.sindonews.com',
    705 => 'sports.sindonews.com',
    708 => 'sports.sindonews.com',
    718 => 'sports.sindonews.com',
    721 => 'sports.sindonews.com',
    
    /* Nasional */
    5 => 'nasional.sindonews.com',
    12 => 'nasional.sindonews.com',
    13 => 'nasional.sindonews.com',
    14 => 'nasional.sindonews.com',
    15 => 'nasional.sindonews.com',    
    17 => 'nasional.sindonews.com',    
    19 => 'nasional.sindonews.com',
    20 => 'nasional.sindonews.com',
    94 => 'nasional.sindonews.com',
    168 => 'nasional.sindonews.com',
    86 => 'nasional.sindonews.com', /* Kaleidoskop Index */
    105 => 'nasional.sindonews.com', /* Outlook */
    
    /* Metronews */
    6 => 'metro.sindonews.com',
    31 => 'metro.sindonews.com',
    63 => 'metro.sindonews.com',
    145 => 'metro.sindonews.com',
    170 => 'metro.sindonews.com',
    171 => 'metro.sindonews.com',
    172 => 'metro.sindonews.com',
    173 => 'metro.sindonews.com',
    88 => 'metro.sindonews.com', /* Kaleidoskop */
    106 => 'metro.sindonews.com', /* Outlook */
    
    /* Daerah */
    7 => 'daerah.sindonews.com',
    21 => 'daerah.sindonews.com',
    22 => 'daerah.sindonews.com',
    23 => 'daerah.sindonews.com',
    24 => 'daerah.sindonews.com',
    25 => 'daerah.sindonews.com',
    26 => 'daerah.sindonews.com',
    27 => 'daerah.sindonews.com',
    28 => 'daerah.sindonews.com',
    29 => 'daerah.sindonews.com',
    30 => 'daerah.sindonews.com',
    97 => 'daerah.sindonews.com',
    174 => 'daerah.sindonews.com',
    175 => 'daerah.sindonews.com',
    176 => 'daerah.sindonews.com',
    189 => 'daerah.sindonews.com',
    190 => 'daerah.sindonews.com',
    191 => 'daerah.sindonews.com',
    192 => 'daerah.sindonews.com',
    193 => 'daerah.sindonews.com',
    194 => 'daerah.sindonews.com',
    89 => 'daerah.sindonews.com', /* Kaleidoskop */
    107 => 'daerah.sindonews.com', /* Outlook */   
    203 => 'daerah.sindonews.com',
    701 => 'daerah.sindonews.com',
    704 => 'daerah.sindonews.com',
    707 => 'daerah.sindonews.com',
    717 => 'daerah.sindonews.com',
    720 => 'daerah.sindonews.com',
    
    /* Ekonomi & Bisnis */
    8 => 'ekbis.sindonews.com',
    32 => 'ekbis.sindonews.com',
    33 => 'ekbis.sindonews.com',
    34 => 'ekbis.sindonews.com',
    35 => 'ekbis.sindonews.com',
    36 => 'ekbis.sindonews.com',
    37 => 'ekbis.sindonews.com',
    38 => 'ekbis.sindonews.com',
    39 => 'ekbis.sindonews.com',
    77 => 'ekbis.sindonews.com',
    178 => 'ekbis.sindonews.com',
    179 => 'ekbis.sindonews.com',
    180 => 'ekbis.sindonews.com',
    181 => 'ekbis.sindonews.com',
    182 => 'ekbis.sindonews.com',
    90 => 'ekbis.sindonews.com', /* Kaleidoskop */
    109 => 'ekbis.sindonews.com', /* Outlook */
    209 => 'ekbis.sindonews.com',
    
    /* International */
    9 => 'international.sindonews.com',
    40 => 'international.sindonews.com',
    41 => 'international.sindonews.com',
    42 => 'international.sindonews.com',
    43 => 'international.sindonews.com',
    44 => 'international.sindonews.com',
    45 => 'international.sindonews.com',
    46 => 'international.sindonews.com',
    177 => 'international.sindonews.com',
    108 => 'international.sindonews.com', /* Outlook */
    784 => 'international.sindonews.com',
    
    /* Sports */
    10 => 'sports.sindonews.com',
    47 => 'sports.sindonews.com',
    48 => 'sports.sindonews.com',
    49 => 'sports.sindonews.com',
    50 => 'sports.sindonews.com',
    51 => 'sports.sindonews.com',
    52 => 'sports.sindonews.com',
    53 => 'sports.sindonews.com',
    96 => 'sports.sindonews.com',
    110 => 'sports.sindonews.com', /* Outlook */
    
    /* Soccer */
    11 => 'sports.sindonews.com',
    54 => 'sports.sindonews.com',
    55 => 'sports.sindonews.com',
    56 => 'sports.sindonews.com',
    57 => 'sports.sindonews.com',
    58 => 'sports.sindonews.com',
    59 => 'sports.sindonews.com',
    60 => 'sports.sindonews.com',
    61 => 'sports.sindonews.com',
    62 => 'sports.sindonews.com',
    95 => 'sports.sindonews.com',
    188 => 'sports.sindonews.com', 
    111 => 'sports.sindonews.com', /* Outlook */
    
    /* Autotekno */
    119 => 'autotekno.sindonews.com',    
    125 => 'autotekno.sindonews.com',
    126 => 'autotekno.sindonews.com',
    127 => 'autotekno.sindonews.com',
    128 => 'autotekno.sindonews.com',
    129 => 'autotekno.sindonews.com',
    130 => 'autotekno.sindonews.com',
    131 => 'autotekno.sindonews.com',
    143 => 'autotekno.sindonews.com',
    146 => 'autotekno.sindonews.com',
    208 => 'autotekno.sindonews.com',
    
    /* Otomotif */
    611 => 'otomotif.sindonews.com',
    120 => 'otomotif.sindonews.com',
    121 => 'otomotif.sindonews.com',
    183 => 'otomotif.sindonews.com',
    184 => 'otomotif.sindonews.com',
    778 => 'otomotif.sindonews.com',
    
    /* Tekno */
    612 => 'tekno.sindonews.com',
    122 => 'tekno.sindonews.com',
    123 => 'tekno.sindonews.com',
    207 => 'tekno.sindonews.com',
    765 => 'tekno.sindonews.com',
    132 => 'tekno.sindonews.com',
    133 => 'tekno.sindonews.com',
    776 => 'tekno.sindonews.com',
    
    /* Sains */
    613 => 'sains.sindonews.com',
    766 => 'sains.sindonews.com',
    767 => 'sains.sindonews.com',
    768 => 'sains.sindonews.com',
    124 => 'sains.sindonews.com',
    774 => 'sains.sindonews.com',
    
    /* Lifestyle */
    154 => 'lifestyle.sindonews.com',
    155 => 'lifestyle.sindonews.com',
    156 => 'lifestyle.sindonews.com',
    157 => 'lifestyle.sindonews.com',
    158 => 'lifestyle.sindonews.com',
    159 => 'lifestyle.sindonews.com',
    164 => 'lifestyle.sindonews.com',
    165 => 'lifestyle.sindonews.com',
    166 => 'lifestyle.sindonews.com',
    167 => 'lifestyle.sindonews.com',
    185 => 'lifestyle.sindonews.com',
    186 => 'lifestyle.sindonews.com',
    187 => 'lifestyle.sindonews.com',
    703 => 'lifestyle.sindonews.com',
    706 => 'lifestyle.sindonews.com',
    709 => 'lifestyle.sindonews.com',
    719 => 'lifestyle.sindonews.com',
    722 => 'lifestyle.sindonews.com',
    
    /* Ramadan & Idul Fitri */
    67 => 'kalam.sindonews.com',
    68 => 'kalam.sindonews.com',
    69 => 'kalam.sindonews.com',
    70 => 'kalam.sindonews.com',
    71 => 'kalam.sindonews.com',
    72 => 'kalam.sindonews.com',
    73 => 'kalam.sindonews.com',
    91 => 'kalam.sindonews.com',
    92 => 'kalam.sindonews.com',
    93 => 'kalam.sindonews.com',
    147 => 'kalam.sindonews.com',
    786 => 'kalam.sindonews.com',
    
    /* Natal & Tahun Baru */
    78 => 'nasional.sindonews.com',
    79 => 'nasional.sindonews.com',
    80 => 'nasional.sindonews.com',
    81 => 'nasional.sindonews.com',
    82 => 'nasional.sindonews.com',
    83 => 'nasional.sindonews.com',
    84 => 'nasional.sindonews.com',
    
    /* Seagames Myanmar */
    98 => 'sports.sindonews.com',
    99 => 'sports.sindonews.com',
    100 => 'sports.sindonews.com',
    101 => 'sports.sindonews.com',
    102 => 'sports.sindonews.com',
    103 => 'sports.sindonews.com',
    
    /* Pemilu 2014 */
    112 => 'nasional.sindonews.com',
    113 => 'nasional.sindonews.com',
    114 => 'nasional.sindonews.com',
    116 => 'nasional.sindonews.com',
    117 => 'nasional.sindonews.com',
    118 => 'nasional.sindonews.com',
    
    /* Piala Dunia */
    134 => 'sports.sindonews.com',
    135 => 'sports.sindonews.com',
    136 => 'sports.sindonews.com',
    137 => 'sports.sindonews.com',
    138 => 'sports.sindonews.com',
    139 => 'sports.sindonews.com',
    140 => 'sports.sindonews.com',
    141 => 'sports.sindonews.com',
    
    /* Koran Sindo */
    148 => 'nasional.sindonews.com',
    149 => 'nasional.sindonews.com',
    150 => 'ekbis.sindonews.com',
    151 => 'daerah.sindonews.com',
    152 => 'lifestyle.sindonews.com',
    153 => 'sports.sindonews.com',
    160 => 'lifestyle.sindonews.com',
    161 => 'nasional.sindonews.com',
    162 => 'nasional.sindonews.com',
    163 => 'nasional.sindonews.com',
    
    /* English Version */
    195 => 'worldwide.sindonews.com',
    196 => 'worldwide.sindonews.com',
    197 => 'worldwide.sindonews.com',
    198 => 'worldwide.sindonews.com',
    199 => 'worldwide.sindonews.com',
    202 => 'worldwide.sindonews.com',
    205 => 'worldwide.sindonews.com',
    
    /* Suplemen WP */
    16 => 'nasional.sindonews.com',
    18 => 'nasional.sindonews.com',
    65 => 'nasional.sindonews.com',
    66 => 'nasional.sindonews.com', 
    74 => 'nasional.sindonews.com',
    75 => 'nasional.sindonews.com',
    76 => 'metro.sindonews.com',
    
    64 => 'nasional.sindonews.com', /* Periskop */
    87 => 'nasional.sindonews.com', /* Kaleidoskop */
    104 => 'nasional.sindonews.com', /* Outlook Index */   
    
    601 => 'daerah.sindonews.com',
    602 => 'daerah.sindonews.com',
    603 => 'daerah.sindonews.com',
    605 => 'daerah.sindonews.com',
    606 => 'daerah.sindonews.com',
    
    604 => 'makassar.sindonews.com',
    710 => 'makassar.sindonews.com',
    711 => 'makassar.sindonews.com',
    712 => 'makassar.sindonews.com',
    713 => 'makassar.sindonews.com',
    714 => 'makassar.sindonews.com',
    715 => 'makassar.sindonews.com',
    716 => 'makassar.sindonews.com',
    
    600 => $dev,
    700 => $dev,
    782 => $dev,

    /* Edukasi */
    144 => 'edukasi.sindonews.com',
    211 => 'edukasi.sindonews.com',
    212 => 'edukasi.sindonews.com',
    213 => 'edukasi.sindonews.com',
    780 => 'edukasi.sindonews.com'
);