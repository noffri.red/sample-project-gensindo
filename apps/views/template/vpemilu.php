<section class="agenda">
	<div class="title-gensindo">Pemilu 2019</div>
	<ul>
		<li class="clearfix">
			<div class="text" style="width:100%;padding:10px 15px 10px 15px;">
				<div class="title">
					Halo teman-teman!
					<br>Yuk kembangkan kemampuan menulismu dengan ikut berpartisipasi menulis tentang "Pemilu Damai" di mata kalian. Tulisan bisa berupa opini, pengalaman, berbagi tips serta lainnya. 
					<br>Tulisan yang menarik akan dimuat di kanal Gensindo 
					<br>Tunggu apalagi, segera kirim tulisan kamu ke : <strong style="font-weight:700;">gensindo.mail@gmail.com.</strong> Kami tunggu hingga 16 April 2019.
				</div>
			</div>
		</li>
	</ul> 
</section>