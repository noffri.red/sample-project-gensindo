<!doctype html>
<html  lang="id-ID">

<head>
  <!-- Meta SEO -->
  <title><?php echo $html['title']; ?></title>
  <meta charset="utf-8">
  <meta http-equiv="x-dns-prefetch-control" content="on">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
  <meta name="theme-color" content="#499AE8">

  <?php echo $html['metaname']; ?>
  <!-- Icon -->
  <link rel="shortcut icon" href="<?php echo m_template_uri(); ?>/image/favicon.ico">
  <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-160x160.png" sizes="160x160">
  <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-192x192.png" sizes="192x192">

  <!-- Font -->
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;900&display=swap" rel="stylesheet"> <!-- Nunito -->
  <!-- CSS -->

  <link rel="canonical" href="<?php echo base_url(); ?>">
  <link rel="alternate" title="SINDOnews | RSS Feed Berita GEN SINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>

  <?php echo $html['css']; ?>
  <?php echo $template['newcss']; ?>
  <!-- Production -->
  <!-- <link rel="stylesheet" href="assets/css/styles.min.css"> -->
  <!-- Style -->
  <!-- Mediaquery -->
  <link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/mediaquery-HDPI.min.css" media="screen and (min-width: 1083px) and (max-width: 1290px)"> <!-- HDPI Screen -->
  <link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/mediaquery-MDPI.min.css" media="screen and (min-width: 993px) and (max-width: 1082px)"> <!-- MDPI Screen -->
  <link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/mediaquery-SDPI.min.css" media="screen and (min-width: 769px) and (max-width: 992px)"> <!-- SDPI Screen -->
  <link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/mediaquery-mobile.min.css" media="screen and (max-width: 768px)"> <!-- Mobile Screen -->

  <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
  <script>
    window.googletag = window.googletag || {
      cmd: []
    };
    
    var wv = navigator.userAgent.toLowerCase();
    var aggr = "0";
    if(wv.includes('topbuzz') || wv.includes('babe') || wv.includes('bacaberita')) {
        aggr = "topbuzz";
    } else if (wv.includes('babe')){
        aggr = "babe";
    } else if (wv.includes('bacaberita')){
        aggr = "babe";
    } else if (wv.includes('ucbrowser')){
        aggr = "ucbrowser";
    } else if (wv.includes('kurio')){
        aggr = "kurio";
    } else {
        aggr = "other";
    }

    var adslot0;

    googletag.cmd.push(function() {
      googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [
        [320, 100],
        [320, 50],
        [1, 1]
      ], 'div-gpt-ad-billboard').setTargeting('pos', ['billboard']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [
        [300, 600],
        [300, 250],
        [1, 1]
      ], 'div-gpt-ad-middle_1').setTargeting('pos', ['middle_1']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [
        [300, 250],
        [1, 1]
      ], 'div-gpt-ad-middle_2').setTargeting('pos', ['middle_2']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [
        [320, 50],
        [1, 1]
      ], 'div-gpt-ad-top-sticky').setTargeting('pos', ['top_sticky']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [
        [320, 50],
        [1, 1]
      ], 'div-gpt-ad-bottom-sticky').setTargeting('pos', ['bottom_sticky']).addService(googletag.pubads());

      googletag.defineOutOfPageSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', 'div-gpt-ad-outstream').setTargeting('pos', ['outstream']).addService(googletag.pubads());
      googletag.defineOutOfPageSlot('/<?php echo $this->config->item('mobile_gam_id'); ?>', 'div-gpt-ad-abm').setTargeting('partner', ['abm']).addService(googletag.pubads());

      adslot0 = googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [
        [300, 250],
        [1, 1]
      ], 'div-gpt-ad-middle_3').setTargeting('pos', ['middle_3']).addService(googletag.pubads());

      googletag.pubads().setTargeting('page_type', ['kanalpage']);
      googletag.pubads().setTargeting('aggregator', aggr);
      googletag.pubads().enableSingleRequest();
      googletag.pubads().collapseEmptyDivs();
      googletag.enableServices();
    });

    function bannerCheck(unitAd) {
      googletag.pubads().addEventListener('slotRenderEnded', function(event) {
        if (event.slot.getSlotElementId() === unitAd) {
          if (event.isEmpty) {
            var eAd = document.getElementById('me-' + unitAd);
            eAd.classList.add('adnone');
          }
        }
      });
    }
  </script>

  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async=""></script>
  <script>
    window.OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
      OneSignal.init({
        appId: '<?php echo $this->config->item('onesignal_appid'); ?>',
        autoResubscribe: false,
        notifyButton: {
          enable: false
        },
        promptOptions: {
          slidedown: {
            enabled: true,
            autoPrompt: true,
            actionMessage: "Dapatkan update berita SINDOnews melalui notifikasi browser Anda.",
            acceptButtonText: "SUBSCRIBE",
            cancelButtonText: "TIDAK"
          }
        }
      });
    });
  </script>

  <?php echo $html['bottom_js']; ?>
</head>

<body>
  <div id="app">
    <!-- Header -->
    <header>
      <div class="container">
        <div id="navigation">
          <!-- Menu -->
          <?php echo $template['header']; ?>
        </div>
        <!-- Logo -->
        <div id="logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo $this->config->item('tpl_uri');  ?>/dyn/220/gensindo/2020-mobile/image/logo.png" alt="Logo Gensindo" width="220" height="33"></a></div>
        <!-- Search -->
        <div id="search">
          <form class="search-field sample two" name="myForm" method="get" action="" onsubmit="return validateForm(this)">
            <input type="text" name="search" id="search-input" placeholder="Cari Berita" autocomplete="off">
            <button type="submit" class="btn btn-search fa fa-search"></button>
            <button onfocus="document.getElementById('search-input').value = ''" type="reset" class="btn btn-reset fa fa-times"></button>
          </form>
        </div>
        <!-- Subkanal Menu -->
        <div id="subkanal-menu">
          <?php echo $template['subkanal']; ?>
        </div>
      </div>
      <!-- Menu Overlay -->
      <div id="menu-overlay">
        <!-- template vmenuverplay.php -->
        <?php echo $template['menuoverlay']; ?>
      </div>
    </header>
    <div class="mask-header">Mask</div>
    <!-- Main -->
    <main>
      <!-- View -->
      <div id="view">
        <div class="container">


          <div class="ads320">
            <div id="div-gpt-ad-billboard">
              <script>
                googletag.cmd.push(function() {
                  googletag.display('div-gpt-ad-billboard');
                });
              </script>
            </div>
          </div>

          <br>
          <!-- Headline -->
          <!-- template vheadline.php -->
          <?php echo $template['headine']; ?>


          <!-- Trending Topic -->
          <!-- template vtrending.php -->
          <?php echo $template['trending']; ?>

          <!-- Arsip -->
          <!-- template vlatest.php -->
          <?php echo $content['latest']; ?>
          
          <div>
            <div id="div-gpt-ad-abm"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-abm');});</script></div>
            <div id="div-gpt-ad-outstream"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-outstream');});</script></div>
        </div>
        </div>
      </div>
      <!-- Sidebar -->
      <div id="sidebar">
      </div>
    </main>
    
    <div class="mask-footer">Mask</div>
    <!-- Footer -->
    <?php echo $template['footer']; ?>

  </div>

  <div class="bottom-block" id="me-div-gpt-ad-bottom-sticky">
    <div class="bottom-float">
      <div class="bottom-genie bottom-extra">
        <div id="div-gpt-ad-bottom-sticky">
          <script>
            googletag.cmd.push(function() {
              googletag.display('div-gpt-ad-bottom-sticky');
            });
          </script>
        </div>
      </div>
    </div>
  </div>

  <!-- Javascript -->
  <?php echo $html['bottom_css']; ?>
  <?php echo $html['js']; ?>

  <script>
    $(function() {

      var imagesLoad = new LazyLoad({
        elements_selector: '.lazyload'
      });

     
      $('.sidenav').sidenav();

      var mycount = 10;
      $('.scroll').jscroll({
        loadingHtml: '<div class="news-load"><img src="<?php echo m_template_uri(); ?>/image/ajax-loader.gif" alt="loading image"></div>',
        nextSelector: '.news-more a',
        autoTrigger: true,
        autoTriggerUntil : 2,
        callback: function() {
          imagesLoad.update();
        
          var slot;
          var slotName = generateNextSlotName();
          var parent = document.getElementById('div-gpt-ad-middle_3-' + mycount);
          var child = document.createElement('div');
          child.setAttribute('id', slotName);
          parent.appendChild(child);

          googletag.cmd.push(function() {
            slot = googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [
              [300, 250],
              [1, 1]
            ], slotName).setTargeting('pos', ['middle_3']).addService(googletag.pubads());
            googletag.display(slotName);
            googletag.pubads().refresh([slot]);
          });

          mycount += 10;
        }
      });


    });

    var nextSlotId = 1;

    function generateNextSlotName() {
      var id = nextSlotId++;
      return 'adslot' + id;
    }

    // search action
    function validateForm() {
      var x = document.forms["myForm"]["search"].value;
      x = x.replace(' ', '+')
      if (x != "") {
        var url = 'https://search.sindonews.com/gokanal?type=artikel&q=' + x + '&pid=600';

        document.location.href = url, true;
        return false;
      }

    }
  </script>


</body>

</html>