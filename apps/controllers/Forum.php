<?php

class Forum extends CI_Controller {

    function __construct(){
        parent::__construct();
    }

    function index(){
        $a = array();
        $a['site_url'] = $this->input->post('site');
        $this->load->view('mobile/read/vreadforum',$a,FALSE);
    }

    function desktopforum(){
        $a = array();
        $a['site_url'] = $this->input->post('site');
        $this->load->view('read/vfbcomment',$a,FALSE);
    }

    function mobileforum(){
        $a = array();
        $a['site_url'] = $this->input->post('site');
        $this->load->view('mobile/read/vfbcomment',$a,FALSE);
    }

    function disqus(){
        $a = array();
        $a['site_url'] = $this->input->post('site');
        $a['site_title'] = $this->input->post('title');
        $this->load->view('read/vdisqus',$a,FALSE);
    }

    function mdisqus(){
        $a = array();
        $a['site_url'] = base64_decode($this->input->get('site'));
        $a['site_title'] = base64_decode($this->input->get('title'));
        $this->load->view('mobile/read/vdisqus',$a,FALSE);
    }

    function simple(){
        $a = array();
        $a['frame_url'] = $this->input->post('fu');
        $this->load->view('template/vsimple',$a,FALSE);
    }
}
