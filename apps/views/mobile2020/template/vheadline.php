<div id="headline">
<div class="head">
    <div class="title">Headline</div>
</div>
<!-- Swiper -->

<?php 
    if(count($headline)>0){
?>
<div class="slider slider-image swiper-container">
    <div class="swiper-wrapper">

    <?php       
        foreach ($headline as $value){
            $dtime = strtotime($value['date_created']);
            $img = images_uri() . '/dyn/541/pena/news/' . $value['created']['year'] . '/' . $value['created']['month'] . '/' . $value['created']['day'] . '/' . $value['channel_id'] . '/' . $value['content_id'] . '/' . $value['image']; 

            $link = site_url('read/' . $value['content_id'] . '/' . $value['channel_id'] . '/' . slug($value['title']) . '-' . $dtime);

            $title = cleanHTML($value['title']);

            ?>
            <div class="swiper-slide">
                <a href="<?php echo $link; ?>"><img class="lazyload" data-src=<?php echo $img; ?> alt="<?php echo $title; ?>" width="541"></a>
                <div class="caption">
                <a href="<?php echo $link; ?>" class="title"><?php echo cleanTitleArticle($value['title']); ?></a>
                </div>
            </div>

        <?php } ?>
    <div class="swiper-button-next swiper-button-white"></div>
    <div class="swiper-button-prev swiper-button-white"></div>
    </div>
</div>
</div>

    <?php } ?>