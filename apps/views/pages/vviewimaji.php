<!DOCTYPE html>
<html lang="id-ID" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<head>
    <title><?php echo $html['title'];?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta name="theme-color" content="#004B8F">
    <?php echo $html['metaname']; ?>

    <link rel="shortcut icon" href="<?php echo template_uri();?>/images/favicon.ico">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-192x192.png" sizes="192x192">

    <meta property="fb:app_id" content="325657497499535">
    <meta property="fb:pages" content="248570208512540"> 
    <?php echo $html['metaname']; ?>
    <?php echo $html['canonical']; ?>

    <link rel="dns-prefetch" href="https://cdn.sindonews.net/">
    <link rel="dns-prefetch" href="https://st-a.sindonews.net/">
	
    <link rel="preconnect" href="https://cdn.sindonews.net/">
    <link rel="preconnect" href="https://st-a.sindonews.net/">
 
    <link rel="amphtml" href="<?php echo $content['amp_url'];?>">
    <link rel="alternate" title="SINDOnews | RSS Feed Berita GEN SINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>

    <?php echo $html['css'];?>
    <?php echo $template['newcss'];?>
    <link rel="stylesheet" href="<?php echo template_uri();?>/css/mediaquery-HDPI.css" media="screen and (min-width: 1083px) and (max-width: 1290px)">
    <link rel="stylesheet" href="<?php echo template_uri();?>/css/mediaquery-MDPI.css" media="screen and (min-width: 993px) and (max-width: 1082px)">
    <link rel="stylesheet" href="<?php echo template_uri();?>/css/mediaquery-SDPI.css" media="screen and (min-width: 769px) and (max-width: 992px)">
    <link rel="stylesheet" href="<?php echo template_uri();?>/css/mediaquery-mobile.css" media="screen and (max-width: 768px)">

    <script src="https://platform-api.sharethis.com/js/sharethis.js#property=5c30039e6aa2aa0011451e11&product=inline-share-buttons" async></script>

    <?php echo $html['bottom_js']; ?>
</head>
<body>
<header>
    <?php echo $template['header'];?>
</header>
<section class="slidenav">
	<ul id="slide-out" class="sidenav">
		<li>
			<div class="user-view">
				<a href="#user"><img class="circle" src="<?php echo template_uri();?>/images/logo.png"></a>
			</div>
		</li>
		<li class="gnews"><a href="<?php echo site_url('gnews');?>">Gen News</a></li>
		<li class="youth"><a href="<?php echo site_url('youth');?>">Gen Shot</a></li>
		<li class="imaji"><a href="<?php echo site_url('imaji');?>">Gen VIew</a></li>
	</ul>
</section>
<main>
    <div class="container clearfix">
        <div class="column-big pull-left clearfix">
            <div class="center pull-right">
                <section class="detail">
                    <?php echo $content['breadcrumb'];?>
                    <div class="border">
                        <?php echo $content['detail'];?>
                        <?php echo $content['relatedtopic'];?>
                        <div class="share">
                            <div class="sharethis-reaction"><div class="sharethis-inline-reaction-buttons"></div></div>
                            <div class="sharethis-box"><div class="sharethis-inline-share-buttons"></div></div>
                        </div>
                        <?php echo $content['profileeditor'];?>
                        <?php echo $content['relatedcontent'];?>
                        <?php echo $content['comment']; ?>
                    </div>
                </section>
            </div>
            <div class="leftbar pull-left">
                <?php echo $template['kategori'];?>
                <?php echo $template['trending'];?>
                <div class="r-side"><img src="<?php echo template_uri();?>/images/r_side_01.jpg" alt="R Side"></div>
            </div>
        </div>
        <div class="rightbar pull-right">
            <div class="r-side"><img src="<?php echo template_uri();?>/images/r_side_02.jpg" alt="R Side"></div>
            <div class="r-side"><img src="<?php echo template_uri();?>/images/r_side_03.jpg" alt="R Side"></div>
            <?php echo $template['agenda'];?>
            <?php echo $template['promoted'];?>
            <?php echo $template['minifooter'];?>
        </div>
    </div>
</main>
<footer></footer>
<?php echo $html['bottom_css']; ?>
<?php echo $html['js'];?>
<script>
    $('.afb').click(function(){
        $(this).addClass('active');
        $('.dfb').removeClass('active');
        $('.fb-comment').show();
        $('.sindodisqus').hide();
    });
    $('.dfb').click(function(){
        $(this).addClass('active');
        $('.afb').removeClass('active');
        $('.sindodisqus').show();
        $('.fb-comment').hide();
    });
    var urlforum = '<?php echo $content['forum_url']; ?>';
    var post_forum = {'site': '<?php echo $content['site_url']; ?>'};
    $.ajax({
        url: urlforum,
        type: 'POST',
        data: post_forum,
        success: function (result) {
            $('#forum').html(result);
            $('.img-loading').hide();
        }
    });
    var urldisqus = '<?php echo $content['disqus_url']; ?>';
    var post_disqus = {'site': '<?php echo $content['site_url']; ?>','title': '<?php echo addslashes($content['site_title']); ?>'};
    $.ajax({
        url: urldisqus,
        type: 'POST',
        data: post_disqus,
        success: function (result) {
            $('.sindodisqus').html(result);
        }
    });
	
	if ( $(window).width() > 770) {
		$(window).scroll(function() {
			if ($(this).scrollTop() > 1495){
				$('.leftbar').addClass("sticky");
				$(".leftbar").animate({top: "30px"}, 0);
				$('.leftbar .r-side').addClass("none");
			}
			else{
				$('.leftbar').removeClass("sticky");
				$('.leftbar .r-side').removeClass("none");
			}
			if ($(this).scrollTop() > 1262){
				$('.rightbar').addClass("sticky");
				$(".rightbar").animate({top: "30px"}, 0);
				$('.rightbar .r-side').addClass("none");
				$('.agenda').addClass("none");
			}
			else{
				$('.leftbar').removeClass("sticky");
				$('.rightbar').removeClass("sticky");
				$('.rightbar .r-side').removeClass("none");
				$('.agenda').removeClass("none");
			}
		});
	}
	
	$('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
	$('.tab ul.tabs li a').click(function (g) {
		var tab = $(this).closest('.tab'),
			index = $(this).closest('li').index();
		tab.find('ul.tabs > li').removeClass('current');
		$(this).closest('li').addClass('current');
		tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
		tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();
		g.preventDefault();
	});
	
	$('.sidenav').sidenav();
</script>
<div id="fb-root"></div>
<script id="facebook-jssdk" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.12&appId=325657497499535&autoLogAppEvents=1" async defer></script>
<script async src="https://platform.twitter.com/widgets.js"></script>
<script async src="https://www.instagram.com/embed.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
</body>
</html>