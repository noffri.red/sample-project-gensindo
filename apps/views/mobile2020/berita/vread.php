<!doctype html>
<html><head>
  <!-- Meta SEO -->
  <title><?php echo $html['title']; ?></title>
	<meta charset="utf-8">
	<meta http-equiv="x-dns-prefetch-control" content="on">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
    <meta name="theme-color" content="#499AE8"> 


	<!-- Icon -->
    <link rel="shortcut icon" href="<?php echo m_template_uri(); ?>/image/favicon.ico">
    <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/image/favicon-192x192.png" sizes="192x192">
    <!-- Font -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;900&display=swap" rel="stylesheet"> <!-- Nunito -->

    <?php echo $html['metaname']; ?>
    <?php echo $html['canonical']; ?>

    <link rel="amphtml" href="<?php echo $content['amp_url']; ?>">
    <meta property="fb:app_id" content="325657497499535">
    <meta property="fb:pages" content="248570208512540">
    
    <link rel="alternate" title="SINDOnews | RSS Feed Berita GEN SINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>
  
    <link rel="alternate" title="GenSINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>
    <!-- CSS -->
    <!-- Development -->
    <?php echo $html['css']; ?>
    <?php echo $template['newcss']; ?>

    <!-- Production -->
        <!-- <link rel="stylesheet" href="assets/css/styles.min.css"> --> <!-- Style -->
    <!-- Mediaquery -->
    <link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/mediaquery-HDPI.min.css" media="screen and (min-width: 1083px) and (max-width: 1290px)"> <!-- HDPI Screen -->
    <link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/mediaquery-MDPI.min.css" media="screen and (min-width: 993px) and (max-width: 1082px)"> <!-- MDPI Screen -->
    <link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/mediaquery-SDPI.min.css" media="screen and (min-width: 769px) and (max-width: 992px)"> <!-- SDPI Screen -->
    <link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/mediaquery-mobile.min.css" media="screen and (max-width: 768px)"> <!-- Mobile Screen -->

  
    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "NewsArticle",
            "mainEntityOfPage": {
                "@type": "WebPage",
                "@id": "<?php echo $content['site_url']; ?>"
            },
            "headline": "<?php echo $content['site_title']; ?>",
            "image": {
                "@type": "ImageObject",
                "url": "<?php echo $content['site_image']; ?>",
                "height": 413,
                "width": 620
            },
            "datePublished": "<?php echo $content['site_publish']; ?>",
            "dateModified": "<?php echo $content['site_publish']; ?>",
            "author": {
                "@type": "Person",
                "name": "<?php echo $content['site_reporter']; ?>"
            },
            "publisher": {
                "@type": "Organization",
                "name": "SINDOnews.com",
                "logo": {
                    "@type": "ImageObject",
                    "url": "https://sm.sindonews.net/dyn/600/mobile/2016/images/logo-sindonews.png",
                    "width": 600,
                    "height": 55
                }
            },
            "description": "<?php echo $content['site_summary']; ?>"
        }
    </script>

    <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "BreadcrumbList",
            "itemListElement": [{
                "@type": "ListItem",
                "position": 1,
                "item": {
                    "@id": "<?php echo base_url(); ?>",
                    "name": "home"
                }
            }, {
                "@type": "ListItem",
                "position": 2,
                "item": {
                    "@id": "<?php echo $content['site_url']; ?>",
                    "name": "<?php echo $content['site_label']; ?>"
                }
            }]
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebPage",
            "headline": "<?php echo addcslashes($content['site_title'],'"\\/'); ?>",
            "url": "<?php echo $content['site_url']; ?>",
            "datePublished": "<?php echo $content['site_publish']; ?>",
            "image": {
                "@type": "ImageObject",
                "url": "<?php echo $content['site_image']; ?>",
                "height": 413,
                "width": 620
            },
            "description": "<?php echo addcslashes($content['site_summary'],'"\\/'); ?>"
        }
    </script>

    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <script>
        window.googletag = window.googletag || {cmd: []};
        
        googletag.cmd.push(function() {
            googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [[300,250],[320,100],[320,50],[1,1]], 'div-gpt-ad-billboard').setTargeting('pos', ['billboard']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [[300,250],[1,1]], 'div-gpt-ad-middle_1').setTargeting('pos', ['middle_1']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [[300,250],[1,1]], 'div-gpt-ad-middle_2').setTargeting('pos', ['middle_2']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [[300,250],[1,1]], 'div-gpt-ad-middle_3').setTargeting('pos', ['middle_3']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [[320,50],[1,1]], 'div-gpt-ad-top-sticky').setTargeting('pos', ['top_sticky']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('mobile_gam_id'); ?>', [[320,50],[1,1]], 'div-gpt-ad-bottom-sticky').setTargeting('pos', ['bottom_sticky']).addService(googletag.pubads());
            
            googletag.pubads().setTargeting('page_type', ['articlepage']);
            googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs();
            googletag.enableServices();
        });
        function bannerCheck(unitAd){googletag.pubads().addEventListener('slotRenderEnded',function(event){if(event.slot.getSlotElementId() === unitAd){if(event.isEmpty){var eAd = document.getElementById('me-'+unitAd);eAd.classList.add('adnone');}}});}
    </script>
    
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        window.OneSignal = window.OneSignal || [];
        OneSignal.push(function () {
            OneSignal.init({
                appId: '<?php echo $this->config->item('onesignal_appid'); ?>',
                autoResubscribe: true,
                notifyButton: {
                    enable: false
                },
                promptOptions: {
                    slidedown: {
                        enabled: true,
                        autoPrompt: true,
                        actionMessage: "Dapatkan update berita SINDOnews melalui notifikasi browser Anda.",
                        acceptButtonText: "SUBSCRIBE",
                        cancelButtonText: "TIDAK"
                    }
                }
            });
            
            OneSignal.on('notificationPermissionChange', function(permissionChange) {
                var currentPermission = permissionChange.to;
                console.log('New permission state:', currentPermission);

                if(currentPermission === 'granted'){
                    window.dataLayer.push({
                        event: 'push-notification-click',
                        article_title: "<?php echo $content['cd_title']; ?>"
                    });
                }
            });
        });
    </script>
    
    <script>
        window.dataLayer = [{
            kanal: 'GenSINDO',
            subkanal: '<?php echo $content['cd_subkanal']; ?>',
            tags: '<?php echo addslashes(trim($content['cd_tags'])); ?>',
            author: '<?php echo addslashes($content['cd_author']); ?>',
            editor: '<?php echo addslashes($content['cd_editor']); ?>',
            publish_date: '<?php echo $content['date_publish']; ?>',
            publish_time: '<?php echo $content['time_publish']; ?>',
            publish_year: '<?php echo date('Y',strtotime($content['cd_publish'])); ?>',
            publish_month: '<?php echo date('m',strtotime($content['cd_publish'])); ?>',
            publish_day: '<?php echo date('d',strtotime($content['cd_publish'])); ?>',
            article_id: '<?php echo $content['content_id']; ?>',
            article_title: "<?php echo $content['cd_title']; ?>",
            content_type: 'artikel',
            page_type: 'article_page',
            data_source: 'Non AMP',
            pagination: 'page <?php echo $content['cd_page']; ?>'
        }];
    </script>
    
    <?php echo $html['bottom_js']; ?>

</head>

<body id="detail">
  <div id="app">
    <!-- Header -->
    <header>
      <div class="container">
        <!-- Back Button -->
        <div id="back-button"><a href="<?php echo base_url(); ?>"><i class="fas fa-angle-left"></i></a></div>
        <!-- Comment -->
        <div id="comment"><a id="comment-scroll"><i class="fas fa-comment"></i></a></div>
        <!-- Share -->
        <div id="share"><a id="share-scroll"><i class="fas fa-share-alt"></i></a></div>
        
      </div>
    </header>
    <!-- Mask -->
    <div class="mask-header">Mask</div>
    <!-- Main -->
    <main>
        <article>
            <!-- Breadcrumb -->
            <?php echo $content['breadcrumb']; ?>
            
            <!-- details berita -->
            <?php echo $content['detail'];   ?>
        </article>
        
        <section>
            <?php echo $content['relatedtopic']; ?>
            <!-- Social Media -->
            <div id="social-media-share">
                <?php echo $template['sosialmedia']; ?>
            </div>
            <!-- Emoticon -->
            <div class="emoticon">
                <div class="share">
                    <div class="sharethis-reaction"><div class="sharethis-inline-reaction-buttons"></div></div>
                    <!-- <div class="sharethis-box"><div class="sharethis-inline-share-buttons"></div></div> -->
                </div>
            </div>
            <!-- komentar -->
            <?php echo $content['comment'];?>
            
            <!-- Trending Topic -->
            <?php echo $template['trending']; ?>
        </section>
        
      <!-- Linked News -->
      <div class="linked-news">        
        <?php echo $content['relatedcontent']; ?>
      
      </div>
      <!-- Sindonews News -->
      <div class="linked-news">
        <?php echo $content['sindonews_terkini']; ?>
       </div>
      <!-- R Square -->
      <!-- <div class="r-square"><a href="#"><img src="<?php echo m_template_uri(); ?>image/r-square.gif" alt=""></a></div> -->
    </main>
    <div class="mask-footer">Mask</div>
    <!-- Footer -->
    <?php echo $template['footer']; ?> 
  </div>
    
    <div class="bottom-block" id="me-div-gpt-ad-bottom-sticky">
        <div class="bottom-float">                
            <div class="bottom-genie bottom-extra">
                <div id="div-gpt-ad-bottom-sticky"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-bottom-sticky');});</script></div>
            </div>
        </div>
    </div>
  
  <?php echo $html['bottom_css']; ?>
  <?php echo $html['js']; ?>

   <script src="https://platform-api.sharethis.com/js/sharethis.js#property=5c30039e6aa2aa0011451e11&product=inline-share-buttons" async></script>
   <script id="dsq-count-scr" src="https://sindonews.disqus.com/count.js" async></script>

    <script>
        (function($){$.fn.clickToggle=function(func1,func2){var funcs=[func1,func2];this.data('toggleclicked',0);this.click(function(){var data=$(this).data();var tc=data.toggleclicked;$.proxy(funcs[tc],this)();data.toggleclicked=(tc+1)%2});return this;}}(jQuery));
        $(function () {
            var imagesLoad = new LazyLoad({
                elements_selector:'.lazyload'
            });
            
            function clearDqAds(){
                $('#disqus_thread').children().first().fadeOut();
            }
            
            setInterval(clearDqAds,2000);
            setTimeout(function(){sindostop(clearDqAds);},15000);

            if ($(window).width() > 770) {
                $(window).scroll(function () {
                    if ($(this).scrollTop() > 1495) {
                        $('.leftbar').addClass("sticky");
                        $(".leftbar").animate({top: "30px"}, 0);
                        $('.leftbar .r-side').addClass("none");
                    } else {
                        $('.leftbar').removeClass("sticky");
                        $('.leftbar .r-side').removeClass("none");
                    }
                    if ($(this).scrollTop() > 1262) {
                        $('.rightbar').addClass("sticky");
                        $(".rightbar").animate({top: "30px"}, 0);
                        $('.rightbar .r-side').addClass("none");
                        $('.agenda').addClass("none");
                    } else {
                        $('.leftbar').removeClass("sticky");
                        $('.rightbar').removeClass("sticky");
                        $('.rightbar .r-side').removeClass("none");
                        $('.agenda').removeClass("none");
                    }
                });
            }

            $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
            $('.tab ul.tabs li a').click(function (g) {
                var tab = $(this).closest('.tab'), index = $(this).closest('li').index();
                tab.find('ul.tabs > li').removeClass('current');
                $(this).closest('li').addClass('current');
                tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
                tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();
                g.preventDefault();
            });

            // scroll to comment
            $("#comment-scroll").click(function() {               
                $('html, body').animate({
                    scrollTop: $("#disqus_thread").offset().top
                }, 1000);
            })

            $("#btn-comment").click(function() {                
                $('html, body').animate({
                    scrollTop: $("#disqus_thread").offset().top
                }, 1000);
            })

            $("#share-scroll").click(function() {               
                $('html, body').animate({
                    scrollTop: $("#social-media-share").offset().top
                }, 1000);
            })
            
            $('.adv-inline a').click(function () {
                var advname = $('.adv-inline a').text();
                window.dataLayer.push({
                    event: 'affiliate-link-click',
                    affiliate_name: "<?php echo $content['cd_title']; ?>"
                });
            });

            $('.ext-link').click(function () {
                var advname = $('.ext-link').text();
                window.dataLayer.push({
                    event: 'affiliate-link-click',
                    affiliate_name: "<?php echo $content['cd_title']; ?>"
                });
            });

            var affiliateImpressionA = new LazyLoad({
                elements_selector:'.adv-inline',
                threshold: 0,
                callback_enter: function(){
                    var advname = $('.adv-inline a').text();
                    window.dataLayer.push({
                        event: 'affiliate-link-impression',
                        affiliate_title: "<?php echo $content['cd_title']; ?>"
                    });
                }
            });

            var affiliateImpressionB = new LazyLoad({
                elements_selector:'.ext-link',
                threshold: 0,
                callback_enter: function(){
                    var advname = $('.ext-link').text();
                    window.dataLayer.push({
                        event: 'affiliate-link-impression',
                        affiliate_title: "<?php echo $content['cd_title']; ?>"
                    });
                }
            });

            setTimeout(function () {
                $('.sharethis-box .st-btn').each(function(i){
                    var shareBtn = $(this).attr('data-network');
                    $(this).click(function(){
                        window.dataLayer.push({
                            event: 'social-share',
                            social_platform: shareBtn
                        });
                    });
                });

                $('.sharethis-box-bottom .st-btn').each(function(i){
                    var shareBtn = $(this).attr('data-network');
                    $(this).click(function(){
                        window.dataLayer.push({
                            event: 'social-share',
                            social_platform: shareBtn
                        });
                    });
                });
            }, 5000);

            var pageStatus = new LazyLoad({
                elements_selector:'.page-status',
                threshold: 0,
                callback_enter: function(){
                    window.dataLayer.push({
                        event : 'article-milestone',
                        article_title: "<?php echo $content['cd_title']; ?>",
                        scroll_measure_percent: '100%',
                        scroll_measure_value: 100
                    });
                }
            });

            $('.comment-go a').on('click', function () {
                window.dataLayer.push({
                    event: 'comment',
                    article_title: "<?php echo $content['cd_title']; ?>"
                });
            });
        });
    </script>

</body>
</html>