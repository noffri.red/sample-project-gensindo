<!DOCTYPE html>
<html lang="id-ID">
    <head>
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="shortcut icon" href="<?php echo template_uri(); ?>/images/favicon.ico">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-32x32.png" sizes="32x32">        
        <?php echo $html['metaname']; ?>
        <link rel="canonical" href="<?php echo $content['site_url']; ?>">

        <style>
            a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font:inherit;vertical-align:baseline}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:1.5}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:'';content:none}table{border-collapse:collapse;border-spacing:0}a,a:active,a:hover,a:visited{outline:0}a{text-decoration:none}
            body {
                font-family: tahoma, helvetica, sans-serif;
                font-size: 16px;
                line-height: 1.5;
                background-color: #282828;
            }
            img {
                display: block
            }
            article {
                margin: 16px auto;
                color: #EFEFEF
            }
            .subtitle {
                color: #B6B6B6;
            }
            .reporter a,.content a {
                color: #DFDFDF;
            }
            time {
                color: #969696;
            }
            figure {
                text-align: center;
                padding: 16px 0;
            }
            figure img {
                margin: 0 auto;
            }
            .content img {
                margin: 16px auto;
                width: 90%;
                height: auto;
                border: 5px solid #282828;
                box-shadow: 0 0 2px 0 rgba(255,255,255,.6);
                -webkit-box-shadow: 0 0 2px 0 rgba(255,255,255,.6);
                -moz-box-shadow: 0 0 2px 0 rgba(255,255,255,.6);
            }
            figcaption,.note-paging {
                font-size: 12px
            }
            .figcaption {
                color: #919191
            }
            .note-paging {
                text-align: center;
                color: #969696;
                padding: 32px 0 16px;
            }
            .next-page:after,.topic ul:after,footer ul:after {
                clear: both;
                display: table;
                content: ""
            }
            .next-page {
                text-align: center;
                padding-bottom: 16px
            }
            .next-page a {
                display: inline-block;
                padding: 8px 24px;
                background-color: #DFDFDF;
                color: #282828;
                font-weight: 700
            }

            section {
                padding: 0 16px 32px
            }

            .topic-heading,.related-heading {
                color: #ffffff;
                font-size: 18px;
                border-bottom: 1px solid #ffffff;
                padding: 0 16px 8px;
            }

            .topic ul {
                margin-top: 16px
            }

            .topic li {
                display: inline;
                margin: 0 6px;
            }

            .topic li a {
                display: inline-block;
                padding: 6px 10px;
                background-color: #595959;
                color: #ffffff;
                font-size: 12px;
            }

            .related {
                margin-top: 32px
            }

            .related li {
                border-bottom: 1px dotted #4D4D4D;
            }

            .related li:last-child {
                border-bottom: none;
            }

            .related li a {
                color: #ffffff;
                display: block;
                padding: 8px 0
            }

            footer {
                background-color: #3C3C3C;
                padding: 16px;
            }

            footer li {
                display: inline
            }

            footer li a {
                display: inline-block;
                color: #DFDFDF;
                font-size: 14px;
                padding: 4px 8px;
                background-color: #303030;
                margin: 4px 1px;
            }

            .foot-box {
                margin: 16px 0 0;
                color: #969696;
                font-size: 12px
            }

            .rendering {
                font-size: 11px
            }

            .gapix {
                width: 1px;
                height: 1px;
                display: none
            }
            
            .ads-geniee {
                margin: 16px auto;
                padding: 16px;
                background-color: #ffffff
            }

            .cxtrack {
                opacity: 0
            }

            @media(min-width:320px)and (max-width:639px) {
                h1 {
                    font-size: 22px
                }
                .subtitle,h1,.reporter,time,.content {
                    padding: 0 16px
                }
                time,.subtitle,.reporter,.editor,.next-page a {
                    font-size: 12px
                }
                figure img {
                    width: 100%;
                    height: auto
                }
                figcaption {
                    padding: 8px 16px;
                    border-bottom: 1px solid #383838;
                }
            }

            @media(min-width:640px) {
                h1 {
                    font-size: 26px
                }
                article {
                    width: 80%;
                }
                time,.subtitle,.reporter,.editor,.next-page a {
                    font-size: 14px
                }
            }
        </style>
        
        <script>
            function sua(ua) {
                Object.defineProperty(window.navigator, atob('dXNlckFnZW50'), {value: ua, writable: true});
            }
            window.innerWidth = <?php echo $content['res']['width']; ?>;
            window.innerHeight = <?php echo $content['res']['height']; ?>;
        </script>
        <?php //echo $html['bottom_js']; ?>
    </head>

    <body>
        <article>
            <?php echo $content['details']; ?>
        </article>
        <footer>
            <?php echo $content['footer']; ?>
            <div class="itrack">
                <div class="axtrack"></div>
                <div class="cxtrack"></div>
                <div class="aisindo"></div>
            </div>
        </footer>
        
        <?php
        $new_scname = 'sc_ads';
        $new_ucname = 'uc_ads';
        $ai_scname = 'ais_ads';
        $ai_ucname = 'aiu_ads';
        ?>
        <?php echo $html['js']; ?>
        <script>
            var xua = '<?php echo $content['ua']; ?>';
            sua(xua);

            var width = '<?php echo $content['res']['width']; ?>';
            var height = '<?php echo $content['res']['height']; ?>';
            var title = '<?php echo rawurlencode($content['site_title']); ?>';
            var ref = '<?php echo urlencode(current_url()); ?>';
            var url = '<?php echo urlencode($content['site_url']); ?>';
            var scname = '<?php echo $new_scname; ?>';
            var ucname = '<?php echo $new_ucname; ?>';
            var randnum = '<?php echo randomString(11); ?>';
            var cxtime = '<?php echo round(microtime(true) * 1000); ?>';
            var cx = cxsindo(cxtime,title,url,ref);
            pxtrack(cx,'cxtrack');
            var ai_scname = '<?php echo $ai_scname; ?>';
            var ai_ucname = '<?php echo $ai_ucname; ?>';
            var ur = airun() + airun() + (new Date).getTime().toString(16) + airun() + airun();
            var aix = axsindo(height,width,title,ref,url,ai_scname,ai_ucname,ur);
            pxtrack(aix,'aisindo');
            
            function pxtrack(urlsource,urlclass){var img = new Image(),url = urlsource,container = document.getElementsByClassName(urlclass);img.onload = function(){container[0].appendChild(img);};img.src = url;}
            function airun(){return (65536 * (1 + Math.random()) | 0).toString(16).substring(1);}
        </script>
    </body>
</html>