<?php
$total = count($blog);
if($total > 0){
	
	echo '<div class="breadcrumb">';
		echo '<ul>';
			echo '<li>' . ucwords($author) . ' Reportase (' . number_format($totalData,0,'','.') . ')</li>'; 
		echo '</ul>';
	echo '</div>';

    $data = $blog;

	echo '<div class="tab_content">';
		echo '<div id="tab1" class="tabs_item">';

			$i = 0;
			$id_content[$i] = $data[$i]['id_content'];
			$img[$i] = images_uri().$this->config->item('dyn_620').'/gensindo/content/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$data[$i]['id_subkanal'].'/'.$id_content[$i].'/'.$data[$i]['images'];
			$subakanal[$i] = $data[$i]['subkanal'];
			$slug_subkanal[$i] = $data[$i]['slug_subkanal'];
			$dtime[$i] = strtotime($data[$i]['date_created']);
			$link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . $data[$i]['slug_title'] . '-' . $dtime[$i]);

			$title[$i] = cleanWords($data[$i]['title']);
			$summary[$i] = cleanWords($data[$i]['summary']);
			$ago[$i] = time_difference($data[$i]['date_published']);

			echo '<div class="article-news">
									<div class="category-links '. strtolower($slug_subkanal[$i]) .'"><a href="'. base_url().$slug_subkanal[$i] .'" title="'. $subakanal[$i] .'">'. $subakanal[$i] .'</a></div>
									<div class="inner">
										<div class="title"><a href="'. $link[$i] .'">'. $title[$i] .'</a></div>
										<time>'. $ago[$i] .'</time>
										<div class="caption">'. $summary[$i] .'</div>
										<div class="img"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></div>
									</div>
								</div>';

			echo '<section class="article">';

				for($i=1; $i<$total; $i++){
					$id_content[$i] = $data[$i]['id_content'];
					$img[$i] = images_uri().$this->config->item('dyn_620').'/gensindo/content/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$data[$i]['id_subkanal'].'/'.$id_content[$i].'/'.$data[$i]['images'];
					$subakanal[$i] = $data[$i]['subkanal'];
					$slug_subkanal[$i] = $data[$i]['slug_subkanal'];
					$dtime[$i] = strtotime($data[$i]['date_created']);
					$link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . $data[$i]['slug_title'] . '-' . $dtime[$i]);
					$title[$i] = cleanWords($data[$i]['title']);
					$summary[$i] = cleanWords($data[$i]['summary']);
					$ago[$i] = time_difference($data[$i]['date_published']);

					echo '<div class="article-news">
									<div class="category-links '. strtolower($slug_subkanal[$i]) .'"><a href="'. base_url().$slug_subkanal[$i] .'" title="'. $subakanal[$i] .'">'. $subakanal[$i] .'</a></div>
									<div class="inner">
										<div class="title"><a href="'. $link[$i] .'" title="'. $title[$i] .'">'. $title[$i] .'</a></div>
										<time>'. $ago[$i] .'</time>
										<div class="caption">'. $summary[$i] .'</div>
										<div class="img"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></div>
									</div>
								</div>';
				}

			echo '</section>';

		echo '</div>';
    echo '</div>';
}