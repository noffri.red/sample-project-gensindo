<?php

class Gennews extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helpers("gen");
        $this->load->model('news/mgonews','mgonews');
    }

    function index(){
        if($this->mobiledevice->device_check()){
            $this->desktop();
        }else{
            $this->mobile();
        }
    }

    function desktop(){
        $parent_id = $this->config->item('gensindo_id');
        $id_subkanal = $this->config->item('gnews_id');

        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['upVersion'] =  $this->config->item('upVersioncssjs');  // for remove caching

        $a['html']['css'] = $this->load->view('destop2020/template/cssheader', $a, TRUE);
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        // $a['html']['bottom_css'] .= m_add_css('/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/materialize.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/swiper.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/script.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/jquery.jscroll.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);


        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/loadingoverlay.min.js'.$a['upVersion']);
        $a['html']['topjs'] = async_m_add_js('/js/prebid4.6.0-criteo.js'.$a['upVersion']);

        $a['template']['header'] = $this->load->view('destop2020/template/vheader',NULL,TRUE);
        // $a['template']['promoted'] = $this->load->view('destop2020/template/vpromoted',NULL,TRUE);
        $a['template']['minimenu'] = $this->load->view('destop2020/template/vminimenu', NULL, TRUE);
        $a['template']['kategori'] = $this->load->view('destop2020/template/vkategori',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('destop2020/template/vfooter',NULL,TRUE);
        $a['template']['newcss'] = $this->load->view('destop2020/template/vnewcss',NULL,TRUE);
        $a['template']['promogen'] = $this->load->view('destop2020/template/vpromogen',NULL,TRUE);

        // $t['event'] = $this->mgonews->getEvent();
        $t['event']['results'] = array();
        $a['template']['agenda'] = $this->load->view('destop2020/template/vagenda',NULL,TRUE);

        $t['trending'] = $this->mgonews->getKanalTrendingTopic($parent_id, 7);
        $a['template']['trending'] = $this->load->view('mobile2020/template/vtrending', $t, TRUE);


        $t['trendingpopular'] = $this->mgonews->getListTitlePopular($parent_id,5);
        $a['template']['popular'] = $this->load->view('destop2020/template/vpopular',$t,TRUE);

        $t['breaking'] = $this->mgonews->getListNews($id_subkanal,10);
        // echo "<pre>"; print_r($t['breaking']['results']); echo "</pre>"; die();
        $a['content']['breaking'] = $this->load->view('destop2020/subkanal/vbreaking',$t,TRUE);
        $a['content']['breadcrumb'] = $this->load->view('destop2020/subkanal/vbreadcrumb',$t,TRUE);

        $a['html']['title'] = 'Berita Gen SINDO dan Generasi Milenial - Gen SINDO';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Berita Gen SINDO dan Generasi Milenial Terkini menyajikan informasi seputar Anak Muda, Tren, Serba serbi Generasi Muda',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $a['html']['metaname'] = metaname($metaname);

        $a['content']['site_url'] = site_url('gnews');
        $this->load->view('destop2020/pages/vsubkanal',$a,FALSE);
    }

    function mobile(){
        $parent_id = $this->config->item('gensindo_id');
        $id_subkanal = $this->config->item('gnews_id');
        
        $a = array();
        
        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['upVersion'] = $this->config->item('upVersioncssjs');  // for remove caching
        $a['html']['css'] = $this->load->view('mobile2020/template/cssheader', $a, TRUE);

        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');


        $a['html']['js'] = m_add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/materialize.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/jquery.jscroll.min.js'.$a['upVersion']);

        $a['html']['js'] .= m_add_js('/js/jquery.fancybox.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/swiper.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/script.js'.$a['upVersion']);
        
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/loadingoverlay.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_external_js(base_url() . 'sn-v1.js'.$a['upVersion']);
        /*
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-app.js');
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-messaging.js');
        $a['html']['js'] .= add_external_js(base_url() . 'fcm.2020.js');
        */
      
        $a['html']['topjs'] = async_m_add_js('/js/prebid4.6.0-criteo.js');

        $a['template']['header'] = $this->load->view('mobile2020/template/vheader', NULL, TRUE);
        $a['template']['menuoverlay'] = $this->load->view('mobile2020/template/vmenuoverlay', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('mobile2020/template/vnewcss',NULL,TRUE);
        $a['template']['subkanal'] = $this->load->view('mobile2020/template/vsubkanalmenu', NULL, TRUE);
        $a['template']['footer'] = $this->load->view('mobile2020/template/vfooter', NULL, TRUE);
        $a['template']['promogen'] = $this->load->view('template/vpromogen',NULL,TRUE);
        $t['pushbox'] = $this->load->view('mobile/template/vpushbox',NULL,TRUE);

        $t['event']['results'] = array();
        // $a['template']['agenda'] = $this->load->view('mobile/template/vagenda',$t,TRUE);

        $t['trending'] = $this->mgonews->getKanalTrendingTopic($parent_id, 7);
        $a['template']['trending'] = $this->load->view('mobile2020/template/vtrending', $t, TRUE);

        $t['headline'] = $this->mgonews->getHeadline($parent_id, 5);
        $a['template']['headine'] = $this->load->view('mobile2020/template/vheadline', $t, TRUE);

  
        $t['breaking'] = $this->mgonews->getListBreakingNews($id_subkanal,10);
        $a['content']['breaking'] = $this->load->view('mobile2020/subkanal/vbreaking',$t,TRUE);
        $a['content']['breadcrumb'] = $this->load->view('mobile2020/subkanal/vbreadcrumb',$t,TRUE);
    
        $a['html']['title'] = 'Berita Gen SINDO dan Generasi Milenial - Gen SINDO';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Berita Gen SINDO dan Generasi Milenial Terkini menyajikan informasi seputar Anak Muda, Tren, Serba serbi Generasi Muda',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $a['html']['metaname'] = metaname($metaname);

        $a['content']['site_url'] = site_url('gnews');
        $this->load->view('mobile2020/pages/vsubkanal',$a,FALSE);
    }

}
