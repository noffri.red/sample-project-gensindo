<!DOCTYPE html>
<html>
    <head>
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#002445">

        <link rel="shortcut icon" href="<?php echo m_template_uri(); ?>/images/icon/favicon.ico">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-32x32.png" sizes="32x32">

        <link rel="canonical" href="<?php echo $content['site_url']; ?>">
        <?php echo $html['metaname']; ?>
        <link type="text/css" rel="stylesheet" href="https://id.ucnews.ucweb.com/cp-lib/css/sdk.css">

        <?php echo $html['bottom_js']; ?>
    </head>

    <body>        
        <div class="w-article">
            <?php echo $content['details']; ?>
        </div>

        <div id="w-social"></div>
        <div id="w-related"></div>
        <div id="w-footer"></div>

        <noscript><img src="<?php echo site_url('xyz') . '?' . $content['pixel']; ?>" width="1" height="1"></noscript>

        <script src="https://img.ucweb.com/s/uae/g/1s/flow/zepto.js"></script>
        <script src="https://id.ucnews.ucweb.com/cp-lib/js/sdk.js" ></script>
    </body>
</html>