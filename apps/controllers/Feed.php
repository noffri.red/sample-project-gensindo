<?php

class Feed extends CI_Controller {

    var $kanal_id;

    function __construct(){
        parent::__construct();
        $this->load->config('site_domain');
        $this->load->model('news/mgonews');

        $this->kanal_id = $this->config->item('gensindo_id');
    }

    function index(){
        $site_domain = $this->config->item('read_domain');

        $numcontent = 15;
        $news = $this->mgonews->getListNews($this->kanal_id,$numcontent);
        $total = count($news);

        $title = "Gen SINDO untuk Generasi Muda";
        $desc = "Berita Terkini seputar Generasi Muda";
        $linkbase = base_url();

        $xmlW = new XMLWriter;
        $xmlW->openMemory();
        $xmlW->setIndent(true);
        $xmlW->startDocument("1.0","UTF-8");
        $xmlW->startElement("rss");
        $xmlW->writeAttribute("version","2.0");
        $xmlW->writeAttribute("xmlns:content","http://purl.org/rss/1.0/modules/content/");
        $xmlW->writeAttribute("xmlns:dc","http://purl.org/dc/elements/1.1/");
        $xmlW->writeAttribute("xmlns:dcterms","http://purl.org/dc/terms/");
        $xmlW->writeAttribute("xmlns:media","http://search.yahoo.com/mrss/");
        $xmlW->writeAttribute("xmlns:atom","http://www.w3.org/2005/Atom");

        $xmlW->startElement("channel");
        $xmlW->writeElement("generator",$linkbase);
        $xmlW->writeElement("title",$title);
        $xmlW->writeElement("description",$desc);
        $xmlW->writeElement("link",$linkbase);
        $xmlW->writeElement("language","id");
        $xmlW->writeElement("lastBuildDate",date('D, d M Y H:i:s O',strtotime($news[0]['date_publish'])));
        $xmlW->writeElement("copyright",date('Y') . " SINDOnews. All rights reserved.");

        $xmlW->startElement("atom:link");
        $xmlW->writeAttribute("href",site_url('feed'));
        $xmlW->writeAttribute("rel","self");
        $xmlW->writeAttribute("type","application/rss+xml");
        $xmlW->endElement();

        $xmlW->startElement("image");
        $xmlW->writeElement("url",m_template_uri() . "/images/sindonews-480.png");
        $xmlW->writeElement("title",$title);
        $xmlW->writeElement("width",300);
        $xmlW->writeElement("height",27);
        $xmlW->writeElement("link",$linkbase);
        $xmlW->endElement();

        for($i = 0; $i < $total; $i++){
            $dtime[$i] = strtotime($news[$i]['date_created']);
            $img[$i] = images_uri() . '/dyn/620/pena/news/' . date('Y/m/d',strtotime($news[$i]['date_created'])) . '/' . $news[$i]['channel_id'] . '/' . $news[$i]['news_id'] . '/' . $news[$i]['thumb'];
            $path[$i] = '/read/' . $news[$i]['news_id'] . '/' . $news[$i]['channel_id'] . '/' . slug($news[$i]['title']) . '-' . $dtime[$i];
            $linkurl[$i] = "https://" . $site_domain[$news[$i]['channel_id']] . $path[$i];

            $xmlW->startElement("item");

            $xmlW->writeElement("title",cleanWords($news[$i]['title']));
            $xmlW->writeElement("link",$linkurl[$i]);
            $xmlW->startElement("guid");
            $xmlW->writeAttribute("isPermaLink","false");
            $xmlW->text($linkurl[$i]);
            $xmlW->endElement();
            $xmlW->writeElement("pubDate",date('D, d M Y H:i:s O',strtotime($news[$i]['date_publish'])));
            $xmlW->writeElement("dc:creator",$news[$i]['reporter_name']);

            $summary[$i] = cleanWords(html_entity_decode($news[$i]['summary']));
            $xmlW->startElement("description");
            $xmlW->writeCData($summary[$i]);
            $xmlW->endElement();

            $xmlW->startElement("media:content");
            $xmlW->writeAttribute("url",$img[$i]);
            $xmlW->writeAttribute("expression","full");
            $xmlW->writeAttribute("type","image/jpeg");
            $xmlW->endElement();

            $xmlW->endElement();
        }

        $xmlW->endElement();
        $xmlW->endElement();
        $xmlW->endDocument();

        $this->output->set_content_type('text/xml')->set_output($xmlW->outputMemory(true));
    }

    function sitemap(){
        $news = $this->mgonews->getListNews($this->kanal_id,30);
        $total = count($news);

        $xmlW = new XMLWriter;
        $xmlW->openMemory();
        $xmlW->setIndent(true);
        $xmlW->startDocument("1.0","UTF-8");
        $xmlW->startElement("urlset");
        $xmlW->writeAttribute("xmlns","http://www.sitemaps.org/schemas/sitemap/0.9");
        $xmlW->writeAttribute("xmlns:news","http://www.google.com/schemas/sitemap-news/0.9");

        for($i = 0; $i < $total; $i++){
            $site_domain = $this->config->item('read_domain');

            $dtime[$i] = strtotime($news[$i]['date_created']);
            $l[$i] = "https://" . $site_domain[$news[$i]['channel_id']] . '/read/' . $news[$i]['content_id'] . '/' . $news[$i]['channel_id'] . '/' . slug($news[$i]['title']) . '-' . $dtime[$i];

            $xmlW->startElement("url");

            $xmlW->writeElement("loc",$l[$i]);

            $xmlW->startElement("news:news");
            $xmlW->startElement("news:publication");
            $xmlW->writeElement("news:name","GenSINDO");
            $xmlW->writeElement("news:language","ID");
            $xmlW->endElement();

            $xmlW->writeElement("news:genres","PressRelease");
            $xmlW->writeElement("news:publication_date",date('Y-m-d\TH:i:sP',strtotime($news[$i]['date_publish'])));
            $xmlW->writeElement("news:title",cleanWords($news[$i]['title']));
            $xmlW->endElement();

            $xmlW->endElement();
        }

        $xmlW->endElement();
        $xmlW->endDocument();

        $this->output->set_content_type('text/xml')->set_output($xmlW->outputMemory(true));
    }

    function channel(){
        $gensindo = $this->mgonews->getListNews($this->kanal_id,1);
        $gennews = $this->mgonews->getListNews(700,1);

        $xmlW = new XMLWriter;
        $xmlW->openMemory();
        $xmlW->setIndent(true);
        $xmlW->startDocument("1.0","UTF-8");
        $xmlW->startElement("urlset");
        $xmlW->writeAttribute("xmlns","http://www.sitemaps.org/schemas/sitemap/0.9");

        $xmlW->startElement("url");
        $xmlW->writeElement("loc",base_url());
        $xmlW->writeElement("lastmod",date('Y-m-d\TH:i:sP',strtotime($gensindo[0]['date_publish'])));
        $xmlW->writeElement("changefreq","always");
        $xmlW->writeElement("priority","0.7");
        $xmlW->endElement();

        $xmlW->startElement("url");
        $xmlW->writeElement("loc",site_url('gnews'));
        $xmlW->writeElement("lastmod",date('Y-m-d\TH:i:sP',strtotime($gennews[0]['date_publish'])));
        $xmlW->writeElement("changefreq","always");
        $xmlW->writeElement("priority","0.7");
        $xmlW->endElement();

        $xmlW->endElement();
        $xmlW->endDocument();

        $this->output->set_content_type('text/xml')->set_output($xmlW->outputMemory(true));
    }

    function web(){
        $site_domain = $this->config->item('read_domain');
        $news = $this->mgonews->getListNews($this->kanal_id,30);
        $total = count($news);

        $xmlW = new XMLWriter;
        $xmlW->openMemory();
        $xmlW->setIndent(true);

        $xmlW->startDocument("1.0","UTF-8");
        $xmlW->startElement("urlset");
        $xmlW->writeAttribute("xmlns","http://www.sitemaps.org/schemas/sitemap/0.9");

        for($i = 0; $i < $total; $i++){
            $dtime[$i] = strtotime($news[$i]['date_created']);
            $l[$i] = "https://" . $site_domain[$news[$i]['channel_id']] . '/read/' . $news[$i]['content_id'] . '/' . $news[$i]['channel_id'] . '/' . slug($news[$i]['title']) . '-' . $dtime[$i];

            $xmlW->startElement("url");

            $xmlW->writeElement("loc",$l[$i]);
            $xmlW->writeElement("lastmod",date('Y-m-d\TH:i:sP',strtotime($news[$i]['date_publish'])));

            $xmlW->endElement();
        }

        $xmlW->endElement();
        $xmlW->endDocument();

        $this->output->set_content_type('text/xml')->set_output($xmlW->outputMemory(true));
    }

    function indextags(){
        $news = $this->mgonews->getKanalTrendingTopic($this->kanal_id,50);
        $total = count($news);

        $xmlW = new XMLWriter;
        $xmlW->openMemory();
        $xmlW->setIndent(true);

        $xmlW->startDocument("1.0","UTF-8");
        $xmlW->startElement("sitemapindex");
        $xmlW->writeAttribute("xmlns","http://www.sitemaps.org/schemas/sitemap/0.9");

        for($i = 0; $i < $total; $i++){
            $l[$i] = site_url('sitemap-tags') . '/' . $news[$i]['topic_id'] . '/' . slug($news[$i]['topic']) . '.xml';

            $xmlW->startElement("sitemap");
            $xmlW->writeElement("loc",$l[$i]);
            $xmlW->endElement();
        }

        $xmlW->endElement();
        $xmlW->endDocument();

        $this->output->set_content_type('text/xml')->set_output($xmlW->outputMemory(true));
    }

    function indextopic(){
        $id_topik = $this->uri->segment(2,0);
        $news = $this->mgonews->getRelatedContent($id_topik,$this->kanal_id,0,30);

        $total = count($news);

        $xmlW = new XMLWriter;
        $xmlW->openMemory();
        $xmlW->setIndent(true);
        $xmlW->startDocument("1.0","UTF-8");
        $xmlW->startElement("urlset");
        $xmlW->writeAttribute("xmlns","http://www.sitemaps.org/schemas/sitemap/0.9");
        $xmlW->writeAttribute("xmlns:news","http://www.google.com/schemas/sitemap-news/0.9");

        for($i = 0; $i < $total; $i++){
            $site_domain[$i] = $this->config->item('read_domain');
            $channel_id[$i] = $news[$i]['id_subkanal'];

            $dtime[$i] = strtotime($news[$i]['date_created']);
            $l[$i] = 'https://' . $site_domain[$i][$channel_id[$i]] . '/read/' . $news[$i]['id_news'] . '/' . $news[$i]['id_subkanal'] . '/' . slug($news[$i]['title']) . '-' . $dtime[$i];

            $xmlW->startElement("url");

            $xmlW->writeElement("loc",$l[$i]);

            $xmlW->startElement("news:news");
            $xmlW->startElement("news:publication");
            $xmlW->writeElement("news:name","GenSINDO");
            $xmlW->writeElement("news:language","ID");
            $xmlW->endElement();

            $xmlW->writeElement("news:genres","PressRelease");
            $xmlW->writeElement("news:publication_date",date('Y-m-d\TH:i:sP',strtotime($news[$i]['date_published'])));
            $xmlW->writeElement("news:title",cleanWords($news[$i]['title']));
            $xmlW->endElement();

            $xmlW->endElement();
        }

        $xmlW->endElement();
        $xmlW->endDocument();

        $this->output->set_content_type('text/xml')->set_output($xmlW->outputMemory(true));
    }

}
