<style>
    html {
        scroll-behavior: smooth;
    }
    .sharethis-reaction{margin: 8px 0}
    .sharethis-box{margin: 8px 0 24px}
    .detail figcaption{margin-bottom: 15px;text-align: center;display: block;font-size: 12px;font-family: Arial,Helvetica,sans-serif;font-size: 12px;margin: 0;padding: 0;vertical-align: baseline;border: 0;}
    .image-content img{width:100%;height:auto;display:block}
    .detail .caption{font-size:16px;line-height:1.7}
    .subtitle-news{display: inline-block;font-weight: 700;font-size: 14px;line-height: 10px;}
    .detail .link-article li{height: auto !important;margin-bottom: 30px;}
    .link-article img{width: 180px;height: 120px;object-fit: cover;}
    .hit-view {font-size: 12px;color: #ffffff;text-align:right;}

    .baca-inline ul {background-color: #F7F7F7;margin-bottom: -24px;margin-left: 0!important;padding: 0!important;list-style: none!important;}
    .baca-inline-head {font-size: 15px;font-weight: 700;border-bottom: 2px solid #DFDFDF;padding-bottom: 4px;color: #969696;}
    .baca-inline li {border-bottom: 2px solid #ffffff;}
    .baca-inline li:hover {background-color: #EBEBEB;}
    .baca-inline li a {display: block!important;padding:12px 16px;font-size: 16px;font-weight:700}
    .baca-inline li a:hover {text-decoration: none!important;}

    .news-load{text-align: center;}
    .caption strong{font-weight: 700;}

    .axsindo{opacity:0}
    .sindoframe,.sindomp,.sindoev,.sindopix {display: none;width: 1px;height: 1px;}

    .fb-comment{display:none;}
    .topic ul li .title{height:100px;}

    .v-youtube {
        position: relative;
        overflow: hidden;
        height: 0;
        padding-bottom: 56.25%;
        background-color: #282828
    }
    .v-youtube iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        display: block
    }

    .mt20 {
        margin-top: 20px
    }
    .mb20 {
        margin-bottom: 20px
    }
    .mt20important{
        margin-top: 20px !important;
    }
    .mt10 {
        margin-top: 10px
    }
    .mb10 {
        margin-bottom: 10px
    }
    img,iframe {
        display: block
    }

    @media(min-width:320px)and (max-width:799px) {
        .link-article li a {
            display: block;
        }

        .link-article li img {
            display: inline-block;
            width: 100%!important;
            height: auto!important
        }

        .link-article li .title,.link-article li .title a {
            height: auto!important
        }

        .link-article li .title a {
            font-weight: 700;
            font-size: 16px!important
        }

        .link-article li {
            box-shadow: 0 0 4px 0 rgba(0,0,0,.4);
            padding: 8px;
            margin-bottom: 16px!important
        }

        .link-article .head {
            margin-bottom: 8px!important
        }

        .sky-banner {
            display: none
        }

        .medium-banner {
            width: 300px;
            height: auto;
            margin-left: auto;
            margin-right: auto
        }
    }

    .cxtrack,.cxrectrack,#artsindo,.sintrack {
        opacity: 0;
        width: 1px;
        height: 0px;
    }
    .sintrack img.sintrack iframe{
        height: 1px;
    }

    .notif-box {
        font-family: Tahoma, Arial, sans-serif;
        background-color: #ffffff;
        position: fixed;
        width: 360px;
        box-shadow: 0 0 5px 0 rgba(0,0,0,.4);
        -moz-box-shadow: 0 0 5px 0 rgba(0,0,0,.4);
        -webkit-box-shadow: 0 0 5px 0 rgba(0,0,0,.4);
        padding: 2px;
        z-index: 99999
    }
    .notif-act {
        text-align: right
    }
    .notif-body:after,.notif-form:after {
        clear: both;
        display: table;
        content: ""
    }
    .notif-body {
        padding: 16px;
        color: #f0f0f0;
        background-color: #282828;
    }
    .notif-logo {
        float: left;
        padding-right: 16px
    }
    .notif-text {
        overflow: hidden;
    }
    .notif-lead {
        font-size: 14px
    }
    .notif-form {
        margin-top: 16px
    }
    .notif-yes,.notif-no {
        padding: 8px 16px;
        color: #ffffff;
        text-align: center;
        cursor: pointer;
        font-size: 12px;
        letter-spacing: 1px;
        width: 78px;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        text-shadow: 0 0 2px rgba(0,0,0,.4)
    }
    .notif-yes {
        float: left;
        background-color: #28ba3d;
        margin-right: 16px;
    }
    .notif-yes:hover {
        background-color: #1B7D29
    }
    .notif-no {
        overflow: hidden;
        background-color: #7b7b7b
    }
    .notif-no:hover {
        background-color: #3E3E3E
    }
    .notif-show {
        left: 16px;
        bottom: 16px;
        transition: all .5s ease-in 0s;
        -moz-transition: all .5s ease-in 0s;
        -webkit-transition: all .5s ease-in 0s;
        -o-transition: all .5s ease-in 0s;
    }
    .notif-hide {
        left: -480px;
        bottom: 16px;
        transition: all .5s ease-out 0s;
        -moz-transition: all .5s ease-out 0s;
        -webkit-transition: all .5s ease-out 0s;
        -o-transition: all .5s ease-out 0s;
    }

    .category li.samsung {
        background-image: url(https://sd.sindonews.net/gensindo/2019/images/bg_category_home.png);
    }
    .category li.samsung a:hover {
        color: #FFF;
    }
    .article-news .category-links.samsung {
        background-image: url(https://sd.sindonews.net/gensindo/2019/images/bg_fold_gnews.png);
        background-size: 100% 100%;
    }

    /* CSS adding at 09/09/2020 */
    .note-paging {
        text-align: center;
        color: #999999;
        font-size: 12px;
        margin: 40px 0 10px
    }
    .box-paging {
        text-align: center
    }
    .article-paging {
        box-shadow: 0 0 5px 0 rgba(0,0,0,.4);
        -webkit-box-shadow: 0 0 5px 0 rgba(0,0,0,.4);
        -moz-box-shadow: 0 0 5px 0 rgba(0,0,0,.4);
        display: inline-block;
        font-size: 16px;
        /* border: 2px solid #fff; */
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px
    }
    .article-paging:after {
        clear: both;
        display: table;
        content: ''
    }
    .article-paging li {
        display: inline-block
    }
    .article-paging li.active {
        background-color: #ffffff;
        color: #499AE8!important;
        padding: 12px 22px;
    }
    .article-paging li a {
        display: block;
        background-color: #499AE8;
        color: #ffffff !important;
        padding: 12px 22px;
        text-decoration: none!important;
        border-right: 1px solid #3671AB;
        border-left: 1px solid #50A9FF;
    }
    .article-paging li a:hover {
        background-color: #eeeeee;
        color: #499AE8!important;
    }
    .article-paging li:last-child a {
        border-right: none;
    }
    .content-show {
        margin: 32px auto 0;
        border-top: 4px solid #499AE8;
        width: 40%;
    }
    .content-show a {
        padding: 8px 16px;
        display: block;
        background-color: #499AE8;
        color: #ffffff!important;
        width: 60%;
        text-align: center;
        margin: 0 auto;
        font-size: 14px;
        text-decoration: none!important;
        border-bottom-right-radius: 8px;
        border-bottom-left-radius: 8px;
    }
    .content-show a:hover {
        background-color: #ffffff;
        color: #282828!important;
        box-shadow: 0 0 5px 0 rgba(0,0,0,.5);
    }
    .article-next a {
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        -webkit-border-top-right-radius: 5px;
        -webkit-border-bottom-right-radius: 5px;
        -moz-border-top-right-radius: 5px;
        -moz-border-bottom-right-radius: 5px;
    }
    .article-prev a {
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        -webkit-border-top-left-radius: 5px;
        -webkit-border-bottom-left-radius: 5px;
        -moz-border-top-left-radius: 5px;
        -moz-border-bottom-left-radius: 5px;
    }

    .breadcrumb li {
        text-transform: lowercase;
    }
    .subtitle-news {
        margin-bottom: 8px;
    }
    .title-news h1 {
        font-size: 120%;
        line-height: 1.5;
        font-weight: 700;
        margin: 0;
    }
    .author {
        margin-bottom: 8px;
    }
    figcaption {
        margin: 0 0 16px 0!important;
        padding: 8px 0!important
    }
    .caption {
        font-size: 17px!important;
        line-height: 1.8!important;
        font-family: helvetica, sans-serif!important;
    }
    .head-comment {
        font-size: 18px;
        font-weight: 700;
        border-bottom: 2px solid #E30000;
    }

    .terpopuler li:after {
        clear: both;
        content: "";
        display: table
    }
    .terpopuler li {
        margin-bottom: 4px;
        background-color: #fff;
        border-bottom: 1px solid #D9F0FB;
    }
    .terpopuler li:hover {
        border-bottom: 1px solid #AEDFF7
    }
    .popular-num {
        float: left;
        padding: 16px 8px 8px 16px;
        font-weight: 700;
        font-size: 32px;
        color: #0272A8;
        font-family: helvetica, sans-serif;
        font-style: italic;
        vertical-align: middle;
        position: relative;
        text-shadow: 0 0 5px rgba(255,255,255,.5);
    }
    .popular-title {
        overflow: hidden
    }
    .popular-title a {
        display: block;
        padding: 12px 16px;
        font-size: 15px;
        font-weight: 700;
        color: #0272A8;
    }
    .popular-title a:hover {
        color: #039BE5
    }
    .detail time {
        padding: 0!important
    }
    .topic-suggest li {
        padding-right: 0!important;
        margin: 4px;
    }
    .topic-suggest li a {
        padding: 8px 16px;
        font-size: 14px;
        background-color: #D9EBFF!important;
        color: #3671AB!important;
    }
    #detail .topic-suggest li a:hover {
        background-color: #EEF5FD!important;
    }

    .photo-box-list {
        display: grid;
        grid-template-columns: repeat(2,1fr);
        grid-gap: 8px;
    }
    .photo-box-item {
        border: 5px solid #ffffff;
    }
    .photo-box-item:hover {
        box-shadow: 0 0 8px 0 rgba(0,0,0,.4);
        -webkit-box-shadow: 0 0 8px 0 rgba(0,0,0,.4);
        -moz-box-shadow: 0 0 8px 0 rgba(0,0,0,.4)
    }
    .photo-box-pict img {
        display: block;
        width: 100%!important;
        height: 161px!important;
        overflow: hidden
    }
    .photo-box-text a {
        display: block;
        color: #BD024B;
        font-weight: 700;
        text-decoration: none;
        padding: 8px;
    }
    .photo-box-text a:hover {
        color: #FA0363
    }
    .trending ul {
        background-color: #ffffff!important
    }
    .trending li {
        padding: 0!important;
        background-color: #3F51B5;
        margin-bottom: 2px
    }
    .trending a {
        display: block;
        padding: 12px 16px;
        font-size: 15px!important;
    }
    .trending a:hover {
        background-color: #ffffff;
        color: #3F51B5!important;
        font-weight: 700
    }

    .text-msg {
        padding: 16px;
        font-size: 14px;
        font-family: tahoma, sans-serif;
        color: #141A3B;
        font-weight: 500;
    }    
    .text-msg strong {
        font-weight: 700
    }

    .billboard {
        width: 970px;
        height: auto;
        margin: -0px auto 32px
    }
    .ads-geniee {
        margin-top: 16px;
        padding: 0 16px
    }
    .ads-geniee:after {
        display: table;
        clear: both;
        content: ""
    }
    .ads300 {
        width: 300px;
        margin-left: auto;
        margin-right: auto
    }
    .ads336 {
        width: 336px;
        margin-left: auto;
        margin-right: auto
    }
    .mbmin {
        margin-bottom: -20px;
    }
    .news-load {
        text-align: center;
        padding: 8px
    }
    .news-more {
        margin: 0 auto;
        text-align: center
    }
    .news-more a {
        display: inline-block;
        background-color: #E71E26;
        color: #ffffff;
        font-size: 16px;
        padding: 12px 16px;
        width: 60%;
        border-radius: 8px;
    }
    .news-more a:hover {
        background-color: #EC4C52
    }
    .detail .topic-suggest {
        padding-bottom: 0!important;
        margin: 16px 0 0
    }

    .r-side{
        margin: 0 30px 30px;
    }

    /* loading */
    #overlay {
        width: 100%;
        height: 100%;
        display:table;
        background: rgba(0, 0, 0, 0.5);
    }
    #overlay i {
        display:table-cell;
        vertical-align:middle;
        text-align:center;
    }
    .content-left{
        border-right: 0px !important;
    }

    #detail .paragraph a{
        display: inline;
    }

    #detail #social-media-share .head{
        justify-content: center;
    }

    #detail .paragraph span{
        font-weight: 700;
    }

    #detail .topic-suggest ul{
        display: flex;
        flex-direction: inherit;
        justify-content: center;
        margin: 20px auto;
    }

    #detail .topic-suggest li a{
        background-color: #007bff;
        color: white;
        border-radius: 5px;
        transition: all .3s ease-in-out;
    }

    #detail .topic-suggest li a:hover{
        background-color: #ffffff;
        color: #282828!important;
    }

    article, section {
        background-color: #ffffff;
        padding: 20px;

    }
    article {
        border-radius: 10px 10px 0 0;
    }
    section {
        border-radius: 0 0 10px 10px;
    }
    .box-ads {
        box-shadow: inset 0 0 8px 0 rgba(0,0,0,.5);
        -moz-box-shadow: inset 0 0 8px 0 rgba(0,0,0,.5);
        -webkit-box-shadow: inset 0 0 8px 0 rgba(0,0,0,.5);
        border-radius: 10px;
        padding: 10px 10px 20px 10px;
        margin: 20px 0;
    }
    .paragraph {
        font-size: 20px!important;
        line-height: 1.8!important;
    }
    .paragraph a {
        color: #499AE8
    }
    .paragraph a:hover {
        color: #3671AB
    }
</style>