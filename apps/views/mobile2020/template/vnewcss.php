<style>
    html {
        scroll-behavior: smooth;
    }

    .sharethis-reaction {
        margin: 8px 0
    }

    .sharethis-box {
        margin: 8px 0
    }

    .detail figcaption {
        margin-bottom: 15px;
        text-align: center;
        display: block;
        font-size: 12px;
        font-family: Arial, Helvetica, sans-serif;
        font-size: 12px;
        margin: 0;
        padding: 0;
        vertical-align: baseline;
        border: 0;
    }

    .caption img {
        width: 100%;
        height: auto;
        display: block
    }

    .detail .caption {
        font-size: 16px;
        line-height: 1.7
    }

    .subtitle-news {
        display: inline-block;
        font-weight: 700;
        font-size: 14px;
        line-height: 10px;
        margin-bottom: 8px;
    }

    .detail .link-article li {
        height: auto !important;
    }

    .link-article img {
        width: 180px;
        height: 120px;
        object-fit: cover;
    }

    .hit-view {
        font-size: 12px;
        color: #ffffff;
        text-align: right;
    }

    .baca-inline ul {
        background-color: #F7F7F7;
        margin-bottom: -24px;
        margin-left: 0 !important;
        padding: 0 !important;
        list-style: none !important;
    }

    .baca-inline-head {
        font-size: 15px;
        font-weight: 700;
        border-bottom: 2px solid #DFDFDF;
        padding-bottom: 4px;
        color: #969696;
    }

    .baca-inline li {
        border-bottom: 2px solid #ffffff;
    }

    .baca-inline li:hover {
        background-color: #EBEBEB;
    }

    .baca-inline li a {
        display: block;
        font-size: 15px;
        padding: 12px 16px;
        font-weight: 700;
        color: #084D8D !important;
    }

    .baca-inline li a:hover {
        text-decoration: none !important;
    }

    .news-load {
        text-align: center;
    }

    .caption strong {
        font-weight: 700;
    }

    .axsindo {
        opacity: 0
    }

    .sindoframe,
    .sindomp,
    .sindoev,
    .sindopix {
        display: none;
        width: 1px;
        height: 1px;
    }

    .fb-comment {
        display: none;
    }

    .topic ul li .title {
        height: 100px;
    }

    .v-youtube {
        position: relative;
        overflow: hidden;
        height: 0;
        padding-bottom: 56.25%;
        background-color: #282828
    }

    .v-youtube iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        display: block
    }

    .mt20 {
        margin-top: 20px
    }

    .mb20 {
        margin-bottom: 20px
    }

    .mt10 {
        margin-top: 10px
    }

    .mb10 {
        margin-bottom: 10px
    }

    img,
    iframe {
        display: block
    }

    @media(min-width:320px)and (max-width:799px) {
        .link-article li a {
            display: block;
        }

        .link-article li img {
            display: inline-block;
            width: 100% !important;
            height: auto !important
        }

        .link-article li .title,
        .link-article li .title a {
            height: auto !important
        }

        .link-article li .title a {
            font-weight: 700;
            font-size: 16px !important
        }

        .link-article li {
            box-shadow: 0 0 4px 0 rgba(0, 0, 0, .4);
            padding: 8px;
            margin-bottom: 16px !important
        }

        .link-article .head {
            margin-bottom: 8px !important
        }

        .sky-banner {
            display: none
        }

        .medium-banner {
            width: 300px;
            height: auto;
            margin-left: auto;
            margin-right: auto
        }
    }

    .forum-box ul:after {
        clear: both;
        content: "";
        display: table
    }

    .forum-box {
        padding: 0 16px
    }

    .forum-title {
        padding: 0 0 4px;
        font-weight: 700;
        margin-bottom: 8px;
        font-size: 20px;
        color: #880E4F;
        border-bottom: 2px solid #880E4F;
    }

    .forum-subtitle {
        padding: 0 16px 8px;
        color: #880E4F;
        font-size: 14px
    }

    .forum-box ul {
        margin: 8px auto 0;
        padding: 0 16px
    }

    .forum-box li {
        border: none !important;
        display: inline-block;
        padding: 8px 12px !important;
        cursor: pointer;
        float: left;
        text-align: center;
        color: #880E4F;
        background-color: #EDDBE4;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        margin: 0 16px 0 0;
        font-size: 14px;
        width: 30%
    }

    .ac-active {
        background-color: #880E4F !important;
        color: #fff !important;
    }

    .mforum {
        text-align: center;
    }

    /*.sindodisqus {
        display: none
    }*/
    .dblock {
        display: block
    }

    .social-share {
        margin: 16px auto 0 !important
    }

    .cxtrack,
    .cxrectrack,
    #artsindo,
    .sintrack {
        opacity: 0;
        width: 1px;
        height: 0px;
    }

    .sintrack img.sintrack iframe {
        height: 1px;
    }

    .notif-box {
        transition: ease-out 1s;
        -moz-transition: ease-out 1s;
        -webkit-transition: ease-out 1s;
        -o-transition: ease-out 1s;
    }

    .notif-logo {
        float: left;
        padding-right: 16px
    }

    .notif-text {
        box-shadow: inset 0 0 5px 0 rgba(0, 0, 0, .5);
        background-color: #eee;
        padding: 16px
    }

    .notif-lead {
        font-size: 16px;
        font-weight: 700;
        text-align: center
    }

    .notif-lead span {
        font-size: 12px;
        font-weight: 500;
    }

    .notif-form {
        margin-top: 16px;
        text-align: center;
    }

    .notif-yes {
        display: inline-block;
        background-color: #282828;
        color: #fff;
        width: 80%;
        padding: 12px 16px;
        cursor: pointer;
        font-size: 14px;
        border-radius: 10px;
    }

    .notif-hide {
        display: none;
    }

    /* Adding at 11/09/2020 */
    .ads336 {
        width: 336px;
        height: auto;
        margin-left: auto;
        margin-right: auto
    }

    .ads300 {
        width: 300px;
        height: auto;
        margin-left: auto;
        margin-right: auto
    }

    .ads320 {
        width: 320px;
        height: auto;
        margin-left: auto;
        margin-right: auto
    }

    .slicknav {
        position: absolute;
        left: 24px;
        top: 12px;
        display: block !important;
        margin: 0 !important
    }

    header {
        z-index: 999;
        position: relative;
    }

    .logo-header {
        text-align: center
    }

    .slicknav i {
        font-size: 22px;
    }

    .slidenav li:nth-child(2n) {
        background-color: #EDEFF8;
    }

    .slidenav li a {
        color: #282828 !important;
        font-weight: 700 !important
    }

    .breadcrumb {
        font-size: 13px !important;
        text-transform: lowercase;
    }

    .breadcrumb li {
        padding-right: 8px !important;
        font-size: 13px !important;
    }

    .breadcrumb li a {
        color: #717171 !important
    }

    .article-news {
        margin-bottom: 16px !important;
        border: 5px solid #ffffff !important;
        box-shadow: 0 0 4px 0 rgba(0, 0, 0, .4);
    }

    .inner {
        padding: 8px 4px !important;
    }

    .article-news time {
        padding-bottom: 8px !important;
        font-size: 12px;
    }

    .article-news .caption {
        padding-bottom: 16px !important;
        color: #282828 !important;
    }

    .article-news .title a {
        display: block;
        padding-bottom: 8px !important;
        font-size: 22px !important;
    }

    .terpopuler li:after {
        clear: both;
        content: "";
        display: table
    }

    .terpopuler li {
        margin-bottom: 4px;
        background-color: #fff;
        border-bottom: 1px solid #D9F0FB;
    }

    .terpopuler li:hover {
        border-bottom: 1px solid #AEDFF7
    }

    .popular-num {
        float: left;
        padding: 16px 8px 8px 16px;
        font-weight: 700;
        font-size: 32px;
        color: #0272A8;
        font-family: helvetica, sans-serif;
        font-style: italic;
        vertical-align: middle;
        position: relative;
        text-shadow: 0 0 5px rgba(255, 255, 255, .5);
    }

    .popular-title {
        overflow: hidden
    }

    .popular-title a {
        display: block;
        padding: 12px 16px;
        font-size: 15px;
        font-weight: 700;
        color: #0272A8;
    }

    .popular-title a:hover {
        color: #039BE5
    }

    .title-gensindo {
        width: 100% !important
    }

    .trending li {
        padding: 0 !important;
        border-bottom: 1px solid #E2E5F4;
    }

    .trending li:nth-child(2n) {
        background-color: #EDEFF8;
    }

    .trending ul li a {
        display: block;
        padding: 12px 16px
    }

    .agenda {
        margin-top: 0 !important
    }

    .text-msg {
        padding: 16px;
        font-size: 14px;
        font-family: tahoma, sans-serif;
        color: #141A3B;
        font-weight: 500;
    }

    .text-msg strong {
        font-weight: 700
    }

    .tab .tab_content {
        padding: 16px 8px !important;
    }

    .tabs_item {
        padding: 0 !important
    }

    .news-load {
        text-align: center;
        padding: 8px
    }

    .news-more {
        margin: 0 auto;
        text-align: center
    }

    .news-more a {
        display: inline-block;
        background-color: #E71E26;
        color: #ffffff;
        font-size: 16px;
        padding: 12px 16px;
        width: 60%;
        border-radius: 8px;
    }

    .news-more a:hover {
        background-color: #EC4C52
    }

    .bottom-block {
        width: 100%;
        left: 0;
        bottom: 0;
        position: fixed
    }

    .bottom-float {
        margin: 0 auto;
        width: 100%
    }

    .bottom-float img {
        display: block
    }

    .bottom-close {
        text-align: right;
        color: #333;
        cursor: pointer;
        width: 320px;
        margin: 0 auto
    }

    .bottom-close i {
        font-size: 20px
    }

    .xhide {
        display: none
    }

    .bottom-genie {
        height: 50px
    }

    .bottom-extra {
        width: 320px;
        margin: 0 auto
    }

    .detail time {
        padding: 0 !important
    }

    .detail .topic-suggest {
        padding-bottom: 0 !important
    }

    .topic-suggest li {
        padding-right: 0 !important;
        margin: 4px;
    }

    .topic-suggest li a {
        padding: 2px 8px !important;
        font-size: 13px !important;
    }

    .note-paging {
        text-align: center;
        color: #999999;
        font-size: 12px;
        margin: 40px 0 10px
    }

    .box-paging {
        text-align: center
    }

    .article-paging {
        /* box-shadow: 0 0 5px 0 rgba(0,0,0,.4); */
        /* -webkit-box-shadow: 0 0 5px 0 rgba(0,0,0,.4); */
        -moz-box-shadow: 0 0 5px 0 rgba(0, 0, 0, .4);
        display: inline-block;
        font-size: 16px;
        /* border: 2px solid #fff; */
        /* border-radius: 5px !important; */
        /* -webkit-border-radius: 5px; */
        -moz-border-radius: 5px;
        border-radius: 10px;
    }

    .article-paging:after {
        clear: both;
        display: table;
        content: ''
    }

    .article-paging li {
        display: inline-block
    }

    .article-paging li.active {
        background-color: #ffffff;
        color: #499AE8 !important;
        padding: 12px 22px;
    }

    .article-paging li a {
        display: block;
        background-color: #499AE8;
        color: #ffffff !important;
        padding: 12px 22px;
        text-decoration: none !important;

    }

    .article-paging li a:hover {
        background-color: #eeeeee;
        border-right: 1px solid #fff;
        border-left: 1px solid #fff;
        color: #499AE8 !important;
    }

    .content-show {
        margin: 32px auto 0;
        border-top: 4px solid #499AE8;
        width: 70%;
    }

    .content-show a {
        padding: 8px 16px;
        display: block;
        background-color: #499AE8;
        color: #ffffff !important;
        width: 60%;
        text-align: center;
        margin: 0 auto;
        font-size: 14px;
        text-decoration: none !important;
        border-bottom-right-radius: 8px;
        border-bottom-left-radius: 8px;
    }

    .content-show a:hover {
        background-color: #ffffff;
        color: #499AE8 !important;
        box-shadow: 0 0 5px 0 rgba(0, 0, 0, .5);
    }

    .article-next a {
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        -webkit-border-top-right-radius: 5px;
        -webkit-border-bottom-right-radius: 5px;
        -moz-border-top-right-radius: 5px;
        -moz-border-bottom-right-radius: 5px;
    }

    .article-prev a {
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        -webkit-border-top-left-radius: 5px;
        -webkit-border-bottom-left-radius: 5px;
        -moz-border-top-left-radius: 5px;
        -moz-border-bottom-left-radius: 5px;
    }

    .ads-geniee {
        margin-top: 16px;
        padding: 0 16px
    }

    .ads-geniee:after {
        display: table;
        clear: both;
        content: ""
    }

    .border {
        padding: 16px !important
    }

    .title-news h1 {
        font-size: 24px;
        line-height: 1.5;
        margin: 0;
        font-weight: 700;
    }

    .author,
    .detail time {
        margin-bottom: 8px;
        font-size: 12px;
    }

    figcaption {
        padding: 8px 0 !important
    }

    .detail .share {
        padding: 16px 0 !important
    }

    .link-article .head {
        font-size: 20px !important;
        padding-bottom: 4px;
        border-bottom: 2px solid #282828;
    }

    .link-article li .title a {
        color: #282828 !important;
        display: block;
        padding: 4px 8px;
        text-align: left;
    }

    .link-article {
        margin-bottom: 0 !important
    }

    .suggest-box:after {
        display: table;
        content: "";
        clear: both
    }

    .suggest-box {
        width: 90%;
        margin: 16px auto;
        background-color: #007aff;
        border-radius: 8px;
    }

    .suggest-pict {
        float: right;
        height: 70px;
        width: 70px;
        margin: 16px 16px 16px 0;
        overflow: hidden
    }

    .suggest-pict img {
        display: block;
        position: relative;
        transform: translate(-20%, 0);
    }

    .suggest-title {
        overflow: hidden
    }

    .suggest-title a {
        display: block;
        font-size: 14px;
        padding: 12px 16px;
        color: #ffffff;
    }

    .sindodisqus {
        padding: 10px;
        margin: 10px;
    }

    .paragraph img {
        width: 100%;
        height: auto;
    }

    .title-gensindo {
        text-align: center;
        margin: 0 auto;
        color: #007aff;
        font-size: 20px;
        font-weight: 700;
        text-align: center;
        background-image: none;
        border-bottom: 3px solid #007aff;
    }

    footer {
        position: unset;
    }

    #detail .paragraph a{
        display: inline-table;
    }

    #detail #social-media-share .head{
        justify-content: center;
    }

    #detail .paragraph span{
        font-weight: 700;
    }
    
    #detail .topic-suggest{
        margin: 15px auto;
       width: 90%;
    }

    #detail .topic-suggest li{
        display: inline-block;
        padding-right: 5px;
        font-size: 18px;
    } 
    #detail .topic-suggest li a{
        background-color: #007bff;
        color: white;
        border-radius: 5px;
        transition: all .3s ease-in-out;
    }

    #detail .topic-suggest li a:hover{
        background-color: #ffffff;
        color: #282828!important;
    }
    
    .paragraph {
        background-color: #ffffff;
        padding: 20px!important;
    }
</style>