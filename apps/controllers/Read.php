<?php
class Read extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->config('site_channel');
        $this->load->library('goarticle');
        $this->load->model('news/mgonews','mgonews');
    }
    function index(){
      
        if($this->mobiledevice->device_check()){
            $this->desktop();
        }else{
            $this->mobile();
        }
    }
    function desktop(){
        $a = array();
        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));
        $a['upVersion'] = $this->config->item('upVersioncssjs');  // for remove caching
        $a['html']['css'] = $this->load->view('destop2020/template/cssheader', $a, TRUE);
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        // $a['html']['bottom_css'] .= m_add_css('/css/font-awesome.min.css');
        $a['html']['js'] = add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/materialize.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/swiper.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_external_js(base_url().'gensindo/2020-destop/js/script.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/jquery.jscroll.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_external_js(base_url() . 'sn-v1.js'.$a['upVersion']);
        $a['html']['js'] .= add_external_js('https://sm.sindonews.net/mobile/2016/js/jquery.sticky.js');

        /*
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-app.js');
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-messaging.js');
        $a['html']['js'] .= add_external_js(base_url() . 'fcm.2020.js');
        */
        
        $a['html']['topjs'] = async_m_add_js('/js/prebid4.6.0-criteo.js');
        /* $a['html']['js'] .= add_external_js(base_url() . 'axcxnative.js');
          $a['html']['js'] .= add_external_js(base_url() . 'axcxfn.js'); */
        $a['template']['header'] = $this->load->view('destop2020/template/vheader',NULL,TRUE);
        // $a['template']['promoted'] = $this->load->view('destop2020/template/vpromoted',NULL,TRUE);
        $a['template']['minimenu'] = $this->load->view('destop2020/template/vminimenu',NULL,TRUE);
        $a['template']['kategori'] = $this->load->view('destop2020/template/vkategori',NULL,TRUE);
        $a['template']['newcss'] = $this->load->view('destop2020/template/vnewcss',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('destop2020/template/vfooter',NULL,TRUE);
        
        $a['template']['promogen'] = $this->load->view('destop2020/template/vpromogen',NULL,TRUE);
      
        $getarticle = $this->goarticle->getNewArticle();
    
        $t['details'] = $getarticle['details'];
        $parent_id = $getarticle['details']['id_kanal'];
        $content_id = $getarticle['details']['id_news'];
        $uriread = $this->goarticle->createPathUrl($t['details']);
        $getEditorData = $this->mgonews->getEditorByNews($content_id);
        $t['content_id'] = $content_id;
        $t['site_url'] = site_url('read') . '/' . $uriread;
        $a['content']['site_url'] = site_url('read') . '/' . $uriread;
        $a['content']['site_publish'] = date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']));
        $a['content']['site_reporter'] = $t['details']['author'];
        $a['content']['site_editor'] = ucwords($getEditorData[0]['fullname']);
        $a['content']['channel_id'] = $t['details']['id_subkanal'];
        $a['content']['channel_name'] = $t['details']['subkanal'];
        $a['content']['amp_url'] = site_url('newsread') . '/' . $uriread;
        $a['content']['forum_url'] = site_url('desktop-forum') . '/' . $uriread;
        $a['content']['disqus_url'] = site_url('disqus') . '/' . $uriread;
        $a['content']['path_url'] = '/read/' . $uriread;
        $a['content']['content_id'] = $content_id;
        $a['content']['date_publish'] = date('Ymd',strtotime($t['details']['date_published']));
        $a['content']['time_publish'] = date('H:i',strtotime($t['details']['date_published']));
        $a['content']['site_label'] = textlimit($t['details']['title'], 40);
  
        $t['parent_id'] = $parent_id;
        $a['content']['parent_id'] = $parent_id;
        $a['content']['content_id'] = $content_id;
        $topic_id_list = '';
        $topic_keyword = array();
        $getRelated = array();
 
        $getbaca = array();
        $getTopicData = array();
        $getTopic = $this->mgonews->getTopicContent($content_id);
        if(count($getTopic) > 0){
            $topic_id_list = $getTopic[0]['id_topik'];
            if($topic_id_list != ''){
                $getRelated = $this->mgonews->getRelatedContent($topic_id_list,$parent_id,$content_id,10);
                //$getbaca = $this->mgonews->getRelatedContent($topic_id_list,$parent_id,$content_id,16);
                $getbaca = $getRelated;
                
                $topicData = explode(',',$topic_id_list);
                for($i = 0; $i < count($topicData); $i++){
                    $getTopicData[$i] = $this->mgonews->getTopicList($topicData[$i]);
                    $topic_keyword[$i] = $getTopicData[$i][0]['topik'];
                }
            }
        }
        $t['topiklist'] = $topic_id_list;
        $t['related'] = $getRelated;
        $a['content']['topic_list'] = $topic_id_list;
        
        $relatedLink = array();
        if(count($getRelated) > 0 && count($getRelated) > 3){
            for($i = 0; $i < 3; $i++){
                $site_domain[$i] = $this->config->item('read_domain');
                $dtime[$i] = strtotime($getRelated[$i]['date_created']);
                $relatedLink[$i] = 'https://' . $site_domain[$i][$getRelated[$i]['id_subkanal']] . '/read/' . $getRelated[$i]['id_news'] . '/' . $getRelated[$i]['id_subkanal'] . '/' . slug($getRelated[$i]['title']) . '-' . $dtime[$i];
            }
        }
        $a['content']['relatedlink'] = $relatedLink;
        /* --- start article pagination --- */
        $baca = array();
        if(count($getbaca) > 0 && count($getbaca) > 3){
            //$databaca = array_slice($getbaca,10,16);
            //$baca = randomData($databaca,6,2);
            $baca = randomData($getbaca,9,3);
        }
       
        $totalData = $this->goarticle->getArticleNumListPageBacaDesktop($t['details'],$baca);
        $start = $this->uri->segment(5,0);
        $defaultpage = 100;
        if(isset($_GET['showpage']) && $_GET['showpage'] == 'all'){
            $offset = 100;
        }else{
            if($totalData > ($defaultpage + 2)){
                $offset = $defaultpage;
            }else{
                $offset = 100;
            }
        }
        
        $t['content'] = $this->goarticle->getArticleListPageBacaDesktop($start,$offset,$t['details'],$baca);
        $t['totalData'] = $totalData;
        $t['per_page'] = $offset;
        $t['start'] = $start;
        if($t['totalData'] <= $offset && $start > 0){
            redirect($t['site_url'],'location',301);
        }
        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('artpaging');
        $config['base_url'] = site_url('read/' . $uriread);
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();
        $currentPage = floor(($start / $config['per_page']) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;
        if($start == 0 || $start == ''){
            $pagetitle = '';
        }
        $t['current_page'] = $currentPage;
        $t['last_page'] = floor(($t['totalData'] / $config['per_page']) + 1);
        /* create canonical URL */
        $paging_status = 0;
        if($t['totalData'] > ($defaultpage + 3)){
            $paging_status = 1;            
        }
        
        $canonical = add_canonical('canonical',$t['site_url']);
        $a['html']['canonical'] = $canonical;

        /* --- end article pagination --- */
        /* ---- start SEO configuration ---- */
        if(isset($t['details']['photo'])){
            $imgmeta = $this->config->item('images_uri') . '/dyn/620/pena/news/' . date('Y/m/d',strtotime($t['details']['date_created'])) . '/' . $t['details']['id_subkanal'] . '/' . $t['details']['id_news'] . '/' . $t['details']['photo'];
        }else{
            $imgmeta = '';
        }
        $a['content']['site_image'] = $imgmeta;
        
        if($t['details']['author'] != ''){
            $reporter = ucwords($t['details']['author']);
        }else{
            $reporter = '';
        }
        
        /* create keyword tags */
        $content_keyword = trim(strtolower($t['details']['keyword']));
       
        if($content_keyword == ''){
            $article_keyword = '';
        }else{
            $article_keyword = ',' . $content_keyword;
        }
        $get_topic_keyword = implode(',',$topic_keyword);
        $keyword = $this->goarticle->newsKeywords($get_topic_keyword . $article_keyword);
        
        $t['keyword'] = $keyword;
        $rich_keywords = explode(',',$keyword);
        $a['content']['keyword'] = $rich_keywords;
        $a['content']['site_tags'] = $keyword;
        $extend_desc = summaryMore($t['details']['content']);
        if(strlen($extend_desc) < 120){
            $article_summary = $t['details']['summary'] . ' ' . $extend_desc;
        }else{
            $article_summary = $t['details']['summary'];
        }
        /* title & description filtering */
        //$metatitle = seo_title($t['details']['title']);
        $metatitle = cleanTitleArticle($t['details']['title']);
        $metadesc = seo_description($article_summary);
        $a['content']['site_title'] = $metatitle;
        $a['content']['site_summary'] = $metadesc;
        
        $a['html']['title'] = $metatitle . $pagetitle;
        $metaname = array(
            'description' => $metadesc . $pagetitle,
            'title' => $metatitle . $pagetitle,
            'image_src' => $imgmeta,
            'keywords' => $keyword,
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );
        $metafb = array(
            'title' => cleanWords($t['details']['title']),
            'type' => 'article',
            'url' => $t['site_url'],
            'image' => $imgmeta,
            'description' => cleanWords($t['details']['summary']),
            'site_name' => 'SINDOnews.com'
        );
        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@SINDOnews',
            'twitter:creator' => '@SINDOnews',
            'twitter:title' => cleanWords($t['details']['title']),
            'twitter:description' => cleanWords($t['details']['summary']),
            'twitter:image' => $imgmeta
        );

        $metadable = array(
            'item_id' => $content_id,
            'title' => cleanWords($t['details']['title']),
            'image' => $imgmeta,
            'author' => $reporter
        );

        if(count($getTopicData) > 1){
            $section_a = trim($getTopicData[0][0]['topik']);
            $section_b = trim($getTopicData[1][0]['topik']);

            $metaArticle = array(
                'section' => strtolower($t['details']['subkanal']),
                'section2' => $section_a,
                'section3' => $section_b,
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }else{
            $metaArticle = array(
                'section' => strtolower($t['details']['subkanal']),
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }
        
        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);
        $a['html']['metaname'] .= metaDable($metadable);
        $a['html']['metaname'] .= metaArticle($metaArticle);

        /* ---- end SEO configuration ---- */
        
        /* GA Custom Dimension Start */
        $a['content']['cd_title'] = cleanTitleArticle($t['details']['title']) . $pagetitle;
        $a['content']['cd_subkanal'] = customDimensionString($a['content']['channel_name']);
        $a['content']['cd_author'] = ucwords($a['content']['site_reporter']);
        $a['content']['cd_editor'] = ucwords($a['content']['site_editor']);
        $a['content']['cd_tags'] = customDimensionTags($a['content']['site_tags']);
        $a['content']['cd_publish'] = $t['details']['date_published'];
        if($t['totalData'] > $offset){
            $cd_page = $currentPage;
        }else{
            $cd_page = 0;
        }
        $a['content']['cd_page'] = $cd_page;
        /* GA Custom Dimension End */
        
        $t['others'] = $this->mgonews->getListNews($parent_id,30);
        
        $a['content']['rich'] = '';
        $contentrich = $t['others'];
        if(count($contentrich) > 0){
            $snippet = array();
            for($i = 0; $i < 3; $i++){
                $snippet[$i]['@type'] = 'ListItem';
                $snippet[$i]['position'] = $i + 1;
                $dtime[$i] = strtotime($contentrich[$i]['date_created']);
                $urlsnippet[$i] = site_url('read') . '/' . $contentrich[$i]['content_id'] . '/' . $contentrich[$i]['channel_id'] . '/' . slug($contentrich[$i]['title']) . '-' . $dtime[$i];
                $snippet[$i]['url'] = $urlsnippet[$i];
            }
            $a['content']['rich'] = json_encode($snippet);
        }
        $t['trending'] = $this->mgonews->getKanalTrendingTopic($parent_id,7);
        $a['template']['trending'] = $this->load->view('destop2020/template/vtrending',$t,TRUE);
        $t['trendingpopular'] = $this->mgonews->getListTitlePopular($parent_id,5);
        $a['template']['popular'] = $this->load->view('destop2020/template/vpopular',$t,TRUE);
        $t['event']['results'] = array();
        $a['template']['agenda'] = $this->load->view('template/vagenda',$t,TRUE);
        $a['content']['breadcrumb'] = $this->load->view('destop2020/read/vbreadcrumb',$t,TRUE);
        $a['content']['detail'] = $this->load->view('destop2020/read/vdetail',$t,TRUE);
        $a['content']['relatedtopic'] = $this->load->view('destop2020/read/vrelatedtopic',$t,TRUE);
        // $a['content']['profileeditor'] = $this->load->view('read/vprofileeditor',$t,TRUE);
        $a['content']['relatedcontent'] = $this->load->view('destop2020/read/vrelatedcontent',$t,TRUE);
        $a['content']['comment'] = $this->load->view('destop2020/read/vcomment',$t,TRUE);
        //$a['content']['others'] = $this->load->view('read/vothers',$t,TRUE);
        // echo "<pre>"; print_r(); echo "</pre>";die();
        $t['sindonews_terkini'] = $this->mgonews->getListNews($parent_id,10);
        $a['content']['sindonewsterkini'] = $this->load->view('destop2020/read/vsindonewsterkini', $t,TRUE);
        /* Hits Token */
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $token_encode = base64_encode($token);
        $a['content']['code'] = $token_encode;
        $a['content']['random'] = generateRandomString(11);
        $a['content']['id_content'] = $t['details']['id_news'];
        $this->load->view('destop2020/pages/vread',$a,FALSE);
    }


    function mobile(){
        $a = array();
        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));
        
        $a['upVersion'] = $this->config->item('upVersioncssjs');  // for remove caching
        $a['html']['css'] = $this->load->view('mobile2020/template/cssheader', $a, TRUE);
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['js'] = m_add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/jquery.fancybox.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/swiper.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/script.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_external_js(base_url() . 'sn-v1.js'.$a['upVersion']);
        $a['html']['js'] .= add_external_js('https://sm.sindonews.net/mobile/2016/js/jquery.sticky.js');

        $a['html']['topjs'] = async_m_add_js('/js/prebid4.6.0-criteo.js'.$a['upVersion']);
     



        $a['template']['header'] = $this->load->view('mobile2020/template/vheader', NULL, TRUE);
        $a['template']['menuoverlay'] = $this->load->view('mobile2020/template/vmenuoverlay', NULL, TRUE);
        $a['template']['sosialmedia'] = $this->load->view('mobile2020/template/vsosialmedia', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('mobile2020/template/vnewcss',NULL,TRUE);
        $a['template']['subkanal'] = $this->load->view('mobile2020/template/vsubkanalmenu', NULL, TRUE);
        $a['template']['footer'] = $this->load->view('mobile2020/template/vfooter', NULL, TRUE);
    
        $getarticle = $this->goarticle->getNewArticle();
        $t['details'] = $getarticle['details'];
        $parent_id = $getarticle['details']['id_kanal'];
        $content_id = $getarticle['details']['id_news'];
        $uriread = $this->goarticle->createPathUrl($t['details']);
        $getEditorData = $this->mgonews->getEditorByNews($content_id);
        $t['content_id'] = $content_id;
        $t['site_url'] = site_url('read') . '/' . $uriread;
        $t['share_url'] = $t['site_url'];
        $t['short_url'] = $t['site_url'];
        $a['content']['site_url'] = site_url('read') . '/' . $uriread;
        $a['content']['site_publish'] = date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']));
        $a['content']['site_reporter'] = $t['details']['author'];
        $a['content']['site_editor'] = ucwords($getEditorData[0]['fullname']);
        $a['content']['channel_id'] = $t['details']['id_subkanal'];
        $a['content']['channel_name'] = $t['details']['subkanal'];
        $a['content']['amp_url'] = site_url('newsread') . '/' . $uriread;
        $a['content']['forum_url'] = site_url('forum') . '/' . $uriread;
        $a['content']['disqus_url'] = site_url('mdisqus') . '/' . $uriread;
        $a['content']['path_url'] = '/read/' . $uriread;
        $a['content']['content_id'] = $content_id;
        $a['content']['date_publish'] = date('Ymd',strtotime($t['details']['date_published']));
        $a['content']['time_publish'] = date('H:i',strtotime($t['details']['date_published']));
       $a['co ntent']['site_label'] = textlimit($t['details']['title'], 40);
  
        $t['parent_id'] = $parent_id;
        $a['content']['parent_id'] = $parent_id;
        $a['content']['content_id'] = $content_id;
        $topic_id_list = '';
        $topic_keyword = array();
        $getRelated = array();
     
        $getbaca = array();
        $getTopicData = array();
        $getTopic = $this->mgonews->getTopicContent($content_id);
        if(count($getTopic) > 0){
            $topic_id_list = $getTopic[0]['id_topik'];
            if($topic_id_list != ''){
                $getRelated = $this->mgonews->getRelatedContent($topic_id_list,$parent_id,$content_id,10);
                //$getbaca = $this->mgonews->getRelatedContent($topic_id_list,$parent_id,$content_id,16);
                $getbaca = $getRelated;
                
                $topicData = explode(',',$topic_id_list);
                for($i = 0; $i < count($topicData); $i++){
                    $getTopicData[$i] = $this->mgonews->getTopicList($topicData[$i]);
                    $topic_keyword[$i] = $getTopicData[$i][0]['topik'];
                }
            }
        }
        $t['topiklist'] = $topic_id_list;
        $t['related'] = $getRelated;
        $a['content']['topic_list'] = $topic_id_list;
        
        $relatedLink = array();
        if(count($getRelated) > 0 && count($getRelated) > 3){
            for($i = 0; $i < 3; $i++){
                $site_domain[$i] = $this->config->item('read_domain');
                $dtime[$i] = strtotime($getRelated[$i]['date_created']);
                
                $relatedLink[$i] = 'https://' . $site_domain[$i][$getRelated[$i]['id_subkanal']] . '/read/' . $getRelated[$i]['id_news'] . '/' . $getRelated[$i]['id_subkanal'] . '/' . slug($getRelated[$i]['title']) . '-' . $dtime[$i];
              
            }
        }
        $a['content']['relatedlink'] = $relatedLink;
        $suggest = $this->mgonews->getSuggest($topic_id_list,$parent_id,$content_id);
        $s_rand = rand(0,count($suggest) - 1);
        $t['suggest'] = $suggest[$s_rand];
        /* --- start article pagination --- */
        $baca = array();
        if(count($getbaca) > 0 && count($getbaca) > 3){
            //$databaca = array_slice($getbaca,10,16);
            //$baca = randomData($databaca,6,2);
            $baca = randomData($getbaca,9,3);
        }

        $totalData = $this->goarticle->getArticleNumListPageBaca($t['details'],$baca);
        $start = $this->uri->segment(5,0);
        $defaultpage = 100;
        if(isset($_GET['showpage']) && $_GET['showpage'] == 'all'){
            $offset = 100;
        }else{
            if($totalData > ($defaultpage + 2)){
                $offset = $defaultpage;
            }else{
                $offset = 100;
            }
        }
        $t['content'] = $this->goarticle->getArticleListPageBaca($start,$offset,$t['details'],$baca,array());
        $t['totalData'] = $totalData;
        $t['per_page'] = $offset;
        $t['start'] = $start;
        if($t['totalData'] <= $offset && $start > 0){
            redirect($t['site_url'],'location',301);
        }
        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('artpaging');
        $config['base_url'] = site_url('read/' . $uriread);
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 5;
        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();
        $currentPage = floor(($start / $config['per_page']) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;
        if($start == 0 || $start == ''){
            $pagetitle = '';
        }
        $t['current_page'] = $currentPage;
        $t['last_page'] = floor(($t['totalData'] / $config['per_page']) + 1);
        /* create canonical URL */
        $paging_status = 0;
        if($t['totalData'] > ($defaultpage + 3)){
            $paging_status = 1;            
        }

        $canonical = add_canonical('canonical',$t['site_url']);
        $a['html']['canonical'] = $canonical;
        
        /* --- end article pagination --- */
        /* ---- start SEO configuration ---- */
        if(isset($t['details']['photo'])){
            $imgmeta = $this->config->item('images_uri') . '/dyn/620/pena/news/' . date('Y/m/d',strtotime($t['details']['date_created'])) . '/' . $t['details']['id_subkanal'] . '/' . $t['details']['id_news'] . '/' . $t['details']['photo'];
        }else{
            $imgmeta = '';
        }
        $a['content']['site_image'] = $imgmeta;
        
        if($t['details']['author'] != ''){
            $reporter = ucwords($t['details']['author']);
        }else{
            $reporter = '';
        }
        // echo "<pre>"; print_r($t); echo "</pre>"; die();
        
        
        /* create keyword tags */
        $content_keyword = trim(strtolower($t['details']['keyword']));
        if($content_keyword == ''){
            $article_keyword = '';
        }else{
            $article_keyword = ',' . $content_keyword;
        }
        $get_topic_keyword = implode(',',$topic_keyword);
        $keyword = $this->goarticle->newsKeywords($get_topic_keyword . $article_keyword);
        $t['keyword'] = $keyword;
        $rich_keywords = explode(',',$keyword);
        $a['content']['keyword'] = $rich_keywords;
        $a['content']['site_tags'] = $keyword;
        $extend_desc = summaryMore($t['details']['content']);
        if(strlen($extend_desc) < 120){
            $article_summary = $t['details']['summary'] . ' ' . $extend_desc;
        }else{
            $article_summary = $t['details']['summary'];
        }
        /* title & description filtering */
        //$metatitle = seo_title($t['details']['title']);
        $metatitle = cleanTitleArticle($t['details']['title']);
        $metadesc = seo_description($article_summary);
        $a['content']['site_title'] = $metatitle;
        $a['content']['site_summary'] = $metadesc;
        
        $a['html']['title'] = $metatitle . $pagetitle;
        $metaname = array(
            'description' => $metadesc . $pagetitle,
            'title' => $metatitle . $pagetitle,
            'image_src' => $imgmeta,
            'keywords' => $keyword,
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );
        $metafb = array(
            'title' => cleanWords($t['details']['title']),
            'type' => 'article',
            'url' => $t['site_url'],
            'image' => $imgmeta,
            'description' => cleanWords($t['details']['summary']),
            'site_name' => 'SINDOnews.com'
        );
        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@SINDOnews',
            'twitter:creator' => '@SINDOnews',
            'twitter:title' => cleanWords($t['details']['title']),
            'twitter:description' => cleanWords($t['details']['summary']),
            'twitter:image' => $imgmeta
        );
        $metadable = array(
            'item_id' => $content_id,
            'title' => cleanWords($t['details']['title']),
            'image' => $imgmeta,
            'author' => $reporter
        );
        if(count($getTopicData) > 1){
            $section_a = trim($getTopicData[0][0]['topik']);
            $section_b = trim($getTopicData[1][0]['topik']);
            $metaArticle = array(
                'section' => 'generasi milenial',
                'section2' => $section_a,
                'section3' => $section_b,
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }else{
            $metaArticle = array(
                'section' => 'generasi milenial',
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }
        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);
        $a['html']['metaname'] .= metaDable($metadable);
        $a['html']['metaname'] .= metaArticle($metaArticle);
        /* ---- end SEO configuration ---- */
        
        /* GA Custom Dimension Start */
        $a['content']['cd_title'] = cleanTitleArticle($t['details']['title']) . $pagetitle;
        $a['content']['cd_subkanal'] = customDimensionString($a['content']['channel_name']);
        $a['content']['cd_author'] = ucwords($a['content']['site_reporter']);
        $a['content']['cd_editor'] = ucwords($a['content']['site_editor']);
        $a['content']['cd_tags'] = customDimensionTags($a['content']['site_tags']);
        $a['content']['cd_publish'] = $t['details']['date_published'];
        if($t['totalData'] > $offset){
            $cd_page = $currentPage;
        }else{
            $cd_page = 0;
        }
        $a['content']['cd_page'] = $cd_page;
        /* GA Custom Dimension End */
        
        $t['others'] = $this->mgonews->getListNews($parent_id,10);
        
        $a['content']['rich'] = '';
        $contentrich = $t['others'];
        if(count($contentrich) > 0){
            $snippet = array();
            for($i = 0; $i < 3; $i++){
                $snippet[$i]['@type'] = 'ListItem';
                $snippet[$i]['position'] = $i + 1;
                $dtime[$i] = strtotime($contentrich[$i]['date_created']);
                $urlsnippet[$i] = site_url('read') . '/' . $contentrich[$i]['content_id'] . '/' . $contentrich[$i]['channel_id'] . '/' . slug($contentrich[$i]['title']) . '-' . $dtime[$i];
                $snippet[$i]['url'] = $urlsnippet[$i];
            }
            $a['content']['rich'] = json_encode($snippet);
        }
        $t['trending'] = $this->mgonews->getKanalTrendingTopic($parent_id,7);
        $a['template']['trending'] = $this->load->view('mobile2020/template/vtrending',$t,TRUE);
        $a['content']['detail'] = $this->load->view('mobile2020/read/vdetail',$t,TRUE);
        $t['trendingpopular'] = $this->mgonews->getListTitlePopular($parent_id,5);
        $a['template']['popular'] = $this->load->view('mobile2020/template/vpopular',$t,TRUE);
        $t['sindonews_terkini'] = $this->mgonews->getListNews($parent_id,10);
        $a['content']['sindonews_terkini'] = $this->load->view('mobile2020/read/vsindonewsterkini',$t,true);
        // $a['content']['others'] = $this->load->view('mobile/read/vothers',$t,TRUE);
        $a['content']['breadcrumb'] = $this->load->view('mobile2020/read/vbreadcrumb',$t,TRUE);
        $a['content']['relatedtopic'] = $this->load->view('mobile2020/read/vrelatedtopic',$t,TRUE);
        // $a['content']['profileeditor'] = $this->load->view('mobile/read/vprofileeditor',$t,TRUE)
        
     
        $a['content']['relatedcontent'] = $this->load->view('mobile2020/read/vrelatedcontent',$t,TRUE);
        $a['content']['comment'] = $this->load->view('mobile2020/read/vdisqus',$t,TRUE);
 
        /* Hits Token */
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $token_encode = base64_encode($token);
        $a['content']['code'] = $token_encode;
        $a['content']['random'] = generateRandomString(11);
        $a['content']['id_content'] = $t['details']['id_news'];
        // echo "<pre>"; print_r($t['content']); echo "</pre>"; die();
        
        $this->load->view('mobile2020/pages/vread',$a,FALSE);
    }
}
