<?php

$total = count($related);
if($total > 0){
    echo '<div class="link-article">';
    echo '<div class="head">Artikel Terkait</div>';
    echo '<div class="photo-box-list">';
    for($i = 0; $i < $total; $i++){
        $site_domain[$i] = $this->config->item('read_domain');
        $dtime[$i] = strtotime($related[$i]['date_created']);
        $l[$i] = 'https://' . $site_domain[$i][$related[$i]['id_subkanal']] . '/read/' . $related[$i]['id_news'] . '/' . $related[$i]['id_subkanal'] . '/' . slug($related[$i]['title']) . '-' . $dtime[$i];
        $title[$i] = cleanWords($related[$i]['title']);
        $subkanal[$i] = $related[$i]['subkanal'];
        
        if($related[$i]['photo'] != ''){
            $img[$i] = images_uri() . '/dyn/287/pena/news/' . date('Y/m/d',strtotime($related[$i]['date_created'])) . '/' . $related[$i]['id_subkanal'] . '/' . $related[$i]['id_news'] . '/' . $related[$i]['photo'];
            $showimg[$i] = '
                <div class="photo-box-pict">
                    <a href="' . $l[$i] . '" class="image"><img class="lazyload" data-src="' . $img[$i] . '" alt="' . cleanHTML($title[$i]) . '"></a>
                </div>';
        }else{
            $showimg[$i] = '';
        }

        echo '
            <div class="photo-box-item">
                ' . $showimg[$i] . '
                <div class="photo-box-text"><a href="' . $l[$i] . '">' . $title[$i] . '</a></div>
            </div>';
    }
    echo '</div>';
    echo '</div>';
}
