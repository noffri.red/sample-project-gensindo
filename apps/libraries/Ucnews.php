<?php

class Ucnews {

    var $CI;
    var $site_channel;
    var $site_domain;

    function __construct(){
        $this->CI = & get_instance();
        $this->site_channel = $this->CI->config->item('site_channel');
        $this->site_domain = $this->CI->config->item('read_domain');
    }

    function getNewArticle(){
        $params = array();
        $current_url = parse_url(current_url());
        $kanal = $current_url['host'];
        $path = $current_url['path'];

        $getpath = array_values(array_filter(explode('/',$path)));
        $getdata = $this->checkDb($getpath[1],$getpath[2]);
        $content_id = $getdata[0]['id_news'];
        $channel_id = $getdata[0]['id_subkanal'];

        $urldate = strtotime($getdata[0]['date_created']);
        $uripath = $content_id . '/' . $channel_id . '/' . slug($getdata[0]['title']) . '-' . $urldate;

        $this->fixKanal($kanal,$channel_id,$uripath);
        $this->fixChannelID($getpath[2],$channel_id,$uripath);

        $timestamp = $this->getTimestamp($getpath[3]);
        $this->fixTimestamp($timestamp,$channel_id,$uripath);

        $getslug = $this->fixTitleSlug($getpath[3],$getdata);

        $params['year'] = date('Y',strtotime($getdata[0]['date_created']));
        $params['month'] = date('m',strtotime($getdata[0]['date_created']));
        $params['day'] = date('d',strtotime($getdata[0]['date_created']));
        $params['id_subkanal'] = $channel_id;
        $params['id_news'] = $content_id;
        $params['slug_title'] = $getslug;
        $params['date_created'] = date('Y/m/d',strtotime($getdata[0]['date_created']));

        $getarticle = $this->CI->mgonews->getJsonDetail($params);
        if(count($getarticle) > 0){
            $article['details'] = $getarticle[0];
            $article['content_id'] = $params['id_news'];
        }else{
            $getDbArticle = $this->CI->mgonews->getDbDetail($params['id_news']);
            $article['details'] = $getDbArticle[0];
            $article['content_id'] = $content_id;
        }

        return $article;
    }

    function fixKanal($kanal,$channel_id,$uripath){
        $urlsite = $this->site_domain[$channel_id];
        if($urlsite != $kanal){
            $urlgo = 'https://' . $this->site_domain[$channel_id] . '/uc/' . $uripath;
            redirect($urlgo,'location',301);
        }
    }

    function fixChannelID($cid_url,$channel_id,$uripath){
        if($cid_url != $channel_id){
            $urlgo = 'https://' . $this->site_domain[$channel_id] . '/uc/' . $uripath;
            redirect($urlgo,'location',301);
        }
    }

    function fixTimestamp($timestamp,$channel_id,$uripath){
        if(count($timestamp) == 0){
            $urlgo = 'https://' . $this->site_domain[$channel_id] . '/uc/' . $uripath;
            redirect($urlgo,'location',301);
        }
    }

    function checkDb($content_id,$channel_id){
        $check = $this->CI->mgonews->getNewsSimple($content_id,$channel_id);
        if(count($check) > 0){
            return $check;
        }else{
            $getAtmaja = $this->CI->mgonews->getSimpleAtmaja($content_id,$channel_id);
            if(count($getAtmaja) > 0){
                $newURL = site_url('uc') . '/' . $getAtmaja[0]['content_id'] . '/' . $getAtmaja[0]['channel_id'] . '/' . slug($getAtmaja[0]['title']);
                redirect($newURL,'location',301);
            }else{
                redirect(base_url(),'location',301);
            }
        }
    }

    function xmlExist($params){
        $check = $this->CI->mgonews->getXmlDetail($params);
        if(count($check) > 0){
            return TRUE;
        }else{
            return FALSE;
        }
    }

    function getArraySlug($title_slug){
        $array_slug = explode('-',$title_slug);
        return $array_slug;
    }

    function getTimestamp($title_slug){
        $date_path = array();
        $array_slug = $this->getArraySlug($title_slug);
        if(is_numeric(end($array_slug))){
            if(date('Y',end($array_slug)) > 1970){
                $last_slug = date('Y-m-d',end($array_slug));
                $date_path = explode('-',$last_slug);
            }
        }

        return $date_path;
    }

    function fixTitleSlug($slug,$getdata){
        $dtime = strtotime($getdata[0]['date_created']);
        $data_slug = slug($getdata[0]['title']) . '-' . $dtime;
        $new_slug = '';
        if($slug == '' || $slug != $data_slug){
            $newurl = site_url('uc') . '/' . $getdata[0]['id_news'] . '/' . $getdata[0]['id_subkanal'] . '/' . slug($getdata[0]['title']) . '-' . $dtime;
            redirect($newurl,'location',301);
        }else{
            $new_slug = $data_slug;
        }

        return $new_slug;
    }

    function fixChannelUrl($details){
        $uriread = $this->createPathUrl($details);
        $urlsite = 'https://' . $this->site_channel[$details['id_subkanal']] . '/';
        $urlgo = '';

        if($urlsite != base_url()){
            $urlgo = 'https://' . $this->site_domain[$details['id_subkanal']] . '/newsread/' . $uriread;
            redirect($urlgo,'location',301);
        }
    }

    function createPathUrl($details){
        $real_timestamp = strtotime($details['date_created']);
        $uriread = $details['id_news'] . '/' . $details['id_subkanal'] . '/' . slug($details['title']) . '-' . $real_timestamp;
        return $uriread;
    }

    function urlGo($details){
        $uriread = $this->createPathUrl($details);
        $urlsite = 'https://' . $this->site_domain[$details['id_subkanal']] . '/';
        if($urlsite != base_url()){
            $urlgo = 'https://' . $this->site_domain[$details['id_subkanal']] . '/newsread/' . $uriread;
        }else{
            $urlgo = base_url();
        }
        return $urlgo;
    }

    function getArticleListPage($start,$offset,$details){
        $content = ampFilter(html_entity_decode($details['content']),$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $cut_content = array_slice($xcontent,$start,$offset);
        $article = implode('<br><br>',$cut_content);

        return $article;
    }

    function getArticleNumListPage($details){
        $content = ampFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $count_exp = count($xcontent);

        return $count_exp;
    }
    
    function getBacaJuga($baca){
        $baca_inline = '';
        $totalbaca = count($baca);

        $baca_inline .= '
            <div class="ads300 mb20">
                <div id="div-gpt-ad-middle_2"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_2\');});</script></div>
            </div>';

        if($totalbaca > 0){
            $baca_inline .= '<div class="baca-inline">';
            $baca_inline .= '<div class="baca-inline-head">Baca Juga:</div>';
            $baca_inline .= '<ul>';
            for($i = 0; $i < $totalbaca; $i++){
                $dtime[$i] = strtotime($baca[$i]['date_created']);
                $urlbaca[$i] = site_url('read') . '/' . $baca[$i]['id_news'] . '/' . $baca[$i]['id_subkanal'] . '/' . slug($baca[$i]['title'] . '-' . $dtime[$i]);
                $titlebaca[$i] = cleanWords($baca[$i]['title']);
                $baca_inline .= '<li><a href="' . $urlbaca[$i] . '">' . $titlebaca[$i] . '</a></li>';
            }

            //$baca_inline .= '<li><a href="https://autotekno.sindonews.com/read/1467437/120/akhir-tahun-saatnya-wujudkan-resolusi-mobil-baru-1576046074?utm_source=sindonews-baca&utm_medium=cpc&utm_campaign=belimobilgue.co.id" target="_blank">Akhir Tahun, Saatnya Wujudkan Resolusi Mobil Baru</a></li>';
            $baca_inline .= '</ul>';
            $baca_inline .= '</div>';
        }

        $result = array($baca_inline);
        return $result;
    }

    function getBacaJugaNum($baca){
        $baca_inline = '';
        $totalbaca = count($this->getBacaJuga($baca));

        $result = array();
        if($totalbaca > 0){
            $baca_inline = '<div class="baca-inline"></div>';
            $result = array($baca_inline);
        }

        return $result;
    }

    function cekArticleParagraph($content,$baca){
        $limit = 4;
        $total = count($content);
        $newcontent = $content;
        if($total > $limit){
            $position = 4;
            $slice_first = array_slice($content,0,$position);
            $offset_two = ($total - $position);
            $slice_two = array_slice($content,$position,$offset_two);

            $bacaJuga = $this->getBacaJuga($baca);
            $totalbaca = count($bacaJuga);
            if($totalbaca > 0){
                $newcontent = array_values(array_merge($slice_first,$bacaJuga,$slice_two));
            }
        }

        return $newcontent;
    }

    function cekArticleParagraphNum($content,$baca){
        $limit = 4;
        $total = count($content);
        $newcontent = $content;
        if($total > $limit){
            $position = 4;
            $slice_first = array_slice($content,0,$position);
            $offset_two = ($total - $position);
            $slice_two = array_slice($content,$position,$offset_two);

            $bacaJuga = $this->getBacaJugaNum($baca);
            $totalbaca = count($bacaJuga);
            if($totalbaca > 0){
                $newcontent = array_values(array_merge($slice_first,$bacaJuga,$slice_two));
            }
        }

        return $newcontent;
    }

    function getAdds($content){
        $limit = 8;
        $total = count($content);
        $newcontent = $content;
        if($total > $limit){
            $position = 8;

            $slice_first = array_slice($content,0,$position);
            $offset_two = ($total - $position);
            $slice_two = array_slice($content,$position,$offset_two);
            $ads = '
                <div class="ads300 mbmin">
                    <div id="div-gpt-ad-middle_3"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_3\');});</script></div>
                </div>';

            $contentads = array($ads);
            $newcontent = array_values(array_merge($slice_first,$contentads,$slice_two));
        }

        return $newcontent;
    }

    function getArticleListPageBaca($start,$offset,$details){
        $content = contentFilter(html_entity_decode($details['content']),$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $cleanspace = str_replace('<br> <br>','<br><br>',$content);
            $xcontent = explode('<br><br>',$cleanspace);
        }else{
            $xcontent = $explode_content;
        }
        
        $cut_content = array_slice($xcontent,$start,$offset);
        $article = implode('<br><br>',$cut_content);

        return $article;
    }

    function getArticleNumListPageBaca($details){
        $content = contentFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $cleanspace = str_replace('<br> <br>','<br><br>',$content);
            $xcontent = explode('<br><br>',$cleanspace);
        }else{
            $xcontent = $explode_content;
        }
        
        $count_exp = count($xcontent);

        return $count_exp;
    }

}
