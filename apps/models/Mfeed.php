<?php

class Mfeed extends CI_Model {

    public function __construct(){}

    public function getChannel(){
        $qString = "SELECT 
                        a.id_subkanal, a.date_created, a.date_published,
                        b.subkanal, b.slug_subkanal
                    FROM td_content a";
        $qJoin = " LEFT JOIN td_subkanal AS b ON b.id_subkanal = a.id_subkanal";
        $qCondition = " WHERE 
                            a.status = '0'
                            AND b.status = '0'";
        $qGroup = " GROUP BY b.id_subkanal";
        $qOrderBy = " ORDER BY b.id_subkanal ASC";

        $qString .= $qJoin.$qCondition.$qGroup.$qOrderBy;
        $response_data = $this->db->query($qString);
        $data['results'] = $response_data->result_array();

        $this->db->close();
        return $data;
    }
    public function getListBreakingNews($howmuch = 3, $start = 0, $total_results = FALSE){
        $qString = "SELECT 
                        a.id_content, a.id_subkanal, a.slug_title, a.date_created
                    FROM td_content a";
        $qJoin = " LEFT JOIN td_subkanal AS b ON b.id_subkanal = a.id_subkanal";
        $qCondition = " WHERE 
                            a.status = '0' 
                            AND b.status = '0'";
        $qOrderBy = " ORDER BY a.date_published DESC";
        $qLimit = " LIMIT ".$start.",".$howmuch;

        $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
        $response_data = $this->db->query($qString);
        $data['results'] = $response_data->result_array();

        $this->db->close();
        return $data;
    }
}
