

<link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/bootstrap.min.css<?php echo $upVersion ?>">
<link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/materialize.min.css<?php echo $upVersion ?>">
<link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/base.min.css<?php echo $upVersion ?>">
<link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/header.min.css<?php echo $upVersion ?>">
<link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/body.min.css<?php echo $upVersion ?>">
<link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/footer.min.css<?php echo $upVersion ?>">

<link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/jquery.fancybox.min.css<?php echo $upVersion ?>">
<link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/font-awesome.min.css<?php echo $upVersion ?>"> <!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo m_template_uri(); ?>/css/swiper.min.css<?php echo $upVersion ?>"> <!-- Swiper -->

