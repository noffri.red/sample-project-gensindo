<?php

$total = count($others);
if($total > 0){
    $data = $others;

    echo '<div class="link-article">
		<div class="head">Berita Lainnya</div>
		<ul class="clearfix">';

    for($i = 0; $i < $total; $i++){
        $id_content[$i] = $data[$i]['id_news'];
        $img[$i] = images_uri() . '/dyn/360/pena/news/' . $data[$i]['year'] . '/' . $data[$i]['month'] . '/' . $data[$i]['day'] . '/' . $data[$i]['id_subkanal'] . '/' . $id_content[$i] . '/' . $data[$i]['photo'];
        $subkanal[$i] = $data[$i]['subkanal'];
        $dtime[$i] = strtotime($data[$i]['date_created']);
        $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . strtolower($data[$i]['url_title']) . '-' . $dtime[$i]);

        $title[$i] = cleanWords($data[$i]['title']);
        $summary[$i] = cleanWords($data[$i]['summary']);
        $ago[$i] = time_difference($data[$i]['date_published']);

        if($data[$i]['images'] != ''){
            $showimg[$i] = '<a href="' . $link[$i] . '"><img src="' . $img[$i] . '" alt="' . $title[$i] . '"></a>';
        }else{
            $showimg[$i] = '';
        }

        echo '
            <li>
                ' . $showimg[$i] . '
                <div class="title"><a href="' . $link[$i] . '">' . $title[$i] . '</a></div>
            </li>';
    }

    echo '</ul>';
    echo '</div>';
}
