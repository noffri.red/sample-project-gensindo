<?php

class Api extends CI_Controller {

    var $apiUrl;
    var $nodeUrl;

    function __construct(){
        parent::__construct();
        $this->load->library('curl');
        $this->load->library('sindocurl');

        $ip_gcp = array(
            '10.184.15.219',/* staging */
            '10.184.0.25',/* ws1 */
            '10.184.0.58',/* ws2 */
            '10.184.0.59',/* ws3 */
            '10.184.0.60' /* ws3 */
        );

        if(isset($_SERVER['SERVER_ADDR']) && in_array($_SERVER['SERVER_ADDR'],$ip_gcp)){
            $this->nodeUrl = 'http://10.11.12.16:8998';
            $this->pushUrl = 'http://10.11.12.16:8558';
        }else{
            $this->nodeUrl = 'http://10.1.12.29:9889';
            $this->pushUrl = 'http://10.1.12.29:5885';
        }
    }

    function index(){
        redirect(base_url(),'location',301);
    }

    function scream(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);

        $success = 0;
        $error = 1;
        $post = 'xyz? what is it?';

        if(isset($_POST['xyz'])){
            $xyz = $_POST['xyz'];
            $decode = base64_decode($xyz);
            if($decode == $token){
                $content_id = $_POST['c'];
                $success = 1;
                $error = 0;

                $params = array(
                    'id_news' => $content_id,
                    'sindonews_token' => $token
                );

                $urlapi = $this->nodeUrl . '/article';
                $post = $this->curl->simple_post($urlapi,$params);
            }
        }

        $this->output->set_content_type('application/json')->set_output($post);
    }

    function xyz(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);

        if(isset($_GET['z'])){
            $xyz = $_GET['z'];
            $decode = base64_decode($xyz);
            if($decode == $token){
                $content_id = $_GET['c'];

                $params = array(
                    'id_news' => $content_id,
                    'sindonews_token' => $token
                );
                $urlapi = $this->nodeUrl . '/article';
                $this->curl->simple_post($urlapi,$params);
            }
        }

        header('Content-Type: image/gif');
        echo base64_decode('R0lGODlhAQABAJAAAP8AAAAAACH5BAUQAAAALAAAAAABAAEAAAICBAEAOw==');
    }

    function blackclouds(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);

        $success = 0;
        $error = 1;
        $data = $this->getempty();

        if(isset($_POST['sin'])){
            $xyz = $_POST['sin'];
            $decode = base64_decode($xyz);
            if($decode == $token){
                $pid = $_POST['pid'];
                $gid = $_POST['gid'];
                $sid = $_POST['sid'];
                $success = 1;
                $error = 0;

                $params = array(
                    'pid' => $pid,
                    'gid' => $gid,
                    'sid' => $sid
                );
                $urlapi = $this->pushUrl . '/store';

                $post = $this->curl->simple_post($urlapi,$params);
                $data = $post;
            }
        }

        echo $data;
    }

    function pushtopic(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);

        $success = 0;
        $error = 1;
        $data = $this->getempty();

        if(isset($_POST['sin'])){
            $xyz = $_POST['sin'];
            $decode = base64_decode($xyz);
            if($decode == $token){
                $token_to = $this->input->post('tk');
                $topic_name = $this->input->post('tp');

                $api_key = 'AAAA258avMc:APA91bFhhpOiIiAmTL7yZFBoyqjBOHNHWojs0N_68IuEbdJZcunLKr50Cd3HSOFmW5xCbHy1nd7dVPZaiIWNKhyA1Coa5VZo7VHhcsxyOSS8xr-ltTZi1iylLynibeqqPpNCmXbSEiZp';
                $url = "https://iid.googleapis.com/iid/v1/" . $token_to . "/rel/topics/" . $topic_name;
                $headers = array(
                    'Authorization: key=' . $api_key,
                    'Content-Type: application/json'
                );

                $data = $this->sindocurl->post($url,$headers,array());

                $success = 1;
                $error = 0;
            }
        }

        echo $data;
    }

    function getempty(){
        $html = array('result' => array('code' => 666,'status' => 'under constructions'));
        return json_encode($html);
    }

}
