<!doctype html>
<html  lang="id-ID">
<head>
  <!-- Meta SEO -->
  <title><?php echo $html['title'];?></title>
  <meta charset="utf-8">
  <meta http-equiv="x-dns-prefetch-control" content="on">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
  <meta name="theme-color" content="#499AE8">
  <?php echo $html['metaname']; ?>

  <!-- Icon -->
  <link rel="shortcut icon" href="assets/image/favicon.ico">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-160x160.png" sizes="160x160">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-192x192.png" sizes="192x192">
  <!-- Font -->
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;900&display=swap" rel="stylesheet"> <!-- Nunito -->
  <!-- CSS -->
  <!-- Development -->
  <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css"> -->
  <!-- Bootstrap -->
  
  <link rel="canonical" href="<?php echo base_url(); ?>">
  <link rel="alternate" title="SINDOnews | RSS Feed Berita GEN SINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>

  <?php echo $html['css']; ?>
  <?php echo $template['newcss']; ?>

  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/header.min.css"> <!-- Header -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/body.min.css"> <!-- Body -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/footer.min.css"> <!-- Footer -->
  <!-- Production -->
  <!-- <link rel="stylesheet" href="assets/css/styles.min.css"> -->
  <!-- Style -->
  <!-- Mediaquery -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-HDPI.min.css" media="screen and (min-width: 1083px) and (max-width: 1290px)"> <!-- HDPI Screen -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-MDPI.min.css" media="screen and (min-width: 993px) and (max-width: 1082px)"> <!-- MDPI Screen -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-SDPI.min.css" media="screen and (min-width: 769px) and (max-width: 992px)"> <!-- SDPI Screen -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-mobile.min.css" media="screen and (max-width: 768px)"> <!-- Mobile Screen -->


  <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
  <script>
    window.googletag = window.googletag || {
      cmd: []
    };

    googletag.cmd.push(function() {
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [970, 250],
        [970, 90],
        [728, 90],
        [1, 1]
      ], 'div-gpt-ad-billboard').setTargeting('pos', ['billboard']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [300, 250],
        [300, 600]
      ], 'div-gpt-ad-right_sidebar_1').setTargeting('pos', ['right_sidebar_1']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [300, 250]
      ], 'div-gpt-ad-right_sidebar_2').setTargeting('pos', ['right_sidebar_2']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [300, 250]
      ], 'div-gpt-ad-right_sidebar_3').setTargeting('pos', ['right_sidebar_3']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [300, 250]
      ], 'div-gpt-ad-right_sidebar_4').setTargeting('pos', ['right_sidebar_4']).addService(googletag.pubads());
      googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [
        [120, 600]
      ], 'div-gpt-ad-left_skinads').setTargeting('pos', ['left_skinads']).addService(googletag.pubads());

      googletag.defineOutOfPageSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', 'div-gpt-ad-outstream').setTargeting('pos', ['outstream']).addService(googletag.pubads());
      googletag.defineOutOfPageSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', 'div-gpt-ad-abm').setTargeting('partner', ['abm']).addService(googletag.pubads());

      googletag.pubads().setTargeting('page_type', ['kanalpage']);
      googletag.pubads().enableSingleRequest();
      googletag.pubads().collapseEmptyDivs();
      googletag.enableServices();
    });

    function bannerCheck(unitAd) {
      googletag.pubads().addEventListener('slotRenderEnded', function(event) {
        if (event.slot.getSlotElementId() === unitAd) {
          if (event.isEmpty) {
            var eAd = document.getElementById('me-' + unitAd);
            eAd.classList.add('adnone');
          }
        }
      });
    }
  </script>

  <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
  <script>
    window.OneSignal = window.OneSignal || [];
    OneSignal.push(function() {
      OneSignal.init({
        appId: '<?php echo $this->config->item('onesignal_appid'); ?>',
        autoResubscribe: true,
        notifyButton: {
          enable: false
        },
        promptOptions: {
          slidedown: {
            enabled: true,
            autoPrompt: true,
            actionMessage: "Dapatkan update berita SINDOnews melalui notifikasi browser Anda.",
            acceptButtonText: "SUBSCRIBE",
            cancelButtonText: "TIDAK"
          }
        }
      });
    });
  </script>

  <?php echo $html['bottom_js']; ?>
</head>

<body>
  <div id="app">


    <!-- Header -->
    <header>
      <div class="header-top">
        <?php echo $template['header']; ?>
      </div>
      <div class="header-middle">
        <div class="container">
          <!-- Logo -->
          <div id="logo">
            <a href="#"><img src="<?php echo template_uri(); ?>/image/logo.png" alt="Logo"></a>
          </div>
          <!-- Search -->
          <div id="search">
            <form class="search-field sample two" name="myForm" method="get" action="" onsubmit="return validateForm(this)">
              <input type="text" name="search" id="search-input" placeholder="Cari Berita" autocomplete="off">
              <button type="submit" class="btn btn-search fa fa-search"></button>
              <button onfocus="document.getElementById('search-input').value = ''" type="reset" class="btn btn-reset fa fa-times"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="header-bottom">
        <?php echo $template['minimenu']; ?>
      </div>
    </header>
    <!-- <div class="mask-header">Mask</div> -->

    <!-- Main -->
    <main>


      <!-- Headline -->
      <div id="headline">

        <div class="billboard">
          <div id="div-gpt-ad-billboard">
            <script>
              googletag.cmd.push(function() {
                googletag.display('div-gpt-ad-billboard');
              });
            </script>
          </div>
        </div>

        <!-- headline -->
        <div class="container">

          <?php //echo $template['headline']; ?>


        </div>

      </div>
      <div class="container">
        <!-- View -->
        <div id="view">
          <!-- Latest News -->
          <div id="latest">
           
            <div class="list-news">


              <?php //echo $content['latest']; ?>
              <?php echo $content['popular'] ?>
            </div>
          </div>
        </div>
        <!-- Sidebar -->
        <div id="sidebar">
          <!-- Trending Topic -->
          <div id="trending-topic">
            <?php echo $template['trending'] ?>
          </div>


          <!-- Announcement -->
          <div id="announcement">
            <?php echo $template['promogen'] ?>
          </div>

          <div class="r-side mb20 medium-banner">
            <div id="div-gpt-ad-right_sidebar_1">
              <script>
                googletag.cmd.push(function() {
                  googletag.display('div-gpt-ad-right_sidebar_1');
                });
              </script>
            </div>
          </div>
          <!-- Terpopuler -->
          <div id="terpopuler">
            <?php //echo $template['popular'] ?>
          </div>

          <div class="r-side mb20 mt20important medium-banner">
            <div id="div-gpt-ad-right_sidebar_2">
              <script>
                googletag.cmd.push(function() {
                  googletag.display('div-gpt-ad-right_sidebar_2');
                });
              </script>
            </div>
          </div>


        </div>
      </div>
    </main>
    <div class="mask-footer">Mask</div>


    <!-- Footer -->
    <footer>
      <?php echo $template['minifooter'] ?>
    </footer>
  </div>

  <div>
    <div id="div-gpt-ad-outstream">
      <script>
        googletag.cmd.push(function() {
          googletag.display('div-gpt-ad-outstream');
        });
      </script>
    </div>
  </div>
  <!-- Javascript -->
  <?php echo $html['bottom_css']; ?>
  <?php echo $html['js']; ?>

  <script>
    $(function() {

      var i = 10;
      var countPaging = 0;
      var totalPaging = 0;
      var imagesLoad = new LazyLoad({
        elements_selector: '.lazyload'
      });

      $('#subkanal-navigation').find('#trending-menu').addClass('active');


      $('.scroll').jscroll({
          loadingHtml: '<div class="news-load"><img src="<?php echo m_template_uri(); ?>/image/ajax-loader.gif" alt="loading image"></div>',
          nextSelector: '.news-more a',
          autoTrigger: false,
          callback: function(){
              imagesLoad.update();
              $.LoadingOverlay("hide");
              if(totalPaging >= 20 ) $('.news-link-more').removeAttr('hidden')
       
          }
      });

      // scrolling

      $(window).scroll(function() {

          if ($(window).scrollTop() >= $(document).height() - $(window).height()) {
              totalPaging = (i * countPaging++);
              if(totalPaging <= 20){
                //   $.LoadingOverlay("show");
                //   tekan(totalPaging);
              }
              
          }


      });

      async function tekan() {
          const resultData = await $('.news-more a').click();
          return resultData;
      }


    });

    // search action
    function validateForm() {
      var x = document.forms["myForm"]["search"].value;
      x = x.replace(' ', '+')

      if (x != "") {
        var url = 'https://search.sindonews.com/gokanal?type=artikel&q=' + x + '&pid=600';

        document.location.href = url, true;
        return false;
      }

    }
  </script>
</body>

</html>