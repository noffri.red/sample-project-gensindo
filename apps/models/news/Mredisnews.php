<?php

class Mredisnews extends CI_Model {

    /* TYPE NEWS
     * 1. HEADLINE
     * 2. BREAKING
     *
     * STATUS
     * 0 Publish
     * 1 Gambar belum diupload
     * 2 Draft
     * 3 Pending Publish
     * 4 Acc Redaksi
     * 5 Content harus direvisi oleh user/pembaca
     *
     * STATUS_PEMBACA
     * 0 Berita yang dibuat oleh redaksi sindo
     * 1 Berita kiriman user/pembaca
     * */
    var $myredis;
    var $dbredis;

    public function __construct(){
        parent::__construct();
		
		$this->myredis = $this->textcache->redis();
        $this->dbredis = $this->textcache->dbredis();
    }

    public function getListBreakingNews($howmuch = 10, $start = 0, $total_results = FALSE){
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        if($total_results == FALSE){ $totalresults = ""; }else{ $totalresults = "_withtotalresults"; }
        $key = $this->config->item('redis_name')."_breakingnews_all_channel". $keyhowMuch.$totalresults;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                            a.id_content, a.id_kanal, a.id_subkanal, a.title, a.summary, a.slug_title, a.images, a.date_published, a.date_created,
                            DATE_FORMAT(a.date_created, '%Y') AS year, 
                            DATE_FORMAT(a.date_created, '%m') AS month,  
                            DATE_FORMAT(a.date_created, '%d') AS day,  
                            b.kanal,
                            c.subkanal, c.slug_subkanal,
                            d.subtitle AS sub_title
                        FROM td_content a";
            $qJoin = " LEFT JOIN td_kanal AS b ON b.id_kanal = a.id_kanal
                      LEFT JOIN td_subkanal AS c ON c.id_subkanal = a.id_subkanal
                      LEFT JOIN td_subtitle AS d ON d.id_subtitle = a.id_subtitle";
            $qCondition = " WHERE 
                                a.status = '0'
                                AND c.status = '0'";
            $qOrderBy = " ORDER BY a.date_published DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();

            if($total_results == TRUE){
                $qStringTotal = "SELECT COUNT(*) AS total FROM td_content a";

                $qStringTotal .= $qJoin.$qCondition;
                $response_total = $this->db->query($qStringTotal);
                $total = $response_total->result_array();
                $data['total_results'] = $total[0]['total'];
            }
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getListBreakingNewsBySubkanal($id_subkanal, $howmuch = 3, $start = 0, $total_results = FALSE){
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        if($total_results == FALSE){ $totalresults = ""; }else{ $totalresults = "_withtotalresults"; }
        $key = $this->config->item('redis_name')."_breakingnews_". $id_subkanal. $keyhowMuch.$totalresults;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                            a.id_content, a.id_kanal, a.id_subkanal, a.title, a.summary, a.slug_title, a.images, 
                            a.date_published, a.date_created, 
                            DATE_FORMAT(a.date_created, '%Y') AS year, 
                            DATE_FORMAT(a.date_created, '%m') AS month,  
                            DATE_FORMAT(a.date_created, '%d') AS day,  
                            b.kanal,
                            c.subkanal, c.slug_subkanal
                        FROM td_content a";
            $qJoin = " LEFT JOIN td_kanal AS b ON b.id_kanal = a.id_kanal
                      LEFT JOIN td_subkanal AS c ON c.id_subkanal = a.id_subkanal";
            $qCondition = " WHERE 
                                a.status = '0'
                                AND a.id_subkanal = '".$id_subkanal."'
                                AND c.status = '0'";
            $qOrderBy = " ORDER BY a.date_published DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();

            if($total_results == TRUE){
                $qStringTotal = "SELECT COUNT(*) AS total FROM td_content a";

                $qStringTotal .= $qJoin.$qCondition;
                $response_total = $this->db->query($qStringTotal);
                $total = $response_total->result_array();
                $data['total_results'] = $total[0]['total'];
            }
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getListImaji($howmuch = 10, $start = 0, $total_results = FALSE){
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        if($total_results == FALSE){ $totalresults = ""; }else{ $totalresults = "_withtotalresults"; }
        $key = $this->config->item('redis_name')."_imaji". $keyhowMuch.$totalresults;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                            id_imaji, title, summary, slug_title, images, date_published, date_created,
                            quotes,
                            DATE_FORMAT(date_created, '%Y') AS year, 
                            DATE_FORMAT(date_created, '%m') AS month,  
                            DATE_FORMAT(date_created, '%d') AS day
                        FROM td_imaji";
            $qCondition = " WHERE 
                                status = '0' ";
            $qOrderBy = " ORDER BY date_published DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();

            if($total_results == TRUE){
                $qStringTotal = "SELECT COUNT(*) AS total FROM td_imaji";

                $qStringTotal .= $qCondition;
                $response_total = $this->db->query($qStringTotal);
                $total = $response_total->result_array();
                $data['total_results'] = $total[0]['total'];
            }
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getDbContentData($id_content){
        $key = $this->config->item('redis_name')."_article_db_content_". $id_content;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                            a.id_content, a.id_kanal, a.id_subkanal, a.id_author, a.id_topic, 
                            a.lokasi, a.title, a.summary, a.slug_title, a.caption_images, a.images, a.date_published, a.date_created, 
                            a.content,
                            DATE_FORMAT(a.date_created, '%Y') AS year, 
                            DATE_FORMAT(a.date_created, '%m') AS month,  
                            DATE_FORMAT(a.date_created, '%d') AS day,  
                            b.kanal,
                            c.subkanal, c.slug_subkanal,
                            d.slug_author, d.author,
                            e.id_user, e.kode_user, e.facebook, e.instagram, e.twitter, e.caption_user, e.photo_user, e.username, 
                            f.subtitle AS sub_title,
                            h.id_group, h.group
                        FROM td_content a";
            $qJoin = " LEFT JOIN td_kanal AS b ON b.id_kanal = a.id_kanal
                      LEFT JOIN td_subkanal AS c ON c.id_subkanal = a.id_subkanal
                      LEFT JOIN td_author AS d ON d.id_author = a.id_author
                      LEFT JOIN td_user AS e ON e.id_user = a.id_editor
                      LEFT JOIN td_subtitle AS f ON f.id_subtitle = a.id_subtitle
                      LEFT JOIN td_group AS h ON e.id_group = h.id_group";
            $qCondition = " WHERE a.status = '0'
                                AND a.id_content = '".$id_content."'
                                AND c.status = '0'";

            $qString .= $qJoin.$qCondition;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,600,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getDbContentImaji($id_content){
        $key = $this->config->item('redis_name')."_article_db_content_". $id_content;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                            a.id_imaji, a.id_author, a.id_topic, a.title, a.summary, a.slug_title, 
                            a.images, a.date_published, a.date_created, a.content,a.quotes,
                            DATE_FORMAT(a.date_created, '%Y') AS year, 
                            DATE_FORMAT(a.date_created, '%m') AS month,  
                            DATE_FORMAT(a.date_created, '%d') AS day,  
                            d.slug_author, d.author,
                            e.id_user, e.kode_user, e.facebook, e.instagram, e.twitter, e.caption_user, e.photo_user, e.username,
                            h.id_group, h.group
                        FROM td_imaji a";
            $qJoin = " LEFT JOIN td_author AS d ON d.id_author = a.id_author
                      LEFT JOIN td_user AS e ON e.id_user = a.id_editor
                      LEFT JOIN td_group AS h ON e.id_group = h.id_group";
            $qCondition = " WHERE a.status = '0'
                                AND a.id_imaji = '".$id_content."' ";

            $qString .= $qJoin.$qCondition;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,600,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getRelatedNewsByTopic($id_content, $id_topic, $howmuch = 3, $start = 0){
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        $key = $this->config->item('redis_name')."_relatednews_". $id_content. $keyhowMuch;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                            a.id_content, a.id_kanal, a.id_subkanal, a.id_author, a.id_topic, 
                            a.lokasi, a.title, a.summary, a.slug_title, a.caption_images, a.images, a.date_published, a.date_created, 
                            a.content,
                            DATE_FORMAT(a.date_created, '%Y') AS year, 
                            DATE_FORMAT(a.date_created, '%m') AS month,  
                            DATE_FORMAT(a.date_created, '%d') AS day,  
                            b.kanal,
                            c.subkanal, c.slug_subkanal
                        FROM td_content a";
            $qJoin = " LEFT JOIN td_kanal AS b ON b.id_kanal = a.id_kanal
                      LEFT JOIN td_subkanal AS c ON c.id_subkanal = a.id_subkanal";
            $qCondition = " WHERE 
                                a.status = '0'
                                AND a.id_content != '". $id_content ."'
                                AND a.id_topic IN(". $id_topic .")";
            $qOrderBy = " ORDER BY a.date_published DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getListNews($howmuch = 10, $start = 0, $total_results = FALSE){
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        if($total_results == FALSE){ $totalresults = ""; }else{ $totalresults = "_withtotalresults"; }
        $key = $this->config->item('redis_name')."_news_all_channel". $keyhowMuch.$totalresults;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                            a.id_content, a.id_kanal, a.id_subkanal, a.id_topic, a.title, a.summary, a.slug_title, a.images, a.date_published, a.date_created,
                            DATE_FORMAT(a.date_created, '%Y') AS year, 
                            DATE_FORMAT(a.date_created, '%m') AS month,  
                            DATE_FORMAT(a.date_created, '%d') AS day,  
                            b.kanal,
                            c.subkanal, c.slug_subkanal,
                            d.subtitle AS sub_title
                        FROM td_content a";
            $qJoin = " LEFT JOIN td_kanal AS b ON b.id_kanal = a.id_kanal
                      LEFT JOIN td_subkanal AS c ON c.id_subkanal = a.id_subkanal
                      LEFT JOIN td_subtitle AS d ON d.id_subtitle = a.id_subtitle";
            $qCondition = " WHERE 
                                a.status = '0' 
                                AND c.status = '0'";
            $qOrderBy = " ORDER BY a.date_published DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();

            if($total_results == TRUE){
                $qStringTotal = "SELECT COUNT(*) AS total FROM td_content a";

                $qStringTotal .= $qJoin.$qCondition;
                $response_total = $this->db->query($qStringTotal);
                $total = $response_total->result_array();
                $data['total_results'] = $total[0]['total'];
            }
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getKanalTrendingTopic($howmuch = 10, $start = 0){
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        $key = $this->config->item('redis_name')."_new_hottopic". $keyhowMuch;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            /* berita terpopular */
            $qString = "SELECT 
                            a.hit,
                            b.id_content, b.id_topic, b.id_subkanal, b.title, b.slug_title, b.date_created
                        FROM td_hit_content a";
            $qJoin = " LEFT JOIN td_content AS b ON b.id_content = a.id_content";
            $qCondition = " WHERE 
                                b.status = '0'
                                AND b.id_topic IS NOT null
                                AND b.id_topic != ''
                                AND b.`date_published` > DATE_SUB(NOW(), INTERVAL 1 DAY)
                                ";
            $qOrderBy = " ORDER BY a.hit DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $results_terpopular = $response_data->result_array();

            /* topik dari berita terpopular */
            $total = count($results_terpopular);
            $data['results'] = array();
            if($total > 0){
                $id_topik_terpopular = '';
                for($i=0; $i<$total; $i++){
                    $id_topik_terpopular .=  ','.$results_terpopular[$i]['id_topic'];
                }

                $sql = $this->db->query("SELECT * FROM `td_topic` WHERE 
                        id_topic IN(". substr($id_topik_terpopular,1) .") 
                        AND status = '0'
                        ORDER BY FIELD(id_topic,". substr($id_topik_terpopular,1) .")
                        LIMIT 0,5
                        ");
                $results_topik = $sql->result_array();
                $data['results'] = $results_topik;
            }
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getListTitlePopular($howmuch = 5, $start = 0){
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        $key = $this->config->item('redis_name')."_trendingpopular". $keyhowMuch;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                        a.hit,
                        b.id_content, b.id_kanal, b.id_subkanal, b.title, b.summary, b.slug_title, b.images, b.date_published, b.date_created,
                        DATE_FORMAT(b.date_created, '%Y') AS year, 
                        DATE_FORMAT(b.date_created, '%m') AS month,  
                        DATE_FORMAT(b.date_created, '%d') AS day,  
                        c.kanal,
                        d.subkanal, d.slug_subkanal,
                        e.subtitle AS sub_title
                        FROM td_hit_content a";
            $qJoin = " LEFT JOIN td_content AS b ON b.id_content = a.id_content
                        LEFT JOIN td_kanal AS c ON c.id_kanal = b.id_kanal
                        LEFT JOIN td_subkanal AS d ON d.id_subkanal = b.id_subkanal
                        LEFT JOIN td_subtitle AS e ON e.id_subtitle = b.id_subtitle";
            $qCondition = " WHERE  
                                b.status = '0' AND 
                                b.`date_published` > DATE_SUB(NOW(), INTERVAL 1 DAY) ";
            $qOrderBy = " ORDER BY a.hit DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getListTitlePopularImaji($howmuch = 5, $start = 0){
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        $key = $this->config->item('redis_name')."_popular". $keyhowMuch;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                        a.hit,
                        b.id_imaji, b.title, b.summary, b.slug_title, b.images, b.date_published, b.date_created,
                        DATE_FORMAT(b.date_created, '%Y') AS year, 
                        DATE_FORMAT(b.date_created, '%m') AS month,  
                        DATE_FORMAT(b.date_created, '%d') AS day
                        FROM td_hit_imaji a";
            $qJoin = " LEFT JOIN td_imaji AS b ON b.id_imaji = a.id_imaji";
            $qCondition = " WHERE  
                                b.status = '0' AND 
                                b.`date_published` > DATE_SUB(NOW(), INTERVAL 1 DAY) ";
            $qOrderBy = " ORDER BY a.hit DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getAbout(){
        $key = $this->config->item('redis_name')."_about";
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT id_about, title, summary, content, slug_title, images, date_published, date_created,
                                DATE_FORMAT(date_created, '%Y') AS year, 
                                DATE_FORMAT(date_created, '%m') AS month,  
                                DATE_FORMAT(date_created, '%d') AS day
                            FROM td_about";
            $qCondition = " WHERE status = '0' ";
            $qOrderBy = " ORDER BY date_published DESC";
            $qLimit = " LIMIT 0,1";

            $qString .= $qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();

            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,900,$redisdata);
            $result = $data;
        }

        return $result;
    }
    public function getEvent($howmuch = 5, $start = 0, $total_results = FALSE){
        $date = date("Y-m-d");
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        if($total_results == FALSE){ $totalresults = ""; }else{ $totalresults = "_withtotalresults"; }
        $key = $this->config->item('redis_name')."_event". $date.$keyhowMuch.$totalresults;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT id_event, title, summary, content, slug_title, images, date_published, date_created, tanggal_event,
                                DATE_FORMAT(date_created, '%Y') AS year, 
                                DATE_FORMAT(date_created, '%m') AS month,  
                                DATE_FORMAT(date_created, '%d') AS day
                            FROM td_event";
            $qCondition = " WHERE status = '0' AND tanggal_event >= '". $date ."' ";
            $qOrderBy = " ORDER BY date_published DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();

            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $data;
        }

        return $result;
    }
	public function getListBreakingNewsDetail($id_content, $howmuch = 3, $start = 0, $total_results = FALSE){
        if($howmuch == NULL){ $keyhowMuch = ""; }else{ $keyhowMuch = "_".$start."_".$howmuch; }
        if($total_results == FALSE){ $totalresults = ""; }else{ $totalresults = "_withtotalresults"; }
        $key = $this->config->item('redis_name')."_breakingnewsdetail_". $id_content.$keyhowMuch.$totalresults;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT 
                            a.id_content, a.id_kanal, a.id_subkanal, a.title, a.summary, a.slug_title, a.images, a.date_published, a.date_created,
                            DATE_FORMAT(a.date_created, '%Y') AS year, 
                            DATE_FORMAT(a.date_created, '%m') AS month,  
                            DATE_FORMAT(a.date_created, '%d') AS day,  
                            b.kanal,
                            c.subkanal,
                            d.subtitle AS sub_title
                        FROM td_content a";
            $qJoin = " LEFT JOIN td_kanal AS b ON b.id_kanal = a.id_kanal
                      LEFT JOIN td_subkanal AS c ON c.id_subkanal = a.id_subkanal
                      LEFT JOIN td_subtitle AS d ON d.id_subtitle = a.id_subtitle";
            $qCondition = " WHERE 
                                a.id_content != '".$id_content."' 
                                AND a.status = '0'";
            $qOrderBy = " ORDER BY a.date_published DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();

            if($total_results == TRUE){
                $qStringTotal = "SELECT 
                                COUNT(*) AS total 
                              FROM td_content a";

                $qStringTotal .= $qJoin.$qCondition;
                $response_total = $this->db->query($qStringTotal);
                $total = $response_total->result_array();
                $data['total_results'] = $total[0]['total'];
            }
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,600,$redisdata);
            $result = $data;
        }

        return $result;
    }
	
	function getListNewsWP($idchannel,$howMuch){
        $key = "json_latestnews_" . $idchannel;
        $getredis = $this->textcache->redisDbGet($key);

        if($getredis){
            $datanews = json_decode(utf8_encode($getredis),TRUE);
            $total = count($datanews);

            if($total > $howMuch){
                $totalshow = $howMuch;
            }else{
                $totalshow = $total;
            }

            $data = array();
            for($i = 0; $i < $totalshow; $i++){
                $data[$i]['content_id'] = $datanews[$i]['content_id'];
                $data[$i]['news_id'] = $data[$i]['content_id'];
                $data[$i]['parent_id'] = $datanews[$i]['parent_id'];
                $data[$i]['parent_name'] = $datanews[$i]['parent_name'];
                $data[$i]['channel_id'] = $datanews[$i]['channel_id'];
                $data[$i]['channel_name'] = $datanews[$i]['channel_name'];
                $data[$i]['date_created'] = $datanews[$i]['date_created'];
                $data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
                $data[$i]['published'] = parseDateTime($datanews[$i]['date_publish']);
                $data[$i]['date_publish'] = $datanews[$i]['date_publish'];
                $data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
                $data[$i]['subtitle'] = $datanews[$i]['subtitle'];
                $data[$i]['location'] = $datanews[$i]['location'];
                $data[$i]['reporter_name'] = $datanews[$i]['reporter_name'];
                $data[$i]['summary'] = stripslashes($datanews[$i]['summary']);

                $thumb[$i] = $datanews[$i]['image'];
                if($thumb[$i] != ''){
                    $data[$i]['image'] = $thumb[$i];
                    $data[$i]['thumb'] = $thumb[$i];
                }else{
                    $data[$i]['image'] = '';
                    $data[$i]['thumb'] = '';
                }
            }

            $result = $data;
        }else{
			$data = array();
				
            $jsonFile = $this->config->item('json_data_sindonews') . '/headline/' . $idchannel . '/news-' . $idchannel . '.json';
			if(is_file($jsonFile)){
				$getJson = file_get_contents($jsonFile);
				$datanews = json_decode($getJson,TRUE);
				
				if($datanews){
					$total = count($datanews);

					if($total > $howMuch){
						$totalshow = $howMuch;
					}else{
						$totalshow = $total;
					}

					for($i = 0; $i < $totalshow; $i++){
						$data[$i]['content_id'] = $datanews[$i]['content_id'];
						$data[$i]['news_id'] = $data[$i]['content_id'];
						$data[$i]['parent_id'] = $datanews[$i]['parent_id'];
						$data[$i]['parent_name'] = $datanews[$i]['parent_name'];
						$data[$i]['channel_id'] = $datanews[$i]['channel_id'];
						$data[$i]['channel_name'] = $datanews[$i]['channel_name'];
						$data[$i]['date_created'] = $datanews[$i]['date_created'];
						$data[$i]['created'] = parseDateTime($datanews[$i]['date_created']);
						$data[$i]['published'] = parseDateTime($datanews[$i]['date_publish']);
						$data[$i]['date_publish'] = $datanews[$i]['date_publish'];
						$data[$i]['title'] = stripslashes(filterText($datanews[$i]['title']));
						$data[$i]['subtitle'] = $datanews[$i]['subtitle'];
						$data[$i]['location'] = $datanews[$i]['location'];
						$data[$i]['reporter_name'] = $datanews[$i]['reporter_name'];
						$data[$i]['summary'] = stripslashes($datanews[$i]['summary']);

						$thumb[$i] = $datanews[$i]['image'];
						if($thumb[$i] != ''){
							$data[$i]['image'] = $thumb[$i];
							$data[$i]['thumb'] = $thumb[$i];
						}else{
							$data[$i]['image'] = '';
							$data[$i]['thumb'] = '';
						}
					} 
				} 
			}

            $result = $data;
        }

        return $result;
    }

    function getTopicData($id_topik){
        $key = $this->config->item('redis_name')."_topic_data_" . $id_topik;
        $getredis = $this->textcache->redisGet($key);

        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else {
            $qString = "SELECT id_topic, topic, slug_topic FROM td_topic";
            $qCondition = " WHERE id_topic IN(". $id_topik .") AND status = '0'";
            $qOrderBy = " ORDER BY topic ASC";

            $qString .= $qCondition.$qOrderBy;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();
            $this->db->close();

            $redisdata = json_encode($data);
            $this->textcache->redisSet($key,300,$redisdata);
            $result = $data;
        }

        return $result;
    }
	
	public function getChannel(){
        $qString = "SELECT 
                        a.id_subkanal, a.date_created, a.date_published,
                        b.subkanal, b.slug_subkanal
                    FROM td_content a";
        $qJoin = " LEFT JOIN td_subkanal AS b ON b.id_subkanal = a.id_subkanal";
        $qCondition = " WHERE 
                            a.status = '0'
                            AND b.status = '0'";
        $qGroup = " GROUP BY b.id_subkanal";
        $qOrderBy = " ORDER BY b.id_subkanal ASC";

        $qString .= $qJoin.$qCondition.$qGroup.$qOrderBy;
        $response_data = $this->db->query($qString);
        $data['results'] = $response_data->result_array();

        $this->db->close();
        return $data;
    }
}