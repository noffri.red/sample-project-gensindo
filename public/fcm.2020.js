var firebaseConfig = {
    apiKey: "AIzaSyBIPnPF-qYzbO8wU8IDodvwFEwmG9eTGkw",
    authDomain: "api-project-943267167431.firebaseapp.com",
    databaseURL: "https://api-project-943267167431.firebaseio.com",
    projectId: "api-project-943267167431",
    storageBucket: "api-project-943267167431.appspot.com",
    messagingSenderId: "943267167431",
    appId: "1:943267167431:android:5844349c9c1c50d3",
    measurementId: "UA-25311844-1"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
messaging.usePublicVapidKey("BHVZcMkRG5xtfuKQxf-b-ROdQs4eq3bf4ww23xnMK3dQs6mm1L_RxPgBTyR-w2B2VaJZO9Or7ZPsdyL6QDJ4IVI");

function reqPermission() {
    Notification.requestPermission().then((permission) => {
        dismissBox();
        if (permission === 'granted') {
            getMyToken();
            renewToken();
        } else {
            console.log('Unable to get permission to notify.');
        }
    }).catch((err) => {
        console.log('An error permission. ', err);
        dismissBox();
    });
}

function getMyToken() {
    messaging.getToken().then((currentToken) => {
        if (currentToken) {
            storeToken(currentToken);
            console.log('new token: ' + currentToken);
        } else {
            console.log('No Instance ID token available. Request permission to generate one.');
        }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
    });
}

function renewToken() {
    messaging.onTokenRefresh(() => {
        messaging.getToken().then((refreshedToken) => {
            storeToken(refreshedToken);
            console.log('Token refreshed. New token: ' + refreshedToken);
        }).catch((err) => {
            console.log('Unable to retrieve refreshed token ', err);
        });
    });
}

messaging.onMessage(function (payload) {
    console.log('onMessage', payload);
});

const notifConfirm = document.getElementsByClassName('notif-yes')[0];
notifConfirm.addEventListener('click', function () {
    dismissBox();
    reqPermission();
});

function dismissBox() {
    const notifbox = document.getElementsByClassName('notif-box')[0];
    notifbox.classList.add('notif-hide');
    notifbox.classList.add('kill-me');
    notifbox.classList.remove('notif-show');
}

const fcminfo = document.querySelector('#pushinfo');
const urlpush = fcminfo.dataset.url;
const urlreg = fcminfo.dataset.reg;
const grouppush = fcminfo.dataset.group;
const groupkey = fcminfo.dataset.key;
const source = fcminfo.dataset.source;

function storeToken(token) {
    fetch(urlpush, {
        method: 'POST',
        headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: 'sin=' + groupkey + '&pid=' + token + '&gid=' + grouppush + '&sid=' + source
    }).then(function (pushresult) {
        console.log(pushresult);
        
        fetch(urlreg, {
            method: 'POST',
            headers: {
                "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
            },
            body: 'sin=' + groupkey + '&tk=' + token + '&tp=' + grouppush
        }).then(function (regresult){
            console.log(regresult);
        }).catch((err) => {
            console.log(err);
        });
    }).catch((err) => {
        console.log(err);
    });


}

function randomuv() {
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
}

function setCookie(cname, cvalue, exdays, domain) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/;domain=" + domain + "; secure; SameSite=None";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}