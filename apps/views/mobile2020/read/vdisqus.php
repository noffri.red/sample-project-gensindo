<div class="forum-box" id="comment">
    <div id="disqus_thread"></div>
    <script>
        var disqus_config = function () {
            this.page.url = '<?php echo $site_url; ?>';
            this.page.identifier = '<?php echo $content_id; ?>';
            this.callbacks.onNewComment = [function () {
                window.dataLayer.push({
                    event: 'comment',
                    article_title: "<?php echo cleanTitleArticle($details['title']); ?>"
                });
            }];
        };
        (function () {
            var d = document, s = d.createElement('script');
            s.src = 'https://sindonews.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
</div>