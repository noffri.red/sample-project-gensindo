<?php
$total = count($event['results']);
if($total > 0) {
    $data = $event['results'];

    echo '<section class="agenda">
        <div class="title-gensindo">Agenda</div>
        <ul>';

        for($i=0; $i<$total; $i++) {
            $id_event[$i] = $data[$i]['id_event'];
            $dtime[$i] = strtotime($data[$i]['date_created']);
            $link[$i] = site_url('event/' . $id_event[$i] . '/'. $data[$i]['slug_title'] . '-' . $dtime[$i]);
            $title[$i] = cleanWords($data[$i]['title']);
            $tanggal_event[$i] = getSimpleIndonesianDate($data[$i]['tanggal_event']);

            echo '<li class="clearfix">
                <div class="icon pull-left"><img src="'. template_uri() .'/images/icon_calendar.png" alt="'. $title[$i] .'"></div>
                <div class="text pull-right">
                    <time>'. $tanggal_event[$i] .'</time>
                    <div class="title">'. $title[$i] .'</div>
                </div>
            </li>';
        }
    echo '</ul>
            <a href="#" class="btn-more">View More</a>
        </section>';
}
