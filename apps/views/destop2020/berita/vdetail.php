<?php

if(count($details) > 0){
    $title = cleanTitleArticle($details['title']);
    $url_title = cleanWords($details['url_title']);
    $datepublish = parseDateTime((string) $details['date_published']);
    $date_published = $datepublish['day_ind_name'] . ", " . $datepublish['day'] . " " . $datepublish['month_ind_name'] . " " . $datepublish['year'];
    $date_published .= ' - ' . $datepublish['hour'] . ":" . $datepublish['minute'] . " WIB";
    $lokasi = strtoupper($details['lokasi']);
    $content = $content;
    $kode_user = strtolower($details['kode_user']);
   
    $sub_title = 'Artikel';
    if($details['sub_title'] != ''){
        $sub_title = cleanWords($details['sub_title']);
      
    }


    echo '<h1 class="title-news">'.$title .'</h1>';

    if($details['author'] != ''){
        $author = ucwords($details['author']);
        $link_author = 'https://index.sindonews.com/blog/' . $details['id_author'] . '/' . slug($details['author']);

        echo '<div class="author-news"><a target="_blank" href='.$link_author.'>'. $author. '</a></div>';
    }

    echo ' <time class="time-news">'. $date_published .'</time>';

    if($start == 0 || $start == ''){
        if($details['photo'] != ''){
            $img = images_uri() . '/dyn/850/gensindo/content/' . date('Y/m/d',strtotime($details['date_created'])) . '/' . $details['id_subkanal'] . '/' . $details['id_news'] . '/' . $details['photo'];
            
            if($details['caption_photo'] != ''){
                $caption_images = cleanWords($details['caption_photo']);
            }else{
                $caption_images = cleanWords($details['title']);
            }

            echo '<div class="primary-image-news"><a data-fancybox="gallery" href="' . $img . '"><img src="' . $img . '" alt="' . cleanHTML($title) . '" width="850"></a></div>';
            echo ' <div class="image-caption">' . $caption_images . '</div>';
        }

        $adspages = '';
    }else{
        echo '
            <div class="ads300 mt20 mb20 adsload">
                <div id="div-gpt-ad-middle_1"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_1\');});</script></div>
            </div>';

        $adspages = '
            <div class="ads300 mt20 mb20 adsload">
                <div id="div-gpt-ad-middle_2"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_2\');});</script></div>
            </div>';
    }

    echo '<div class="paragraph">';

    if(!empty($lokasi)){
        if($start == 0 || $start == ''){
            echo '<span>' . $lokasi . '</span> - ';
        }
    }

    echo $content;
    
    echo '<div class="page-status"></div>';
    
    if($details['author'] != ''){
        $author = ucwords($details['author']);
        $link_author = 'https://index.sindonews.com/blog/' . $details['id_author'] . '/' . slug($details['author']);

        echo '<div class="author-news"><a target="_blank" href="'.$link_author.'">'. $author. '</a></div>';
    }


    $lasturl = (($per_page * $last_page) - $per_page);
    if($totalData > $per_page){
        if($start == $lasturl){
            echo '<br><br><span class="reporter">(' . $kode_user . ')</span>';
        }
    }else{
        echo '<br><br><span class="reporter">(' . $kode_user . ')</span>';
    }

    echo '</div>';

    if($totalData > $per_page){
        echo $adspages;

        echo '<div class="note-paging">halaman ke-' . $current_page . '</div>';
        echo $pagination;

        if(!isset($_GET['showpage'])){
            echo '<div class="content-show"><a href="' . $site_url . '?showpage=all">show all</a></div>';
        }
    }

}
