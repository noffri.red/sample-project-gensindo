<?php
$total = count($trending);
if($total > 0) { 
    echo '<section class="trending">';
    echo '<div class="title-gensindo">Trending Topic</div>';
    echo '<ul>';
        for ($i = 0; $i < $total; $i++) {
            $link[$i] = 'https://www.sindonews.com/topic/' . $trending[$i]['topic_id'] . '/' . slug($trending[$i]['topic']);;
            $title[$i] = cleanWords($trending[$i]['topic']);

            echo '<li><a href="'. $link[$i] .'">#'. $title[$i] .'</a></li>';
        }
    echo '</ul>';
    echo '</section>';
}