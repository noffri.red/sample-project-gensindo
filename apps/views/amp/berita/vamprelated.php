<?php

$total = count($related);
if($total > 0){
    if($topiklist != ''){
        /* Topik Terkait */
        $topic = explode(',',$topiklist);

        echo '<div class="topik-terkait">';
        echo '<div class="topik-terkait-title">Topik Terkait :</div>';

        for($i = 0; $i < count($topic); $i++){
            $getTopicData[$i] = $this->mgonews->getTopicListGensindo($topic[$i]);
            $urltopic[$i] = $this->mgonews->getTopicURL($getTopicData[$i][0]['slug_topic']);
            $topic_title[$i] = strtolower($getTopicData[$i][0]['topic']);
            echo '<a href="' . $urltopic[$i] . '"><i class="fa fa-tags" aria-hidden="true"></i> ' . $topic_title[$i] . '</a>';
        }
        echo '</div>';

        /* Berita Terkait */
        echo '<div class="news article-topic">';
        echo '<div class="heading">Berita Terkait</div>';
        echo '<ul>';

        for($i = 0; $i < $total; $i++){
            $dtime[$i] = strtotime($related[$i]['date_created']);
            $l[$i] = site_url('berita') . '/' . $related[$i]['id_content'] . '/' . $related[$i]['id_subkanal'] . '/' . slug($related[$i]['title']) . '-' . $dtime[$i];
            $title[$i] = cleanWords($related[$i]['title']);

            echo '
                <li>
                    <div class="breaking-title">
                        <a href="' . $l[$i] . '">' . $title[$i] . '</a>
                    </div>
                </li>';
        }

        echo '</ul>';
        echo '</div>';
    }
}