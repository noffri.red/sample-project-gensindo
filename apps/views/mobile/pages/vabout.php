<!DOCTYPE html>
<html lang="id-ID">
<head>
    <title><?php echo $html['title'];?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
	<meta name="theme-color" content="#004B8F">
    <?php echo $html['metaname']; ?>

    <link rel="shortcut icon" href="<?php echo template_uri();?>/images/favicon.ico">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="<?php echo template_uri();?>/images/favicon-192x192.png" sizes="192x192">

    <link rel="dns-prefetch" href="https://cdn.sindonews.net/">
    <link rel="preconnect" href="https://cdn.sindonews.net/">

    <link rel="canonical" href="<?php echo base_url(); ?>">
    <link rel="alternate" title="SINDOnews | RSS Feed Berita GEN SINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>

    <?php echo $html['css'];?>
    <?php echo $template['newcss'];?>
    <link rel="stylesheet" href="<?php echo template_uri();?>/css/mediaquery-HDPI.css" media="screen and (min-width: 1083px) and (max-width: 1290px)">
    <link rel="stylesheet" href="<?php echo template_uri();?>/css/mediaquery-MDPI.css" media="screen and (min-width: 993px) and (max-width: 1082px)">
    <link rel="stylesheet" href="<?php echo template_uri();?>/css/mediaquery-SDPI.css" media="screen and (min-width: 769px) and (max-width: 992px)">
    <link rel="stylesheet" href="<?php echo template_uri();?>/css/mediaquery-mobile.css" media="screen and (max-width: 768px)">

    <?php echo $html['bottom_js']; ?>
</head>
<body>
<header>
    <?php echo $template['header'];?>
</header>
<section class="slidenav">
	<ul id="slide-out" class="sidenav">
		<li>
			<div class="user-view">
				<a href="#user"><img class="circle" src="<?php echo template_uri();?>/images/logo.png"></a>
			</div>
		</li>
		<li><a href="<?php echo site_url();?>">Home</a></li>
		<li class="gnews"><a href="<?php echo site_url('gnews');?>">Gen News</a></li>
		<li><a href="https://nasional.sindonews.com/">Nasional</a></li>
		<li><a href="https://metro.sindonews.com/">Metronews</a></li>
		<li><a href="https://daerah.sindonews.com/">Daerah</a></li>
		<li><a href="https://ekbis.sindonews.com/">Ekonomi Bisnis</a></li>
		<li><a href="https://international.sindonews.com/">Nasional</a></li>
		<li><a href="https://sports.sindonews.com/">Sports</a></li>
		<li><a href="https://autotekno.sindonews.com/">Autotekno</a></li>
		<li><a href="https://lifestyle.sindonews.com/">Lifestyle</a></li>
		<li><a href="https://kalam.sindonews.com/">Kalam</a></li> 
		<li><a href="https://infografis.sindonews.com/">Infografis</a></li>
		<li><a href="https://informasia.sindonews.com/">Informasia</a>
		<li><a href="https://photo.sindonews.com/">Photo</a></li>
		<li><a href="https://video.sindonews.com/">Video</a>
	</ul>
</section>
<main>
    <div class="container clearfix">
        <div class="column-big pull-left clearfix">
            <div class="center pull-right">
                <section class="tab">
					<div class="tab-head">
						<ul class="tabs">
							<li><a href="<?php echo base_url();?>">Latest</a></li>
							<li><a href="<?php echo site_url('trending');?>">Trending</a></li>
							<li class="current"><a href="<?php echo site_url('about');?>">About</a></li>
						</ul>
					</div>
                    <div class="tab_content">
                        <?php echo $content['about'];?>
                    </div>
                </section>
            </div>
            <div class="leftbar pull-left">
                <?php echo $template['kategori'];?>
                <?php echo $template['trending'];?>
                <div class="r-side"><img src="<?php echo template_uri();?>/images/r_side_01.jpg" alt="R Side"></div>
            </div>
        </div>
        <div class="rightbar pull-right">
			<?php echo $template['promogen'];?>
            <div class="r-side"><img src="<?php echo template_uri();?>/images/r_side_02.jpg" alt="R Side"></div>
            <div class="r-side"><img src="<?php echo template_uri();?>/images/r_side_03.jpg" alt="R Side"></div>
            <?php echo $template['agenda'];?>
            <?php //echo $template['promoted'];?>
            <?php echo $template['minifooter'];?>
        </div>
    </div>
</main>
<footer></footer>
<?php echo $html['bottom_css']; ?>
<?php echo $html['js'];?>
<script>
    $(document).ready(function() { 
		if ( $(window).width() > 770) {
			$(window).scroll(function() { 
				if ($(this).scrollTop() > 1495){
					$('.leftbar').addClass("sticky");
					$(".leftbar").animate({top: "30px"}, 0);
					$('.leftbar .r-side').addClass("none");
				}
				else{
					$('.leftbar').removeClass("sticky");
					$('.leftbar .r-side').removeClass("none");
				}
				if ($(this).scrollTop() > 1262){
					$('.rightbar').addClass("sticky");
					$(".rightbar").animate({top: "30px"}, 0);
					$('.rightbar .r-side').addClass("none");
					$('.agenda').addClass("none");
				}
				else{
					$('.leftbar').removeClass("sticky");
					$('.rightbar').removeClass("sticky");
					$('.rightbar .r-side').removeClass("none");
					$('.agenda').removeClass("none");
				}
			});
		}
	
		$('.sidenav').sidenav();
		
		$('.scroll').jscroll({
			loadingHtml: '<section><div class="news-load"><img src="<?php echo template_uri(); ?>/images/ajax-loader.gif" alt="loading image"></div></section>',
			nextSelector: '.news-more a'
		});
    });
</script>
</body>
</html>