<?php

class Amparticle {

    var $CI;
    var $site_channel;
    var $site_domain;

    function __construct(){
        $this->CI = & get_instance();
        $this->site_channel = $this->CI->config->item('site_channel');
        $this->site_domain = $this->CI->config->item('read_domain');
    }

    function getArticle(){
        $current_url = parse_url(current_url());
        $kanal = $current_url['host'];
        $path = $current_url['path'];
        $params = array();
        $getpath = array_values(array_filter(explode('/',$path)));

        $getdata = $this->checkDb($getpath[1],$getpath[2]);
        if(isset($getpath[3])){
            $getslug = $this->fixTitleSlug(urldecode($getpath[3]),$getdata);
        } else {
            $getslug = '';
        }

        $content_id = $getdata[0]['id_content'];
        $channel_id = $getdata[0]['id_subkanal'];
        $uripath = $content_id . '/' . $channel_id . '/' . slug($getdata[0]['title']);

        $this->fixKanal($kanal,$channel_id,$uripath);
        $this->fixChannelID($getpath[2],$channel_id,$uripath);

        $getarticle = $this->CI->mgonews->getGen($getdata[0]['id_content']);

        $article['details'] = $getarticle;
        $article['id_content'] = $content_id;
        $article['create'] = $getarticle['create'];
        $article['slug_title'] = $getslug;

        return $article;
    }

    function fixKanal($kanal,$channel_id,$uripath){
        $urlsite = 'gensindo.sindonews.com';
        if($urlsite != $kanal){
            $urlgo = 'https://' . $urlsite . '/beritaamp/' . $uripath;
            redirect($urlgo,'location',301);
        }
    }

    function fixChannelID($cid_url,$channel_id,$uripath){
        if($cid_url != $channel_id){
            $urlsite = 'gensindo.sindonews.com';
            $urlgo = 'https://' . $urlsite . '/beritaamp/' . $uripath;
            redirect($urlgo,'location',301);
        }
    }

    function checkDb($content_id,$channel_id){
        $check = $this->CI->mgonews->getSimpleGen($content_id,$channel_id);
        if(count($check) > 0){
            return $check;
        }else{
            redirect(base_url(),'location',301);
        }
    }

    function getArraySlug($title_slug){
        $array_slug = explode('-',$title_slug);
        return $array_slug;
    }

    function getTimestamp($title_slug){
        $date_path = array();
        $array_slug = $this->getArraySlug($title_slug);
        if(is_numeric(end($array_slug))){
            if(date('Y',end($array_slug)) > 1970){
                $last_slug = date('Y-m-d',end($array_slug));
                $date_path = explode('-',$last_slug);
            }
        }

        return $date_path;
    }

    function fixTitleSlug($slug,$getdata){
        $data_slug = slug($getdata[0]['title']);
        $new_slug = '';
        if($slug == '' || $slug != $data_slug){
            $newurl = site_url('beritaamp') . '/' . $getdata[0]['id_content'] . '/' . $getdata[0]['id_subkanal'] . '/' . slug($getdata[0]['title']);
            redirect($newurl,'location',301);
        }else{
            $new_slug = $data_slug;
        }

        return $new_slug;
    }

    function fixChannelUrl($details){
        $uriread = $this->createPathUrl($details);
        $urlsite = 'https://' . $this->site_channel[$details['id_subkanal']] . '/';
        $urlgo = '';

        if($urlsite != base_url()){
            $urlgo = 'https://' . $this->site_domain[$details['id_subkanal']] . '/berita/' . $uriread;
            redirect($urlgo,'location',301);
        }
    }

    function createPathUrl($details){
        $uriread = $details['id_content'] . '/' . $details['id_subkanal'] . '/' . slug($details['title']);
        return $uriread;
    }

    function urlGo($details){
        $uriread = $this->createPathUrl($details);
        $urlsite = 'https://' . $this->site_domain[$details['id_subkanal']] . '/';
        if($urlsite != base_url()){
            $urlgo = 'https://' . $this->site_domain[$details['id_subkanal']] . '/beritaamp/' . $uriread;
        }else{
            $urlgo = base_url();
        }
        return $urlgo;
    }

    function getArticleListPageAds($start,$offset,$details){
        $content = contentFilter(html_entity_decode($details['content']),$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $total = count($xcontent);
        $slice_first = array_slice($xcontent,0,2);
        $offset_two = ($total - 2);
        $slice_two = array_slice($xcontent,2,$offset_two);

        /*
          $admid = '
          <div id="me-geniee-mobile-article-native">
          <div class="admid" id="geniee-mobile-article-native"><script>googletag.cmd.push(function(){googletag.display(\'geniee-mobile-article-native\');});</script></div>
          </div>';
         */
        $admid = '
            <div class="adsload" id="me-big-mobile-reactangle">
                <div class="admid" id="big-mobile-reactangle"><script>googletag.cmd.push(function(){googletag.display(\'big-mobile-reactangle\');});</script></div>
            </div>';

        $array_mid = array($admid);
        $newcontent = array_values(array_merge($slice_first,$array_mid,$slice_two));
        /*
          if($total > 7){
          $slice_bottom = array_slice($newcontent,0,5);
          $total_bottom = count($newcontent);
          $offset_bottom = ($total_bottom - 5);
          $slice_bottom_ads = array_slice($newcontent,5,$offset_bottom);
          $admidbottom = '<div id="me-native-adx-mobile-feed"><div class="admidbottom" id="native-adx-mobile-feed"><script>googletag.cmd.push(function(){googletag.display(\'native-adx-mobile-feed\');});</script></div></div>';
          $array_midbottom = array($admidbottom);
          $newcontent = array_values(array_merge($slice_bottom,$array_midbottom,$slice_bottom_ads));
          }
         */
        $cut_content = array_slice($newcontent,$start,$offset);
        $article = implode('<br><br>',$cut_content);

        return $article;
    }

    function getArticleNumListPageAds($details){
        $content = contentFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $total = count($xcontent);
        $slice_first = array_slice($xcontent,0,2);
        $offset_two = ($total - 2);
        $slice_two = array_slice($xcontent,2,$offset_two);

        /*
          $admid = '
          <div id="me-geniee-mobile-article-native">
          <div class="admid" id="geniee-mobile-article-native"><script>googletag.cmd.push(function(){googletag.display(\'geniee-mobile-article-native\');});</script></div>
          </div>';
         */

        $admid = '
            <div class="adsload" id="me-big-mobile-reactangle">
                <div class="admid" id="big-mobile-reactangle"><script>googletag.cmd.push(function(){googletag.display(\'big-mobile-reactangle\');});</script></div>
            </div>';

        $array_mid = array($admid);
        $newcontent = array_values(array_merge($slice_first,$array_mid,$slice_two));
        /*
          if($total > 7){
          $slice_bottom = array_slice($newcontent,0,5);
          $total_bottom = count($newcontent);
          $offset_bottom = ($total_bottom - 5);
          $slice_bottom_ads = array_slice($newcontent,5,$offset_bottom);
          $admidbottom = '<div id="me-native-adx-mobile-feed"><div class="admidbottom" id="native-adx-mobile-feed"><script>googletag.cmd.push(function(){googletag.display(\'native-adx-mobile-feed\');});</script></div></div>';
          $array_midbottom = array($admidbottom);
          $newcontent = array_values(array_merge($slice_bottom,$array_midbottom,$slice_bottom_ads));
          }
         */

        $count_exp = count($newcontent);
        return $count_exp;
    }

    function getArticleListPageAdsDesktop($start,$offset,$details){
        $content = contentFilter(html_entity_decode($details['content']),$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $newcontent = $xcontent;
        $total = count($xcontent);
        if($total > 7){
            $slice_first = array_slice($xcontent,0,2);
            $offset_two = ($total - 2);
            $slice_two = array_slice($xcontent,2,$offset_two);

            $admid = '
                <div id="me-detail-mediumrectangle">
                    <div class="admid" id="detail-mediumrectangle"><script>googletag.cmd.push(function(){googletag.display(\'detail-mediumrectangle\');});</script></div>
                </div>';
            /*
              $admid = '<div id="me-native-adx-mobile-feed"><div class="admid" id="native-adx-mobile-feed"><script>googletag.cmd.push(function(){googletag.display(\'native-adx-mobile-feed\');});</script></div></div>';
             */
            $array_mid = array($admid);
            $newcontent = array_values(array_merge($slice_first,$array_mid,$slice_two));
        }

        $cut_content = array_slice($newcontent,$start,$offset);
        $article = implode('<br><br>',$cut_content);

        return $article;
    }

    function getArticleNumListPageAdsDesktop($details){
        $content = contentFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $newcontent = $xcontent;
        $total = count($xcontent);
        if($total > 7){
            $slice_first = array_slice($xcontent,0,2);
            $offset_two = ($total - 2);
            $slice_two = array_slice($xcontent,2,$offset_two);

            $admid = '
                <div id="me-detail-mediumrectangle">
                    <div class="admid" id="detail-mediumrectangle"><script>googletag.cmd.push(function(){googletag.display(\'detail-mediumrectangle\');});</script></div>
                </div>';
            /*
              $admid = '<div id="me-native-adx-mobile-feed"><div class="admid" id="native-adx-mobile-feed"><script>googletag.cmd.push(function(){googletag.display(\'native-adx-mobile-feed\');});</script></div></div>';
             */
            $array_mid = array($admid);
            $newcontent = array_values(array_merge($slice_first,$array_mid,$slice_two));
        }
        $count_exp = count($newcontent);

        return $count_exp;
    }

    function getBacaJuga($baca){
        $baca_inline = '';
        $totalbaca = count($baca);
        
        if($totalbaca > 0){
            $baca_inline .= '<div class="baca-inline">';
            $baca_inline .= '<div class="baca-inline-head">Baca Juga:</div>';
            $baca_inline .= '<ul>';
            for($i = 0; $i < $totalbaca; $i++){
                $dtime[$i] = strtotime($baca[$i]['date_created']);
                $urlbaca[$i] = site_url('read') . '/' . $baca[$i]['content_id'] . '/' . $baca[$i]['channel_id'] . '/' . slug($baca[$i]['title'] . '-' . $dtime[$i]);
                $titlebaca[$i] = cleanWords($baca[$i]['title']);
                $baca_inline .= '<li><a href="' . $urlbaca[$i] . '">' . $titlebaca[$i] . '</a></li>';
            }
            
            //$baca_inline .= '<li><a href="https://autotekno.sindonews.com/newsread/1467437/120/akhir-tahun-saatnya-wujudkan-resolusi-mobil-baru-1576046074?utm_source=sindonews-baca&utm_medium=cpc&utm_campaign=belimobilgue.co.id" target="_blank">Akhir Tahun, Saatnya Wujudkan Resolusi Mobil Baru</a></li>';
            $baca_inline .= '</ul>';
            $baca_inline .= '</div>';            
        }
        
        $baca_inline .= '
            <div class="ads300 mt20 pt20 mbmin">
                <amp-fx-flying-carpet height="300px">
                    <amp-ad 
                        width=300 
                        height=600 
                        data-multi-size-validation="false" 
                        layout="fixed" 
                        type="doubleclick" 
                        data-slot="' . $this->CI->config->item('amp_gam_id') . '" 
                        json=\'{"targeting":{"pos":["middle_2"]}}\'>
                    </amp-ad>
                </amp-fx-flying-carpet>
            </div>';

        $result = array($baca_inline);
        return $result;
    }

    function getBacaJugaNum($baca){
        $baca_inline = '';
        $totalbaca = count($this->getBacaJuga($baca));

        $result = array();
        if($totalbaca > 0){
            $baca_inline = '<div class="baca-inline"></div>';
            $result = array($baca_inline);
        }

        return $result;
    }

    function cekArticleParagraph($content,$baca){
        $limit = 2;
        $total = count($content);
        $newcontent = $content;
        if($total > $limit){
            $position = 2;
            $slice_first = array_slice($content,0,$position);
            $offset_two = ($total - $position);
            $slice_two = array_slice($content,$position,$offset_two);

            $bacaJuga = $this->getBacaJuga($baca);
            $totalbaca = count($bacaJuga);
            if($totalbaca > 0){
                $newcontent = array_values(array_merge($slice_first,$bacaJuga,$slice_two));
            }
        }

        return $newcontent;
    }

    function cekArticleParagraphNum($content,$baca){
        $limit = 2;
        $total = count($content);
        $newcontent = $content;
        if($total > $limit){
            $position = 2;
            $slice_first = array_slice($content,0,$position);
            $offset_two = ($total - $position);
            $slice_two = array_slice($content,$position,$offset_two);

            $bacaJuga = $this->getBacaJugaNum($baca);
            $totalbaca = count($bacaJuga);
            if($totalbaca > 0){
                $newcontent = array_values(array_merge($slice_first,$bacaJuga,$slice_two));
            }
        }

        return $newcontent;
    }

    function getAdds($content){
        $limit = 8;
        $total = count($content);
        $newcontent = $content;
        if($total > $limit){
            $position = 8;

            $slice_first = array_slice($content,0,$position);
            $offset_two = ($total - $position);
            $slice_two = array_slice($content,$position,$offset_two);
            $ads = '
                <div class="ads300 mbmin">
                    <amp-ad width=300 height=250
                        type="doubleclick"
                        data-slot="' . $this->CI->config->item('amp_gam_id') . '"
                        json=\'{"targeting":{"pos":["middle_3"]}}\'>
                    </amp-ad>
                </div>';

            $contentads = array($ads);
            $newcontent = array_values(array_merge($slice_first,$contentads,$slice_two));
        }

        return $newcontent;
    }

    function getArticleListPageBaca($start,$offset,$details,$baca){
        //$debug = $this->debugYoutube() . $this->debugTwitter() . $this->debugInstagram() . $this->debugVideoIframe() . html_entity_decode($details['content']);
        $content = ampFilter(html_entity_decode($details['content']),$details['title']);

        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $cleanspace = str_replace('<br> <br>','<br><br>',$content);
            $xcontent = explode('<br><br>',$cleanspace);
        }else{
            $xcontent = $explode_content;
        }

        $newcontent = $this->cekArticleParagraph($xcontent,$baca);
        $contentads = $this->getAdds($newcontent);
        $cut_content = array_slice($contentads,$start,$offset);
        $article = implode('<br><br>',$cut_content);

        return $article;
    }

    function getArticleNumListPageBaca($details,$baca){
        $content = ampFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $cleanspace = str_replace('<br> <br>','<br><br>',$content);
            $xcontent = explode('<br><br>',$cleanspace);
        }else{
            $xcontent = $explode_content;
        }

        $newcontent = $this->cekArticleParagraphNum($xcontent,$baca);
        $contentads = $this->getAdds($newcontent);
        $count_exp = count($contentads);

        return $count_exp;
    }
    
    function getDesktopAdds($content){
        $limit = 7;
        $total = count($content);
        $newcontent = $content;
        if($total > $limit){
            $position = 7;

            $slice_first = array_slice($content,0,$position);
            $offset_two = ($total - $position);
            $slice_two = array_slice($content,$position,$offset_two);
            $ads = '
                <div class="ads300 mbmin">
                    <div id="div-gpt-ad-middle_2"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_2\');});</script></div>
                </div>';

            $contentads = array($ads);
            $newcontent = array_values(array_merge($slice_first,$contentads,$slice_two));
        }

        return $newcontent;
    }

    function getArticleListPageBacaDesktop($start,$offset,$details,$baca){
        $content = contentFilter(html_entity_decode($details['content']),$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $total = count($xcontent);
        $slice_first = array_slice($xcontent,0,3);
        $offset_two = ($total - 3);
        $slice_two = array_slice($xcontent,3,$offset_two);

        $baca_inline = '';
        $baca_inline .= '
            <div class="ads300 mb20">
                <div id="div-gpt-ad-middle_1"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_1\');});</script></div>
            </div>';
        
        $totalbaca = count($baca);
        if($totalbaca > 0){
            $baca_inline .= '<div class="baca-inline">';
            $baca_inline .= '<div class="baca-inline-head">Baca Juga:</div>';
            $baca_inline .= '<ul>';
            for($i = 0; $i < $totalbaca; $i++){
                $dtime[$i] = strtotime($baca[$i]['date_created']);
                $urlbaca[$i] = site_url('read') . '/' . $baca[$i]['id_news'] . '/' . $baca[$i]['id_subkanal'] . '/' . slug($baca[$i]['title'] . '-' . $dtime[$i]);
                $titlebaca[$i] = cleanWords($baca[$i]['title']);
                $baca_inline .= '<li><a href="' . $urlbaca[$i] . '">' . $titlebaca[$i] . '</a></li>';
            }
            //$baca_inline .= '<li><a href="https://lifestyle.sindonews.com/read/1406160/156/yang-wajib-anda-ketahui-sebelum-memulai-perjalanan-perdana-ke-singapura-1558434021?utm_source=traveloka.com&utm_medium=cpc&utm_campaign=Traveloka&utm_term=Tiket%20Pesawat%20Singapura" target="_blank">Yang Wajib Anda Ketahui Sebelum Memulai Perjalanan Perdana ke Singapura</a></li>';
            $baca_inline .= '</ul>';
            $baca_inline .= '</div>';
            
            $array_mid = array($baca_inline);            
            $first = array_values(array_merge($slice_first,$array_mid,$slice_two));
            $newcontent = $this->getDesktopAdds($first);
        }else{
            $newcontent = array_values(array_merge($slice_first,$slice_two));
        }

        $cut_content = array_slice($newcontent,$start,$offset);
        $article = implode('<br><br>',$cut_content);

        return $article;
    }

    function getArticleNumListPageBacaDesktop($details,$baca){
        $content = contentFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $total = count($xcontent);
        $slice_first = array_slice($xcontent,0,3);
        $offset_two = ($total - 3);
        $slice_two = array_slice($xcontent,3,$offset_two);

        $baca_inline = '';
        $totalbaca = count($baca);
        if($totalbaca > 0){
            $baca_inline = '<div class="baca-inline"></div>';
            $array_mid = array($baca_inline);
            $newcontent = array_values(array_merge($slice_first,$array_mid,$slice_two));
        }else{
            $newcontent = array_values(array_merge($slice_first,$slice_two));
        }

        $count_exp = count($newcontent);
        return $count_exp;
    }

    function getArticleListPage($start,$offset,$details){
        $content = contentFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }
        $cut_content = array_slice($xcontent,$start,$offset);
        $article = implode('<br><br>',$cut_content);

        return $article;
    }

    function getArticleNumListPage($details){
        $content = contentFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }
        $count_exp = count($xcontent);

        return $count_exp;
    }

    function articlePaging($pagearticle,$details){
        $urlgo = $this->urlGo($details);
        $explode_content = explode('<br /><br />',$details['content']);
        $count_exp = count($explode_content);
        $cut_content = array_chunk($explode_content,7);
        $num_cut_content = count($cut_content);
        for($i = 0; $i < $num_cut_content; $i++){
            $pagebreak[] = implode('<br><br>',$cut_content[$i]);
        }

        if($count_exp > 14 || $count_exp > 21 || $count_exp > 28 || $count_exp > 35 || $count_exp > 41){
            $checkpage = count($pagebreak);
        }else{
            $checkpage = 0;
        }

        $data['checkpage'] = $checkpage;

        if($checkpage > 1){
            $data['numpage'] = $checkpage;
            if($pagearticle < $checkpage){
                $x = $pagearticle;
                $data['page'] = $pagearticle + 1;
                $data['xcontent'] = stripslashes($pagebreak[$x]);
            }else{
                redirect($urlgo,'location',301);
            }
        }else{
            $data['xcontent'] = stripslashes($details['content']);
            $data['numpage'] = 0;
            $data['page'] = 0;
        }

        if($pagearticle > 0){
            $data['pagenumeber'] = ' | Halaman ' . ($pagearticle + 1);
        }else{
            $data['pagenumeber'] = '';
        }

        return $data;
    }

    function createCanonical($pagearticle,$site_url,$checkpage){
        $canonical = '';
        if($pagearticle == 0){
            $nnext = ($pagearticle + 1);
            $canonical .= add_canonical('next',$site_url . '/' . $nnext);
        }else{
            $nprev = ($pagearticle - 1);
            if($nprev == 0){
                $canonical = add_canonical('prev',$site_url);
            }else{
                $canonical = add_canonical('prev',$site_url . '/' . $nprev);
            }

            $nnext = ($pagearticle + 1);
            if($nnext < $checkpage){
                $canonical .= add_canonical('next',$site_url . '/' . $nnext);
            }else{
                $canonical .= '';
            }
        }
        $canonical .= add_canonical('canonical',$site_url);

        return $canonical;
    }

    function pagingCanonical($url_current,$site_url,$start,$offset,$totalData){
        $canonical = '';
        if($start == 0){
            $nnext = ($start + $offset);
            $canonical .= add_canonical('next',$site_url . '/' . $nnext);
        }else{
            $nprev = ($start - $offset);
            if($nprev <= 0){
                $canonical .= add_canonical('prev',$site_url);
            }else{
                $canonical .= add_canonical('prev',$site_url . '/' . $nprev);
            }

            $nnext = ($start + $offset);
            if($nnext < $totalData){
                $canonical .= add_canonical('next',$site_url . '/' . $nnext);
            }else{
                $canonical .= '';
            }
        }
        $canonical .= add_canonical('canonical',$url_current);

        return $canonical;
    }

    function newsKeywords($input){
        $explode = explode(',',$input);
        $remove_duplicate = array_unique($explode);
        $sorting = array_values($remove_duplicate);
        $keywords = implode(',',$sorting);

        return $keywords;
    }

}
