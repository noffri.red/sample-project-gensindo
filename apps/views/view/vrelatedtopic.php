<?php
$total_topic = count($topic);
if($total_topic > 0) {
    $data_topic = $topic;

    echo '<div class="topic-suggest">';
    echo '<ul>';
    echo '<li class="bold">TOPIC :</li>';
    for ($i = 0; $i < $total_topic; $i++) {
        $link[$i] = site_url('tag/' . $data_topic[$i]['id_topic'] . '/' . $data_topic[$i]['slug_topic']);
        $topik[$i] = cleanWords($data_topic[$i]['topic']);
        echo '<li><a href="'. $link[$i] .'">#'. $topik[$i] .'</a></li>';
    }
    echo '</ul>';
    echo '</div>';
}