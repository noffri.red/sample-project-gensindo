<?php

class articleimaji {

    var $CI;
    var $site_channel;

    function __construct(){
        $this->CI = & get_instance();
        $this->CI->load->helper('textlimiter');

        $this->site_channel = $this->CI->config->item('site_channel');
    }

    function getArticle(){
        $article = array();
        $params = $this->checkArticle();

        /*$key = "article_" . $params['content_id'] . "_" . $params['page'];
        $getredis = $this->CI->textcache->redisGet($key);
        if($getredis){
            $result = json_decode($getredis,TRUE);
        }else{
            if($this->xmlExist($params)){
                $getarticle = $this->CI->mredisnews->getXmlDetail($params);
            }else{
                $getarticle = $this->CI->mredisnews->getDbDetail($params['content_id']);
            }

            $article['details'] = $getarticle;
            $article['content_id'] = $params['content_id'];
            $article['create'] = $getarticle['create'];

            $redisdata = json_encode($article);
            $this->CI->textcache->redisSet($key,600,$redisdata);
            $result = $article;
        }*/

        return $params;
    }

    function checkArticle(){
        $current_url = parse_url(current_url());
        $path = $current_url['path'];
        $params = array();

        $getpath = array_values(array_filter(explode('/',$path)));
        if(count($getpath) >= 3 && count($getpath) < 6){
            $getdata = $this->checkDb($getpath[1]);
            $id_content = $getdata[0]['id_imaji'];

            $urldate = strtotime($getdata[0]['date_created']);
            $uripath = $id_content . '/' . $getdata[0]['slug_title'] . '-' . $urldate;

            /*$this->fixSubKanal($getpath[2],$id_subkanal,$uripath);*/

            /*$timestamp = $this->getTimestamp($getpath[2]);
            $this->fixTimestamp($timestamp,$uripath);*/

            $getslug = $this->fixTitleSlug($getpath[2],$getdata);

            $params = $getdata[0];
        }else{
            redirect(base_url(),'location',301);
        }

        return $params;
    }

    function checkDb($id_imaji){
        $check = $this->CI->mredisnews->getDbContentImaji($id_imaji);
        $res_check = $check['results'];
        if(count($res_check) > 0){
            return $res_check;
        }else{
            redirect(base_url(),'location',301);
        }
    }

    function getTimestamp($title_slug){
        $date_path = array();
        $array_slug = $this->getArraySlug($title_slug);
        if(is_numeric(end($array_slug))){
            if(date('Y',end($array_slug)) > 1970){
                $last_slug = date('Y-m-d',end($array_slug));
                $date_path = explode('-',$last_slug);
            }
        }

        return $date_path;
    }

    function getArraySlug($title_slug){
        $array_slug = explode('-',$title_slug);
        return $array_slug;
    }

    function fixSubKanal($cid_url,$id_content,$uripath){
        if($cid_url != $id_content){
            $urlgo = site_url('read/'). $uripath;
            redirect($urlgo,'location',301);
        }
    }

    function fixTimestamp($timestamp,$id_subkanal,$uripath){
        if(count($timestamp) == 0){
            $urlgo = $this->site_channel[$id_subkanal] . '/read/' . $uripath;
            redirect($urlgo,'location',301);
        }
    }

    function fixTitleSlug($slug,$getdata){
        $dtime = strtotime($getdata[0]['date_created']);
        $data_slug = $getdata[0]['slug_title'] . '-' . $dtime;
        $new_slug = '';
        if($slug == '' || $slug != $data_slug){
            $newurl = site_url('read') . '/' . $getdata[0]['id_content'] . '/' . $getdata[0]['id_subkanal'] . '/' . $getdata[0]['slug_title'] . '-' . $dtime;
            redirect($newurl,'location',301);
        }else{
            $new_slug = $data_slug;
        }

        return $new_slug;
    }

    function getArticleListPage($start,$offset,$details){
        $content = contentFilter($details['content'], $details['title']);
        $explode_content = explode('<hr />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<hr />',$content);
        } else {
            $xcontent = $explode_content;
        }
        $cut_content = array_slice($xcontent,$start,$offset);
        $article = implode('<hr />',$cut_content);

        return $article;
    }

    function getArticleNumListPage($details){
        $content = contentFilter($details['content'], $details['title']);
        $explode_content = explode('<hr />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<hr />',$content);
        } else {
            $xcontent = $explode_content;
        }
        $count_exp = count($xcontent);

        return $count_exp;
    }

    function pagingCanonical($url_current,$start,$offset,$totalData){
        $canonical = '';
        if($start == 0){
            $nnext = ($start + $offset);
            $canonical .= add_canonical('next',$url_current . '/' . $nnext);
        }else{
            $nprev = ($start - $offset);
            if($nprev <= 0){
                $canonical .= add_canonical('prev',$url_current);
            }else{
                $canonical .= add_canonical('prev',$url_current . '/' . $nprev);
            }

            $nnext = ($start + $offset);
            if($nnext < $totalData){
                $canonical .= add_canonical('next',$url_current . '/' . $nnext);
            }else{
                $canonical .= '';
            }
        }
        $canonical .= add_canonical('canonical',$url_current);

        return $canonical;
    }

    function newsKeywords($input){
        $explode = explode(',',$input);
        $remove_duplicate = array_unique($explode);
        $sorting = array_values($remove_duplicate);
        if(count($sorting) > 5){
            $keywords_data = array_slice($sorting,0,5);
        } else {
            $keywords_data = $sorting;
        }
        $keywords = implode(',',$keywords_data);

        return $keywords;
    }

    function getArticleListPageBacaDesktop($start,$offset,$details,$baca){
        $content = contentFilter(html_entity_decode($details['content']),$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $total = count($xcontent);
        $slice_first = array_slice($xcontent,0,3);
        $offset_two = ($total - 3);
        $slice_two = array_slice($xcontent,3,$offset_two);

        $baca_inline = '';
        $totalbaca = count($baca);
        if($totalbaca > 0){
            $baca_inline .= '<div class="baca-inline">';
            $baca_inline .= '<div class="baca-inline-head">Baca Juga:</div>';
            $baca_inline .= '<ul>';
            for($i = 0; $i < $totalbaca; $i++){
                $dtime[$i] = strtotime($baca[$i]['date_created']);
                $urlbaca[$i] = site_url('read') . '/' . $baca[$i]['id_content'] . '/' . $baca[$i]['id_subkanal'] . '/' . $baca[$i]['slug_title'] . '-' . $dtime[$i];
                $titlebaca[$i] = cleanWords($baca[$i]['title']);
                $baca_inline .= '<li><a href="' . $urlbaca[$i] . '">' . $titlebaca[$i] . '</a></li>';
            }
            $baca_inline .= '</ul>';
            $baca_inline .= '</div>';
        }

        $array_mid = array($baca_inline);
        $newcontent = array_values(array_merge($slice_first,$array_mid,$slice_two));

        $cut_content = array_slice($newcontent,$start,$offset);
        $article = implode('<br><br>',$cut_content);

        return $article;
    }

    function getArticleNumListPageBacaDesktop($details){
        $content = contentFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $total = count($xcontent);
        $slice_first = array_slice($xcontent,0,3);
        $offset_two = ($total - 3);
        $slice_two = array_slice($xcontent,3,$offset_two);

        $baca_inline = '<div class="baca-inline"></div>';

        $array_mid = array($baca_inline);
        $newcontent = array_values(array_merge($slice_first,$array_mid,$slice_two));

        $count_exp = count($newcontent);
        return $count_exp;
    }

    function getArticleListPageBaca($start,$offset,$details,$baca){
        $content = contentFilter(html_entity_decode($details['content']),$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $total = count($xcontent);
        $slice_first = array_slice($xcontent,0,2);
        $offset_two = ($total - 2);
        $slice_two = array_slice($xcontent,2,$offset_two);

        $baca_inline = '';
        $totalbaca = count($baca);
        if($totalbaca > 0){
            $baca_inline .= '<div class="baca-inline">';
            $baca_inline .= '<div class="baca-inline-head">Baca Juga:</div>';
            $baca_inline .= '<ul>';
            for($i = 0; $i < $totalbaca; $i++){
                $dtime[$i] = strtotime($baca[$i]['date_created']);
                $urlbaca[$i] = site_url('read') . '/' . $baca[$i]['id_content'] . '/' . $baca[$i]['id_subkanal'] . '/' . $baca[$i]['slug_title'] . '-' . $dtime[$i];
                $titlebaca[$i] = cleanWords($baca[$i]['title']);
                $baca_inline .= '<li><a href="' . $urlbaca[$i] . '">' . $titlebaca[$i] . '</a></li>';
            }
            $baca_inline .= '</ul>';
            $baca_inline .= '</div>';
        }

        $array_mid = array($baca_inline);
        $newcontent = array_values(array_merge($slice_first,$array_mid,$slice_two));

        $cut_content = array_slice($newcontent,$start,$offset);
        $article = implode('<br><br>',$cut_content);

        return $article;
    }

    function getArticleNumListPageBaca($details){
        $content = contentFilter($details['content'],$details['title']);
        $explode_content = explode('<br /><br />',$content);
        if(count($explode_content) == 1){
            $xcontent = explode('<br><br>',$content);
        }else{
            $xcontent = $explode_content;
        }

        $total = count($xcontent);
        $slice_first = array_slice($xcontent,0,2);
        $offset_two = ($total - 2);
        $slice_two = array_slice($xcontent,2,$offset_two);

        $baca_inline = '<div class="baca-inline"></div>';

        $array_mid = array($baca_inline);
        $newcontent = array_values(array_merge($slice_first,$array_mid,$slice_two));

        $count_exp = count($newcontent);
    }
}