<?php

$total = count($related);
if($total > 0){
    echo '<div class="link-article">';
    echo '<div class="head">Artikel Terkait</div>';
    echo '<div class="photo-box-list">';

    for($i = 0; $i < $total; $i++){
        $dtime[$i] = strtotime($related[$i]['date_created']);
        $l[$i] = site_url('berita') . '/' . $related[$i]['id_content'] . '/' . $related[$i]['id_subkanal'] . '/' . slug($related[$i]['title']) . '-' . $dtime[$i];
        $title[$i] = cleanWords($related[$i]['title']);

        $subkanal[$i] = $related[$i]['subkanal'];

        if($related[$i]['images'] != ''){
            $img[$i] = images_uri() . '/dyn/287/gensindo/content/' . date('Y/m/d',strtotime($related[$i]['date_created'])) . '/' . $related[$i]['id_subkanal'] . '/' . $related[$i]['id_content'] . '/' . $related[$i]['images'];
            $showimg[$i] = '
                <div class="photo-box-pict">
                    <a href="' . $l[$i] . '" class="image"><img src="' . $img[$i] . '" alt="' . cleanHTML($title[$i]) . '"></a>
                </div>';
        }else{
            $showimg[$i] = '';
        }

        echo '
            <div class="photo-box-item">
                ' . $showimg[$i] . '
                <div class="photo-box-text"><a href="' . $l[$i] . '">' . $title[$i] . '</a></div>
            </div>';
    }

    echo '</div>';
    echo '</div>';
}
