<?php

$total = count($related);
if($total > 0){
    if($topiklist != ''){
        /* Topik Terkait */
        $topic = explode(',',$topiklist);

        echo '<div class="topik-terkait">';
        echo '<div class="topik-terkait-title">Topik Terkait :</div>';

        for($i = 0; $i < count($topic); $i++){
            $getTopicData[$i] = $this->mgonews->getTopicList($topic[$i]);
            $urltopic[$i] = 'https://www.sindonews.com/topic/' . $getTopicData[$i][0]['id_topik'] . '/' . slug($getTopicData[$i][0]['topik']);
            $topic_title[$i] = strtolower($getTopicData[$i][0]['topik']);
            echo '<a href="' . $urltopic[$i] . '"><i class="fa fa-tags" aria-hidden="true"></i> ' . $getTopicData[$i][0]['topik'] . '</a>';
        }
        echo '</div>';

        /* Berita Terkait */
        echo '<div class="news article-topic">';
        echo '<div class="heading">Berita Terkait</div>';
        echo '<ul>';

        for($i = 0; $i < $total; $i++){
            $site_domain[$i] = $this->config->item('read_domain');

            $dtime[$i] = strtotime($related[$i]['date_created']);
            $l[$i] = 'https://' . $site_domain[$i][$related[$i]['id_subkanal']] . '/read/' . $related[$i]['id_news'] . '/' . $related[$i]['id_subkanal'] . '/' . slug($related[$i]['title']) . '-' . $dtime[$i];
            $title[$i] = cleanWords($related[$i]['title']);

            echo '
            <li>
                <div class="breaking-title">
                    <a href="' . $l[$i] . '">' . $title[$i] . '</a>
                </div>
            </li>';
        }

        echo '</ul>';
        echo '</div>';
    }
}