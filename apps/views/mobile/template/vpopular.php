<?php

$total = count($trendingpopular);
if($total > 0){
    echo '<section class="terpopuler">';
    echo '<div class="title-gensindo">Terpopuler</div>';
    echo '<ul>';

    for($i = 0; $i < $total; $i++){
        $dtime[$i] = strtotime($trendingpopular[$i]['date_created']);
        $link[$i] = site_url('read/' . $trendingpopular[$i]['content_id'] . '/' . $trendingpopular[$i]['channel_id'] . '/' . slug($trendingpopular[$i]['title']) . '-' . $dtime[$i]);
        $title[$i] = cleanWords($trendingpopular[$i]['title']);

        echo '
            <li>
                <div class="popular-num">' . ($i + 1) . '</div>
                <div class="popular-title"><a href="' . $link[$i] . '">' . $title[$i] . '</a></div>
            </li>';
    }

    echo '</ul>';
    echo '</section>';
}