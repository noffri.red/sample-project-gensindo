<?php
echo '<section class="about">
    <div class="our-page">
        <ul>
            <li class="title">Visit Our Page :</li>
            <li><a href="https://www.facebook.com/sindonews" target="_blank" title="Facebook"><img src="'. template_uri() .'/images/icon_facebook_color.jpg" alt="Facebook"></a></li>
            <li><a href="https://twitter.com/sindonews" target="_blank" title="twitter"><img src="'. template_uri() .'/images/icon_twitter_color.jpg" alt="Twitter"></a></li>
            <li><a href="'.site_url('feed').'"><img src="'. template_uri() .'/images/icon_rss_color.jpg" alt="Rss"></a></li>
        </ul>
    </div>
    <div class="sitemap">
        <ul>
            <li><a href="http://about.sindonews.com/">About</a></li>
            <li><a href="http://about.sindonews.com/privacy-policy">Privacy</a></li>
            <li><a href="http://about.sindonews.com/term-of-service">Terms</a></li>
            <li><a href="http://about.sindonews.com/#kontak-kami">Advertising</a></li>
            <li><a href="http://about.sindonews.com/#kontak-kami">Contact</a></li>
        </ul>
    </div>
    <div class="copyright clearfix">
        <div class="mnc-logo pull-left">
            <img src="'. template_uri() .'/images/logo_mnc.png" alt="MNC">
        </div>
        <div class="text pull-right">
            <div class="copy">Copyright &copy; '.date("Y").' <span>SINDOnews.com</span> All Rights Reserved</div>
            <div class="render">/ '.$this->uri->segment(1).'- {elapsed_time}s ('.infoServer().')</div>
        </div>
    </div>
</section>';

$key = 's1nd0n3ws.c0m-MNCm3d14';
$token = md5($key);
$token_encode = base64_encode($token);
echo '<div class="wpinfo" id="pushinfo" data-url="'. site_url('blackclouds') . '?code=sindoapi&v=11.0&source=cloud' . rand(1,99) . '&z=' . time() .'" data-reg="'. site_url('nature') . '?code=sindoapi&v=11.0&source=cloud' . rand(1,99) . '&z=' . time() .'" data-group="gensindo" data-key="'. $token_encode .'" data-source="'. base_url() .'"></div>';

echo '
	<div class="sintrack">
        <div class="sindoframe"></div>
        <div class="sindomp"></div>
        <div class="sindoev"></div>
        <div class="sindopix"></div>
        <div class="axtrack"></div>
        <div class="cxtrack"></div>
        <div class="axsindo"></div>
        <div class="cxrectrack"></div>
        <div class="axrecsindo"></div>
        <div class="adtrack"></div>
        <div class="adbounce"></div>
        <div class="ock"></div>
        <div class="ick"></div>
        <div id="artsindo"></div>
        <div id="artcode"></div>
    </div>
';