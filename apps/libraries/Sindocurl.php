<?php

class Sindocurl {

    function post($url,$headers,$data_post){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_POST,true);
        if($headers != ''){
            curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
        }
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$data_post);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    function get($url,$headers,$data_post = array()){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        if($headers != ''){
            curl_setopt($ch,CURLOPT_HTTPHEADER,$headers);
        }
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        if(count($data_post) > 0){
            curl_setopt($ch,CURLOPT_POSTFIELDS,$data_post);
        }

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    function getssl($url){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,true);

        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

}
