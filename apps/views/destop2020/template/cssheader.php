<link rel="stylesheet" href="<?php echo template_uri(); ?>/css/materialize.min.css<?php echo $upVersion;  ?>"> 
<link rel="stylesheet" href="<?php echo template_uri(); ?>/css/base.min.css<?php echo $upVersion;  ?>">
<link rel="stylesheet" href="<?php echo template_uri(); ?>/css/header.min.css<?php echo $upVersion;  ?>"> <!-- Header -->
<link rel="stylesheet" href="<?php echo template_uri(); ?>/css/body.min.css<?php echo $upVersion;  ?>"> <!-- Body -->
<link rel="stylesheet" href="<?php echo template_uri(); ?>/css/footer.min.css<?php echo $upVersion;  ?>"> <!-- Footer -->

<link rel="stylesheet" href="<?php echo template_uri(); ?>/css/font-awesome.min.css<?php echo $upVersion;  ?>"> <!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo template_uri(); ?>/css/jquery.fancybox.min.css<?php echo $upVersion;  ?>"> <!-- Fancybox -->
<link rel="stylesheet" href="<?php echo template_uri(); ?>/css/swiper.min.css<?php echo $upVersion;  ?>"> <!-- Swiper -->