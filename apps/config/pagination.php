<?php

$config['paging']['full_tag_open'] = '<div class="news-link-more">';
$config['paging']['full_tag_close'] = '</div>';

$config['paging']['first_link'] = '&laquo;';
$config['paging']['first_tag_open'] = '<li>';
$config['paging']['first_tag_close'] = '</li>';

$config['paging']['prev_link'] = '&lsaquo;';
$config['paging']['prev_tag_open'] = '<li>';
$config['paging']['prev_tag_close'] = '</li>';

$config['paging']['last_link'] = '&raquo;';
$config['paging']['last_tag_open'] = '<li>';
$config['paging']['last_tag_close'] = '</li>';

$config['paging']['next_link'] = '&rsaquo;';
$config['paging']['next_tag_open'] = '<li>';
$config['paging']['next_tag_close'] = '</li>';

$config['paging']['cur_tag_open'] = '<li><a class="active">';
$config['paging']['cur_tag_close'] = '</a></li>';

$config['paging']['num_tag_open'] = '<li>';
$config['paging']['num_tag_close'] = '</li>';

$config['artpaging']['full_tag_open'] = '<div class="box-paging"><ul class="article-paging">';
$config['artpaging']['full_tag_close'] = '</ul></div>';

$config['artpaging']['first_link'] = '<i class="fa fa-angle-double-left" aria-hidden="true"></i>';
$config['artpaging']['first_tag_open'] = '<li>';
$config['artpaging']['first_tag_close'] = '</li>';

$config['artpaging']['prev_link'] = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
$config['artpaging']['prev_tag_open'] = '<li>';
$config['artpaging']['prev_tag_close'] = '</li>';

$config['artpaging']['last_link'] = '<i class="fa fa-angle-double-right" aria-hidden="true"></i>';
$config['artpaging']['last_tag_open'] = '<li>';
$config['artpaging']['last_tag_close'] = '</li>';

$config['artpaging']['next_link'] = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
$config['artpaging']['next_tag_open'] = '<li>';
$config['artpaging']['next_tag_close'] = '</li>';

$config['artpaging']['cur_tag_open'] = '<li class="active">';
$config['artpaging']['cur_tag_close'] = '</li>';

$config['artpaging']['num_tag_open'] = '<li>';
$config['artpaging']['num_tag_close'] = '</li>';

$config['paging_search']['full_tag_open'] = '<ul>';
$config['paging_search']['full_tag_close'] = '</ul>';

$config['paging_search']['first_link'] = '<i class="fa fa-angle-double-left"></i>';
$config['paging_search']['first_tag_open'] = '<li>';
$config['paging_search']['first_tag_close'] = '</li>';

$config['paging_search']['prev_link'] = '<i class="fa fa-angle-left"></i>';
$config['paging_search']['prev_tag_open'] = '<li>';
$config['paging_search']['prev_tag_close'] = '</li>';

$config['paging_search']['last_link'] = '<i class="fa fa-angle-double-right"></i>';
$config['paging_search']['last_tag_open'] = '<li>';
$config['paging_search']['last_tag_close'] = '</li>';

$config['paging_search']['next_link'] = '<i class="fa fa-angle-right"></i>';
$config['paging_search']['next_tag_open'] = '<li>';
$config['paging_search']['next_tag_close'] = '</li>';

$config['paging_search']['cur_tag_open'] = '<li class="current">';
$config['paging_search']['cur_tag_close'] = '</li>';

$config['paging_search']['num_tag_open'] = '<li>';
$config['paging_search']['num_tag_close'] = '</li>';