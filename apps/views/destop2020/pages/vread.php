<!doctype html>
<html  lang="id-ID">

<head>
  <!-- Meta SEO -->
  <title><?php echo $html['title']; ?></title>
  <meta charset="utf-8">
  <meta http-equiv="x-dns-prefetch-control" content="on">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=0">
  <meta name="theme-color" content="#499AE8">

  <!-- Icon -->
  <link rel="shortcut icon" href="<?php echo template_uri(); ?>/image/favicon.ico">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-16x16.png" sizes="16x16">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-96x96.png" sizes="96x96">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-160x160.png" sizes="160x160">
  <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/image/favicon-192x192.png" sizes="192x192">
  <!-- Font -->
   <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;900&display=swap" rel="stylesheet"> <!-- Nunito -->
   <?php echo $html['metaname']; ?>
   <?php echo $html['canonical']; ?>
    
   <meta property="fb:app_id" content="325657497499535">
   <meta property="fb:pages" content="248570208512540">

   <link rel="amphtml" href="<?php echo $content['amp_url']; ?>">
   <link rel="alternate" title="GenSINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>

  
  <!-- Development -->
  <?php echo $html['css']; ?>
  <?php echo $template['newcss']; ?>

  <!-- Production -->
  <!-- <link rel="stylesheet" href="assets/css/styles.min.css"> -->
  <!-- Style -->

  <!-- Mediaquery -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-HDPI.min.css" media="screen and (min-width: 1083px) and (max-width: 1290px)"> <!-- HDPI Screen -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-MDPI.min.css" media="screen and (min-width: 993px) and (max-width: 1082px)"> <!-- MDPI Screen -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-SDPI.min.css" media="screen and (min-width: 769px) and (max-width: 992px)"> <!-- SDPI Screen -->
  <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-mobile.min.css" media="screen and (max-width: 768px)"> <!-- Mobile Screen -->


  <script type="application/ld+json">
        {
            "@context": "https://schema.org",
            "@type": "NewsArticle",
            "mainEntityOfPage": {
                "@type": "WebPage",
                "@id": "<?php echo $content['site_url']; ?>"
            },
            "headline": "<?php echo $content['site_title']; ?>",
            "image": {
                "@type": "ImageObject",
                "url": "<?php echo $content['site_image']; ?>",
                "height": 413,
                "width": 620
            },
            "datePublished": "<?php echo $content['site_publish']; ?>",
            "dateModified": "<?php echo $content['site_publish']; ?>",
            "author": {
                "@type": "Person",
                "name": "<?php echo $content['site_reporter']; ?>"
            },
            "publisher": {
                "@type": "Organization",
                "name": "SINDOnews.com",
                "logo": {
                    "@type": "ImageObject",
                    "url": "https://sm.sindonews.net/dyn/600/mobile/2016/images/logo-sindonews.png",
                    "width": 600,
                    "height": 55
                }
            },
            "description": "<?php echo $content['site_summary']; ?>"
        }
    </script>
    <script type="application/ld+json">
        {
            "@context":"https://schema.org",
            "@type":"BreadcrumbList",
            "itemListElement":[{
                "@type":"ListItem",
                "position":1,
                "item":{
                    "@id":"<?php echo base_url(); ?>",
                    "name":"home"
                }
            },{
                "@type":"ListItem",
                "position":2,
                "item":{
                    "@id": "<?php echo $content['site_url']; ?>",
                    "name": "<?php echo $content['site_label']; ?>"
                }
            }]
        }
    </script>
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebPage",
            "headline": "<?php echo addcslashes($content['site_title'],'"\\/'); ?>",
            "url": "<?php echo $content['site_url']; ?>",
            "datePublished": "<?php echo $content['site_publish']; ?>",
            "image": {
                "@type": "ImageObject",
                "url": "<?php echo $content['site_image']; ?>",
                "height": 413,
                "width": 620
            },
            "description": "<?php echo addcslashes($content['site_summary'],'"\\/'); ?>"
        }
    </script>
    
    <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
    <?php echo $html['topjs']; ?>
    <script>
        var PREBID_TIMEOUT = 700;
        var FAILSAFE_TIMEOUT = 3000;

        var adUnits = [{
            code: 'div-gpt-ad-billboard',
            mediaTypes: {
                banner: {
                    sizes: [[970,250],[970,90]]
                }
            },
            bids: [{
                bidder: 'criteo',
                params: {
                    networkId: 9519,
                    publisherSubId: 'sindonews'
                }

            }]
        },{
          code: 'div-gpt-ad-middle_1',
            mediaTypes: {
                banner: {
                    sizes: [[336,280],[300,250]]
                }
            },
            bids: [{
                bidder: 'criteo',
                    params: {
                        networkId: 9519,
                        publisherSubId: 'sindonews'
                    }

            }]
        },{
          code: 'div-gpt-ad-middle_2',
            mediaTypes: {
                banner: {
                    sizes: [[336,280],[300,250]]
                }
            },
            bids: [{
                bidder: 'criteo',
                params: {
                    networkId: 9519,
                    publisherSubId: 'sindonews'
                }

            }]
        },{
          code: 'div-gpt-ad-right_sidebar_1',
            mediaTypes: {
                banner: {
                    sizes: [[300,250],[300,600]]
                }
            },
            bids: [{
                bidder: 'criteo',
                params: {
                    networkId: 9519,
                    publisherSubId: 'sindonews'
                }

            }]
        }];

        var googletag = googletag || {};
        googletag.cmd = googletag.cmd || [];
        googletag.cmd.push(function() {
            googletag.pubads().disableInitialLoad();
        });

        var pbjs = pbjs || {};
        pbjs.que = pbjs.que || [];

        pbjs.que.push(function() {
          pbjs.setConfig({
                userSync: {
                    userIds: [{
                        name: "criteo",
                    }]
                },
                priceGranularity: "auto"
            });
            pbjs.addAdUnits(adUnits);
            pbjs.requestBids({
                bidsBackHandler: initAdserver,
                timeout: PREBID_TIMEOUT
            });
        });

        function initAdserver() {
            if (pbjs.initAdserverSet) return;
            pbjs.initAdserverSet = true;
            googletag.cmd.push(function() {
                pbjs.setTargetingForGPTAsync && pbjs.setTargetingForGPTAsync();
                googletag.pubads().refresh();
            });
        }
        
        setTimeout(function() {
            initAdserver();
        }, FAILSAFE_TIMEOUT);
        
        window.googletag = window.googletag || {cmd: []};
        
        var wv = navigator.userAgent.toLowerCase();
        var aggr = "0";
        if(wv.includes('topbuzz') || wv.includes('babe') || wv.includes('bacaberita')) {
            aggr = "topbuzz";
        } else if (wv.includes('babe')){
            aggr = "babe";
        } else if (wv.includes('bacaberita')){
            aggr = "babe";
        } else if (wv.includes('ucbrowser')){
            aggr = "ucbrowser";
        } else if (wv.includes('kurio')){
            aggr = "kurio";
        } else {
            aggr = "other";
        }

        googletag.cmd.push(function() {
            googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [[970,250],[970,90],[1,1]], 'div-gpt-ad-billboard').setTargeting('pos', ['billboard']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [[300,250],[300,600]], 'div-gpt-ad-right_sidebar_1').setTargeting('pos', ['right_sidebar_1']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [[300,250]], 'div-gpt-ad-right_sidebar_2').setTargeting('pos', ['right_sidebar_2']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [[336,280],[300,250],[1,1]], 'div-gpt-ad-middle_1').setTargeting('pos', ['middle_1']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [[336,280],[300,250],[1,1]], 'div-gpt-ad-middle_2').setTargeting('pos', ['middle_2']).addService(googletag.pubads());
            googletag.defineSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', [[120,600],[160,600]], 'div-gpt-ad-left_skinads').setTargeting('pos', ['left_skinads']).addService(googletag.pubads());
            
            googletag.defineOutOfPageSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', 'div-gpt-ad-outstream').setTargeting('pos', ['outstream']).addService(googletag.pubads());
            googletag.defineOutOfPageSlot('<?php echo $this->config->item('desktop_gam_id'); ?>', 'div-gpt-ad-abm').setTargeting('partner', ['abm']).addService(googletag.pubads());
            
            googletag.pubads().setTargeting('page_type', ['articlepage']);
            googletag.pubads().setTargeting('aggregator', aggr);
            googletag.pubads().enableSingleRequest();
            googletag.pubads().collapseEmptyDivs();
            googletag.enableServices();
        });
        function bannerCheck(unitAd){googletag.pubads().addEventListener('slotRenderEnded',function(event){if(event.slot.getSlotElementId() === unitAd){if(event.isEmpty){var eAd = document.getElementById('me-'+unitAd);eAd.classList.add('adnone');}}});}
    </script>
    
    <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
    <script>
        window.OneSignal = window.OneSignal || [];
        OneSignal.push(function () {
            OneSignal.init({
                appId: '<?php echo $this->config->item('onesignal_appid'); ?>',
                autoResubscribe: true,
                notifyButton: {
                    enable: false
                },
                promptOptions: {
                    slidedown: {
                        enabled: true,
                        autoPrompt: true,
                        actionMessage: "Dapatkan update berita SINDOnews melalui notifikasi browser Anda.",
                        acceptButtonText: "SUBSCRIBE",
                        cancelButtonText: "TIDAK"
                    }
                }
            });
            
            OneSignal.on('notificationPermissionChange', function(permissionChange) {
                var currentPermission = permissionChange.to;
                console.log('New permission state:', currentPermission);

                if(currentPermission === 'granted'){
                    window.dataLayer.push({
                        event: 'push-notification-click',
                        article_title: "<?php echo $content['cd_title']; ?>"
                    });
                }
            });
        });
    </script>

    <script>
        window.dataLayer = [{
            kanal: 'GenSINDO',
            subkanal: '<?php echo $content['cd_subkanal']; ?>',
            tags: '<?php echo addslashes(trim($content['cd_tags'])); ?>',
            author: '<?php echo addslashes($content['cd_author']); ?>',
            editor: '<?php echo addslashes($content['cd_editor']); ?>',
            publish_date: '<?php echo $content['date_publish']; ?>',
            publish_time: '<?php echo $content['time_publish']; ?>',
            publish_year: '<?php echo date('Y',strtotime($content['cd_publish'])); ?>',
            publish_month: '<?php echo date('m',strtotime($content['cd_publish'])); ?>',
            publish_day: '<?php echo date('d',strtotime($content['cd_publish'])); ?>',
            article_id: '<?php echo $content['content_id']; ?>',
            article_title: "<?php echo $content['cd_title']; ?>",
            content_type: 'artikel',
            page_type: 'article_page',
            data_source: 'Non AMP',
            pagination: 'page <?php echo $content['cd_page']; ?>'
        }];
    </script>

    
    <?php echo $html['bottom_js']; ?>

    


</head>

<body id="detail">
<div id="app">
    <!-- Header -->
    <header>
      <div class="header-top">
         <?php echo $template['header']; ?>
      </div>
      <div class="header-middle">
        <div class="container">
          <!-- Logo -->
          <div id="logo">
            <a href="<?php echo base_url(); ?>"><img src="<?php echo template_uri(); ?>/image/logo.png" alt="Logo"></a>
          </div>
          <!-- Search -->
          <div id="search">
            <form class="search-field sample two" name="myForm" method="get" action="" onsubmit="return validateForm(this)">
              <input type="text" name="search" id="search-input" placeholder="Cari Berita" autocomplete="off">
              <button type="submit" class="btn btn-search fa fa-search"></button>
              <button onfocus="document.getElementById('search-input').value = ''" type="reset" class="btn btn-reset fa fa-times"></button>
            </form>
          </div>
        </div>
      </div>
      <div class="header-bottom">
         <?php echo $template['minimenu']; ?>
      </div>
    </header>
    <!-- <div class="mask-header">Mask</div> -->
    <!-- Main -->
    <main>

    <div class="billboard">
              <div id="div-gpt-ad-billboard"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-billboard');});</script></div>
          </div>
      <div class="container">
        <!-- View -->
        <div id="view">
            <article>
                <?php echo $content['breadcrumb']; ?>
                <?php echo $content['detail']; ?>
            </article>
            
            <section>
                <?php echo $content['relatedtopic']; ?>
                
                <!-- Social Media -->
                <div id="social-media-share">
                    <div class="head">Share:</div>
                </div> 
                <div class="sharethis-box">
                    <div class="sharethis-inline-share-buttons"></div>
                </div> 
                <!-- Emoticon -->
                <div class="emoticon">
                    <div class="sharethis-reaction"><div class="sharethis-inline-reaction-buttons"></div></div>
                </div>

                <?php echo $content['comment'] ?>
            </section>
            
            <div class="box-ads">            
                <div class="ads-geniee adsload" id="popinDiv"></div>
                <div class="ads-geniee adsload" id="mgidDiv"></div>
                <div class="ads-geniee adsload" id="advertnativeDiv"></div>
            </div>

          
          <!-- Linked News -->
          <div class="linked-news">
   
            <div class="list-news">
               <?php echo $content['relatedcontent'] ?>
            </div>
          </div>
          <!-- Sindonews News -->
          <div class="linked-news">
            <?php echo $content['sindonewsterkini'] ?>
          </div>
        </div>
        <!-- Sidebar -->
        <!-- <div id="sidebar"> -->
        <div id="sidebar">
          <!-- Trending Topic -->
          <div id="trending-topic">
            <?php echo $template['trending'] ?>
          </div>

          <div id="trending-topic">

            <div class="r-side mb20 medium-banner">
              <div id="div-gpt-ad-right_sidebar_1">
                <script>
                  googletag.cmd.push(function() {
                    googletag.display('div-gpt-ad-right_sidebar_1');
                  });
                </script>
              </div>
            </div>

          </div>

          <!-- <div class="r-side sky-banner">
              <div id="div-gpt-ad-left_skinads"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-left_skinads');});</script></div>
          </div> -->

          <!-- Announcement -->
          <div id="announcement">
            <?php echo $template['promogen'] ?>
            
          </div>
          <!-- <div class="r-side mb20 medium-banner">
              <div id="div-gpt-ad-right_sidebar_1"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-right_sidebar_1');});</script></div>
          </div> -->
          
          <!-- Terpopuler -->
          <div class="r-side mb20 mt20important medium-banner">
              <div id="div-gpt-ad-right_sidebar_2"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-right_sidebar_2');});</script></div>
          </div>

          <div class="r-side mb20 mt20important medium-banner">
                <div id="dablewidget_Pl1qvelE" data-widget_id="Pl1qvelE"></div>
          </div>
          
          <div>
            <div id="div-gpt-ad-abm"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-abm');});</script></div>
            <div id="div-gpt-ad-outstream"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-outstream');});</script></div>
        </div>

          <div id="terpopuler">
             <?php echo $template['popular'] ?>
          </div>


        </div>
      </div>
    </main>
    
    <div class="mask-footer">Mask</div>
    <!-- Footer -->
    <footer>
       <?php echo $template['minifooter'] ?>
    </footer> 
  </div>

  <!-- Javascript -->
  <?php echo $html['bottom_css']; ?>
  <?php echo $html['js']; ?>

  <script id="dsq-count-scr" src="https://sindonews.disqus.com/count.js" async></script>
  <script src="https://platform-api.sharethis.com/js/sharethis.js#property=5c3002466aa2aa0011451e10&product=inline-share-buttons" async></script>

  <script>
      function number_format(e,n,t,i){e=(e+"").replace(/[^0-9+\-Ee.]/g,"");var r=isFinite(+e)?+e:0,a=isFinite(+n)?Math.abs(n):0,o="undefined"==typeof i?",":i,d="undefined"==typeof t?".":t,u="",f=function(e,n){var t=Math.pow(10,n);return""+(Math.round(e*t)/t).toFixed(n)};return u=(a?f(r,a):""+Math.round(r)).split("."),u[0].length>3&&(u[0]=u[0].replace(/\B(?=(?:\d{3})+(?!\d))/g,o)),(u[1]||"").length<a&&(u[1]=u[1]||"",u[1]+=new Array(a-u[1].length+1).join("0")),u.join(d)}
      $(function () {
          $.ajax({url:'<?php echo site_url('scream'); ?>?sin='+sindoid()+'&do='+sindotime+'&z='+sindonum(0,11),method:"POST",data:{xyz:'<?php echo $content['code']; ?>',c:'<?php echo $content['id_content']; ?>'},success: function(result){var screamdata = $.parseJSON(JSON.stringify(result));var screamviews = 0;if(screamdata.data[0].hit > 0){screamviews = screamdata.data[0].hit;}$('.hit-view').text('views: '+number_format(screamviews,0,'','.'));}});

          var imagesLoad = new LazyLoad({
              elements_selector:'.lazyload'
          });

          $('#subkanal-navigation').find('#latest-menu').addClass('active')
          
          window.dataLayer.push({event: 'mgidAd'});
          window.dataLayer.push({event: 'dableEvent'});
          window.dataLayer.push({event: 'adPopinEvent'});
          window.dataLayer.push({event: 'AdvertnativeDesktopEvent'});
          
          function clearDqAds(){
              $('#disqus_thread').children().first().fadeOut();
          }
          
          setInterval(clearDqAds,2000);
          setTimeout(function(){sindostop(clearDqAds);},15000);


          $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
          $('.tab ul.tabs li a').click(function (g) {
              var tab = $(this).closest('.tab'),index = $(this).closest('li').index();
              tab.find('ul.tabs > li').removeClass('current');
              $(this).closest('li').addClass('current');
              tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
              tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();
              g.preventDefault();
          });

          $('.sidenav').sidenav();
          
          /*
          $(document).scroll(function(){var y = $(this).scrollTop();if(y <= 800){$('.notif-box').removeClass('notif-show').addClass('notif-hide');}else{if($('.notif-box').hasClass('kill-me')){$('.notif-box').removeClass('notif-show').addClass('notif-hide');}else{$('.notif-box').removeClass('notif-hide').addClass('notif-show');}}});                   
          var subdomain='<?php echo $this->config->item('domain'); ?>';var cookiename='__gensindo_push_d';var getme = getCookie(cookiename);if(getme != ''){dismissPush();}
          $('.notif-yes').click(function(){dismissPush();});
          $('.notif-no').click(function(){setCookie(cookiename,randomuv(),30,subdomain);dismissPush();});
          if(Notification.permission==='granted' || Notification.permission==='denied'){setCookie(cookiename,randomuv(),30,subdomain);}
          
          function dismissPush(){$('.notif-box').removeClass('notif-show').addClass('notif-hide').addClass('kill-me');}
          */

          $('#terpopuler').sticky({topSpacing:32});
          
          $('.adv-inline a').click(function () {
                var advname = $('.adv-inline a').text();
                window.dataLayer.push({
                    event: 'affiliate-link-click',
                    affiliate_name: "<?php echo $content['cd_title']; ?>"
                });
            });

            $('.ext-link').click(function () {
                var advname = $('.ext-link').text();
                window.dataLayer.push({
                    event: 'affiliate-link-click',
                    affiliate_name: "<?php echo $content['cd_title']; ?>"
                });
            });

            var affiliateImpressionA = new LazyLoad({
                elements_selector:'.adv-inline',
                threshold: 0,
                callback_enter: function(){
                    var advname = $('.adv-inline a').text();
                    window.dataLayer.push({
                        event: 'affiliate-link-impression',
                        affiliate_title: "<?php echo $content['cd_title']; ?>"
                    });
                }
            });

            var affiliateImpressionB = new LazyLoad({
                elements_selector:'.ext-link',
                threshold: 0,
                callback_enter: function(){
                    var advname = $('.ext-link').text();
                    window.dataLayer.push({
                        event: 'affiliate-link-impression',
                        affiliate_title: "<?php echo $content['cd_title']; ?>"
                    });
                }
            });

            setTimeout(function () {
                $('.sharethis-box .st-btn').each(function(i){
                    var shareBtn = $(this).attr('data-network');
                    $(this).click(function(){
                        window.dataLayer.push({
                            event: 'social-share',
                            social_platform: shareBtn
                        });
                    });
                });

                $('.sharethis-box-bottom .st-btn').each(function(i){
                    var shareBtn = $(this).attr('data-network');
                    $(this).click(function(){
                        window.dataLayer.push({
                            event: 'social-share',
                            social_platform: shareBtn
                        });
                    });
                });
            }, 5000);

            var pageStatus = new LazyLoad({
                elements_selector:'.page-status',
                threshold: 0,
                callback_enter: function(){
                    window.dataLayer.push({
                        event : 'article-milestone',
                        article_title: "<?php echo $content['cd_title']; ?>",
                        scroll_measure_percent: '100%',
                        scroll_measure_value: 100
                    });
                }
            });

            $('.comment-go a').on('click', function () {
                window.dataLayer.push({
                    event: 'comment',
                    article_title: "<?php echo $content['cd_title']; ?>"
                });
            });
      });

        // search action
        function validateForm() {
            var x = document.forms["myForm"]["search"].value;
            x = x.replace(' ', '+')

            if (x != "") {
                var url = 'https://search.sindonews.com/gokanal?type=artikel&q=' + x + '&pid=600';

                document.location.href = url, true;
                return false;
            }

        }
  </script>



</body>

</html>