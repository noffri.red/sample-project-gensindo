<?php

$total = count($rekomendasi);
if($total > 0){
    echo '<div class="news mt10 article-rekomedasi">';
    echo '<div class="heading">Rekomendasi</div>';
    echo '<ul>';
    for($i = 0; $i < $total; $i++){
        $site_domain[$i] = $this->config->item('read_domain');

        $dtime[$i] = strtotime($rekomendasi[$i]['date_created']);
        $d[$i] = date('Y/m/d',strtotime($rekomendasi[$i]['date_created']));
        $l[$i] = 'https://' . $site_domain[$i][$rekomendasi[$i]['channel_id']] . '/read/' . $rekomendasi[$i]['content_id'] . '/' . $rekomendasi[$i]['channel_id'] . '/' . slug($rekomendasi[$i]['title'] . '-' . $dtime[$i]);
        $title[$i] = cleanWords($rekomendasi[$i]['title']);
        $ago[$i] = time_difference($rekomendasi[$i]['date_publish']);

        echo '
            <li>
                <div class="breaking-title">
                    <a href="' . $l[$i] . '">' . $title[$i] . '</a>
                </div>
            </li>';
    }

    echo '</ul>';
    echo '</div>';
}