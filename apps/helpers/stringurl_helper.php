<?php

function randomString($charNum){
    return random_string('alnum',$charNum);
}

function unique_vistors(){
    if(!empty($_SERVER['HTTP_CLIENT_IP'])){
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
        $ip = $_SERVER['REMOTE_ADDR'];
    }

    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $urlpath = $_SERVER['REQUEST_URI'];

    $unique = $ip . '#' . $_SERVER['HTTP_USER_AGENT'] . '#' . $_SERVER['REQUEST_URI'];
    return base64_encode($unique);
}

function infoServer(){
    if(isset($_SERVER['SERVER_ADDR'])){
        $server = explode('.',$_SERVER['SERVER_ADDR']);
        $serverip = $server[3];
    }else{
        $serverip = 'sn';
    }

    $ipdb = __IP_SLAVE;
    $db = explode('.',$ipdb);
    $dbslave = $db[3];

    $output = $serverip . '#' . $dbslave;

    return $output;
}

function guidv4($data){
    assert(strlen($data) == 16);

    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s',str_split(bin2hex($data),4));
}

function uuid(){
    return guidv4(random_bytes(16));
}

function convertDateTime($date,$format = 'Y-m-d H:i:s'){
    $tz1 = 'GMT+7';
    $tz2 = 'Asia/Jakarta';

    $d = new DateTime($date,new DateTimeZone($tz1));
    $d->setTimeZone(new DateTimeZone($tz2));

    return $d->format($format);
}

function deviceCookie($device){
    $CI = & get_instance();
    $domain = $CI->config->item('domain');
    $age = 2592000;
    $utcDateTime = date('D, d-M-Y H:i:s',time() + $age);
    $expires = convertDateTime($utcDateTime) . ' GMT+7';
    $dataCookie = 'sinua=' . $device;

    $CI->output->set_header('Set-Cookie: ' . $dataCookie . '; expires=' . $expires . '; Max-Age=' . $age . '; path=/; domain=' . $domain . '; secure; samesite=none');
}

function generateUserTrack($iuser){
    $CI = & get_instance();
    $uniqueUser = uuid();
    if(isset($_COOKIE['gsin' . $iuser . '_ads'])){
        $uniqueUser = $_COOKIE['gsin' . $iuser . '_ads'];
    }else{
        $domain = 'sindonews.com';
        if(isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] == '10.148.15.229'){
            $domain = 'sindo.it';
        }

        $utcDateTime = date('D, d-M-Y H:i:s',time() + 2592000);
        $expires = convertDateTime($utcDateTime) . ' GMT+7';
        $CI->output->set_header('Set-Cookie: gsin' . $iuser . '_ads=' . $uniqueUser . '; expires=' . $expires . '; Max-Age=2592000; path=/; domain=' . $domain . '; secure; samesite=none');
        
        /* delete old */
        setcookie('gUT' . $iuser . '11_',time() + 3600,'/',$domain);
        setcookie('AUsin' . $iuser . 'do',time() + 2592000,'/',$domain);
    }

    return $uniqueUser;
}

function uvTrack($iuser){
    $CI = & get_instance();
    $uniqueUser = uuid();
    if(isset($_COOKIE['g' . $iuser . 'uv'])){
        $uniqueUser = $_COOKIE['g' . $iuser . 'uv'];
    }else{
        $domain = 'sindonews.com';
        if(isset($_SERVER['SERVER_ADDR']) && $_SERVER['SERVER_ADDR'] == '10.148.15.229'){
            $domain = 'sindo.it';
        }

        $utcDateTime = date('D, d-M-Y H:i:s',time() + 2592000);
        $expires = convertDateTime($utcDateTime) . ' GMT+7';
        $CI->output->set_header('Set-Cookie: g' . $iuser . 'uv=' . $uniqueUser . '; expires=' . $expires . '; Max-Age=2592000; path=/; domain=' . $domain . '; secure; samesite=none');
        
        /* delete old */
        setcookie('gUV' . $iuser . 'pro',time() - 3600,'/',$domain);
        setcookie('fresh' . $iuser . 'rand',time() - 3600,'/',$domain);
    }

    return $uniqueUser;
}

function base64url_encode($data) { 
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '='); 
} 

function base64url_decode($data) { 
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT)); 
}

function randomData($data,$totaldata,$show_num){
    $totalnews = count($data);
    if($totaldata > $totalnews){
        $output = array_slice($data,0,$show_num);
    }else{
        $slice_num = $totaldata / $show_num;
        $setdata = array();
        for($i = 0; $i < $slice_num; $i ++){
            $setdata[$i]['s'] = $i * $show_num;
            $setdata[$i]['o'] = $show_num;
        }

        $slice_max = ($slice_num - 1);
        $randind = rand(0,$slice_max);
        $output = array_slice($data,$setdata[$randind]['s'],$setdata[$randind]['o']);
    }

    return $output;
}

function customDimensionString($string){
    $char = array('\'','"');
    $replace_char = trim(str_replace($char,'',trim(strtolower($string))));
    $convert = trim(str_replace(' ','_',$replace_char));
    return $convert;
}

function customDimensionTags($string){
    $char = array('\'','"');
    $replace_char = trim(str_replace($char,'',trim(strtolower($string))));
    $clean_string = trim(preg_replace('/\(.*\)/','',trim($replace_char)));
    
    $convert_space = trim(str_replace(' ','-',$clean_string));
    //$convert_comma = trim(str_replace(',',' ',$convert_space));
    return $convert_space;
}

