<div class="list">
          <div class="logo-sindonews"><a href="https://sindonews.com"><img src="<?php echo m_template_uri(); ?>/image/logo-sindonews.png" alt=""></a></div>
          <ul class="mainmenu">
            <li>
              <div class="menu">
                <i class="fas fa-home icon-kanal"></i>
                <a href="<?php echo base_url(); ?>" class="active">Home</a>
              </div>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-university icon-kanal"></i>
                <a  href="https://nasional.sindonews.com">Nasional</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://nasional.sindonews.com/politik">Politik</a></li>
                <li><a  href="https://nasional.sindonews.com/hukum">Hukum</a></li>
                <li><a  href="https://nasional.sindonews.com/hankam">Hankam</a></li>
                <li><a  href="https://nasional.sindonews.com/humaniora">Humaniora</a></li>
                <li><a  href="https://nasional.sindonews.com/edukasi">Edukasi</a></li>
                <li><a  href="https://nasional.sindonews.com/more/16">Tajuk Sindo</a></li>
                <li><a  href="https://nasional.sindonews.com/more/18">Opini</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-city icon-kanal"></i>
                <a  href="https://metro.sindonews.com">Metro</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://metro.sindonews.com/perkotaan">Perkotaan</a></li>
                <li><a  href="https://metro.sindonews.com/peristiwa">Peristiwa</a></li>
                <li><a  href="https://metro.sindonews.com/more/173">Jadoel</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-spa icon-kanal"></i>
                <a  href="https://daerah.sindonews.com">Daerah</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://daerah.sindonews.com/nusantara">Nusantara</a></li>
                <li><a  href="https://sumut.sindonews.com/">Sumatera Utara</a></li>
                <li><a  href="https://sumsel.sindonews.com/">Sumatera Selatan</a></li>
                <li><a  href="https://jabar.sindonews.com/">Jawa Barat</a></li>
                <li><a  href="https://jateng.sindonews.com/">Jawa Tengah &amp; DIY</a></li>
                <li><a  href="https://jatim.sindonews.com/">Jawa Timur</a></li>
                <li><a  href="https://makassar.sindonews.com/">Makassar</a></li>
                <li><a  href="https://daerah.sindonews.com/manado">Manado</a></li>
                <li><a  href="https://daerah.sindonews.com/batam">Batam</a></li>
                <li><a  href="https://daerah.sindonews.com/more/29">Cerita Pagi</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-chart-line icon-kanal"></i>
                <a  href="https://ekbis.sindonews.com">Ekonomi Bisnis</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://ekbis.sindonews.com/makro">Makro</a></li>
                <li><a  href="https://ekbis.sindonews.com/bursa-finansial">Bursa Finansial</a></li>
                <li><a  href="https://ekbis.sindonews.com/sektor-riil">Sektor Riil</a></li>
                <li><a  href="https://ekbis.sindonews.com/more/39">Serba Serbi Bisnis</a></li>
                <li><a href="https://ekbis.sindonews.com/more/36">Success Stories</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-handshake icon-kanal"></i>
                <a  href="https://international.sindonews.com">International</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://international.sindonews.com/asia-pasifik">Asia Pasifik</a></li>
                <li><a  href="https://international.sindonews.com/eropa">Eropa</a></li>
                <li><a  href="https://international.sindonews.com/amerika">Amerika</a></li>
                <li><a  href="https://international.sindonews.com/timur-tengah">Timur Tengah</a></li>
                <li><a  href="https://international.sindonews.com/afrika">Afrika</a></li>
                <li><a  href="https://international.sindonews.com/more/46">Jagad Jungkir Balik</a></li>
                <li><a  href="https://international.sindonews.com/more/45">Fokus</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="far fa-futbol icon-kanal"></i>
                <a  href="https://sports.sindonews.com">Sports</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://sports.sindonews.com/bola">Bola</a></li>
                <li><a  href="https://sports.sindonews.com/motosport">Motosport</a></li>
                <li><a  href="https://sports.sindonews.com/tinju">Tinju</a></li>
                <li><a  href="https://sports.sindonews.com/allsports">All Sports</a></li>
                <li><a  href="https://sports.sindonews.com/more/53">Hot Shot</a></li>
                <li><a  href="https://sports.sindonews.com/more/52">Sang Bintang</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-car icon-kanal"></i>
                <a  href="https://autotekno.sindonews.com">Autotekno</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://autotekno.sindonews.com/mobil">Mobil</a></li>
                <li><a  href="https://autotekno.sindonews.com/motor">Motor</a></li>
                <li><a  href="https://autotekno.sindonews.com/gadget">Gadget</a></li>
                <li><a  href="https://autotekno.sindonews.com/elektronik">Elektronik</a></li>
                <li><a  href="https://autotekno.sindonews.com/telco">Telco</a></li>
                <li><a  href="https://autotekno.sindonews.com/more/124">Sains</a></li>
                <li><a  href="https://autotekno.sindonews.com/more/208">Bengkel Kita</a></li>
                <li><a  href="https://autotekno.sindonews.com/more/184">Aksesoris</a></li>
                <li><a  href="https://autotekno.sindonews.com/more/183">Rest Area</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-glass-martini-alt icon-kanal"></i>
                <a  href="https://lifestyle.sindonews.com">Lifestyle</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://lifestyle.sindonews.com/music">Music</a></li>
                <li><a  href="https://lifestyle.sindonews.com/cinefilm">Cinefilm</a></li>
                <li><a  href="https://lifestyle.sindonews.com/health">Health</a></li>
                <li><a  href="https://lifestyle.sindonews.com/travel">Travel</a></li>
                <li><a  href="https://lifestyle.sindonews.com/more/166">Ragam</a></li>
                <li><a  href="https://lifestyle.sindonews.com/more/187">Red Carpet</a></li>
                <li><a  href="https://lifestyle.sindonews.com/more/185">Kuliner</a></li>
                <li><a  href="https://lifestyle.sindonews.com/more/186">Fashion</a></li>
                <li><a  href="https://lifestyle.sindonews.com/more/165">Resensi</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-graduation-cap icon-kanal"></i>
                <a  href="https://edukasi.sindonews.com">Edukasi</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://edukasi.sindonews.com/kampus">Kampus</a></li>
                <li><a  href="https://edukasi.sindonews.com/sekolah">Sekolah</a></li>
                <li><a  href="https://edukasi.sindonews.com/beasiswa">Beasiswa</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-book-open icon-kanal"></i>
                <a  href="https://kalam.sindonews.com">Kalam</a>
                <i class="fas fa-angle-right icon-dropdown"></i>
              </div>
              <ul class="submenu">
                <li><a  href="https://kalam.sindonews.com/hikmah">Hikmah</a></li>
                <li><a  href="https://kalam.sindonews.com/tausyiah">Tausyiah</a></li>
                <li><a  href="https://kalam.sindonews.com/quran">Qur'an Digital</a></li>
                <li><a  href="https://kalam.sindonews.com/murottal">Murottal Qur'an</a></li>
                <li><a  href="https://kalam.sindonews.com/jadwalsholat">Jadwal Sholat</a></li>
                <li><a  href="https://kalam.sindonews.com/syiar">Syiar</a></li>
              </ul>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-user-friends icon-kanal"></i>
                <a   href="https://gensindo.sindonews.com">Gen Sindo</a>
              </div>
            </li>
            <li>
              <div class="menu">
                <i class="fab fa-uikit icon-kanal"></i>
                <a  href="https://infografis.sindonews.com/">Infografis</a>
              </div>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-camera icon-kanal"></i>
                <a  href="https://photo.sindonews.com/">Photo</a>
              </div>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-play icon-kanal"></i>
                <a  href="https://video.sindonews.com/">Video</a>
              </div>
            </li>
            <li>
              <div class="menu">
                <i class="fas fa-server icon-kanal"></i>
                <a  href="https://index.sindonews.com/">Indeks</a>
              </div>
            </li>
          </ul>
        </div>
        </div>