<?php

if($topiklist != ''){
    $topic = explode(',',$topiklist);

    echo '<div class="topic-suggest">';
    echo '<ul>';
    echo '<li class="bold">TOPIC :</li>';

    for($i = 0; $i < count($topic); $i++){
        $getTopicData[$i] = $this->mgonews->getTopicListGensindo($topic[$i]);
        $urltopic[$i] = $this->mgonews->getTopicURL($getTopicData[$i][0]['slug_topic']);
        $topic_title[$i] = strtolower($getTopicData[$i][0]['topic']);
        echo '<li><a target="_blank" href="' . $urltopic[$i] . '"><i class="fa fa-tags" aria-hidden="true"></i> ' . $topic_title[$i] . '</a></li>';
    }

    echo '</ul>';
    echo '</div>';
} 