<?php

$total = count($others);
if($total > 0){
    echo '<div class="article-others news mt10">';
    echo '<div class="heading">Berita Lainnya</div>';
    echo '<ul>';
    for($i = 0; $i < $total; $i++){
        $site_domain[$i] = $this->config->item('read_domain');
        
        $dtime[$i] = strtotime($others[$i]['date_created']);
        $l[$i] = 'https://' . $site_domain[$i][$others[$i]['channel_id']] . '/read/' . $others[$i]['content_id'] . '/' . $others[$i]['channel_id'] . '/' . slug($others[$i]['title'] . '-' . $dtime[$i]);
        $title[$i] = cleanWords($others[$i]['title']);
        $summary[$i] = cleanWords($others[$i]['summary']);
        
        if($others[$i]['content_id'] != $content_id){
            echo '
                <li>
                    <div class="breaking-title">
                        <a href="' . $l[$i] . '">' . $title[$i] . '</a>
                    </div>
                </li>';
        }
    }

    echo '</ul>';
    echo '</div>';
    
    echo '
        <div class="news-more">
            <a href="' . base_url() . '">berita lainnya</a>
        </div>';
}