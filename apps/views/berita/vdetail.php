<?php

if(count($details) > 0){
    $title = cleanHTML(cleanWords($details['title']));
    $url_title = cleanWords($details['url_title']);
    $datepublish = parseDateTime((string) $details['date_published']);
    $date_published = $datepublish['day_ind_name'] . ", " . $datepublish['day'] . " " . $datepublish['month_ind_name'] . " " . $datepublish['year'];
    $date_published .= ' - ' . $datepublish['hour'] . ":" . $datepublish['minute'] . " WIB";
    $lokasi = $details['lokasi'];
    $content = $content;
    $kode_user = $details['kode_user'];

    if($details['sub_title'] != ''){
        $sub_title = cleanWords($details['sub_title']);
        echo '<div class="subtitle-news">' . $sub_title . '</div>';
    }

    echo '<div class="title-news"><h1>' . $title . '</h1></div>';

    if($details['author'] != ''){
        $author = $details['author'];
        $link_author = site_url('reporter') . '/' . $details['url_author'];
        echo '<div class="author"><a href="#">' . $author . '</a></div>';
    }

    echo '<time>' . $date_published . '</time>';
    echo '<div class="hit-view"></div>';

    if($start == 0 || $start == ''){
        if($details['photo'] != ''){
            $img = images_uri() . $this->config->item('dyn_603') . '/gensindo/content/' . date('Y/m/d',strtotime($details['date_created'])) . '/' . $details['id_subkanal'] . '/' . $details['id_news'] . '/' . $details['photo'];

            if($details['caption_photo'] != ''){
                $caption_images = cleanWords($details['caption_photo']);
            }else{
                $caption_images = cleanWords($details['title']);
            }

            echo '<img src="' . $img . '" class="image-news" width="603" alt="' . cleanHTML($title) . '">';
            echo '<figcaption>' . $caption_images . '</figcaption>';
        }
        
        $adspages = '';
    }else{
        echo '
            <div class="ads300 mt20 mb20 adsload">
                <div id="div-gpt-ad-middle_1"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_1\');});</script></div>
            </div>';
        
        $adspages = '
            <div class="ads300 mt20 mb20 adsload">
                <div id="div-gpt-ad-middle_2"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_2\');});</script></div>
            </div>';
    }

    echo '<div class="caption">';

    if(!empty($lokasi)){
        if($start == 0 || $start == ''){
            echo '<span>' . strtoupper($lokasi) . '</span> - ';
        }
    }

    echo $content;

    $lasturl = (($per_page * $last_page) - $per_page);
    if($totalData > $per_page){
        if($start == $lasturl){
            echo '<br><br><span class="reporter">(' . $kode_user . ')</span>';
        }
    }else{
        echo '<br><br><span class="reporter">(' . $kode_user . ')</span>';
    }

    if($totalData > $per_page){
        echo $adspages;
        
        echo '<div class="note-paging">halaman ke-' . $current_page . ' dari ' . $last_page . '</div>';
        echo $pagination;

        if(!isset($_GET['showpage'])){
            echo '<div class="content-show"><a href="' . $site_url . '?showpage=all">show all</a></div>';
        }
    }

    echo '</div>';
}