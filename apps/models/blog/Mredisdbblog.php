<?php

class Mredisdbblog extends CI_Model {

    public function __construct(){
        // $this->myredis = $this->textcache->redis();
    }

    function getBlog($param=array(), $howmuch = 3, $start = 0, $total_results = FALSE){
        // if($total_results == FALSE){ $totalresults = ""; }else{ $totalresults = "_withtotalresults"; }
        // $key = $this->config->item('redis_name')."_authorcontent_" . $start. '_' . $howmuch  . '_' . base64slug_encode(json_encode($param).$totalresults);
        // $getredis = $this->myredis->get($key);

        // if($getredis){
            // $result = json_decode($getredis,TRUE);
        // }else {
            $id_content = '';
            if(!empty($param['id_content'])){
                $id_content = "AND a.id_content != '". $param['id_content'] ."'";
            }

            $qString = "SELECT 
                            a.id_content, a.id_subkanal, a.title, a.slug_title, a.summary, a.images, a.date_created, a.date_published, 
                            DATE_FORMAT(a.date_created, '%Y') AS year, 
                            DATE_FORMAT(a.date_created, '%m') AS month,  
                            DATE_FORMAT(a.date_created, '%d') AS day,  
                            b.kanal,
                            c.subkanal, c.slug_subkanal
                        FROM td_content a";
            $qJoin = " LEFT JOIN td_kanal AS b ON b.id_kanal = a.id_kanal
                      LEFT JOIN td_subkanal AS c ON c.id_subkanal = a.id_subkanal";
            $qCondition = " WHERE 
                                a.status = '0'
                                AND a.id_author = '". $param['id_author'] ."'
                                $id_content ";
            $qOrderBy = " ORDER BY a.date_published DESC";
            $qLimit = " LIMIT ".$start.",".$howmuch;

            $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();

            if($total_results == TRUE){
                $qStringTotal = "SELECT COUNT(*) AS total FROM td_content a";
                $qStringTotal .= $qJoin.$qCondition;
                $response_total = $this->db->query($qStringTotal);
                $total = $response_total->result_array();
                $data['total_results'] = $total[0]['total'];
            }
            $this->db->close();

            // $redisdata = json_encode($data);
            // $this->myredis->setex($key,300,$redisdata);
            $result = $data;
        // }

        return $result;
    }
    function cekAuthor($slug_author){
        // $key = $this->config->item('redis_name')."_authordata_".$slug_author;
        // $getredis = $this->myredis->get($key);

        // if($getredis){
            // $result = json_decode($getredis,TRUE);
        // }else {
            $qString = "SELECT id_author, slug_author, author FROM td_author";
            $qCondition = " WHERE slug_author = '". $slug_author ."'";

            $qString .= $qCondition;
            $response_data = $this->db->query($qString);
            $data['results'] = $response_data->result_array();
            $this->db->close();

            // $redisdata = json_encode($data);
            // $this->myredis->setex($key,300,$redisdata);
            $result = $data;
        // }

        return $result;
    }
    function getLatestNewsByAuthor($num){
        // $key = $this->config->item('redis_name')."blog_latest_" . $num;
        // $getredis = $this->myredis->get($key);
        // if($getredis){
            // $result = json_decode($getredis,TRUE);
        // }else{
            $sql = "SELECT DISTINCT id_author FROM td_content WHERE status = 0 AND id_author > 0 ORDER BY date_published DESC LIMIT 0," . $num;
            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            // $redisdata = json_encode($data);
            // $this->myredis->setex($key,300,$redisdata);
            $result = $data;
        // }

        return $result;
    }
    function getDataAuthor($id_author){
        // $key = $this->config->item('redis_name')."blog_author_" . $id_author;
        // $getredis = $this->myredis->get($key);
        // if($getredis){
            // $result = json_decode($getredis,TRUE);
        // }else{
            $sql = 'SELECT id_author,author,slug_author FROM td_author WHERE id_author = "' . $id_author . '"';
            $query = $this->db->query($sql);
            $data = $query->result_array();
            $this->db->close();

            // $redisdata = json_encode($data);
            // $this->myredis->setex($key,43200,$redisdata);
            $result = $data;
        // }

        return $result;
    }
}