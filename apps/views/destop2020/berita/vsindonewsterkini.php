
<?php if(count($sindonews_terkini)>0){
  echo '<div class="head">
            <div class="title">Berita Terkini</div>
          </div>
          <div class="list-news">
        <ul>';

        foreach($sindonews_terkini as $value){ 
                    
            
          $dtime = strtotime($value['date_created']);
          $d = date('Y/m/d',strtotime($value['date_created']));
          $subtitle = cleanWords($value['subtitle']);
          $title = cleanWords($value['title']);
          $img = $this->config->item('images_uri') . '/dyn/300/pena/news/' . $d . '/' . $value['channel_id'] . '/' . $value['news_id'] . '/' . $value['thumb'];

          $channel_name = ucwords($value['channel_name']);
          $ago = time_difference($value['date_publish']);
          $subkanal = ucwords($value['channel_name']);

          $channel_id = $value['channel_id'];
          $site_domain = $this->config->item('read_domain');

          if($channel_id ==""){
              $l = 'https://www.sindonews.com/read/' . $value['news_id'] . '/' . $value['channel_id'] . '/' . slug($value['title']) . '-' . $dtime;
              $linka = '<a target="_blank" href="'. $l .'">'. $title.'</a>';
          }else{
              $l = 'http://' . $site_domain[$channel_id] . '/read/' . $value['news_id'] . '/' . $value['channel_id'] . '/' . slug($value['title']) . '-' . $dtime;
              $linka = '<a href="'. $l .'">'. $title.'</a>';
          }
          
          if($value['thumb'] != ''){
              $showimg = '<a href="'. $l .'"><img class="lazyload" src="'. $img .'" alt="'. $title .'"></a>';
          }else{
              $showimg = '';
          }

          echo ' <li>
              <div class="image">'.$showimg.'</a></div>
              <div class="block-caption">
                <div class="title">'.$linka.'</div>
                <div class="category gen-news"><a href="#">'. $subkanal .'</a></div>
                <div class="time">'. $ago .'</div>
              </div>
            </li>';
      }
           echo '</ul>
            </div>'; 
  } 