<!DOCTYPE html>
<html amp lang="id">
    <head>
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
        <meta name="theme-color" content="#002445">

        <link rel="shortcut icon" href="<?php echo m_template_uri(); ?>/images/icon/favicon.ico">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-192x192.png" sizes="192x192">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo m_template_uri(); ?>/images/icon/favicon-32x32.png" sizes="32x32">
        <?php echo $html['metaname']; ?>
        <link rel="canonical" href="<?php echo $content['share_url']; ?>">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
        
        <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
        <?php echo $html['mcss']; ?>
        
        <script type="application/ld+json">
        {
            "@context":"https://schema.org",
            "@type":"NewsArticle",
            "mainEntityOfPage":{
                "@type":"WebPage",
                "@id":"<?php echo $content['site_url']; ?>"
            },
            "headline":"<?php echo $content['site_title']; ?>",
            "image":{
                "@type":"ImageObject",
                "url":"<?php echo $content['site_image']; ?>",
                "height":413,
                "width":620
            },
            "datePublished":"<?php echo $content['site_publish']; ?>",
            "dateModified":"<?php echo $content['site_publish']; ?>",
            "author": {
                "@type":"Person",
                "name":"<?php echo $content['site_reporter']; ?>"
            },
            "publisher":{
                "@type":"Organization",
                "name":"SINDOnews.com",
                "logo":{
                    "@type":"ImageObject",
                    "url":"https://asset.sindonews.net/dyn/600/mobile/2015/images/logo-sindonews.png",
                    "width":600,
                    "height":55
                }
            },
            "description":"<?php echo $content['site_summary']; ?>"
        }
        </script>
        <script type="application/ld+json">
        {
            "@context":"https://schema.org",
            "@type":"BreadcrumbList",
            "itemListElement":[{
                "@type":"ListItem",
                "position":1,
                "item":{
                    "@id":"<?php echo base_url(); ?>",
                    "name":"home"
                }
            },{
                "@type":"ListItem",
                "position":2,
                "item":{
                    "@id":"<?php echo site_url('loadmore/' . $content['id_subkanal']); ?>",
					"name":"<?php echo strtolower($this->config->item('biro_name')); ?>"
                }
            }]
        } 
        </script>
        <script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "WebPage",
			"headline": "<?php echo addcslashes($content['site_title'],'"\\/'); ?>",
			"url": "<?php echo $content['site_url']; ?>",
			"datePublished": "<?php echo $content['site_publish']; ?>",
			"image": {
				"@type": "ImageObject",
				"url": "<?php echo $content['site_image']; ?>",
				"height": 413,
				"width": 620
			},
			"description": "<?php echo addcslashes($content['site_summary'],'"\\/'); ?>",
			<?php
				if(count($content['relatedlink']) > 0){
					if(count($content['relatedlink']) == 1){
						echo '"relatedLink":"' . $content['relatedlink'] . '",';
					} else {
						echo '"relatedLink":["' . implode('","',$content['relatedlink']) . '"],';
					}
				}                
				if(count($content['keyword']) > 0){
					if(count($content['keyword']) == 1){
						echo '"keywords":"' . stripslashes($content['keyword'][0]) . '"';
					} else {
						echo '"keywords":["' . stripslashes(implode('","',$content['keyword'])) . '"]';
					}
				}
			?>
		}
		</script>
        <script type="application/ld+json">{"@context":"http://schema.org","@type":"ItemList","itemListElement": <?php echo $content['rich']; ?>}</script>
        
        <script async custom-element="amp-youtube" src="https://cdn.ampproject.org/v0/amp-youtube-0.1.js"></script>
        <script async custom-element="amp-twitter" src="https://cdn.ampproject.org/v0/amp-twitter-0.1.js"></script>
        <script async custom-element="amp-social-share" src="https://cdn.ampproject.org/v0/amp-social-share-0.1.js"></script>
        <script async custom-element="amp-font" src="https://cdn.ampproject.org/v0/amp-font-0.1.js"></script>
        <script async custom-element="amp-analytics" src="https://cdn.ampproject.org/v0/amp-analytics-0.1.js"></script>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>
        <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>
        <script async src="https://cdn.ampproject.org/v0.js"></script>
    </head>

    <body>
        <amp-analytics type="alexametrics">
            <script type="application/json">
                {"vars":{"atrk_acct":"p03zj1aEsk00MA","domain":"sindonews.com"}}
            </script>
        </amp-analytics>
        
        <amp-analytics type="comscore">
            <script type="application/json">
                {"vars":{"c2":"9013027"}}
            </script>
        </amp-analytics>
		
        <amp-analytics type="googleanalytics" id="google-analytics">
            <script type="application/json">
                {
                    "vars":{
                        "account":"UA-25311844-1"
                    },"triggers":{
                        "trackPageview":{
                            "on":"visible",
                            "request":"pageview"
                        },
                        "trackClickContent" :{
                            "on":"click",
                            "selector": ".content a",
                            "request": "event",
                            "vars":{
                                "eventCategory": "AMP Makassar",
                                "eventAction": "Inline Article Click"
                            }
                        },
                        "trackClickRelated" :{
                            "on":"click",
                            "selector": ".article-related a",
                            "request": "event",
                            "vars":{
                                "eventCategory": "AMP Makassar",
                                "eventAction": "Berita Terkait Click"
                            }
                        },
                        "trackClickTopicRelated" :{
                            "on":"click",
                            "selector": ".topik-terkait a",
                            "request": "event",
                            "vars":{
                                "eventCategory": "AMP Makassar",
                                "eventAction": "Topic Terkait Click"
                            }
                        },
                        "trackClickOthers" :{
                            "on":"click",
                            "selector": ".article-others a",
                            "request": "event",
                            "vars":{
                                "eventCategory": "AMP Makassar",
                                "eventAction": "Berita Terkini Click"
                            }
                        },
                        "trackClickArticleNext" :{
                            "on":"click",
                            "selector": ".ctn-next a",
                            "request": "event",
                            "vars":{
                                "eventCategory": "AMP Makassar",
                                "eventAction": "Article Next Click"
                            }
                        },
                        "pageTimer":{
                            "on":"timer",
                            "timerSpec": {
                                "interval": 30,
                                "maxTimerLength": 30
                            },
                            "request": "event",
                            "vars": {
                                "eventId": "AMPBR",
                                "eventCategory": "30 AMP",
                                "eventAction": "Timeout 30 Seconds"
                            }
                        }
                    }
                }
            </script>
        </amp-analytics>
    
        <amp-sidebar id="sidebar-left" layout="nodisplay" side="left">
            <ul class="nav-side">
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li><a href="<?php echo base_url().'news';?>">News</a></li>
				<li><a href="<?php echo base_url().'makassar-city';?>">Makassar City</a></li>
				<li><a href="<?php echo base_url().'ekbis';?>">Ekbis</a></li>
				<li><a href="<?php echo base_url().'daerah';?>">Daerah</a></li>
				<li><a href="<?php echo base_url().'sports';?>">Sports</a></li>
				<li><a href="<?php echo base_url().'ewako-psm';?>">Ewako PSM</a></li>
            </ul>
        </amp-sidebar>
    
        <header>
            <div class="btn-menu">
                <button on="tap:sidebar-left.toggle" class="ampstart-btn caps m2">
                    <amp-img width="24" height="24" src="<?php echo m_template_uri(); ?>/images/menu_ic.png"></amp-img>
                </button>
            </div>
            <div class="logo-box">
                <div class="logo"></div>
                <div class="text">Sumber Informasi Terpercaya</div>
            </div>
            <div class="btn-search">
                <a href="<?php echo base_url().'find';?>" target="_blank">
                    <amp-img width="24" height="24" src="<?php echo m_template_uri(); ?>/images/search_ic.png"></amp-img>
                </a>
            </div>
        </header>

        <article>
            <?php echo $content['details']; ?>
        </article>

        <section>
            <?php echo $content['related']; ?>
            <?php echo $content['youtube']; ?>
            <?php echo $content['others']; ?>
            <?php echo $content['sindonews_terkini']; ?> 
        </section>

        <footer>
            <div class="footer-nav"> 
                <ul>
                    <li><a href="<?php echo base_url();?>">Home</a></li>
                    <li><a href="<?php echo base_url().'news';?>">News</a></li>
                    <li><a href="<?php echo base_url().'makassar-city';?>">Makassar City</a></li>
                    <li><a href="<?php echo base_url().'ekbis';?>">Ekbis</a></li>
                    <li><a href="<?php echo base_url().'daerah';?>">Daerah</a></li>
                    <li><a href="<?php echo base_url().'sports';?>">Sports</a></li>
                    <li><a href="<?php echo base_url().'ewako-psm';?>">Ewako PSM</a></li>
                </ul>
            </div>
            <div class="box-text">
                <p>Copyright &copy; <?php echo date('Y'); ?> SINDOnews.com, All Rights Reserved</p>
                <p class="rendering"><?php echo $this->uri->segment(1); ?>/ rendering in {elapsed_time} seconds (<?php echo infoServer(); ?>)</p>
            </div>
        </footer> 
        
        <amp-font
            layout="nodisplay"
            font-family="FontAwesome"
            timeout="30000">
        </amp-font>
    </body>
<script async src="https://www.instagram.com/embed.js"></script>
</html>