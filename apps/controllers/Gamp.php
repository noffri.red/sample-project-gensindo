<?php

class Gamp extends CI_Controller {

    var $parent_id;

    function __construct(){
        parent::__construct();
        $this->load->config('site_channel');
        $this->load->config('site_domain');

        $this->load->model('news/mgonews');
        $this->parent_id = 600;
    }

    function index(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $t = $this->input->post('tx');
        $keycode = base64_encode($token);
        if($t == $keycode){
            $total_latest = 15;
            $newarticle = $this->mgonews->getListNews($this->parent_id,$total_latest);
            $i = $this->input->post('in');
            //$ga_cid = $this->input->post('gid');
            
            $r_cid = rand(0,11);
            $ga_cid = uvTrack($r_cid);
            $ga_cid_b = generateUserTrack($r_cid);
            $ref = base64url_decode($this->input->post('u'));
            $domainref = parse_url($ref);
            $pathref = urlencode($domainref['path']);
            $title_article = base64url_decode($this->input->post('at'));

            $dtime[$i] = strtotime($newarticle[$i]['date_created']);
            if($newarticle[$i]['content_id'] != ''){
                $l[$i] = site_url('read') . '/' . $newarticle[$i]['content_id'] . '/' . $newarticle[$i]['channel_id'] . '/' . slug($newarticle[$i]['title']) . '-' . $dtime[$i];
                $path[$i] = urlencode('/read/' . $newarticle[$i]['content_id'] . '/' . $newarticle[$i]['channel_id'] . '/' . slug($newarticle[$i]['title']) . '-' . $dtime[$i]);

                $ga_loc = urlencode($l[$i]);
                $ga_title = rawurlencode(seo_title($newarticle[$i]['title']));
                $ga_ref = urlencode($ref);

                $screen[$i] = $this->setFrame();
                $res[$i] = $screen[$i]['width'] . 'x' . $screen[$i]['height'];

                $domain = parse_url(base_url());
                $ga_host = rawurldecode($domain['host']);

                $ga_pixels = 'https://www.google-analytics.com/collect?v=1&t=pageview&tid=UA-25311844-1&ul=en-us&de=UTF-8&cid=' . $ga_cid . '&dh=' . $ga_host . '&dl=' . $ga_loc . '&dp=' . $path[$i] . '&dt=' . $ga_title . '&z=' . time() . '&coid=sindoamp-' . $r_cid;
                $ga_article = 'https://www.google-analytics.com/collect?v=1&t=pageview&tid=UA-25311844-1&ul=en-us&de=UTF-8&cid=' . $ga_cid . '&dh=' . $ga_host . '&dl=' . $ga_ref . '&dp=' . $pathref . '&dt=' . $title_article . '&z=' . time() . '&coid=sindoamp-' . $r_cid;

                $pixel = 'r=' . randomString(11) . '&z=' . $keycode . '&c=' . $newarticle[$i]['content_id'] . '&ch=' . $newarticle[$i]['channel_id'];
                $sindo_pixel = site_url('xyz') . '?' . $pixel;

                $result = array(
                    'p' => base64_encode('<img src="' . $ga_pixels . '" data-id="' . $i . '" width="1" height="1" alt="gatrack"><img src="' . $ga_article . '" data-id="' . $i . '" width="1" height="1" alt="gatrack">'),
                    'h' => base64_encode('<img src="' . $sindo_pixel . '" data-id="' . $i . '" width="1" height="1" alt="pixtrack">'),
                    'e' => ''
                );
            }else{
                $result = array(
                    'p' => '',
                    'h' => '',
                    'e' => ''
                );
            }
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }else{
            $result = array('error' => 1,'code' => 403);
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    function framesimple(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $t = $this->input->post('tx');
        $keycode = base64_encode($token);
        if($t == $keycode){
            $total_data = 10;
            $id_article = $this->input->post('aid');
            $newarticle = $this->mgonews->getListNews($this->parent_id,$total_data);
            $i = $this->input->post('n');
            $dtime[$i] = strtotime($newarticle[$i]['date_created']);
            $l[$i] = site_url('readsimple') . '/' . $newarticle[$i]['content_id'] . '/' . $newarticle[$i]['channel_id'] . '/' . slug($newarticle[$i]['title']) . '-' . $dtime[$i];
            echo '<iframe src="' . $l[$i] . '" data-id="' . $i . '" width="1" height="1" noresize scrolling="no" frameborder="0" marginheight="0"></iframe>';
        }else{
            $result = array('error' => 1,'code' => 403);
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    function garec(){
        $url_ref = $this->input->get('su');
        $ga_ref = urlencode(base64url_decode($url_ref));

        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $t = $this->input->get('tx');
        $z = $this->input->get('z');
        $keycode = base64_encode($token);
        $cid = rand(0,11);

        $output = array();
        if($t == $keycode){
            $newsdata = $this->mgonews->getListNews($this->parent_id,15);
            $newarticle = randomData($newsdata,15,3);

            for($i = 0; $i < count($newarticle); $i++){
                $site_domain[$i] = $this->config->item('read_domain');

                $ga_cid[$i] = generateUserTrack($cid);
                $dtime[$i] = strtotime($newarticle[$i]['date_created']);
                $l[$i] = 'https://' . $site_domain[$i][$newarticle[$i]['channel_id']] . '/read/' . $newarticle[$i]['content_id'] . '/' . $newarticle[$i]['channel_id'] . '/' . slug($newarticle[$i]['title'] . '-' . $dtime[$i]);
                $path[$i] = '/read/' . $newarticle[$i]['content_id'] . '/' . $newarticle[$i]['channel_id'] . '/' . slug($newarticle[$i]['title'] . '-' . $dtime[$i]);
                $title[$i] = seo_title($newarticle[$i]['title']);
                $host[$i] = $site_domain[$i][$newarticle[$i]['channel_id']];

                $ga_loc[$i] = urlencode($l[$i]);
                $ga_title[$i] = rawurlencode($title[$i]);
                $ga_host[$i] = rawurlencode($host[$i]);

                $screen[$i] = $this->setFrame();
                $res[$i] = $screen[$i]['width'] . 'x' . $screen[$i]['height'];

                $ga_pixels[$i] = base64_encode('https://www.google-analytics.com/collect?v=1&t=pageview&tid=UA-25311844-1&ul=en-us&de=UTF-8&cid=' . $ga_cid[$i] . '&dh=' . $ga_host[$i] . '&dl=' . $ga_loc[$i] . '&dp=' . $path[$i] . '&dt=' . $ga_title[$i] . '&z=' . time() . '&coid=sindo-' . $cid);

                $output[$i]['id'] = ($i + 1);
                $output[$i]['gapix'] = $ga_pixels[$i];
            }

            $result = array('data' => $output);
        }else{
            $result = array('data' => $output,'status' => '403 Forbidden');
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    function article(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $t = $this->input->post('tx');
        $keycode = base64_encode($token);
        if($t == $keycode){
            $url_aticle = base64url_decode($this->input->post('un'));
            $article_title = base64url_decode($this->input->post('tn'));

            $ga_loc = urlencode($url_aticle);
            $ga_title = rawurlencode(seo_title($article_title));
            $ga_ref = urlencode(base_url());

            $ga_img = '';
            for($i = 0; $i < 7; $i++){
                $ga_cid[$i] = generateUserTrack($i);
                $ga_pixels[$i] = 'https://www.google-analytics.com/collect?v=1&t=pageview&tid=UA-25311844-1&ul=en-us&de=UTF-8&cid=' . $ga_cid[$i] . '&dl=' . $ga_loc . '&dt=' . $ga_title . '&dr=' . $ga_ref;
                $ga_img .= '<img src="' . $ga_pixels[$i] . '" data-id="' . $i . '" width="1" height="1">';
            }
            echo $ga_img;
        }else{
            $result = array('error' => 1,'code' => 403);
            $this->output->set_content_type('application/json')->set_output(json_encode($result));
        }
    }

    function emptyspace(){
        $result = array('success' => 0,'error' => 1,'data' => array('message' => 'are u missing something?'));
        $this->output->set_content_type('application/json')->set_output(json_encode($result));
    }

    function aimatrix(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $t = $this->input->post('tx');
        $keycode = base64_encode($token);

        $newarticle = $this->mgonews->getListNews($this->parent_id,20);
        $total = count($newarticle);
        $i = $this->uri->segment(2,0);

        $getParams = $this->getAiParams($i);
        if($total > 0){
            if($t == $keycode){
                $site_domain = $this->config->item('read_domain');
                $dtime = strtotime($newarticle[$i]['date_created']);
                $o['ax_u'] = base64_encode(urlencode('https://' . $site_domain[$newarticle[$i]['channel_id']] . '/read/' . $newarticle[$i]['content_id'] . '/' . $newarticle[$i]['channel_id'] . '/' . slug($newarticle[$i]['title']) . '-' . $dtime));
                $o['ax_w'] = base64_encode($getParams['width']);
                $o['ax_h'] = base64_encode($getParams['height']);
                $o['uc'] = base64_encode($getParams['user_cookie']);
                $o['sc'] = base64_encode($getParams['sess_cookie']);
                $o['ax_t'] = base64_encode(rawurlencode(seo_title($newarticle[$i]['title'])));
                $o['ax_r'] = base64_encode(urlencode(base64url_decode($this->input->post('site'))));
                $o['ua'] = base64_encode($this->setUA());
                $o['success'] = 1;
            }else{
                $o['result'] = 'Let\'s War Begin!';
                $o['success'] = 0;
            }
        }else{
            $o['result'] = 'Let\'s War Begin!';
            $o['success'] = 0;
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($o));
    }

    function getAiParams($i){
        $getres = $this->setFrame();

        if($i > 9){
            $output = array(
                'width' => $getres['width'],
                'height' => $getres['height'],
                'sess_cookie' => 'sco_' . $i . 'ads',
                'user_cookie' => 'uco_' . $i . 'ads'
            );
        }else{
            $output = array(
                'width' => $getres['width'],
                'height' => $getres['height'],
                'sess_cookie' => 'co_s' . $i . 'ads',
                'user_cookie' => 'cu_u' . $i . 'ads'
            );
        }

        return $output;
    }

    function generator(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $t = $this->input->post('tx');
        $keycode = base64_encode($token);

        $newarticle = $this->mgonews->getListNews($this->parent_id,20);
        $total = count($newarticle);
        $i = $this->uri->segment(2,0);

        $getParams = $this->getAlexaParams($i);
        if($total > 0){
            if($t == $keycode){
                $dtime = strtotime($newarticle[$i]['date_created']);
                $o['ax_u'] = base64_encode(urlencode(site_url('read') . '/' . $newarticle[$i]['content_id'] . '/' . $newarticle[$i]['channel_id'] . '/' . slug($newarticle[$i]['title']) . '-' . $dtime));
                $o['ax_w'] = base64_encode($getParams['width']);
                $o['ax_h'] = base64_encode($getParams['height']);
                $o['uc'] = base64_encode($getParams['user_cookie']);
                $o['sc'] = base64_encode($getParams['sess_cookie']);
                $o['ax_t'] = base64_encode(rawurlencode($newarticle[$i]['title']));
                $o['ax_r'] = base64_encode(urlencode(base64url_decode($this->input->post('site'))));
                $o['ua'] = base64_encode($this->setUA());
                $o['success'] = 1;
            }else{
                $o['result'] = 'Let\'s War Begin!';
                $o['success'] = 0;
            }
        }else{
            $o['result'] = 'Let\'s War Begin!';
            $o['success'] = 0;
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($o));
    }

    function getAlexaParams($i){
        $getres = $this->setFrame();

        if($i > 9){
            $output = array(
                'width' => $getres['width'],
                'height' => $getres['height'],
                'sess_cookie' => 's_c' . $i . 'gads',
                'user_cookie' => 'u_c' . $i . 'gads'
            );
        }else{
            $output = array(
                'width' => $getres['width'],
                'height' => $getres['height'],
                'sess_cookie' => 'csg' . $i . 'ads',
                'user_cookie' => 'usg' . $i . 'ads'
            );
        }

        return $output;
    }

    function reckoning(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $t = $this->input->post('tx');
        $keycode = base64_encode($token);

        $newarticle = $this->mgonews->getListNews($this->parent_id,10);
        $total = count($newarticle);
        $i = $this->uri->segment(2,0);

        $getParams = $this->getReckoningParams($i);
        if($total > 0){
            if($t == $keycode){
                $site_domain = $this->config->item('read_domain');
                $dtime = strtotime($newarticle[$i]['date_created']);
                $o['ax_u'] = base64_encode(urlencode('https://' . $site_domain[$newarticle[$i]['channel_id']] . '/read/' . $newarticle[$i]['content_id'] . '/' . $newarticle[$i]['channel_id'] . '/' . slug($newarticle[$i]['title']) . '-' . $dtime));
                $o['ax_w'] = base64_encode($getParams['width']);
                $o['ax_h'] = base64_encode($getParams['height']);
                $o['uc'] = base64_encode($getParams['user_cookie']);
                $o['sc'] = base64_encode($getParams['sess_cookie']);
                $o['ax_t'] = base64_encode(rawurlencode(seo_title($newarticle[$i]['title'])));
                $o['ax_r'] = base64_encode(urlencode(base64url_decode($this->input->post('site'))));
                $o['ua'] = base64_encode($this->setUA());
                $o['success'] = 1;
            }else{
                $o['result'] = 'Let\'s War Begin!';
                $o['success'] = 0;
            }
        }else{
            $o['result'] = 'Let\'s War Begin!';
            $o['success'] = 0;
        }

        $this->output->set_content_type('application/json')->set_output(json_encode($o));
    }

    function getReckoningParams($i){
        $getres = $this->setFrame();

        if($i > 5){
            $output = array(
                'width' => $getres['width'],
                'height' => $getres['height'],
                'sess_cookie' => 'ai' . $i . '_resurrection',
                'user_cookie' => 'ai' . $i . '_resurrection'
            );
        }else{
            $output = array(
                'width' => $getres['width'],
                'height' => $getres['height'],
                'sess_cookie' => 'ai' . $i . '_reckoning',
                'user_cookie' => 'ai' . $i . '_reckoning'
            );
        }

        return $output;
    }

    function setFrame(){
        if($this->mobiledevice->device_check()){
            $res = array(
                array(
                    'w' => 1366,
                    'h' => 768
                ),array(
                    'w' => 1920,
                    'h' => 1080
                ),array(
                    'w' => 1280,
                    'h' => 800
                ),array(
                    'w' => 1440,
                    'h' => 900
                ),array(
                    'w' => 1280,
                    'h' => 1024
                ),array(
                    'w' => 1600,
                    'h' => 900
                ),array(
                    'w' => 1680,
                    'h' => 1050
                ),array(
                    'w' => 1920,
                    'h' => 1200
            ));
        }else{
            $res = array(
                array(
                    'w' => 360,
                    'h' => 640
                ),array(
                    'w' => 360,
                    'h' => 640
                ),array(
                    'w' => 320,
                    'h' => 570
                ),array(
                    'w' => 720,
                    'h' => 1280
                ),array(
                    'w' => 480,
                    'h' => 800
                ),array(
                    'w' => 480,
                    'h' => 854
                ),array(
                    'w' => 540,
                    'h' => 960
                ),array(
                    'w' => 600,
                    'h' => 1024
            ));
        }

        $r = rand(0,7);
        $w = $res[$r]['w'];
        $h = $res[$r]['h'];

        $out = array(
            'width' => $w,
            'height' => $h
        );

        return $out;
    }

    function setUA(){
        if($this->mobiledevice->device_check()){
            $ua = array(
                'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Safari/605.1.15',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8',
                'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36'
            );
        }else{
            $ua = array(
                'Mozilla/5.0 (Linux; Android 9; SM-G965F Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 9; SM-G950F Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 9; SM-G960F Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 8.0.0; SM-G930F Build/R16NW; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 8.0.0; FIG-LX3 Build/HUAWEIFIG-LX3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 8.0.0; WAS-LX3 Build/HUAWEIWAS-LX3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36',
                'Mozilla/5.0 (Android 9; Mobile; rv:68.0) Gecko/68.0 Firefox/68.0',
                'Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Mobile/15E148 Safari/604.1',
                'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148',
                'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Mobile/15E148 Safari/604.1',
                'Mozilla/5.0 (Linux; Android 8.0.0; SAMSUNG SM-G935A Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 8.1.0; SAMSUNG SM-J727A Build/M1AJQ) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G973F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; U; Android 8.1.0; zh-CN; EML-AL00 Build/HUAWEIEML-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.108 UCBrowser/11.9.4.974 UWS/2.13.1.48 Mobile Safari/537.36 AliApp(DingTalk/4.5.11) com.alibaba.android.rimet/10487439 Channel/227200 language/zh-CN',
                'Mozilla/5.0 (Linux; U; Android 8.1.0; zh-CN; EML-AL00 Build/HUAWEIEML-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.108 baidu.sogo.uc.UCBrowser/11.9.4.974 UWS/2.13.1.48 Mobile Safari/537.36 AliApp(DingTalk/4.5.11) com.alibaba.android.rimet/10487439 Channel/227200 language/zh-CN'
            );
        }

        $r = rand(0,14);
        $out = $ua[$r];

        return $out;
    } 

    function adtrack(){
        $i = $this->input->post('ac');
        $site_url = $this->input->post('su');

        if(isset($_COOKIE["__ads" . $i . "_promo"])){
            $ga_cid = $_COOKIE["__ads" . $i . "_promo"];

            $ga_id = 'UA-82922228-1';
            $ga_loc = urlencode('https://sindikasi.okezone.com/widget/okezone/segment/0');
            $ga_title = rawurlencode('Okezone Widget');
            $ga_ref = urlencode(base64_decode($site_url));

            $ga_pixels = 'https://www.google-analytics.com/collect?v=1&t=pageview&tid=' . $ga_id . '&ul=en-us&de=UTF-8&cid=' . $ga_cid . '&dl=' . $ga_loc . '&dt=' . $ga_title . '&dr=' . $ga_ref;
            $ga_event = 'https://www.google-analytics.com/collect?v=1&t=event&tid=UA-25311844-1&ul=en-us&de=UTF-8&cid=' . $ga_cid . '&ec=Daihatsu%20Sirion&ea=cpc%20banner&el=' . $ga_ref . '&dr=' . $ga_ref . '&cs=Daihatsu%20Sirion&cm=cpc&ck=Daihatsu%20Sirion&cn=All%20New%20Sirion';

            echo base64_encode('<img src="' . $ga_pixels . '" width="1" height="1"><img src="' . $ga_event . '" width="1" height="1">');
        }else{
            $ga_cid = uuid();
            setcookie("__ads" . $i . "_promo",$ga_cid,time() + 604800,'/');
        }
    } 

    function adbounce(){
        $ga_cid = uuid();
        $i = $this->input->post('ac');
        if(isset($_COOKIE["__iTv_"])){
            $ga_sindo_id = $_COOKIE["__iTv_" . $i];
        }else{
            $ga_sindo_id = $ga_cid;
            setcookie("__iTv_" . $i,$ga_sindo_id,time() + 86400,'/');
        }

        $ga_br_okz = 'https://www.google-analytics.com/collect?v=1&t=event&tid=UA-82922228-1&cid=fdb551c2-3457-4ed8-9273-e6d33174852d&dl=https%3A%2F%2Ftv.okezone.com%2Fstreaming_embed&dr=https%3A%2F%2Fvideo.sindonews.com%2Ftv%2Frcti&ec=okezone+RCTI&ea=Playing%20okezone+RCTI&el=https%3A%2F%2Fvideo.sindonews.com%2Ftv%2Frcti';
        $ga_pixels_okz = 'https://www.google-analytics.com/collect?v=1&t=pageview&tid=UA-82922228-1&cid=fdb551c2-3457-4ed8-9273-e6d33174852d&dl=https%3A%2F%2Ftv.okezone.com%2Fstreaming_embed&dt=Okezone%20TV%20%3A%3A%20rcti%20live%20streaming%20online&dr=https%3A%2F%2Fvideo.sindonews.com%2Ftv%2Frcti&cs=SINDOnews&cm=tv&ck=RCTI%20Live%20Streaming&cn=Live%20Streaming';
        $ga_event_okz = 'https://www.google-analytics.com/collect?v=1&t=event&tid=UA-25311844-1&cid=fdb551c2-3457-4ed8-9273-e6d33174852d&dl=https%3A%2F%2Fvideo.sindonews.com%2Ftv%2Frcti&dr=https%3A%2F%2Ftv.okezone.com%2Fstreaming_embed&dt=Okezone%20TV%20%3A%3A%20rcti%20live%20streaming%20online&ec=okezone+RCTI&ea=Playing%20okezone+RCTI&el=https%3A%2F%2Ftv.okezone.com%2Fstreaming_embed&cs=SINDOnews&cm=tv&ck=RCTI%20Live%20Streaming&cn=Live%20Streaming';
        $img_pixels = '<img src="' . $ga_br_okz . '" width="1" height="1"><img src="' . $ga_pixels_okz . '" width="1" height="1"><img src="' . $ga_event_okz . '" width="1" height="1">';

        $ga_br_metube = 'https://www.google-analytics.com/collect?v=1&t=event&tid=UA-85299917-1&cid=fdb551c2-3457-4ed8-9273-e6d33174852d&dl=https%3A%2F%2Fwww.metube.id%2Fembed%2F1%2F%3Ftype%3Dlive%26autoplay%3Dtrue&dr=https%3A%2F%2Fvideo.sindonews.com%2Ftv%2Frcti%3Fs%3Dmetube&ec=metube+RCTI&ea=Playing%20metube+RCTI&el=https%3A%2F%2Fvideo.sindonews.com%2Ftv%2Frcti%3Fs%3Dmetube';
        $ga_pixels_metube = 'https://www.google-analytics.com/collect?v=1&t=pageview&tid=UA-85299917-1&cid=fdb551c2-3457-4ed8-9273-e6d33174852d&dl=https%3A%2F%2Fwww.metube.id%2Fembed%2F1%2F%3Ftype%3Dlive%26autoplay%3Dtrue&dt=meTube.id%20-%20RCTI&dr=https%3A%2F%2Fvideo.sindonews.com%2Ftv%2Frcti%3Fs%3Dmetube&cs=SINDOnews&cm=tv&ck=RCTI%20Live%20Streaming&cn=Live%20Streaming';
        $ga_event_metube = 'https://www.google-analytics.com/collect?v=1&t=event&tid=UA-25311844-1&cid=fdb551c2-3457-4ed8-9273-e6d33174852d&dl=https%3A%2F%2Fvideo.sindonews.com%2Ftv%2Frcti%3Fs%3Dmetube&dr=https%3A%2F%2Fwww.metube.id%2Fembed%2F1%2F%3Ftype%3Dlive%26autoplay%3Dtrue&dt=meTube.id%20-%20RCTI&ec=metube+RCTI&ea=Playing%20metube+RCTI&el=https%3A%2F%2Fwww.metube.id%2Fembed%2F1%2F%3Ftype%3Dlive%26autoplay%3Dtrue&cs=SINDOnews&cm=tv&ck=RCTI%20Live%20Streaming&cn=Live%20Streaming';
        $img_pixels .= '<img src="' . $ga_br_metube . '" width="1" height="1"><img src="' . $ga_pixels_metube . '" width="1" height="1"><img src="' . $ga_event_metube . '" width="1" height="1">';

        echo base64_encode($img_pixels);
    }

    function okztrack(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $t = $this->input->post('tx');
        $keycode = base64_encode($token);

        if($t == $keycode){
            $source = array(
                'https://international.sindonews.com/',
                'https://nasional.sindonews.com/',
                'https://daerah.sindonews.com/',
                'https://www.sindonews.com/'
            );
            $total = count($source);
            $max = $total - 1;
            $random_site = rand(0,$max);
            $site_url = $source[$random_site];
            $ga_cid = uuid();
            $ga_id = 'UA-82922228-1';
            $ga_loc = urlencode('https://sindikasi.okezone.com/widget/okezone/segment/0');
            $ga_title = rawurlencode('Okezone Widget');
            $ga_ref = urlencode($site_url);

            $ga_pixels = 'https://www.google-analytics.com/collect?v=1&t=pageview&tid=' . $ga_id . '&ul=en-us&de=UTF-8&cid=' . $ga_cid . '&dl=' . $ga_loc . '&dt=' . $ga_title . '&dr=' . $ga_ref;
            echo $ga_pixels;
        }else{
            echo 'Where Shall My Blood be Spilled? Let\'s War Begin!';
        }
    }

    function inewstrack(){
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $t = $this->input->post('tx');
        $keycode = base64_encode($token);

        if($t == $keycode){
            $source = array(
                'https://www.sindonews.com/',
                'https://nasional.sindonews.com/',
                'https://sin.do/Xva'
            );
            $total = count($source);
            $max = $total - 1;
            $random_site = rand(0,$max);
            $site_url = $source[$random_site];

            $urlrand = rand(1,10);
            $urlinews = 'https://www.inews.id/widget/sindonews/news/' . $urlrand;

            $ga_cid = uuid();
            $ga_id = 'UA-109056487-1';
            $ga_loc = urlencode($urlinews);
            $ga_title = rawurlencode('iNews Portal - Home');
            $ga_ref = urlencode($site_url);

            $ga_pixels = 'https://www.google-analytics.com/collect?v=1&t=pageview&tid=' . $ga_id . '&ul=en-us&de=UTF-8&cid=' . $ga_cid . '&dl=' . $ga_loc . '&dt=' . $ga_title . '&dr=' . $ga_ref;
            echo $ga_pixels;
        }else{
            echo 'Where Shall My Blood be Spilled? Let\'s War Begin!';
        }
    }

}
