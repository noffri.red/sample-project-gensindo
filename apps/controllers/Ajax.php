<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helpers("gen");
        $this->load->model('news/mgonews');
    }

    function index(){
        redirect(base_url(),'location',301);
    }

    function breakinghome(){
        $id_kanal = $this->config->item('gensindo_id');

        $offset = 10;
        $start = $this->uri->segment(2,0);

        $t['latest'] = $this->mgonews->getDbListNews($id_kanal,$start,$offset);
        $t['totalData'] = $this->mgonews->getNumListNews($id_kanal);
        $t['per_page'] = $offset;

        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('paging');

        $config['base_url'] = site_url('go');
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 2;

        $config['display_pages'] = FALSE;
        $config['prev_link'] = FALSE;
        $config['last_link'] = '';
        $config['last_link'] = FALSE;
        $config['first_link'] = '';
        $config['first_link'] = FALSE;

        $config['next_link'] = 'Load More';
        $config['next_tag_open'] = '<div class="news-more">';
        $config['next_tag_close'] = '</div>';

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        $this->load->view('destop2020/home/vmore',$t,FALSE);
    }

    function subkanal(){
        $offset = 10;
        $id_subkanal = $this->uri->segment(2,0);
        $start = $this->uri->segment(3,0);

        $t['latest'] = $this->mgonews->getDbListNewsSubkanal($id_subkanal,$start,$offset);
        $t['totalData'] = $this->mgonews->getNumListNewsSubkanal($id_subkanal);
        $t['per_page'] = $offset;

        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('paging');

        $config['base_url'] = site_url('gosub/' . $id_subkanal);
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 3;

        $config['display_pages'] = FALSE;
        $config['prev_link'] = FALSE;
        $config['last_link'] = '';
        $config['last_link'] = FALSE;
        $config['first_link'] = '';
        $config['first_link'] = FALSE;

        $config['next_link'] = 'Load More';
        $config['next_tag_open'] = '<div class="news-more">';
        $config['next_tag_close'] = '</div>';

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        $this->load->view('destop2020/home/vmore',$t,FALSE);
    }

    function mbreakinghome(){
        $id_kanal = $this->config->item('gensindo_id');
        
        $offset = 10;
        $start = $this->uri->segment(2,0);

        $t['latest'] = $this->mgonews->getDbListNews($id_kanal,$start,$offset);
        $t['totalData'] = $this->mgonews->getNumListNews($id_kanal);
        $t['per_page'] = $offset;

        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('paging');

        $config['base_url'] = site_url('mgo');
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 2;

        $config['display_pages'] = FALSE;
        $config['prev_link'] = FALSE;
        $config['last_link'] = '';
        $config['last_link'] = FALSE;
        $config['first_link'] = '';
        $config['first_link'] = FALSE;

        $config['next_link'] = 'Load More';
        $config['next_tag_open'] = '<div class="news-more">';
        $config['next_tag_close'] = '</div>';

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        // $this->load->view('mobile/home/vmore',$t,FALSE); 
        $this->load->view('mobile2020/home/vmore',$t,FALSE);
    }
    
    function msubkanal(){
        $offset = 10;
        $id_subkanal = $this->uri->segment(2,0);
        $start = $this->uri->segment(3,0);

        $t['latest'] = $this->mgonews->getDbListNewsSubkanal($id_subkanal,$start,$offset);
        $t['totalData'] = $this->mgonews->getNumListNewsSubkanal($id_subkanal);
        $t['per_page'] = $offset;

        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('paging');

        $config['base_url'] = site_url('mgosub/' . $id_subkanal);
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 3;

        $config['display_pages'] = FALSE;
        $config['prev_link'] = FALSE;
        $config['last_link'] = '';
        $config['last_link'] = FALSE;
        $config['first_link'] = '';
        $config['first_link'] = FALSE;

        $config['next_link'] = 'Load More';
        $config['next_tag_open'] = '<div class="news-more">';
        $config['next_tag_close'] = '</div>';

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        // $this->load->view('mobile/home/vmore',$t,FALSE);
        $this->load->view('mobile2020/home/vmore',$t,FALSE);
    }

}
