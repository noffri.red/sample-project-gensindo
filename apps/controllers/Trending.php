<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Trending extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helpers("gen");
        // $this->load->model('topic/mdbtopic','mdbtopic');
        $this->load->model('news/mgonews','mgonews');
    }

    function index(){
        if($this->mobiledevice->device_check()){
            $this->desktop();
        }else{
            $this->mobile();
        }
    }

    function desktop(){
        $parent_id = $this->config->item('gensindo_id');
        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['title'] = 'GEN SINDO | Berita dan Opini Generasi Muda Milenial Terpopuler';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Berita Gen SINDO Terpopuler menyajikan informasi seputar Tren, Opini, Fashion dan Serba serbi Generasi Muda dan Milenial',
            'Slurp' => 'all',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow'
        );

        $a['html']['metaname'] = metaname($metaname);

        // $a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= add_external_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

        $a['upVersion'] = $this->config->item('upVersioncssjs');  // for remove caching
        $a['html']['css'] = $this->load->view('destop2020/template/cssheader', $a, TRUE);
       
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        // $a['html']['bottom_css'] .= m_add_css('/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/materialize.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/swiper.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/script.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/jquery.jscroll.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);


        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/loadingoverlay.min.js'.$a['upVersion']);

        $a['template']['header'] = $this->load->view('destop2020/template/vheader', NULL, TRUE);
        $a['template']['minimenu'] = $this->load->view('destop2020/template/vminimenu', NULL, TRUE);
        $a['template']['minifooter'] = $this->load->view('destop2020/template/vfooter', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('destop2020/template/vnewcss', NULL, TRUE);
        $a['template']['promogen'] = $this->load->view('destop2020/template/vpromogen', NULL, TRUE);

        // $t['event'] = $this->mgonews->getEvent();
        $t['event'] = array();
        $a['template']['agenda'] = $this->load->view('template/vagenda',$t,TRUE);

        // $t['trending'] = $this->mgonews->getKanalTrendingTopic();
        // $t['trending']['results'] = array();
        $t['trending'] = array();
        $a['template']['trending'] = $this->load->view('destop2020/template/vtrending',$t,TRUE);

        $t['topic'] = '';
        $t['trendingtopic'] = array();
        if(!empty($t['trending']['results'])){
            $id_topic = $t['trending']['results'][0]['id_topic'];
            $t['topic'] = $t['trending']['results'][0]['topic'];
            $trendingtopic = $this->mdbtopic->getTopicContent($id_topic);
            $t['trendingtopic'] = $trendingtopic['results'];
        }

        $t['popular'] = array();
        $a['content']['popular'] = $this->load->view('destop2020/home/vpopular',$t,TRUE);

        

        $this->load->view('destop2020/pages/vtrending',$a,FALSE);
    }

    function mobile(){
        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['title'] = 'GEN SINDO | Berita dan Opini Generasi Muda Milenial Terpopuler';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Berita Gen SINDO Terpopuler menyajikan informasi seputar Tren, Opini, Fashion dan Serba serbi Generasi Muda dan Milenial',
            'Slurp' => 'all',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $a['html']['metaname'] = metaname($metaname);

        //$a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= add_external_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

        $a['upVersion'] = $this->config->item('upVersioncssjs');  // for remove caching
        $a['html']['css'] = $this->load->view('mobile2020/template/cssheader', $a, TRUE);

        $a['html']['js'] = m_add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/materialize.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/jquery.jscroll.min.js'.$a['upVersion']);

        $a['html']['js'] .= m_add_js('/js/jquery.fancybox.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/swiper.min.js').$a['upVersion'];
        $a['html']['js'] .= m_add_js('/js/script.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/loadingoverlay.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);

        $a['html']['js'] .= add_external_js(base_url() . 'sn-v1.js'.$a['upVersion']);

        $a['template']['header'] = $this->load->view('mobile2020/template/vheader', NULL, TRUE);
        $a['template']['menuoverlay'] = $this->load->view('mobile2020/template/vmenuoverlay', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('mobile2020/template/vnewcss', NULL, TRUE);
        $a['template']['subkanal'] = $this->load->view('mobile2020/template/vsubkanalmenu', NULL, TRUE);
        $a['template']['footer'] = $this->load->view('mobile2020/template/vfooter', NULL, TRUE);



        // $t['event'] = $this->mgonews->getEvent();
        $t['event'] = array();
        $a['template']['agenda'] = $this->load->view('mobile/template/vagenda',$t,TRUE);

        // $t['trending'] = $this->mgonews->getKanalTrendingTopic();
        $t['trending']= array();
        $a['template']['trending'] = $this->load->view('mobile/template/vtrending',$t,TRUE);

        $t['popular'] = array();
        $a['content']['popular'] = $this->load->view('mobile/home/vpopular',$t,TRUE);

        $this->load->view('mobile2020/pages/vtrending',$a,FALSE);
    }

}
