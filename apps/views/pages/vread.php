<!DOCTYPE html>
<html lang="id-ID" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
    <head>
        <title><?php echo $html['title']; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
        <meta name="theme-color" content="#004B8F">

        <link rel="shortcut icon" href="<?php echo template_uri(); ?>/images/favicon.ico">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo template_uri(); ?>/images/favicon-192x192.png" sizes="192x192">

        <?php echo $html['metaname']; ?>
        <?php echo $html['canonical']; ?>
        
        <meta property="fb:app_id" content="325657497499535">
        <meta property="fb:pages" content="248570208512540">

        <link rel="amphtml" href="<?php echo $content['amp_url']; ?>">
        <link rel="alternate" title="GenSINDO" href="<?php echo site_url('feed'); ?>" type="application/rss+xml"/>

        <?php echo $html['css']; ?>
        <?php echo $template['newcss']; ?>
        
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-HDPI.css" media="screen and (min-width: 1083px) and (max-width: 1290px)">
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-MDPI.css" media="screen and (min-width: 993px) and (max-width: 1082px)">
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-SDPI.css" media="screen and (min-width: 769px) and (max-width: 992px)">
        <link rel="stylesheet" href="<?php echo template_uri(); ?>/css/mediaquery-mobile.css" media="screen and (max-width: 768px)">

        <script type="application/ld+json">
            {
                "@context": "https://schema.org",
                "@type": "NewsArticle",
                "mainEntityOfPage": {
                    "@type": "WebPage",
                    "@id": "<?php echo $content['site_url']; ?>"
                },
                "headline": "<?php echo $content['site_title']; ?>",
                "image": {
                    "@type": "ImageObject",
                    "url": "<?php echo $content['site_image']; ?>",
                    "height": 413,
                    "width": 620
                },
                "datePublished": "<?php echo $content['site_publish']; ?>",
                "dateModified": "<?php echo $content['site_publish']; ?>",
                "author": {
                    "@type": "Person",
                    "name": "<?php echo $content['site_reporter']; ?>"
                },
                "publisher": {
                    "@type": "Organization",
                    "name": "SINDOnews.com",
                    "logo": {
                        "@type": "ImageObject",
                        "url": "https://sm.sindonews.net/dyn/600/mobile/2016/images/logo-sindonews.png",
                        "width": 600,
                        "height": 55
                    }
                },
                "description": "<?php echo $content['site_summary']; ?>"
            }
        </script>
        <script type="application/ld+json">
            {
                "@context":"https://schema.org",
                "@type":"BreadcrumbList",
                "itemListElement":[{
                    "@type":"ListItem",
                    "position":1,
                    "item":{
                        "@id":"<?php echo base_url(); ?>",
                        "name":"home"
                    }
                },{
                    "@type":"ListItem",
                    "position":2,
                    "item":{
                        "@id":"<?php echo site_url('loadmore/' . $content['channel_id']); ?>",
                        "name":"GEN SINDO"
                    }
                }]
            }
        </script>
        <script type="application/ld+json">
            {
                "@context": "http://schema.org",
                "@type": "WebPage",
                "headline": "<?php echo addcslashes($content['site_title'],'"\\/'); ?>",
                "url": "<?php echo $content['site_url']; ?>",
                "datePublished": "<?php echo $content['site_publish']; ?>",
                "image": {
                    "@type": "ImageObject",
                    "url": "<?php echo $content['site_image']; ?>",
                    "height": 413,
                    "width": 620
                },
                "description": "<?php echo addcslashes($content['site_summary'],'"\\/'); ?>"
            }
        </script>
        
        <script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
        <?php echo $html['topjs']; ?>
        <script>
            var PREBID_TIMEOUT = 700;
            var FAILSAFE_TIMEOUT = 3000;

            var adUnits = [{
                code: 'div-gpt-ad-billboard',
                mediaTypes: {
                    banner: {
                        sizes: [[970,250],[970,90],[728,90]]
                    }
                },
                bids: [{
                    bidder: 'criteo',
                    params: {
                       networkId: 9519,
                       publisherSubId: 'sindonews'
                    }

                }]
            },{
            	code: 'div-gpt-ad-middle_1',
                mediaTypes: {
                    banner: {
                        sizes: [[336,280],[300,250]]
                    }
                },
                bids: [{
                    bidder: 'criteo',
                        params: {
                           networkId: 9519,
                           publisherSubId: 'sindonews'
                        }

                }]
            },{
            	code: 'div-gpt-ad-middle_2',
                mediaTypes: {
                    banner: {
                        sizes: [[336,280],[300,250]]
                    }
                },
                bids: [{
                    bidder: 'criteo',
                    params: {
                       networkId: 9519,
                       publisherSubId: 'sindonews'
                    }

                }]
            },{
            	code: 'div-gpt-ad-right_sidebar_1',
                mediaTypes: {
                    banner: {
                        sizes: [[300,250],[300,600]]
                    }
                },
                bids: [{
                    bidder: 'criteo',
                    params: {
                       networkId: 9519,
                       publisherSubId: 'sindonews'
                    }

                }]
            }];

            var googletag = googletag || {};
            googletag.cmd = googletag.cmd || [];
            googletag.cmd.push(function() {
                googletag.pubads().disableInitialLoad();
            });

            var pbjs = pbjs || {};
            pbjs.que = pbjs.que || [];

            pbjs.que.push(function() {
            	pbjs.setConfig({
                    userSync: {
                        userIds: [{
                            name: "criteo",
                        }]
                    },
                    priceGranularity: "auto"
                });
                pbjs.addAdUnits(adUnits);
                pbjs.requestBids({
                    bidsBackHandler: initAdserver,
                    timeout: PREBID_TIMEOUT
                });
            });

            function initAdserver() {
                if (pbjs.initAdserverSet) return;
                pbjs.initAdserverSet = true;
                googletag.cmd.push(function() {
                    pbjs.setTargetingForGPTAsync && pbjs.setTargetingForGPTAsync();
                    googletag.pubads().refresh();
                });
            }
            
            setTimeout(function() {
                initAdserver();
            }, FAILSAFE_TIMEOUT);
            
            window.googletag = window.googletag || {cmd: []};
            
            var wv = navigator.userAgent.toLowerCase();
            var aggr = "0";
            if(wv.includes('topbuzz') || wv.includes('babe') || wv.includes('bacaberita')) {
                aggr = "topbuzz";
            } else if (wv.includes('babe')){
                aggr = "babe";
            } else if (wv.includes('bacaberita')){
                aggr = "babe";
            } else if (wv.includes('ucbrowser')){
                aggr = "ucbrowser";
            } else if (wv.includes('kurio')){
                aggr = "kurio";
            } else {
                aggr = "other";
            }

            googletag.cmd.push(function() {
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[970,250],[970,90],[728,90],[1,1]], 'div-gpt-ad-billboard').setTargeting('pos', ['billboard']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[300,250],[300,600]], 'div-gpt-ad-right_sidebar_1').setTargeting('pos', ['right_sidebar_1']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[300,250]], 'div-gpt-ad-right_sidebar_2').setTargeting('pos', ['right_sidebar_2']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[336,280],[300,250],[1,1]], 'div-gpt-ad-middle_1').setTargeting('pos', ['middle_1']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[336,280],[300,250],[1,1]], 'div-gpt-ad-middle_2').setTargeting('pos', ['middle_2']).addService(googletag.pubads());
                googletag.defineSlot('/105246217/sindonews_desktop/gensindo', [[120,600]], 'div-gpt-ad-left_skinads').setTargeting('pos', ['left_skinads']).addService(googletag.pubads());
                
                googletag.defineOutOfPageSlot('/105246217/sindonews_desktop/gensindo', 'div-gpt-ad-outstream').setTargeting('pos', ['outstream']).addService(googletag.pubads());
                googletag.defineOutOfPageSlot('/105246217/sindonews_desktop/gensindo', 'div-gpt-ad-abm').setTargeting('partner', ['abm']).addService(googletag.pubads());
                
                googletag.pubads().setTargeting('page_type', ['articlepage']);
                googletag.pubads().setTargeting('aggregator', aggr);
                googletag.pubads().enableLazyLoad({
                    fetchMarginPercent: 500,
                    renderMarginPercent: 200,
                    mobileScaling: 2.0
                });
                googletag.pubads().collapseEmptyDivs();
                googletag.enableServices();
                
                googletag.pubads().addEventListener('slotRenderEnded', function(event) {
                    var size = event.size;
                    if(size === null) return;
                    
                    var slot = event.slot;
                    var slotDiv = document.getElementById(slot.getSlotElementId());
                    if(!event.isEmpty){
                        slotDiv.style.height = size[1] + 'px';
                    }
                });
            });
            function bannerCheck(unitAd){googletag.pubads().addEventListener('slotRenderEnded',function(event){if(event.slot.getSlotElementId() === unitAd){if(event.isEmpty){var eAd = document.getElementById('me-'+unitAd);eAd.classList.add('adnone');}}});}
        </script>
        
        <script src="https://cdn.onesignal.com/sdks/OneSignalSDK.js" async></script>
        <script>
            window.OneSignal = window.OneSignal || [];
            OneSignal.push(function () {
                OneSignal.init({
                    appId: '<?php echo $this->config->item('onesignal_appid'); ?>',
                    autoResubscribe: true,
                    notifyButton: {
                        enable: false
                    },
                    promptOptions: {
                        slidedown: {
                            enabled: true,
                            autoPrompt: true,
                            actionMessage: "Dapatkan update berita SINDOnews melalui notifikasi browser Anda.",
                            acceptButtonText: "SUBSCRIBE",
                            cancelButtonText: "TIDAK"
                        }
                    }
                });
            });
        </script>

        <script>
            window.dataLayer = [{
                kanalName: 'GenSINDO',
                subkanalName: '<?php echo $content['cd_subkanal']; ?>',
                keywordTag: '<?php echo addslashes(trim($content['cd_tags'])); ?>',
                authorName: '<?php echo addslashes($content['cd_author']); ?>',
                editorName: '<?php echo addslashes($content['cd_editor']); ?>',
                datePublish: '<?php echo $content['date_publish']; ?>',
                timePublish: '<?php echo $content['time_publish']; ?>',
                articleID: '<?php echo $content['content_id']; ?>',
                contentType: 'artikel',
                pageType: 'article_page',
                dataSource: 'Non AMP'
            }];
        </script>
        
        <?php echo $html['bottom_js']; ?>
    </head>
    <body>
        <header>
            <?php echo $template['header']; ?>
        </header>
        <section class="slidenav">
            <ul id="slide-out" class="sidenav">
                <li>
                    <div class="user-view">
                        <a href="#user"><img class="circle" src="<?php echo template_uri(); ?>/images/logo.png"></a>
                    </div>
                </li>
                <li class="gnews"><a href="<?php echo site_url('gnews'); ?>">Gen News</a></li>
                <li class="youth"><a href="<?php echo site_url('youth'); ?>">Gen Shot</a></li>
                <li class="imaji"><a href="<?php echo site_url('imaji'); ?>">Gen VIew</a></li>
            </ul>
        </section>
        <main>
            <div class="container clearfix">
                <div class="billboard">
                    <div id="div-gpt-ad-billboard" style="height:250px"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-billboard');});</script></div>
                </div>
                <div class="column-big pull-left clearfix">
                    <div class="center pull-right">
                        <section class="detail">
                            <?php echo $content['breadcrumb']; ?>
                            <div class="border">
                                <?php echo $content['detail']; ?>
                                <?php echo $content['relatedtopic']; ?>                                
                                <div class="share">
                                    <div class="sharethis-reaction"><div class="sharethis-inline-reaction-buttons"></div></div>
                                    <div class="sharethis-box"><div class="sharethis-inline-share-buttons"></div></div>
                                </div>
                                <div class="ads-geniee adsload" id="mgidDiv"></div>
                                <?php //echo $content['profileeditor']; ?>                                
                                <div class="ads-geniee">
                                    <div id="dablewidget_ml6pMeo4_6oM4zLXb" data-widget_id-pc="ml6pMeo4" data-widget_id-mo="6oM4zLXb"></div>
                                </div>
                                <?php echo $content['relatedcontent']; ?>
                                <?php echo $content['comment']; ?>
                            </div>
                        </section>
                    </div>
                    <div class="leftbar pull-left">
                        <?php echo $template['kategori']; ?>
                        <?php echo $template['trending']; ?>
                        <div class="r-side sky-banner">
                            <div id="div-gpt-ad-left_skinads"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-left_skinads');});</script></div>
                        </div>
                    </div>
                </div>
                <div class="rightbar pull-right">
                    <?php echo $template['promogen']; ?>
                    <div class="r-side mb20 medium-banner">
                        <div id="div-gpt-ad-right_sidebar_1"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-right_sidebar_1');});</script></div>
                    </div>
                    <?php echo $template['popular'];?>
                    <div class="r-side mb20 medium-banner">
                        <div id="div-gpt-ad-right_sidebar_2"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-right_sidebar_2');});</script></div>
                    </div>
                    <?php echo $template['agenda']; ?>
                    <?php echo $template['promoted']; ?>
                    <?php echo $template['minifooter']; ?>
                </div>
            </div>
        </main>
        
        <div class="ads1x1">
            <div id="div-gpt-ad-outstream"><script>googletag.cmd.push(function(){googletag.display('div-gpt-ad-outstream');});</script></div>
        </div>
        
        <?php echo $html['bottom_css']; ?>
        <?php echo $html['js']; ?>
        <script id="dsq-count-scr" src="https://sindonews.disqus.com/count.js" async></script>
        <script src="https://platform-api.sharethis.com/js/sharethis.js#property=5c3002466aa2aa0011451e10&product=inline-share-buttons" async></script>

        <script>
            function number_format(e,n,t,i){e=(e+"").replace(/[^0-9+\-Ee.]/g,"");var r=isFinite(+e)?+e:0,a=isFinite(+n)?Math.abs(n):0,o="undefined"==typeof i?",":i,d="undefined"==typeof t?".":t,u="",f=function(e,n){var t=Math.pow(10,n);return""+(Math.round(e*t)/t).toFixed(n)};return u=(a?f(r,a):""+Math.round(r)).split("."),u[0].length>3&&(u[0]=u[0].replace(/\B(?=(?:\d{3})+(?!\d))/g,o)),(u[1]||"").length<a&&(u[1]=u[1]||"",u[1]+=new Array(a-u[1].length+1).join("0")),u.join(d)}
            $(function () {
                $.ajax({url:'<?php echo site_url('scream'); ?>?sin='+sindoid()+'&do='+sindotime+'&z='+sindonum(0,11),method:"POST",data:{xyz:'<?php echo $content['code']; ?>',c:'<?php echo $content['id_content']; ?>'},success: function(result){var screamdata = $.parseJSON(JSON.stringify(result));var screamviews = 0;if(screamdata.data[0].hit > 0){screamviews = screamdata.data[0].hit;}$('.hit-view').text('views: '+number_format(screamviews,0,'','.'));}});

                var imagesLoad = new LazyLoad({
                    elements_selector:'.lazyload'
                });
                
                window.dataLayer.push({event: 'mgidAd'});
                window.dataLayer.push({event: 'adOptEvent'});
                window.dataLayer.push({event: 'dableEvent'});
                
                $.ajax({
                    url:'<?php echo $content['disqus_url']; ?>',
                    method:'GET',
                    data:{
                        site:'<?php echo base64_encode($content['site_url']); ?>',
                        title:'<?php echo base64_encode($content['site_title']); ?>'
                    },
                    success:function(result){
                        $('.sindodisqus').html(result);
                        $('#disqus_thread').children().first().fadeOut();
                    }
                });
                
                function clearDqAds(){
                    $('#disqus_thread').children().first().fadeOut();
                }
                
                setInterval(clearDqAds,2000);
                setTimeout(function(){sindostop(clearDqAds);},15000);

                if ($(window).width() > 770) {
                    $(window).scroll(function () {
                        if ($(this).scrollTop() > 1495) {
                            $('.leftbar').addClass("sticky");
                            $(".leftbar").animate({top: "30px"}, 0);
                            $('.leftbar .r-side').addClass("none");
                        } else {
                            $('.leftbar').removeClass("sticky");
                            $('.leftbar .r-side').removeClass("none");
                        }
                        if ($(this).scrollTop() > 1262) {
                            $('.rightbar').addClass("sticky");
                            $(".rightbar").animate({top: "30px"}, 0);
                            $('.rightbar .r-side').addClass("none");
                            $('.agenda').addClass("none");
                        } else {
                            $('.leftbar').removeClass("sticky");
                            $('.rightbar').removeClass("sticky");
                            $('.rightbar .r-side').removeClass("none");
                            $('.agenda').removeClass("none");
                        }
                    });
                }

                $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');
                $('.tab ul.tabs li a').click(function (g) {
                    var tab = $(this).closest('.tab'),index = $(this).closest('li').index();
                    tab.find('ul.tabs > li').removeClass('current');
                    $(this).closest('li').addClass('current');
                    tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
                    tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();
                    g.preventDefault();
                });

                $('.sidenav').sidenav();
                
                /*
                $(document).scroll(function(){var y = $(this).scrollTop();if(y <= 800){$('.notif-box').removeClass('notif-show').addClass('notif-hide');}else{if($('.notif-box').hasClass('kill-me')){$('.notif-box').removeClass('notif-show').addClass('notif-hide');}else{$('.notif-box').removeClass('notif-hide').addClass('notif-show');}}});                   
                var subdomain='<?php echo $this->config->item('domain'); ?>';var cookiename='__gensindo_push_d';var getme = getCookie(cookiename);if(getme != ''){dismissPush();}
                $('.notif-yes').click(function(){dismissPush();});
                $('.notif-no').click(function(){setCookie(cookiename,randomuv(),30,subdomain);dismissPush();});
                if(Notification.permission==='granted' || Notification.permission==='denied'){setCookie(cookiename,randomuv(),30,subdomain);}
                
                function dismissPush(){$('.notif-box').removeClass('notif-show').addClass('notif-hide').addClass('kill-me');}
                */
            });
        </script>
    </body>
</html>