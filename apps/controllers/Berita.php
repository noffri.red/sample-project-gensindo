<?php

class Berita extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->config('site_channel');
        $this->load->library('article');
        $this->load->model('news/mgonews','mgonews');
    }

    function index(){
        if($this->mobiledevice->device_check()){
            $this->desktop();
        }else{
            $this->mobile();
        }
    }

    function desktop(){
        $parent_id = 1;
        $parent_id_new = $this->config->item('gensindo_id');

        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['upVersion'] = $this->config->item('upVersioncssjs');  // for remove caching
        $a['html']['css'] = $this->load->view('destop2020/template/cssheader', $a, TRUE);
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        // $a['html']['bottom_css'] .= m_add_css('/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/materialize.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/swiper.min.js'.$a['upVersion']);
        $a['html']['js'] .= add_external_js(base_url().'gensindo/2020-destop/js/script.js'.$a['upVersion']);
        $a['html']['js'] .= add_js('/js/jquery.jscroll.min.js'.$a['upVersion']);

        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);

        $a['html']['js'] .= add_external_js(base_url() . 'sn-v1.js'.$a['upVersion']);
        $a['html']['js'] .= add_external_js('https://sm.sindonews.net/mobile/2016/js/jquery.sticky.js');


        /*
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-app.js');
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-messaging.js');
        $a['html']['js'] .= add_external_js(base_url() . 'fcm.2020.js');
        */
        
        $a['html']['topjs'] = async_m_add_js('/js/prebid4.6.0-criteo.js');


        $a['template']['header'] = $this->load->view('destop2020/template/vheader', NULL, TRUE);
        $a['template']['minimenu'] = $this->load->view('destop2020/template/vminimenu', NULL, TRUE);
        $a['template']['promoted'] = $this->load->view('template/vpromoted', NULL, TRUE);
        $a['template']['kategori'] = $this->load->view('destop2020/template/vkategori', NULL, TRUE);
        $a['template']['minifooter'] = $this->load->view('destop2020/template/vfooter', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('destop2020/template/vnewcss', NULL, TRUE);
        $a['template']['promogen'] = $this->load->view('destop2020/template/vpromogen', NULL, TRUE);

        $getarticle = $this->article->getArticle();
        $t['details'] = $getarticle['details'];
        $content_id = $getarticle['details']['id_news'];
        $a['content']['content_id'] = $content_id;

        $uriread = $t['details']['id_news'] . '/' . $t['details']['id_subkanal'] . '/' . strtolower($t['details']['url_title']);
        $t['site_url'] = site_url('berita') . '/' . $uriread;
        $t['slug_title'] = $t['details']['url_title'];
        $a['content']['site_url'] = site_url('berita') . '/' . $uriread;
        $a['content']['site_title'] = cleanTitleArticle($t['details']['title']);
        $a['content']['site_publish'] = date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']));
        $a['content']['site_reporter'] = $t['details']['author'];
        $a['content']['site_editor'] = $t['details']['editor_name'];
        $a['content']['site_summary'] = seo_description($t['details']['summary']);
        $a['content']['id_subkanal'] = $t['details']['id_subkanal'];
        $a['content']['subkanal'] = $t['details']['subkanal'];
        $a['content']['amp_url'] = site_url('beritaamp') . '/' . $uriread;
        $a['content']['forum_url'] = site_url('desktop-forum') . '/' . $uriread;
        $a['content']['disqus_url'] = site_url('disqus') . '/' . $uriread;
        $a['content']['path_url'] = '/berita/' . $uriread;
        $a['content']['related'] = "";
        $a['content']['id_content'] = $t['details']['id_news'];
        $a['content']['date_publish'] = date('Ymd',strtotime($t['details']['publish']));
        $a['content']['time_publish'] = date('H:i',strtotime($t['details']['publish']));
        $a['content']['site_label'] = textlimit($t['details']['title'], 40);
        
        $topic_id_list = '';
        $topic_keyword = array();
        $getRelated = array();
        $getTopicData = array();

        $getTopic = $this->mgonews->getTopicGensindo($content_id);
        if(count($getTopic) > 0){
            $topic_id_list = $getTopic[0]['id_topic'];
            if($topic_id_list != ''){
                $getRelated = $this->mgonews->getRelatedGensindo($topic_id_list,$parent_id,9);
                
                $topicData = explode(',',$topic_id_list);
                for($i = 0; $i < count($topicData); $i++){
                    $getTopicData[$i] = $this->mgonews->getTopicListGensindo($topicData[$i]);
                    $topic_keyword[$i] = $getTopicData[$i][0]['topic'];
                }
            }
        }

 

        $t['topiklist'] = $topic_id_list;
        $t['related'] = $getRelated;
        $a['content']['topic_list'] = $topic_id_list;
        $a['content']['relatedtopic'] = $this->load->view('destop2020/berita/vrelatedtopic',$t,TRUE);
        $a['content']['relatedcontent'] = $this->load->view('destop2020/berita/vrelatedcontent',$t,TRUE);

        /* --- start article pagination --- */

        $listbaca = $this->mgonews->getListNews($parent_id_new,12);
        $baca = randomData($listbaca,12,3);

        $totalData = $this->article->getArticleNumListPageBacaDesktop($t['details'],$baca);

        $start = $this->uri->segment(5,0);
        $defaultpage = 100;
        if(isset($_GET['showpage']) && $_GET['showpage'] == 'all'){
            $offset = 100;
        }else{
            if($totalData > ($defaultpage + 3)){
                $offset = $defaultpage;
            }else{
                $offset = 100;
            }
        }

        $t['content'] = $this->article->getArticleListPageBacaDesktop($start,$offset,$t['details'],$baca);
        $t['totalData'] = $totalData;
        $a['content']['totalData'] = $totalData;

        $t['per_page'] = $offset;
        $t['start'] = $start;

        if($t['totalData'] <= $offset && $start > 0){
            redirect($t['site_url'],'location',301);
        }

        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('artpaging');
        $config['base_url'] = site_url('berita') . '/' . $uriread;
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        $currentPage = floor(($start / $config['per_page']) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;

        if($start == 0 || $start == ''){
            $pagetitle = '';
        }
        $t['current_page'] = $currentPage;
        $t['last_page'] = floor(($t['totalData'] / $config['per_page']) + 1);

        /* create canonical URL */
        $paging_status = 0;
        if($t['totalData'] > ($defaultpage + 3)){
            $paging_status = 1;            
        }
        
        $canonical = add_canonical('canonical',$t['site_url']);
        if($paging_status){
            $canonical = add_canonical('canonical',$t['site_url'] . '?showpage=all');
        }
        
        $a['html']['canonical'] = $canonical;
        /* --- end article pagination --- */

        /* ---- start SEO configuration ---- */

        if(isset($t['details']['images'])){
            $imgmeta = $this->config->item('images_uri') . $this->config->item('dyn_620') . '/gensindo/content/' . date('Y/m/d',strtotime($t['details']['date_created'])) . '/' . $t['details']['id_subkanal'] . '/' . $t['details']['id_content'] . '/' . $t['details']['images'];
        }else{
            $imgmeta = '';
        }
        $a['content']['site_image'] = $imgmeta;
        
        if($t['details']['author'] != ''){
            $reporter = ucwords($t['details']['author']);
        }else{
            $reporter = '';
        }
        
        /* create keyword tags */
        $get_topic_keyword = implode(',',$topic_keyword);
        $keyword = $this->article->newsKeywords($get_topic_keyword);
        $t['keyword'] = $keyword;
        $rich_keywords = explode(',',$keyword);
        $a['content']['keyword'] = $rich_keywords;
        $a['content']['site_tags'] = $keyword;

        $extend_desc = summaryMore($t['details']['content']);
        if(strlen($extend_desc) < 120){
            $article_summary = $t['details']['summary'] . ' ' . $extend_desc;
        }else{
            $article_summary = $t['details']['summary'];
        }
        /* title & description filtering */
        //$metatitle = seo_title($t['details']['title']);
        $metatitle = cleanTitleArticle($t['details']['title']);
        $metadesc = seo_description($article_summary);
        
        $a['html']['title'] = $metatitle . $pagetitle;
        $metaname = array(
            'description' => $metadesc . $pagetitle,
            'title' => $metatitle . $pagetitle,
            'image_src' => $imgmeta,
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $metafb = array(	
            'title' => cleanTitleArticle($t['details']['title']),	
            'type' => 'article',	
            'url' => $t['site_url'],	
            'image' => $imgmeta,	
            'image:type' => 'image/jpeg',	
            'image:width' => 620,	
            'image:height' => 413,	
            'description' => cleanSummary($metaname['description']),	
            'site_name' => 'SINDOnews.com'	
        );

        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@SINDOnews',
            'twitter:creator' => '@SINDOnews',
            'twitter:title' => cleanWords($t['details']['title']),
            'twitter:description' => cleanWords($t['details']['summary']),
            'twitter:image' => $imgmeta
        );

        $metadable = array(
            'item_id' => $content_id . $t['details']['id_kanal'],
            'title' => cleanWords($t['details']['title']),
            'image' => $imgmeta,
            'author' => $reporter
        );

        if(count($getTopicData) > 1){
            $section_a = trim($topic_keyword[0]);
            $section_b = trim($topic_keyword[1]);

            $metaArticle = array(
                'section' => 'milenial',
                'section2' => $section_a,
                'section3' => $section_b,
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }else{
            $metaArticle = array(
                'section' => 'milenial',
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }
        
        
        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);
        $a['html']['metaname'] .= metaDable($metadable);
        $a['html']['metaname'] .= metaArticle($metaArticle);
        /* ---- end SEO configuration ---- */
        
        /* GA Custom Dimension Start */
        $a['content']['cd_title'] = cleanTitleArticle($t['details']['title']) . $pagetitle;
        $a['content']['cd_subkanal'] = customDimensionString($t['details']['subkanal']);
        $a['content']['cd_author'] = ucwords($a['content']['site_reporter']);
        $a['content']['cd_editor'] = ucwords($a['content']['site_editor']);
        $a['content']['cd_tags'] = customDimensionTags($a['content']['site_tags']);
        $a['content']['cd_publish'] = $t['details']['publish'];
        if($t['totalData'] > $offset){
            $cd_page = $currentPage;
        }else{
            $cd_page = 0;
        }
        $a['content']['cd_page'] = $cd_page;
        /* GA Custom Dimension End */

        $t['others'] = $this->mgonews->getListNews($parent_id_new,30);

        $t['trending'] = $this->mgonews->getKanalTrendingTopic($parent_id_new,7);
        $a['template']['trending'] = $this->load->view('destop2020/template/vtrending',$t,TRUE);

        $t['trendingpopular'] = $this->mgonews->getListTitlePopular($parent_id_new,5);
        $a['template']['popular'] = $this->load->view('destop2020/template/vpopular',$t,TRUE);

        

        $t['event']['results'] = array();
        $a['template']['agenda'] = $this->load->view('destop2020/template/vagenda',$t,TRUE);

        $a['content']['breadcrumb'] = $this->load->view('destop2020/berita/vbreadcrumb',$t,TRUE);
        $a['content']['detail'] = $this->load->view('destop2020/berita/vdetail',$t,TRUE);

        $t['sindonews_terkini'] = $this->mgonews->getListNews($parent_id_new,10);
        $a['content']['sindonewsterkini'] = $this->load->view('destop2020/berita/vsindonewsterkini', $t,TRUE);


        // $a['content']['profileeditor'] = $this->load->view('berita/vprofileeditor',$t,TRUE);
        $a['content']['comment'] = $this->load->view('destop2020/berita/vcomment',$t,TRUE);
        //$a['content']['others'] = $this->load->view('berita/vothers',$t,TRUE);

        $this->load->view('destop2020/berita/vread',$a,FALSE);
    }

    function mobile(){
        $parent_id = 1;
        $parent_id_new = $this->config->item('gensindo_id');
        
        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['upVersion'] = $this->config->item('upVersioncssjs');  // for remove caching
        $a['html']['css'] = $this->load->view('mobile2020/template/cssheader', $a, TRUE);

        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');


        $a['html']['js'] = m_add_js('/js/jquery-3.5.1.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/jquery.fancybox.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/swiper.min.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/script.js'.$a['upVersion']);
        $a['html']['js'] .= m_add_js('/js/vanilla-lazyload-8.17.0.min.js'.$a['upVersion']);

        $a['html']['js'] .= add_external_js(base_url() . 'sn-v1.js'.$a['upVersion']);
        $a['html']['js'] .= add_external_js('https://sm.sindonews.net/mobile/2016/js/jquery.sticky.js');

        /*
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-app.js');
        $a['html']['js'] .= add_external_js('https://www.gstatic.com/firebasejs/7.1.0/firebase-messaging.js');
        $a['html']['js'] .= add_external_js(base_url() . 'fcm.2020.js');
        */
      
        $a['html']['topjs'] = async_m_add_js('/js/prebid4.6.0-criteo.js'.$a['upVersion']);

        $a['template']['header'] = $this->load->view('mobile2020/template/vheader', NULL, TRUE);
        $a['template']['menuoverlay'] = $this->load->view('mobile2020/template/vmenuoverlay', NULL, TRUE);
        $a['template']['newcss'] = $this->load->view('mobile2020/template/vnewcss',NULL,TRUE);
        $a['template']['subkanal'] = $this->load->view('mobile2020/template/vsubkanalmenu', NULL, TRUE);
        $a['template']['footer'] = $this->load->view('mobile2020/template/vfooter', NULL, TRUE);

        $t['trending'] = $this->mgonews->getKanalTrendingTopic($parent_id_new, 7);
        $a['template']['trending'] = $this->load->view('mobile2020/template/vtrending', $t, TRUE);

        // $t['headine'] = $this->mgonews->getListTitlePopular($parent_id, 5);
        // $a['template']['headine'] = $this->load->view('mobile2020/template/vheadline', $t, TRUE);

        // $t['pushbox'] = $this->load->view('mobile/template/vpushbox',NULL,TRUE);

        $getarticle = $this->article->getArticle();

        
        $t['details'] = $getarticle['details'];
        $content_id = $getarticle['details']['id_news'];
        $a['content']['content_id'] = $content_id;

        $uriread = $this->article->createPathUrl($t['details']);
        $t['site_url'] = site_url('berita') . '/' . $uriread;
        $t['slug_title'] = $t['details']['url_title'];
        $a['content']['site_url'] = site_url('berita') . '/' . $uriread;
        $a['content']['site_title'] = cleanTitleArticle($t['details']['title']);
        $a['content']['site_publish'] = date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']));
        $a['content']['site_reporter'] = $t['details']['author'];
        $a['content']['site_editor'] = $t['details']['editor_name'];
        $a['content']['site_summary'] = seo_description($t['details']['summary']);
        $a['content']['id_subkanal'] = $t['details']['id_subkanal'];
        $a['content']['subkanal'] = $t['details']['subkanal'];
        $a['content']['amp_url'] = site_url('beritaamp') . '/' . $uriread;
        $a['content']['forum_url'] = site_url('mobile-forum') . '/' . $uriread;
        $a['content']['disqus_url'] = site_url('mdisqus') . '/' . $uriread;
        $a['content']['path_url'] = '/berita/' . $uriread;
        $a['content']['related'] = "";
        $a['content']['id_content'] = $t['details']['id_news'];
        $a['content']['date_publish'] = date('Ymd',strtotime($t['details']['publish']));
        $a['content']['time_publish'] = date('H:i',strtotime($t['details']['publish']));
        $a['content']['site_label'] = textlimit($t['details']['title'], 40);
        $topic_id_list = '';
        $topic_keyword = array();
        $getRelated = array();
        $getTopicData = array();

        $getTopic = $this->mgonews->getTopicGensindo($content_id);
        if(count($getTopic) > 0){
            $topic_id_list = $getTopic[0]['id_topic'];
            if($topic_id_list != ''){
                $getRelated = $this->mgonews->getRelatedGensindo($topic_id_list,$parent_id,9);
                
                $topicData = explode(',',$topic_id_list);
                for($i = 0; $i < count($topicData); $i++){
                    $getTopicData[$i] = $this->mgonews->getTopicListGensindo($topicData[$i]);
                    $topic_keyword[$i] = $getTopicData[$i][0]['topic'];
                }
            }
        }

        $t['topiklist'] = $topic_id_list;
        $t['related'] = $getRelated;
        $a['content']['topic_list'] = $topic_id_list;
        $a['content']['relatedtopic'] = $this->load->view('mobile2020/berita/vrelatedtopic',$t,TRUE);
        $a['content']['relatedcontent'] = $this->load->view('mobile2020/berita/vrelatedcontent',$t,TRUE);

        /* --- start article pagination --- */
        $listbaca = $this->mgonews->getListNews($parent_id_new,12);
        $baca = randomData($listbaca,12,3);

        $totalData = $this->article->getArticleNumListPageBaca($t['details'],$baca);

        $start = $this->uri->segment(5,0);
        $defaultpage = 100;
        if(isset($_GET['showpage']) && $_GET['showpage'] == 'all'){
            $offset = 100;
        }else{
            if($totalData > ($defaultpage + 3)){
                $offset = $defaultpage;
            }else{
                $offset = 100;
            }
        }

        $t['content'] = $this->article->getArticleListPageBaca($start,$offset,$t['details'],$baca,array());
        $t['totalData'] = $totalData;

        $t['per_page'] = $offset;
        $t['start'] = $start;


        if($t['totalData'] <= $offset && $start > 0){
            redirect($t['site_url'],'location',301);
        }

        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('artpaging');
        $config['base_url'] = site_url('berita/' . $uriread);
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        $currentPage = floor(($start / $config['per_page']) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;
        if($start == 0 || $start == ''){
            $pagetitle = '';
        }
        $t['current_page'] = $currentPage;
        $t['last_page'] = floor(($t['totalData'] / $config['per_page']) + 1);

        $paging_status = 0;
        if($t['totalData'] > ($defaultpage + 3)){
            $paging_status = 1;            
        }
        
        $canonical = add_canonical('canonical',$t['site_url']);
        if($paging_status){
            $canonical = add_canonical('canonical',$t['site_url'] . '?showpage=all');
        }
        
        $a['html']['canonical'] = $canonical;
        /* --- end article pagination --- */

        /* ---- start SEO configuration ---- */

        if(isset($t['details']['images'])){
            $imgmeta = $this->config->item('images_uri') . $this->config->item('dyn_620') . '/pena/news/' . date('Y/m/d',strtotime($t['details']['date_created'])) . '/' . $t['details']['id_subkanal'] . '/' . $t['details']['id_content'] . '/' . $t['details']['photo'];
        }else{
            $imgmeta = '';
        }
        $a['content']['site_image'] = $imgmeta;
        
        if($t['details']['author'] != ''){
            $reporter = ucwords($t['details']['author']);
        }else{
            $reporter = '';
        }
        
        /* create keyword tags */
        $get_topic_keyword = implode(',',$topic_keyword);
        $keyword = $this->article->newsKeywords($get_topic_keyword);
        $t['keyword'] = $keyword;
        $rich_keywords = explode(',',$keyword);
        $a['content']['keyword'] = $rich_keywords;
        $a['content']['site_tags'] = $keyword;

        $extend_desc = summaryMore($t['details']['content']);
        if(strlen($extend_desc) < 120){
            $article_summary = $t['details']['summary'] . ' ' . $extend_desc;
        }else{
            $article_summary = $t['details']['summary'];
        }
        /* title & description filtering */
        //$metatitle = seo_title($t['details']['title']);
        $metatitle = cleanTitleArticle($t['details']['title']);
        $metadesc = seo_description($article_summary);
        
        $a['html']['title'] = $metatitle . $pagetitle;
        $metaname = array(
            'description' => $metadesc . $pagetitle,
            'title' => $metatitle . $pagetitle,
            'image_src' => $imgmeta,
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );
        $metafb = array(	
            'title' => cleanTitleArticle($t['details']['title']),	
            'type' => 'article',	
            'url' => $t['site_url'],	
            'image' => $imgmeta,	
            'image:type' => 'image/jpeg',	
            'image:width' => 620,	
            'image:height' => 413,	
            'description' => cleanSummary($metaname['description']),	
            'site_name' => 'SINDOnews.com'	
        );
        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@SINDOnews',
            'twitter:creator' => '@SINDOnews',
            'twitter:title' => cleanWords($t['details']['title']),
            'twitter:description' => cleanWords($t['details']['summary']),
            'twitter:image' => $imgmeta
        );
        $metadable = array(
            'item_id' => $content_id . $t['details']['id_kanal'],
            'title' => cleanWords($t['details']['title']),
            'image' => $imgmeta,
            'author' => $reporter
        );

        if(count($getTopicData) > 1){
            $section_a = trim($topic_keyword[0]);
            $section_b = trim($topic_keyword[1]);

            $metaArticle = array(
                'section' => 'milenial',
                'section2' => $section_a,
                'section3' => $section_b,
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }else{
            $metaArticle = array(
                'section' => 'milenial',
                'published_time' => date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']))
            );
        }


        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);
        $a['html']['metaname'] .= metaDable($metadable);
        $a['html']['metaname'] .= metaArticle($metaArticle);

        /* ---- end SEO configuration ---- */
        
        /* GA Custom Dimension Start */
        $a['content']['cd_title'] = cleanTitleArticle($t['details']['title']) . $pagetitle;
        $a['content']['cd_subkanal'] = customDimensionString($t['details']['subkanal']);
        $a['content']['cd_author'] = ucwords($a['content']['site_reporter']);
        $a['content']['cd_editor'] = ucwords($a['content']['site_editor']);
        $a['content']['cd_tags'] = customDimensionTags($a['content']['site_tags']);
        $a['content']['cd_publish'] = $t['details']['publish'];
        if($t['totalData'] > $offset){
            $cd_page = $currentPage;
        }else{
            $cd_page = 0;
        }
        $a['content']['cd_page'] = $cd_page;
        /* GA Custom Dimension End */

        $t['others'] = $this->mgonews->getListNews($parent_id,10);

        $t['trending'] = $this->mgonews->getKanalTrendingTopic($this->config->item('gensindo_id'),7);
        $a['template']['trending'] = $this->load->view('mobile2020/template/vtrending',$t,TRUE);
        $a['template']['sosialmedia'] = $this->load->view('mobile2020/template/vsosialmedia', NULL, TRUE);
        // $t['event']['results'] = array();
        // $a['template']['agenda'] = $this->load->view('mobile/template/vagenda',$t,TRUE);
        
        $t['trendingpopular'] = $this->mgonews->getListTitlePopular($this->config->item('gensindo_id'),5);
        
        $a['template']['popular'] = $this->load->view('mobile2020/template/vpopular',$t,TRUE);
    
        $t['sindonews_terkini'] = $this->mgonews->getListNews($parent_id_new,10);
        $a['content']['sindonews_terkini'] = $this->load->view('mobile2020/berita/vsindonewsterkini',$t,true);

        // $a['content']['others'] = $this->load->view('mobile/berita/vothers',$t,TRUE);

        $a['content']['breadcrumb'] = $this->load->view('mobile2020/berita/vbreadcrumb',$t,TRUE);
        $a['content']['detail'] = $this->load->view('mobile2020/berita/vdetail',$t,TRUE);
        
        $a['content']['profileeditor'] = $this->load->view('mobile/berita/vprofileeditor',$t,TRUE);
        $a['content']['comment'] = $this->load->view('mobile2020/read/vdisqus',$t,TRUE);

        $this->load->view('mobile2020/berita/vread',$a,FALSE);

    }

}
