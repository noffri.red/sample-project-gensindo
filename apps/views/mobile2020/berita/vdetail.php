<?php
if(count($details) > 0){
    $title = cleanTitleArticle($details['title']);
    $datepublish = parseDateTime((string) $details['date_published']);
    $date_published = $datepublish['day_ind_name'] . ", " . $datepublish['day'] . " " . $datepublish['month_ind_name'] . " " . $datepublish['year'];
    $date_published .= ' - ' . $datepublish['hour'] . ":" . $datepublish['minute'] . " WIB";
    $lokasi = strtoupper($details['lokasi']);
    $content = $content;
    $kode_user = strtolower($details['kode_user']);
    $author = ucwords($details['author']);

    $img = "";
    $caption_images = "";
    if($start == 0 || $start == ''){
        if($details['photo'] != ''){
            $img = images_uri() . '/dyn/360/gensindo/content/' . date('Y/m/d',strtotime($details['date_created'])) . '/' . $details['id_subkanal'] . '/' . $details['id_news'] . '/' . $details['photo'];
            if($details['caption_photo'] != ''){
                $caption_images = cleanWords($details['caption_photo']);
            }else{
                $caption_images = cleanWords($details['title']);
            }
           
        }

        $adspages = '';
    }else{
        echo '
       
        <div class="ads300 mt20 mb20 adsload">
            <div id="div-gpt-ad-middle_1"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_1\');});</script></div>
        </div>
        ';
    
    $adspages = '
     
        <div class="ads300 mt20 mb20 adsload">
            <div id="div-gpt-ad-middle_2"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_2\');});</script></div>
        </div>';

    }


    echo '<h1 class="title-news">'.$title.'</h1>
          <div class="author-news">'.$author.'</a></div>
          <time class="time-news"><?php echo $date_published; ?></time>';
    
    
    if($img !="") echo '<div class="primary-image-news"><a data-fancybox="gallery" href="'.$img.'"><img src="'.$img.'" alt="'. cleanHTML($title) .'" width="360"></a></div>' ;
    
    if($caption_images !="") echo '<div class="image-caption">"'.$caption_images.'"</div>';
    
   
    echo '<div class="paragraph">'; 
        
    if(!empty($lokasi)){
        if($start == 0 || $start == ''){
            echo '<span>' . $lokasi . '</span> - ';
        }
    }
        
    echo $content;
    
    echo '<div class="page-status"></div>';


     $lasturl = (($per_page * $last_page) - $per_page);
     if($totalData > $per_page){
         if($start == $lasturl){
             echo '<br><br><span class="reporter">(' . $kode_user . ')</span>';
         }
     }else{
             echo '<br><br><span class="reporter">(' . $kode_user . ')</span>';
     }

     echo '</div>';
 
     if($totalData > $per_page){
         echo $adspages;
         
         echo '<div class="note-paging">halaman ke-' . $current_page . '</div>';
         echo $pagination;
 
         if(!isset($_GET['showpage'])){
             echo '<div class="content-show"><a href="' . $site_url . '?showpage=all">show all</a></div>';
         }
     }
    
 

 } 