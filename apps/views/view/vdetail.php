<?php
    $title = cleanHTML(cleanWords($details['title']));
    $author = $details['author'];
    $link_author = site_url('reporter') .'/'. $details['slug_author'];
    $img = $this->config->item('images_uri') . $this->config->item('dyn_620').'/gensindo/content/' . $details['year'] . '/' . $details['month'] . '/' . $details['day'] . '/' . $details['id_imaji'] . '/' . $details['images'];
    $datepublish = parseDateTime((string) $details['date_published']);
    $date_published = $datepublish['day_ind_name'] . ", " . $datepublish['day'] . " " . $datepublish['month_ind_name'] . " " . $datepublish['year'];
    $date_published .= ' - ' . $datepublish['hour'] . ":" . $datepublish['minute'] . " WIB";
    $content = $content;
    $kode_user = $details['kode_user'];

    echo '<div class="title-news">'. $title .'</div>';
    echo '<div class="author"><a href="'. $link_author .'">'. $author .'</a></div>';
    echo '<time>'. $date_published .'</time>';

    if($start == 0 || $start == '') {
        echo '<img src="'. $img .'" class="image-news" alt="'. $title .'">';
    }

    echo '<div class="share">';
        echo '<div class="sharethis-reaction"><div class="sharethis-inline-reaction-buttons"></div></div>';
        echo '<div class="sharethis-box"><div class="sharethis-inline-share-buttons"></div></div>';
    echo '</div>';

    echo '<div class="caption">';

        echo $content;

        echo '<br><br><br><br>';
        echo '(<span class="reporter">'. $kode_user .'</span>)';

        if($totalData > $per_page){
            echo '<div class="note-paging">halaman ke-' . $current_page . ' dari ' . $last_page . '</div>';
            echo $pagination;
        }

    echo '</div>';
