<?php

class Mdbtopic extends CI_Model {

    function getTopicData($id_topic){
        $qString = "SELECT id_topic, topic, slug_topic FROM td_topic";
        $qCondition = " WHERE id_topic IN(". $id_topic .") AND status = '0'";
        $qOrderBy = " ORDER BY topic ASC";

        $qString .= $qCondition.$qOrderBy;
        $response_data = $this->db->query($qString);
        $data['results'] = $response_data->result_array();

        $this->db->close();
        return $data;
    }

    function getTopicContent($id_topic, $howmuch = 10, $start = 0, $total_results = FALSE){
        $qString = "SELECT 
                        a.id_content, a.id_subkanal,
                        a.title, a.summary, a.slug_title, a.images, a.date_published, a.date_created,
                        DATE_FORMAT(a.date_created, '%Y') AS year, 
                        DATE_FORMAT(a.date_created, '%m') AS month,  
                        DATE_FORMAT(a.date_created, '%d') AS day,  
                        b.kanal,
                        c.subkanal, c.slug_subkanal
                    FROM td_content AS a";
        $qJoin = " LEFT JOIN td_kanal AS b ON b.id_kanal = a.id_kanal
                      LEFT JOIN td_subkanal AS c ON c.id_subkanal = a.id_subkanal";
        $qCondition = " WHERE 
                            a.status = '0'
                            AND FIND_IN_SET('". $id_topic ."',a.id_topic)
							";
        $qOrderBy = " ORDER BY a.date_published DESC";
        $qLimit = " LIMIT ".$start.",".$howmuch;

        $qString .= $qJoin.$qCondition.$qOrderBy.$qLimit;
        $response_data = $this->db->query($qString);
        $data['results'] = $response_data->result_array();

        if($total_results == TRUE){
            $qStringTotal = "SELECT 
                            COUNT(*) AS total 
                          FROM td_content AS a";

            $qStringTotal .= $qJoin.$qCondition;
            $response_total = $this->db->query($qStringTotal);
            $total = $response_total->result_array();
            $data['total_results'] = $total[0]['total'];
        }

        $this->db->close();
        return $data;
    }
}
