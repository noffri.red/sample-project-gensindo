<?php

$total = count($sindonews_terkini);
if($total > 0){
    $data = $sindonews_terkini;

    echo '<div class="article-related news mt10">';
    echo '<div class="heading">BACA JUGA</div>';
    echo '<ul>';

    for($i = 0; $i < $total; $i++){
        $channel_id[$i] = $data[$i]['channel_id'];
        $site_domain[$i] = $this->config->item('read_domain');

        $dtime[$i] = strtotime($data[$i]['date_created']);
        if($channel_id[$i] == ''){
            $link[$i] = 'https://www.sindonews.com/read/' . $data[$i]['news_id'] . '/' . $data[$i]['channel_id'] . '/' . slug($data[$i]['title']) . '-' . $dtime[$i];
        }else{
            $link[$i] = 'https://' . $site_domain[$i][$channel_id[$i]] . '/read/' . $data[$i]['news_id'] . '/' . $data[$i]['channel_id'] . '/' . slug($data[$i]['title']) . '-' . $dtime[$i];
        }
        $title[$i] = cleanWords($data[$i]['title']);

        echo '
			<li>
				<div class="breaking-title">
					<a href="' . $link[$i] . '">' . $title[$i] . '</a>
				</div>
			</li>';
    }

    echo '</ul>';
    echo '</div>';
}