<?php

$total = count($latest);
if($total > 0){
    echo '
    <div class="ads300 mt20 mb20 adsload">
        <div id="div-gpt-ad-middle_1"><script>googletag.cmd.push(function(){googletag.display(\'div-gpt-ad-middle_1\');});</script></div>
    </div>';
    
    for($i = 0; $i < $total; $i++){
        $d[$i] = date('Y/m/d',strtotime($latest[$i]['date_created']));
        $id_content[$i] = $latest[$i]['id_news'];
        $img[$i] = images_uri() . '/dyn/300/pena/news/' . $d[$i] . '/' . $latest[$i]['id_subkanal'] . '/' . $latest[$i]['id_news'] . '/' . $latest[$i]['photo'];
        $subakanal[$i] = $latest[$i]['subkanal'];
        $dtime[$i] = strtotime($latest[$i]['date_created']);
        $link[$i] = site_url('read') . '/' . $id_content[$i] . '/' . $latest[$i]['id_subkanal'] . '/' . slug($latest[$i]['title']) . '-' . $dtime[$i];

        $title[$i] = cleanTitleArticle($latest[$i]['title']);
        $summary[$i] = cleanWords($latest[$i]['summary']);
        $ago[$i] = time_difference($latest[$i]['date_published']);

        echo '<li>
                <div class="img image"><a href="'. $link[$i] .'"><img class="lazyload" data-src="'.$img[$i].'" alt="'. cleanHTML($title[$i]).'" width="300"></a></div>
                <div class="block-caption">
                    <div class="title"><a href="'.$link[$i].'">'.$title[$i].'</a></div>
                    <div class="category gen-news"><a href="#">'.ucwords($subakanal[$i]).'</a></div>
                    <div class="time">'.$ago[$i].'</div>
                </div>
            </li>';
    }



    if($totalData > $per_page){
        echo '<div class="news-link-more">';
        echo $pagination;
        echo '</div>';
    }
} 