<div id="footer-top">
    <div class="content-left">
        <div class="logo"><img src="<?php echo template_uri(); ?>/image/logo-sindonews-white.webp" alt=""></div>
        <!-- Social Media -->
        <div id="social-media">
            <ul>
                <li><a target="_blank" href="https://www.facebook.com/sindonews"><img src="<?php echo template_uri(); ?>/image/icon-facebook.png" alt="Gensindo" width="65" height="45"></a></li>
                <li><a target="_blank" href="https://twitter.com/sindonews"><img src="<?php echo template_uri(); ?>/image/icon-twitter.png" alt="Gensindo" width="65" height="45"></a></li>
                <li><a target="_blank" href="https://www.youtube.com/channel/UCCrfYLen-jXQAAJRo4oRbZQ"><imag src="<?php echo template_uri(); ?>/image/icon-youtube.png" alt="Gensindo" width="65" height="45"></a></li>
                <li><a target="_blank" href="https://www.instagram.com/sindonews/"><img src="<?php echo template_uri(); ?>/image/icon-instagram.png" alt="Gensindo" width="65" height="45"></a></li>
            </ul>
        </div>
        <div class="download-apps">
            <ul>
                <li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.sindonews.android"><img src="<?php echo template_uri(); ?>/image/google-play.png" alt="Gensindo" width="150" height="50"></a></li>
                <li><a target="_blank" href="https://apps.apple.com/us/app/sindonews/id1509383487"><img src="<?php echo template_uri(); ?>/image/play-store.png" class="app-store" alt="Gensindo" width="150" height="50"></a></li>
            </ul>
        </div>
        <div class="mnc"><a target="_blank" href="https://mnc.co.id/id" class="mnc-logo"><i class="sprite mnc-media"></i></a></div>
    </div>
    <div class="content-right">
        <div class="sitemap-top">
            <div class="list sitemap">
                <div class="head">Channel</div>
                <ul>
                    <li><a  href="<?php echo base_url()?>">Home</a></li>
                    <li><a  href="https://nasional.sindonews.com/">Nasional</a></li>
                    <li><a  href="https://metro.sindonews.com">Metro</a></li>
                    <li><a  href="https://daerah.sindonews.com">Daerah</a></li>
                    <li><a  href="https://ekbis.sindonews.com">Ekbis</a></li>
                    <li><a  href="https://international.sindonews.com">International</a></li>
                    <li><a  href="https://sports.sindonews.com">Sports</a></li>
                    <li><a  href="https://autotekno.sindonews.com">Autotekno</a></li>
                    <li><a  href="https://lifestyle.sindonews.com">Lifestyle</a></li>
                    <li><a  href="https://edukasi.sindonews.com">Edukasi</a></li>
                    <li><a  href="https://kalam.sindonews.com">Kalam</a></li>
                    <li><a  href="https://gensindo.sindonews.com">Gen Sindo</a></li>
                    <li><a  href="https://photo.sindonews.com/">Photo</a></li>
                    <li><a  href="https://video.sindonews.com/">Video</a></li>
                    <li><a  href="https://infografis.sindonews.com/">Infografis</a></li>
                </ul>
            </div>
            <div class="list group">
                <div class="head">MNC Media</div>
                <ul>
                    <li><a target="_blank" href="https://www.rcti.tv/">RCTI</a></li>
                    <li><a target="_blank" href="https://www.gtv.id/">GTV</a></li>
                    <li><a target="_blank" href="https://www.mnctv.com/">MNC TV</a></li>
                    <li><a target="_blank" href="https://www.inews.id/">iNEWS</a></li>
                    <li><a href="https://index.sindonews.com/blog/119/koran-sindo">Koran Sindo</a></li>
                    <li><a target="_blank" href="http://www.mnctrijaya.com/">MNC Trijaya FM</a></li>
                    <li><a target="_blank" href="https://www.okezone.com/">Okezone</a></li>
                    <li><a target="_blank" href="https://www.rctiplus.com/">RCTI+</a></li>
                    <li><a target="_blank" href="https://www.visionplus.id/">Vision +</a></li>
                    <li><a target="_blank" href="http://www.roov.id/">ROOV</a></li>
                </ul>
            </div>
        </div>
        <div class="list about">
            <div class="head">Management</div>
            <ul>
                <li><a href="https://index.sindonews.com/about/#redaksi">Tentang Kami</a></li>
                <li><a href="https://index.sindonews.com/about/#tentang-kami">Redaksi</a></li>
                <li><a href="https://index.sindonews.com/about/#kode-etik">Kode Etik</a></li>
                <li><a href="https://index.sindonews.com/about/#disclaimer">Disclaimer</a></li>
                <li><a href="https://index.sindonews.com/about/privacy-policy">Privacy Policy</a></li>
                <li><a href="https://index.sindonews.com/about/term-of-service">Term Of Service</a></li>
                <li><a href="https://index.sindonews.com/about/#kontak-kami">Kontak Kami</a></li>
            </ul>
        </div>
    </div>
</div>
<div id="footer-bottom">
    <div class="copyright" class="left">Copyright &copy; <?php echo date('Y'); ?> SINDOnews.com, All Rights Reserved</div>
    <div class="render-time">/<?php $this->uri->segment(1)?> rendering {elapsed_time}s (<?php echo infoServer(); ?>)</div>
</div>
<div class="to-top"><i class="fas fa-arrow-up"></i></div>
<!-- <div id="copyright" class="left">&copy; 2020 Arif Lutfhansah</div> -->
<?php
$key = 's1nd0n3ws.c0m-MNCm3d14';
$token = md5($key);
$token_encode = base64_encode($token);
echo '<div class="wpinfo" id="pushinfo" data-url="' . site_url('blackclouds') . '?code=sindoapi&v=11.0&source=cloud' . rand(1,99) . '&z=' . time() . '" data-reg="' . site_url('nature') . '?code=sindoapi&v=11.0&source=cloud' . rand(1,99) . '&z=' . time() . '" data-group="gensindo" data-key="' . $token_encode . '" data-source="' . base_url() . '"></div>';

echo '
	<div class="sintrack">
        <div class="sindoframe"></div>
        <div class="sindomp"></div>
        <div class="sindoev"></div>
        <div class="sindopix"></div>
        <div class="axtrack"></div>
        <div class="cxtrack"></div>
        <div class="axsindo"></div>
        <div class="cxrectrack"></div>
        <div class="axrecsindo"></div>
        <div class="adtrack"></div>
        <div class="adbounce"></div>
        <div class="ock"></div>
        <div class="ick"></div>
        <div id="artsindo"></div>
        <div id="artcode"></div>
    </div>
';
?>
