<?php

$total = count($related);
if($total > 0){

    echo '<div class="link-article">';
    echo '<div class="head">Artikel Terkait</div>';
    echo '<ul class="clearfix">';
    for($i = 0; $i < $total; $i++){
        $site_domain[$i] = $this->config->item('read_domain');
        $dtime[$i] = strtotime($related[$i]['date_created']);
        $l[$i] = 'https://' . $site_domain[$i][$related[$i]['id_subkanal']] . '/read/' . $related[$i]['id_news'] . '/' . $related[$i]['id_subkanal'] . '/' . slug($related[$i]['title']) . '-' . $dtime[$i];
        $title[$i] = cleanWords($related[$i]['title']);
        $img[$i] = images_uri() . '/dyn/360/pena/news/' . date('Y/m/d',strtotime($related[$i]['date_created'])) . '/' . $related[$i]['id_subkanal'] . '/' . $related[$i]['id_news'] . '/' . $related[$i]['photo'];
        $subkanal[$i] = $related[$i]['subkanal'];

        echo '
            <li>
                <a href="' . $l[$i] . '" class="image"><img class="lazyload" data-src="' . $img[$i] . '" alt="' . $title[$i] . '"></a>
                <div class="title"><a href="' . $l[$i] . '">' . $title[$i] . '</a></div>
            </li>';
    }
    echo '</ul>';
    echo '</div>';
}
