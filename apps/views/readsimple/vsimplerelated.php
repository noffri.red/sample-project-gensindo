<?php

$total = count($topic);
if($total > 0){
    echo '<div class="topic">';
    echo '<div class="topic-heading">Topik Terkait</div>';
    echo '<ul>';
    for($i = 0; $i < $total; $i++){
        $urltopic[$i] = site_url('tag') . '/' . $topic[$i]['id_topic'] . '/' . $topic[$i]['slug_topic'];
        $filterone[$i] = preg_replace('/\(.*\)/','',strtolower($topic[$i]['topic']));
        $pattern = array('\'','"',',','.','pt','ltd');
        $filtertwo[$i] = str_replace($pattern,'',$filterone[$i]);
        $topic_title[$i] = trim($filtertwo[$i]);
        
        echo '<li><a href="' . $urltopic[$i] . '">' . $topic_title[$i] . '</a></li>';
    }
    echo '</ul>';
    echo '</div>';

    if($topic_id_list != ''){
        echo '<div class="related">';
        echo '<div class="related-heading">Berita Terkait</div>';
        echo '<ul>';
        $total_related = count($related['results']);
        if($total_related > 0){
            $related = $related['results'];
            for($a = 0; $a < $total_related; $a++){
                $dtime[$a] = strtotime($related[$a]['date_created']);
                $l[$a] = site_url('read') . '/' . $related[$a]['id_content'] . '/' . $related[$a]['id_subkanal'] . '/' . $related[$a]['url_title'] . '-' . $dtime[$a];
                $title[$a] = cleanWords($related[$a]['title']);
                
                echo '<li><a href="' . $l[$a] . '">' . $title[$a] . '</a></li>';
            }
        }
        echo '</ul>';
        echo '</div>';
    }

}