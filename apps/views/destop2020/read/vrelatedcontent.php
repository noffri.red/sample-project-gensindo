<?php if (count($related) > 0) {
        echo '<div class="head">
        <div class="title">Berita Terkait</div>
      </div>
      <ul>';

        foreach ($related as $value) {
            // xDebug($value);
            $site_domain = $this->config->item('read_domain');
            $dtime = date('Y/m/d',strtotime($value['date_created']));
            $l = 'https://' . $site_domain[$value['id_subkanal']] . '/read/' . $value['id_news'] . '/' . $value['id_subkanal'] . '/' . slug($value['title']) . '-' . $dtime;
            $title = cleanWords($value['title']);
            $subkanal = $value['subkanal'];
            $ago = time_difference($value['date_published']);

            $img = '';
            if ($value['photo'] != '') $img = images_uri() . '/dyn/300/pena/news/' . date('Y/m/d', strtotime($value['date_created'])) . '/' . $value['id_subkanal'] . '/' . $value['id_news'] . '/' . $value['photo'];

    ?>

           <li>
               <div class="image"><a href="<?php echo $l; ?>"><img class="lazyload" data-src="<?php echo $img ?>" alt="<?php echo cleanHTML($title) ?>" width="300"></a></div>
               <div class="block-caption">
                   <div class="title"><a href="<?php echo $l; ?>"><?php echo cleanHTML($title) ?></a></div>
                   <!-- <div class="category gen-news"><a href="#"><?php// echo ucwords($subkanal) ?></a></div> -->
                   <!-- <div class="time"><?php //echo $ago ?></div> -->
               </div>
           </li>
           </ul>

   <?php  }
        echo '</ul>';
    } ?>