<?php
$total = count($breaking['results']);
if($total > 0){
    $data = $breaking['results'];

    echo '<div id="tab1" class="tabs_item">';

        $i = 0;
        $id_content[$i] = $data[$i]['id_news'];
		$img[$i] = images_uri().$this->config->item('dyn_620').'/pena/news/'.date('Y/m/d/', strtotime($data[$i]['date_created'])).'/'.$data[$i]['id_subkanal'].'/'.$id_content[$i].'/'.$data[$i]['photo'];
        $subakanal[$i] = $data[$i]['subkanal'];
        $dtime[$i] = strtotime($data[$i]['date_created']);
        $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . strtolower($data[$i]['url_title']) . '-' . $dtime[$i]);

        $title[$i] = cleanWords($data[$i]['title']);
        $summary[$i] = cleanWords($data[$i]['summary']);
        $ago[$i] = time_difference($data[$i]['date_published']);

        echo '<div class="article-news">
                                <div class="category-links samsung"><a href="'. site_url('samsung') .'" title="Samsung">Samsung</a></div>
                                <div class="inner">
                                    <div class="title"><a href="'. $link[$i] .'">'. $title[$i] .'</a></div>
                                    <time>'. $ago[$i] .'</time>
                                    <div class="caption">'. $summary[$i] .'</div>
                                    <div class="img"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></div>
                                </div>
                            </div>';

        echo '<section class="article">';

            for($i=1; $i<$total; $i++){
                $id_content[$i] = $data[$i]['id_news'];
				$img[$i] = images_uri().$this->config->item('dyn_620').'/pena/news/'.date('Y/m/d', strtotime($data[$i]['date_created'])).'/'.$data[$i]['id_subkanal'].'/'.$id_content[$i].'/'.$data[$i]['photo'];
                $subakanal[$i] = $data[$i]['subkanal'];
                $dtime[$i] = strtotime($data[$i]['date_created']);
                $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['id_subkanal'] . '/' . strtolower($data[$i]['url_title']) . '-' . $dtime[$i]);
                $title[$i] = cleanWords($data[$i]['title']);
                $summary[$i] = cleanWords($data[$i]['summary']);
                $ago[$i] = time_difference($data[$i]['date_published']);

                echo '<div class="article-news">
                                <div class="category-links samsung"><a href="'. site_url('samsung') .'" title="Samsung">Samsung</a></div>
                                <div class="inner">
                                    <div class="title"><a href="'. $link[$i] .'" title="'. $title[$i] .'">'. $title[$i] .'</a></div>
                                    <time>'. $ago[$i] .'</time>
                                    <div class="caption">'. $summary[$i] .'</div>
                                    <div class="img"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></div>
                                </div>
                            </div>';
            }

        echo '</section>';

        // echo '<a href="'. base_url() .'loadmore/'. $data[0]['id_subkanal'] .'" class="btn-more">View More</a>';
    echo '</div>';
}