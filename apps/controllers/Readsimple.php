<?php

class Readsimple extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->config('site_channel');
        $this->load->config('site_domain');

        $this->load->library('goarticle');

        $this->load->model('news/mgonews');
    }

    function index(){
        $a = array();
        
        $current_url = parse_url(current_url());
        if(isset($current_url['host']) && isset($current_url['path'])){
            $new_url = str_replace('readsimple','read',$current_url['path']);
            $urlgo = site_url($new_url);
            redirect($urlgo,'location',301);
        } else {
            redirect(base_url(),'location',301);
        }
        
        $a['html']['bottom_js'] = ua_alexa_coms($this->config->item('google_id'));
		
        $a['html']['js'] = add_external_js('https://sm.sindonews.net/mobile/2016/js/jquery-1.11.1.min.js');
        $a['html']['js'] .= add_external_js('https://sm.sindonews.net/mobile/2016/js/blitzkrieg.min.js');
        $a['html']['js'] .= add_external_js('https://sm.sindonews.net/mobile/2016/js/azwethinkweiz.min.js'); 
        
        $a['html']['preload'] = add_preload_js_external('https://sm.sindonews.net/mobile/2016/js/blitzkrieg.min.js');
        $a['html']['preload'] .= add_preload_js_external('https://sm.sindonews.net/mobile/2016/js/azwethinkweiz.min.js');

        $a['html']['prefetch'] = add_preload_js_external('https://sm.sindonews.net/mobile/2016/js/blitzkrieg.min.js');
        $a['html']['prefetch'] .= add_preload_js_external('https://sm.sindonews.net/mobile/2016/js/azwethinkweiz.min.js');

        $a['content']['footer'] = $this->load->view('readsimple/vsimplefooter',NULL,TRUE);

        /* get article data */
        $getarticle = $this->goarticle->getNewArticle();
        $t['details'] = $getarticle['details'];
        $content_id = $getarticle['content_id'];
        $t['content_id'] = $content_id;

        $uriread = $t['details']['id_news'] . '/' . $t['details']['id_subkanal'] . '/' . $t['details']['url_title'] . '-' . strtotime($t['details']['date_created']);
        $t['site_url'] = site_url('read') . '/' . $uriread;
        $a['content']['site_url'] = site_url('read') . '/' . $uriread;

        /* article pagination */
        $start = $this->uri->segment(5,0);
        $offset = 5;

        $t['content'] = $this->goarticle->getArticleListPage($start,$offset,$t['details']);
        $t['totalData'] = $this->goarticle->getArticleNumListPage($t['details']);

        $t['per_page'] = $offset;
        $t['start'] = $start;

        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('artpaging');
        $config['base_url'] = site_url('read/' . $uriread);
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        $currentPage = floor(($start / $config['per_page']) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;
        if($start == 0 || $start == ''){
            $pagetitle = '';
        }
        $t['current_page'] = $currentPage;
        $t['last_page'] = floor(($t['totalData'] / $config['per_page']) + 1);

        /* checking image availability */
        if(isset($t['details']['photo'])){
            $imgmeta = $this->config->item('images_uri') . '/dyn/620/pena/news/' . date('Y/m/d',strtotime($t['details']['date_created'])) . '/' . $t['details']['id_subkanal'] . '/' . $t['details']['id_news'] . '/' . $t['details']['photo'];
        }else{
            $imgmeta = '';
        }
        $a['content']['site_image'] = $imgmeta;

        /* title & description filtering */
        $metatitle = seo_title($t['details']['title']);
        $metadesc = seo_description($t['details']['summary']);
        $a['content']['site_title'] = seo_title($t['details']['title']);

        /* create html meta tags */
        $a['html']['title'] = $metatitle . $pagetitle;
        $metaname = array(
            'description' => $metadesc . $pagetitle,
            'title' => $metatitle . $pagetitle,
            'image_src' => $imgmeta,
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        /* set all SEO meta tags */
        $a['html']['metaname'] = metaname($metaname);
        
        $a['content']['details'] = $this->load->view('readsimple/vsimpledetail',$t,true);
        $a['content']['res'] = $this->setFrame();
        $a['content']['ua'] = $this->setUA();

        /* Hits Token */
        $key = 's1nd0n3ws.c0m-MNCm3d14';
        $token = md5($key);
        $token_encode = base64_encode($token);
        $pixel = 'r=' . randomString(11) . '&z=' . $token_encode . '&c=' . $t['details']['id_news'] . '&ch=' . $t['details']['id_subkanal'];
        $a['content']['pixel'] = site_url('xyz') . '?' . $pixel;

        $this->load->view('pages/vreadsimple',$a,false);
    }

    function setFrame(){
        if($this->mobiledevice->device_check()){
            $res = array(
                array(
                    'w' => 1366,
                    'h' => 768
                ),array(
                    'w' => 1920,
                    'h' => 1080
                ),array(
                    'w' => 1280,
                    'h' => 800
                ),array(
                    'w' => 1440,
                    'h' => 900
                ),array(
                    'w' => 1280,
                    'h' => 1024
                ),array(
                    'w' => 1600,
                    'h' => 900
                ),array(
                    'w' => 1680,
                    'h' => 1050
                ),array(
                    'w' => 1920,
                    'h' => 1200
            ));
        }else{
            $res = array(
                array(
                    'w' => 360,
                    'h' => 640
                ),array(
                    'w' => 360,
                    'h' => 640
                ),array(
                    'w' => 320,
                    'h' => 570
                ),array(
                    'w' => 720,
                    'h' => 1280
                ),array(
                    'w' => 480,
                    'h' => 800
                ),array(
                    'w' => 480,
                    'h' => 854
                ),array(
                    'w' => 540,
                    'h' => 960
                ),array(
                    'w' => 600,
                    'h' => 1024
            ));
        }

        $r = rand(0,7);
        $w = $res[$r]['w'];
        $h = $res[$r]['h'];

        $out = array(
            'width' => $w,
            'height' => $h
        );

        return $out;
    }

    function setUA(){
        if($this->mobiledevice->device_check()){
            $ua = array(
                'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:67.0) Gecko/20100101 Firefox/67.0',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0) Gecko/20100101 Firefox/63.0',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0',
                'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0) Gecko/20100101 Firefox/68.0',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Safari/605.1.15',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Safari/605.1.15',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.1 Safari/605.1.15',
                'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8',
                'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36'
            );
        }else{
            $ua = array(
                'Mozilla/5.0 (Linux; Android 9; SM-G965F Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 9; SM-G950F Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 9; SM-G960F Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 8.0.0; SM-G930F Build/R16NW; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/74.0.3729.157 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 8.0.0; FIG-LX3 Build/HUAWEIFIG-LX3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 8.0.0; WAS-LX3 Build/HUAWEIWAS-LX3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/537.36',
                'Mozilla/5.0 (Android 9; Mobile; rv:68.0) Gecko/68.0 Firefox/68.0',
                'Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.1 Mobile/15E148 Safari/604.1',
                'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148',
                'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Mobile/15E148 Safari/604.1',
                'Mozilla/5.0 (Linux; Android 8.0.0; SAMSUNG SM-G935A Build/R16NW) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 8.1.0; SAMSUNG SM-J727A Build/M1AJQ) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G973F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36',
                'Mozilla/5.0 (Linux; U; Android 8.1.0; zh-CN; EML-AL00 Build/HUAWEIEML-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.108 UCBrowser/11.9.4.974 UWS/2.13.1.48 Mobile Safari/537.36 AliApp(DingTalk/4.5.11) com.alibaba.android.rimet/10487439 Channel/227200 language/zh-CN',
                'Mozilla/5.0 (Linux; U; Android 8.1.0; zh-CN; EML-AL00 Build/HUAWEIEML-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2987.108 baidu.sogo.uc.UCBrowser/11.9.4.974 UWS/2.13.1.48 Mobile Safari/537.36 AliApp(DingTalk/4.5.11) com.alibaba.android.rimet/10487439 Channel/227200 language/zh-CN'
            );
        }

        $r = rand(0,14);
        $out = $ua[$r];

        return $out;
    }
}
