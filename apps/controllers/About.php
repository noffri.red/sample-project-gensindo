<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->helpers("gen");
        $this->load->model('topic/mdbtopic','mdbtopic');
        $this->load->model('news/mredisnews','mredisnews');
    }

    function index(){
		$urlgo = 'https://index.sindonews.com/about';
		redirect($urlgo,'location',301);exit;
		
        // if($this->mobiledevice->device_check()){
            // $this->desktop();
        // }else{
            // $this->mobile();
        // }
    }

	function desktop(){
        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['title'] = 'GEN SINDO | Berita, Opini, Generasi Muda, Milenial';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Berita Gen SINDO Terkini menyajikan informasi seputar Tren, Opini, Fashion dan Serba serbi Generasi Muda dan Milenial',
            'Slurp' => 'all',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $a['html']['metaname'] = metaname($metaname);

        $a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= add_external_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-1.10.2.min.js');
        $a['html']['js'] .= add_js('/js/materialize.min.js');
        $a['html']['js'] .= add_js('/js/jquery.jscroll.min.js');

        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['minimenu'] = $this->load->view('template/vminimenu',NULL,TRUE);
        $a['template']['promoted'] = $this->load->view('template/vpromoted',NULL,TRUE);
        $a['template']['kategori'] = $this->load->view('template/vkategori',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('template/vminifooter',NULL,TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss',NULL,TRUE);
        $a['template']['promogen'] = $this->load->view('template/vpromogen',NULL,TRUE);

        $t['event'] = $this->mredisnews->getEvent();
        $a['template']['agenda'] = $this->load->view('template/vagenda',$t,TRUE);

        $t['trending'] = $this->mredisnews->getKanalTrendingTopic();
        $a['template']['trending'] = $this->load->view('template/vtrending',$t,TRUE);

        $t['topic'] = '';
        $t['trendingtopic'] = array();
        if(!empty($t['trending']['results'])){
            $id_topic = $t['trending']['results'][0]['id_topic'];
            $t['topic'] = $t['trending']['results'][0]['topic'];
            $trendingtopic = $this->mdbtopic->getTopicContent($id_topic);
            $t['trendingtopic'] = $trendingtopic['results'];
        }

        $news = $this->mredisnews->getListBreakingNews();
        $imaji = $this->mredisnews->getListImaji();
        $t['latest'] =  array_merge($news['results'],$imaji['results']);
        array_sort_by_column($t['latest'], 'date_published', SORT_DESC);

        $popular_news = $this->mredisnews->getListTitlePopular();
        $popular_imaji = $this->mredisnews->getListTitlePopularImaji();
        $t['popular'] = array_merge($popular_news['results'],$popular_imaji['results']);
        array_sort_by_column($t['popular'], 'date_published', SORT_DESC);

        $about = $this->mredisnews->getAbout();
        $t['about'] =  $about['results'];
 
        $a['content']['about'] = $this->load->view('home/vabout',$t,TRUE);

        $this->load->view('pages/vabout',$a,FALSE);
    }

	function mobile(){
        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['title'] = 'GEN SINDO | Berita, Opini, Generasi Muda, Milenial';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Berita Gen SINDO Terkini menyajikan informasi seputar Tren, Opini, Fashion dan Serba serbi Generasi Muda dan Milenial',
            'Slurp' => 'all',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );

        $a['html']['metaname'] = metaname($metaname);

        $a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= add_external_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-1.10.2.min.js');
        $a['html']['js'] .= add_js('/js/materialize.min.js');
        $a['html']['js'] .= add_js('/js/jquery.jscroll.min.js');

        $a['template']['header'] = $this->load->view('mobile/template/vheader',NULL,TRUE);
        $a['template']['minimenu'] = $this->load->view('mobile/template/vminimenu',NULL,TRUE);
        $a['template']['promoted'] = $this->load->view('mobile/template/vpromoted',NULL,TRUE);
        $a['template']['kategori'] = $this->load->view('mobile/template/vkategori',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('mobile/template/vminifooter',NULL,TRUE);
        $a['template']['newcss'] = $this->load->view('mobile/template/vnewcss',NULL,TRUE);
        $a['template']['promogen'] = $this->load->view('mobile/template/vpromogen',NULL,TRUE);

        $t['event'] = $this->mredisnews->getEvent();
        $a['template']['agenda'] = $this->load->view('mobile/template/vagenda',$t,TRUE);

        $t['trending'] = $this->mredisnews->getKanalTrendingTopic();
        $a['template']['trending'] = $this->load->view('mobile/template/vtrending',$t,TRUE);

        $t['topic'] = '';
        $t['trendingtopic'] = array();
        if(!empty($t['trending']['results'])){
            $id_topic = $t['trending']['results'][0]['id_topic'];
            $t['topic'] = $t['trending']['results'][0]['topic'];
            $trendingtopic = $this->mdbtopic->getTopicContent($id_topic);
            $t['trendingtopic'] = $trendingtopic['results'];
        }

        $news = $this->mredisnews->getListBreakingNews();
        $imaji = $this->mredisnews->getListImaji();
        $t['latest'] =  array_merge($news['results'],$imaji['results']);
        array_sort_by_column($t['latest'], 'date_published', SORT_DESC);

        $popular_news = $this->mredisnews->getListTitlePopular();
        $popular_imaji = $this->mredisnews->getListTitlePopularImaji();
        $t['popular'] = array_merge($popular_news['results'],$popular_imaji['results']);
        array_sort_by_column($t['popular'], 'date_published', SORT_DESC);

        $about = $this->mredisnews->getAbout();
        $t['about'] =  $about['results'];
 
        $a['content']['about'] = $this->load->view('mobile/home/vabout',$t,TRUE);

        $this->load->view('mobile/pages/vabout',$a,FALSE);
    }
}
