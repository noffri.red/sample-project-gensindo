<div id="menu">
<span class="menu-circle"></span>
<a href="#" class="menu-link">
    <span class="menu-icon">
    <span class="menu-line menu-line-1"></span>
    <span class="menu-line menu-line-3"></span>
    </span>
</a>
</div>
<!-- Social Media Header -->
<div id="social-media-header">
<div class="button open-tooltip">
    <span class="dot"></span>
    <span class="dot"></span>
</div>
<div class="list">
    <a target="_blank" href="https://www.facebook.com/sindonews"><i class="fab fa-facebook-f"></i></a>
    <a target="_blank" href="https://twitter.com/sindonews"><i class="fab fa-twitter"></i></a>
    <a href="<?php echo site_url('feed'); ?>"><i class="fas fa-rss"></i></a>
    <span class="close-tooltip"></span>
</div>
</div>