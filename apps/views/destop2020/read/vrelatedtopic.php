<?php

if($topiklist != ''){
    $topic = explode(',',$topiklist);

    echo '<div class="topic-suggest">';
    echo '<ul >';
    for($i = 0; $i < count($topic); $i++){
        $getTopicData[$i] = $this->mgonews->getTopicList($topic[$i]);
        $urltopic[$i] = 'https://www.sindonews.com/topic/' . $getTopicData[$i][0]['id_topik'] . '/' . slug($getTopicData[$i][0]['topik']);
        $topic_title[$i] = strtolower($getTopicData[$i][0]['topik']);
        echo '<li><a target="_blank" href="' . $urltopic[$i] . '"><i class="fa fa-tags" aria-hidden="true"></i> ' . $getTopicData[$i][0]['topik'] . '</a></li>';
    }

    echo '</ul>';
    echo '</div>';
} 