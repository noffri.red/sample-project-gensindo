<?php

$total = count($breaking);
if($total > 0){
    $data = $breaking;

    echo '<ul>';

    $i = 0;

    $id_content[$i]= $data[$i]['content_id'];
    $img[$i] = images_uri() .'/dyn/300/pena/news/' . date('Y/m/d',strtotime($data[$i]['date_created'])) . '/' . $data[$i]['channel_id'] . '/' . $id_content[$i] . '/' . $data[$i]['image'];
    $subakanal[$i] = $data[$i]['channel_name'];
    $dtime[$i]= strtotime($data[$i]['date_created']);
    $link[$i]= site_url('read/' . $id_content[$i] . '/' . $data[$i]['channel_id'] . '/' . slug($data[$i]['title']) . '-' . $dtime[$i]);

    $title[$i] = cleanTitleArticle($data[$i]['title']);
    $summary[$i] = cleanWords($data[$i]['summary']);
    $ago[$i] = time_difference($data[$i]['date_publish']);
    $subkanal[$i] = ucwords($data[$i]['channel_name']);

    echo '<li>
            <div class="image"><a href="'.site_url('gnews').'"><img class="lazyload" src="'.$img[$i].'" alt="'.$title[$i].'" width="300"></a></div>
            <div class="block-caption">
                <div class="title"><a href="'.$link[$i].'">'.$title[$i].'</a></div>
                <div class="category gen-news"><a href="#">'. ucwords($subakanal[$i]) .'</a></div>
                <div class="time">'.$ago[$i].'</div>
            </div>
        </li>';
    
    echo '</ul>';

    echo '<ul class="scroll">';

    $j = 0 ;
    foreach ($data as $value){

        if($j >= 1){

            $id_content= $value['content_id'];
            $img = images_uri() .'/dyn/300/pena/news/' . date('Y/m/d',strtotime($value['date_created'])) . '/' . $value['channel_id'] . '/' . $id_content . '/' . $value['image'];
            $subakanal = $value['channel_name'];
            $dtime= strtotime($value['date_created']);
            $link= site_url('read/' . $id_content . '/' . $value['channel_id'] . '/' . slug($value['title']) . '-' . $dtime);
        
            $title = cleanTitleArticle($value['title']);
            $summary = cleanWords($value['summary']);
            $ago = time_difference($value['date_publish']);
            $subkanal = ucwords($value['channel_name']);
    
            
            echo '<li>
                <div class="image"><a href="'.site_url('gnews').'"><img class="lazyload" src="'.$img.'" alt="'.$title.'" width="300"></a></div>
                <div class="block-caption">
                    <div class="title"><a href="'.$link.'">'.$title.'</a></div>
                    <div class="category gen-news"><a href="'.site_url('gnews').'">'. ucwords($subakanal) .'</a></div>
                    <div class="time">'.$ago.'</div>
                </div>
            </li>';

        }
        $j++;
       
    }

    

    echo '
        <div class="news-link-more">
            <div class="news-more">
                <a href="' . site_url('gosub') . '/' . $data[0]['channel_id'] . '/10">Load More</a>
            </div>
        </div>';
    echo '</ul>';
   
}