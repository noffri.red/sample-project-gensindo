<?php if (count($trendingpopular)>0){?>
  <div id="arsip">
      <div class="head">
        <div class="title">Berita Terpopular</div>
      </div>
      <div class="list-news scroll">
        <ul>
      <?php foreach($trendingpopular as $value) {


          $id_content = $value['content_id'];
          $img  = images_uri() . '/dyn/360/pena/news/' . $value['created']['year'] . '/' . $value['created']['month'] . '/' . $value['created']['day'] . '/' . $value['channel_id'] . '/' . $id_content. '/' . $value['image'];  
          $dtime = strtotime($value['date_created']);
          $link = site_url('read/' . $value['content_id'] . '/' . $value['channel_id'] . '/' . slug($value['title']) . '-' . $dtime);
          $title = cleanWords($value['title']);
          $channel_name = ucwords($value['channel_name']);
          $ago = time_difference($value['date_publish']);
        
       ?>

          <li>
            <div class="image"><a href="<?php echo $link; ?>"><img class="lazyload" data-src="<?php echo $img; ?>" alt="<?php echo $title ?>"></a></div>
            <div class="block-caption">
              <div class="title"><a href="<?php echo $link; ?>"><?php echo $title; ?></a></div>
              <div class="category gen-news"><a href="#"><?php echo $channel_name ?></a></div>
              <div class="time"><?php echo $ago ?></div>
            </div>
          </li>
      
        <!-- end foreach -->
       <?php }?>

       </ul>
      </div>
    </div>

 <?php } ?>
  