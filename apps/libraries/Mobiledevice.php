<?php

class mobiledevice {

    function __construct(){
        $this->CI = & get_instance();
    }

    function checkread($url){
        if($this->CI->mobile_detect->isMobile() == TRUE){
            if($this->CI->mobile_detect->isTablet() == TRUE){
                redirect($url);
            }else{
                redirect($url);
            }
        }else{
            return TRUE;
        }
    }

    function device_check(){
        if($this->CI->mobile_detect->isMobile() == TRUE || $this->CI->mobile_detect->isTablet() == TRUE){
            return FALSE;
        }else{
            return TRUE;
        }
    }

    function strict_device(){
        if($this->CI->mobile_detect->isSafari() == TRUE || $this->CI->mobile_detect->isOpera() == TRUE || $this->CI->mobile_detect->isBlackBerryOS() == TRUE){
            return TRUE;
        }else{
            return FALSE;
        }
    }

}
