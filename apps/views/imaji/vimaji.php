<?php
$total = count($imaji['results']);
if($total > 0){
    $data = $imaji['results'];

    echo '<div id="tab1" class="tabs_item">';

        $i = 0;
        $id_imaji[$i] = $data[$i]['id_imaji'];
        $img[$i] = images_uri().$this->config->item('dyn_300').'/gensindo/imaji/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$id_imaji[$i].'/'.$data[$i]['images'];
        $dtime[$i] = strtotime($data[$i]['date_created']);
        $link[$i] = site_url('view/' . $id_imaji[$i] . '/' . $data[$i]['slug_title'] . '-' . $dtime[$i]);

        $title[$i] = cleanWords($data[$i]['title']);
        $summary[$i] = cleanWords($data[$i]['summary']);
        $ago[$i] = time_difference($data[$i]['date_published']);

        echo '<div class="article-news">
                                <div class="category-links imaji">Imaji</div>
                                <div class="inner">
                                    <a href="'. $link[$i] .'"><div class="title">'. $title[$i] .'</div></a>
                                    <time>'. $ago[$i] .'</time>
                                    <div class="caption">'. $summary[$i] .'</div>
                                    <div class="img"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></div>
                                </div>
                            </div>';

        echo '<section class="article">';

            for($i=1; $i<$total; $i++){
                $id_imaji[$i] = $data[$i]['id_imaji'];
                $img[$i] = images_uri().$this->config->item('dyn_300').'/gensindo/imaji/'.$data[$i]['year'].'/'.$data[$i]['month'].'/'.$data[$i]['day'].'/'.$id_imaji[$i].'/'.$data[$i]['images'];
                $dtime[$i] = strtotime($data[$i]['date_created']);
                $link[$i] = site_url('view/' . $id_imaji[$i] .'/'. $data[$i]['slug_title'] . '-' . $dtime[$i]);
                $title[$i] = cleanWords($data[$i]['title']);
                $summary[$i] = cleanWords($data[$i]['summary']);
                $ago[$i] = time_difference($data[$i]['date_published']);

                echo '<div class="article-news">
                                <div class="category-links imaji">Imaji</div>
                                <div class="inner">
                                    <a href="'. $link[$i] .'" title="'. $title[$i] .'"><div class="title">'. $title[$i] .'</div></a>
                                    <time>'. $ago[$i] .'</time>
                                    <div class="caption">'. $summary[$i] .'</div>
                                    <div class="img"><img src="'. $img[$i] .'" alt="'. $title[$i] .'"></div>
                                </div>
                            </div>';
            }

            echo '<div class="article-news">
                                <div class="category-links rcenter">Adv</div>
                                <div class="inner">
                                    <div class="title">Vivamus Eu Tincidunt Nisl, In Ultricies Odio Olio</div>
                                    <time>15 menit yang lalu</time>
                                    <div class="caption">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae venenatis lacus. Vestibulum at arcu ac sem ultrices viverra. Vivamus eu tincidunt nisl, in ultricies odio. Integer laoreet diam eu sem auctor convallis id id odio.</div>
                                    <div class="img"><img src="'. template_uri() .'/images/article_img_04.jpg" alt="Article 4"></div>
                                </div>
                            </div>';

        echo '</section>';
    echo '</div>';
}