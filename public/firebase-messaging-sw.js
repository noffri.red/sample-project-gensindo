importScripts('https://www.gstatic.com/firebasejs/7.6.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.6.1/firebase-messaging.js');

var firebaseConfig = {
    apiKey: "AIzaSyBIPnPF-qYzbO8wU8IDodvwFEwmG9eTGkw",
    authDomain: "api-project-943267167431.firebaseapp.com",
    databaseURL: "https://api-project-943267167431.firebaseio.com",
    projectId: "api-project-943267167431",
    storageBucket: "api-project-943267167431.appspot.com",
    messagingSenderId: "943267167431",
    appId: "1:943267167431:android:5844349c9c1c50d3",
    measurementId: "UA-25311844-1"
};

firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);

    var notificationTitle = 'SINDOnews Terkini';
    var notificationOptions = {
        title: payload.notification.title,
        body: payload.notification.body,
        icon: payload.notification.icon,
        click_action: payload.notification.click_action
    };

    return self.registration.showNotification(notificationTitle, notificationOptions);
});