<?php

class Mes extends CI_Model {

    var $esurl;
    var $totaldata;
    var $totaldataquran;

    function __construct(){
        parent::__construct();
        $this->load->library('sindocurl');

        $this->esurl = 'http://10.1.8.58';
        if(isset($_SERVER['SERVER_ADDR'])){
            if($_SERVER['SERVER_ADDR'] == '10.184.15.219'){
                $this->esurl = 'http://10.184.15.212';
            }
        }

        $this->totaldata = 0;
        $this->totaldatakanal = 0;
    }

    function getEsQuery($index,$query,$start,$offset){
        $params = array(
            'from' => $start,
            'size' => $offset,
            'sort' => array(
                'date_publish' => array(
                    'order' => 'desc'
                )
            ),
            'query' => array(
                'bool' => array(
                    'must' => array(
                        array('match_phrase' => array('title' => $query)),
                        array('match_phrase' => array('summary' => $query))
                    ),
                    'filter' => array(
                        array(
                            'term' => array(
                                'parent_id' => 67
                            )
                        )
                    )
                )
            )
        );

        $urlput = $this->esurl . '/' . $index . '/_search?pretty=true';
        $data_post = json_encode($params,JSON_PRETTY_PRINT);
        $headers = array(
            'Content-Type: application/json'
        );

        $result = $this->sindocurl->post($urlput,$headers,$data_post);
        $output = json_decode($result,TRUE);

        return $output;
    }

    function getSearch($index,$query,$start,$offset){
        $out = array();

        $news = $this->getEsQuery($index,$query,$start,$offset);
        if(isset($news['hits'])){
            $totalall = $news['hits']['total']['value'];
            $this->totaldata = $totalall;

            $datanews = $news['hits']['hits'];
            $total = count($datanews);
            if($total > 0){
                for($i = 0; $i < $total; $i++){
                    $out[$i]['content_id'] = $datanews[$i]['_source']['content_id'];
                    $out[$i]['parent_id'] = $datanews[$i]['_source']['parent_id'];
                    $out[$i]['parent_name'] = $datanews[$i]['_source']['parent_name'];
                    $out[$i]['channel_id'] = $datanews[$i]['_source']['channel_id'];
                    $out[$i]['channel_name'] = $datanews[$i]['_source']['channel_name'];
                    $out[$i]['title'] = stripslashes($datanews[$i]['_source']['title']);
                    $out[$i]['summary'] = $datanews[$i]['_source']['summary'];
                    $out[$i]['image'] = $datanews[$i]['_source']['image'];
                    $out[$i]['date_created'] = $datanews[$i]['_source']['date_created'];
                    $out[$i]['date_publish'] = $datanews[$i]['_source']['date_publish'];
                    $out[$i]['code'] = $datanews[$i]['_source']['code'];
                    $out[$i]['codename'] = $datanews[$i]['_source']['codename'];
                    $out[$i]['slug'] = $datanews[$i]['_source']['slug'];
                }
            }
        }

        return $out;
    }

    function getNumSearch(){
        return $this->totaldata;
    }

    function getEsQuran($index,$query,$start,$offset){
        $params = array(
            'from' => $start,
            'size' => $offset,
            'query' => array(
                'query_string' => array(
                    'fields' => array('title_surat','text'),
                    'query' => $query
                )
            )
        );

        $urlput = $this->esurl . '/' . $index . '/_search?pretty=true';
        $data_post = json_encode($params,JSON_PRETTY_PRINT);
        $headers = array(
            'Content-Type: application/json'
        );

        $result = $this->sindocurl->post($urlput,$headers,$data_post);
        $output = json_decode($result,TRUE);

        return $output;
    }

    function getSearchQuran($index,$query,$start,$offset){
        $out = array();

        $news = $this->getEsQuran($index,$query,$start,$offset);
        if(isset($news['hits'])){
            $totalall = $news['hits']['total']['value'];
            $this->totaldataquran = $totalall;

            $datanews = $news['hits']['hits'];
            $total = count($datanews);
            if($total > 0){
                for($i = 0; $i < $total; $i++){
                    $out[$i]['quran_id'] = $datanews[$i]['_source']['quran_id'];
                    $out[$i]['no_surat'] = $datanews[$i]['_source']['no_surat'];
                    $out[$i]['no_ayat'] = $datanews[$i]['_source']['no_ayat'];
                    $out[$i]['no_juz'] = $datanews[$i]['_source']['no_juz'];
                    $out[$i]['title'] = stripslashes($datanews[$i]['_source']['title_surat']);
                    $out[$i]['text'] = $datanews[$i]['_source']['text'];
                    $out[$i]['ayat_image'] = $datanews[$i]['_source']['ayat_image'];
                    $out[$i]['ayat_audio'] = $datanews[$i]['_source']['ayat_audio'];
                    $out[$i]['indopak'] = $datanews[$i]['_source']['indopak'];
                }
            }
        }

        return $out;
    }

    function getNumSearchQuran(){
        return $this->totaldataquran;
    }

    function getRandomAtmajaQuery($offset){
        $params = array(
            'size' => $offset,
            'query' => array(
                'function_score' => array(
                    'query' => array(
                        'bool' => array(
                            'must' => array(
                                array('match_all' => (object) null)
                            ),
                            'filter' => array(
                                array(
                                    'term' => array(
                                        'parent_id' => 67
                                    )
                                )
                            )
                        )
                    ),
                    'functions' => array(
                        array('random_score' => (object) null)
                    )
                )
            )
        );

        $urlput = $this->esurl . '/article/_search?pretty=true';
        $data_post = json_encode($params,JSON_PRETTY_PRINT);
        $headers = array(
            'Content-Type: application/json'
        );

        $result = $this->sindocurl->post($urlput,$headers,$data_post);
        $output = json_decode($result,TRUE);

        return $output;
    }

    function getRandomAtmaja($offset){
        $out = array();

        $news = $this->getRandomAtmajaQuery($offset);
        if(isset($news['hits'])){
            $totalall = $news['hits']['total']['value'];
            $this->totaldata = $totalall;

            $datanews = $news['hits']['hits'];
            $total = count($datanews);
            if($total > 0){
                for($i = 0; $i < $total; $i++){
                    $out[$i]['content_id'] = $datanews[$i]['_source']['content_id'];
                    $out[$i]['parent_id'] = $datanews[$i]['_source']['parent_id'];
                    $out[$i]['parent_name'] = $datanews[$i]['_source']['parent_name'];
                    $out[$i]['channel_id'] = $datanews[$i]['_source']['channel_id'];
                    $out[$i]['channel_name'] = $datanews[$i]['_source']['channel_name'];
                    $out[$i]['title'] = stripslashes($datanews[$i]['_source']['title']);
                    $out[$i]['summary'] = $datanews[$i]['_source']['summary'];
                    $out[$i]['image'] = $datanews[$i]['_source']['image'];
                    $out[$i]['date_created'] = $datanews[$i]['_source']['date_created'];
                    $out[$i]['date_publish'] = $datanews[$i]['_source']['date_publish'];
                    $out[$i]['code'] = $datanews[$i]['_source']['code'];
                    $out[$i]['codename'] = $datanews[$i]['_source']['codename'];
                    $out[$i]['slug'] = $datanews[$i]['_source']['slug'];
                }
            }
        }

        return $out;
    }

    function getRandomSearchQuery($index,$query,$offset){
        $params = array(
            'size' => $offset,
            'query' => array(
                'function_score' => array(
                    'query' => array(
                        'bool' => array(
                            'must' => array(
                                array('match' => array('title' => $query)),
                                array('match' => array('summary' => $query)),
                            ),
                            'filter' => array(
                                array(
                                    'term' => array(
                                        'parent_id' => 67
                                    )
                                )
                            )
                        )
                    ),
                    'functions' => array(
                        array('random_score' => (object) null)
                    )
                )
            )
        );

        $urlput = $this->esurl . '/' . $index . '/_search?pretty=true';
        $data_post = json_encode($params,JSON_PRETTY_PRINT);
        $headers = array(
            'Content-Type: application/json'
        );

        $result = $this->sindocurl->post($urlput,$headers,$data_post);
        $output = json_decode($result,TRUE);

        return $output;
    }

    function getRandomSearch($index,$query,$offset){
        $out = array();

        $news = $this->getRandomSearchQuery($index,$query,$offset);
        if(isset($news['hits'])){
            $totalall = $news['hits']['total']['value'];
            $this->totaldata = $totalall;

            $datanews = $news['hits']['hits'];
            $total = count($datanews);
            if($total > 0){
                for($i = 0; $i < $total; $i++){
                    $out[$i]['content_id'] = $datanews[$i]['_source']['content_id'];
                    $out[$i]['parent_id'] = $datanews[$i]['_source']['parent_id'];
                    $out[$i]['parent_name'] = $datanews[$i]['_source']['parent_name'];
                    $out[$i]['channel_id'] = $datanews[$i]['_source']['channel_id'];
                    $out[$i]['channel_name'] = $datanews[$i]['_source']['channel_name'];
                    $out[$i]['title'] = stripslashes($datanews[$i]['_source']['title']);
                    $out[$i]['summary'] = $datanews[$i]['_source']['summary'];
                    $out[$i]['image'] = $datanews[$i]['_source']['image'];
                    $out[$i]['date_created'] = $datanews[$i]['_source']['date_created'];
                    $out[$i]['date_publish'] = $datanews[$i]['_source']['date_publish'];
                    $out[$i]['code'] = $datanews[$i]['_source']['code'];
                    $out[$i]['codename'] = $datanews[$i]['_source']['codename'];
                    $out[$i]['slug'] = $datanews[$i]['_source']['slug'];
                }
            }
        }

        return $out;
    }

    function getEsRelated($index,$query,$start,$offset){
        $params = array(
            'from' => $start,
            'size' => $offset,
            'query' => array(
                'bool' => array(
                    'should' => array(
                        'query_string' => array(
                            'fields' => array('title','summary'),
                            'query' => $query
                        )
                    ),
                    'filter' => array(
                        array(
                            'term' => array(
                                'parent_id' => 144
                            )
                        )
                    )
                )
            )
        );

        $urlput = $this->esurl . '/' . $index . '/_search';
        $data_post = json_encode($params,JSON_PRETTY_PRINT);
        $headers = array(
            'Content-Type: application/json'
        );

        $result = $this->sindocurl->post($urlput,$headers,$data_post);
        $output = json_decode($result,TRUE);

        return $output;
    }

    function getSearchRelated($index,$query,$start,$offset){
        $out = array();

        $news = $this->getEsRelated($index,$query,$start,$offset);
        if(isset($news['hits'])){
            $totalall = $news['hits']['total']['value'];
            $this->totaldata = $totalall;

            $datanews = $news['hits']['hits'];
            $total = count($datanews);
            if($total > 0){
                for($i = 0; $i < $total; $i++){
                    $out[$i]['content_id'] = $datanews[$i]['_source']['content_id'];
                    $out[$i]['parent_id'] = $datanews[$i]['_source']['parent_id'];
                    $out[$i]['parent_name'] = $datanews[$i]['_source']['parent_name'];
                    $out[$i]['channel_id'] = $datanews[$i]['_source']['channel_id'];
                    $out[$i]['channel_name'] = $datanews[$i]['_source']['channel_name'];
                    $out[$i]['title'] = stripslashes($datanews[$i]['_source']['title']);
                    $out[$i]['summary'] = $datanews[$i]['_source']['summary'];
                    $out[$i]['image'] = $datanews[$i]['_source']['image'];
                    $out[$i]['date_created'] = $datanews[$i]['_source']['date_created'];
                    $out[$i]['date_publish'] = $datanews[$i]['_source']['date_publish'];
                    $out[$i]['code'] = $datanews[$i]['_source']['code'];
                    $out[$i]['codename'] = $datanews[$i]['_source']['codename'];
                    $out[$i]['slug'] = $datanews[$i]['_source']['slug'];
                }
            }
        }

        return $out;
    }
    
    function getEsVideo($query,$start,$offset){
        $params = array(
            'from' => $start,
            'size' => $offset,
            'query' => array(
                'bool' => array(
                    'should' => array(
                        'query_string' => array(
                            'fields' => array('title','summary'),
                            'query' => $query
                        )
                    ),
                    'filter' => array(
                        array(
                            'term' => array(
                                'code' => 'mediavideo'
                            )
                        )
                    )
                )
            )
        );

        $urlput = $this->esurl . '/mediavideo/_search';
        $data_post = json_encode($params,JSON_PRETTY_PRINT);
        $headers = array(
            'Content-Type: application/json'
        );

        $result = $this->sindocurl->post($urlput,$headers,$data_post);
        $output = json_decode($result,TRUE);

        return $output;
    }

    function getSearchVideo($query,$start,$offset){
        $out = array();

        $news = $this->getEsVideo($query,$start,$offset);
        if(isset($news['hits'])){
            $totalall = $news['hits']['total']['value'];
            $this->totaldata = $totalall;

            $datanews = $news['hits']['hits'];
            $total = count($datanews);
            if($total > 0){
                for($i = 0; $i < $total; $i++){
                    $out[$i]['content_id'] = $datanews[$i]['_source']['content_id'];
                    $out[$i]['parent_id'] = $datanews[$i]['_source']['parent_id'];
                    $out[$i]['parent_name'] = $datanews[$i]['_source']['parent_name'];
                    $out[$i]['channel_id'] = $datanews[$i]['_source']['channel_id'];
                    $out[$i]['channel_name'] = $datanews[$i]['_source']['channel_name'];
                    $out[$i]['title'] = stripslashes($datanews[$i]['_source']['title']);
                    $out[$i]['summary'] = $datanews[$i]['_source']['summary'];
                    $out[$i]['image'] = $datanews[$i]['_source']['image'];
                    $out[$i]['date_created'] = $datanews[$i]['_source']['date_created'];
                    $out[$i]['date_publish'] = $datanews[$i]['_source']['date_publish'];
                    $out[$i]['code'] = $datanews[$i]['_source']['code'];
                    $out[$i]['codename'] = $datanews[$i]['_source']['codename'];
                    $out[$i]['slug'] = $datanews[$i]['_source']['slug'];
                }
            }
        }

        return $out;
    }

}
