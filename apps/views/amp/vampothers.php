<?php

$total = count($others);
if($total > 0){
    echo '<div class="article-others news mt10">';
    echo '<div class="heading">Berita Lainnya</div>';
    echo '<ul>';
    for($i = 0; $i < $total; $i++){
        $dtime[$i] = strtotime($others[$i]['date_created']);
        $l[$i] = site_url('read') . '/' . $others[$i]['content_id'] . '/' . $others[$i]['channel_id'] . '/' . slug($others[$i]['title'] . '-' . $dtime[$i]);
        $title[$i] = cleanWords($others[$i]['title']);
        $summary[$i] = cleanWords($others[$i]['summary']);
        $ago[$i] = time_difference($others[$i]['date_publish']);
        
        if($others[$i]['image'] != ''){
            $d[$i] = date('Y/m/d',strtotime($others[$i]['date_created']));
            $img[$i] = images_uri() . '/dyn/140/pena/news/' . $d[$i] . '/' . $others[$i]['channel_id'] . '/' . $others[$i]['content_id'] . '/' . $others[$i]['image'];
            $showimg[$i] = '
                <div class="breaking-img">
                    <a href="' . $l[$i] . '"><amp-img layout="responsive" width="120" height="90" src="' . $img[$i] . '" alt="' . cleanHTML($others[$i]['title']) . '"></amp-img></a>
                </div>';
        }else{
            $showimg[$i] = '';
        }
        
        if($others[$i]['content_id'] != $content_id){
            echo '
                <li>
                    ' . $showimg[$i] . '
                    <div class="breaking-title">
                        <a href="' . $l[$i] . '">' . $title[$i] . '</a>
                    </div>
                </li>';
        }
    }

    echo '</ul>';
    echo '</div>';
    
    echo '
        <div class="news-more">
            <a href="' . base_url() . '">berita lainnya</a>
        </div>';
}