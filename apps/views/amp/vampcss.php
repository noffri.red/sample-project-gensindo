<style amp-custom>
    a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{border:0;font:inherit;vertical-align:baseline;margin:0;padding:0}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:none}table{border-collapse:collapse;border-spacing:0}em,em *,i,i *{font-style:italic}a{text-decoration:none;outline:0}
    html {
        scroll-behavior: smooth;
    }
    body{
        overflow-x:hidden;
        font-size:16px;
        font-family: helvetica, arial, sans-serif;
        line-height: 1.5
    }
    h1,b,b *,strong,strong *,.location,.breaking-title a,.popular li a,.topik-terkait-title,h1,.heading, .popular-heading,.xtitle {
        font-family: tahoma, arial, sans-serif;
        font-weight: 700
    }
    .ads300 {
        width: 300px;
        height: auto;
        margin-left: auto;
        margin-right: auto
    }
    .ads320 {
        width: 320px;
        height: auto;
        margin-left: auto;
        margin-right: auto
    }
    .ads336 {
        width: 336px;
        height: auto;
        margin-left: auto;
        margin-right: auto
    }
    .p16 {
        padding: 16px
    }
    .pt20 {
        padding-top: 20px
    }
    header {
        background-color: #FAFAFA;
        text-align: center;
        padding: 10px 0;
        box-shadow: 0 1px 4px 0 rgba(0,0,0,.5)
    }
    .btn-menu, .btn-search {
        position: absolute;
        top: 10px
    }
    .btn-menu {
        left: 16px
    }
    .btn-menu button {
        border: none;
        background-color: #FAFAFA;
        padding: 8px 12px 8px;
        cursor: pointer;
    }
    .btn-search {
        right: 16px
    }
    .btn-search a {
        display: inline-block;
        padding: 8px 12px 8px;
    }
    amp-sidebar {
        background-color: #ffffff;
    }
    .nav-close {
        padding: 16px
    }
    .nav-close i {
        font-size: 24px
    }
    .nav-side {
        width: 220px;
        font-size: 14px;
        background-color: #ffffff
    }
    .nav-side li {
        border-bottom: 1px solid #DFDFDF
    }
    .nav-side li:last-child {
        border-bottom: none
    }
    .nav-side li a {
        display: block;
        padding: 12px 16px;
        color: #282828;
        font-weight: 700;
    }
    .nav-side li a:hover {
        background-color: #282828;
        color: #ffffff
    }
    .text {
        padding-top: 5px;
        font-size: 12px;
        color: #717171;
    }
    article {
        margin-bottom: 16px;
    }
    article h1 {
        padding: 0 16px;
        color: #484848;
    }
    h2 {
        font-size: 14px;
        color: #1A237E;
        padding: 8px 16px;
    }
    .bread a, .fa-angle-double-right {
        color: #9E9E9E;
    }
    .bread {
        padding: 8px 16px 16px;
        font-size: 14px;   
    }
    .bread a {        
        padding: 0;        
        display: inline-block;
    }
    .fa-angle-double-right {
        padding: 0 8px;
    }
    .reporter {
        margin: 16px 0 0;
    }
    .reporter a {
        font-size: 14px;
        color: #E91E63;
        display: block;
        padding: 0 16px 6px;
    }
    .subtitle {
        font-size: 12px;
        color: #9C27B0;
        padding-bottom: 8px
    }
    .editor {
        color: #282828;
        font-size: 14px;
        padding: 0 16px 8px;
    }
    .time {
        color: #9E9E9E;
        font-size: 12px;
        padding: 0 16px 16px;
    }
    .caption {
        color: #9E9E9E;
        font-size: 12px;
    }
    .content {
        position: relative;
        color: #282828;
        line-height: 1.8;
        font-size: 18px
    }
    .content a span[style] {
        font-size: 16px;
        font-weight: 700
    }
    .content span[style] {
        color: #34495E;
        font-weight: 700
    }
    .content amp-img,.content quote {
        margin: 0 auto;
        display: block
    }
    .content img,.content amp-img {
        height: auto;
        padding: 0;
        width: 96%
    }
    .content a {
        color: #7A1313;
        font-size: 16px
    }
    .content td {
        padding: .2em .5em;
        border: 1px solid #E7E9EA
    }
    .content table {
        width: 100%;
        font-size: 14px
    }
    .content quote {
        background-color: #F5F8FB;
        padding: 10px;
        width: 90%;
        box-shadow: 0 0 .3em 0 rgba(0, 0, 0, .4);
        -webkit-box-shadow: 0 0 .3em 0 rgba(0, 0, 0, .4);
        -moz-box-shadow: 0 0 .3em 0 rgba(0, 0, 0, .4)
    }
    .content-next {
        padding: 0 16px;
        font-weight: 700;
    }
    .pagebreak {
        padding: 5px 0 32px
    }
    .page-title {
        color: #BF360C;
        font-size: 14px;
        padding: 0 16px;
    }
    .page-article ul {
        padding: 8px 16px;
    }
    .page-article li {
        display: inline-block;
        margin: 12px 12px 12px 0;
    }
    .page-article li a {
        border-radius: 8px;
        -moz-border-radius: 8px;
        -webkit-border-radius: 8px;
        background-color: #BF360C;
        color: #ffffff;
        padding: 8px 16px
    }
    .page-article li a:hover {
        background-color: #E64A19;
    }
    .page-article li.active {
        background-color: #f0f0f0;
        border-radius: 8px;
        -moz-border-radius: 8px;
        -webkit-border-radius: 8px;
        color: #BF360C;
        font-weight: 700;
        padding: 8px 16px;
    }
    .vembed {
        padding-bottom: 56.25%;
        padding-top: 35px;
        height: 0;
        overflow: hidden
    }
    .vembed iframe {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%
    }
    .v-youtube {
        height: 0;
        padding-bottom: 68%;
        padding-top: 25px;
        position: relative;
        width: 100%
    }
    .v-youtube iframe {
        height: 100%;
        width: 100%;
        left: 0;
        position: absolute;
        top: 0
    }
    .ximg amp-img {
        display: block
    }
    .heading, .popular-heading {        
        padding: 8px 16px 8px;
        font-size: 120%
    }
    .heading {
        border-bottom: 2px solid #303F9F;
        color: #303F9F;
    }
    .popular-heading {
        border-bottom: 2px solid #880E4F;
        color: #880E4F;
    }
    .news li {
        border-bottom: 1px dotted #9FA8DA
    }
    .popular li {
        border-bottom: 1px dotted #FBE9E7;        
    }
    .popular li a {
        display: block;
        padding: 12px 16px;
        color: #880E4F
    }
    .news li:last-child,.popular li:last-child {
        border-bottom: none
    }
    .news li:after {
        display: table;
        content: "";
        clear: both
    }
    .news-more {
        font-size: 14px;
        margin: 12px 0;
        text-align: center
    }
    .news-more a {
        background-color: #3F51B5;
        color: #fff;
        display: inline-block;
        padding: 8px 16px;
        font-weight: 700;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px;
        border-radius: 8px
    }
    .topik-terkait {
        padding: 0 16px;
    }
    .topik-terkait-title {
        color: #e91e63;
        padding: 5px 0;
        font-size: 120%
    }
    .topik-terkait a {
        background-color: #ec407a;
        color: #fff;
        display: inline-block;
        font-size: 12px;
        margin: 8px;
        padding: 6px 12px;
        border-radius: 4px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
    }
    .topik-terkait i.fa {
        padding-right: 4px
    }
    .breaking-img,.headline-pict {
        float: left;
        overflow: hidden;
        padding: 16px 0 16px 16px;
    }
    .breaking-img-large img {
        height: auto;
        width: 100%
    }
    .breaking-title {
        overflow: hidden
    }
    .breaking-title a {
        display: block;
        color: #282828;
        padding: 12px 16px
    }
    .breaking-title p {
        color: #9E9E9E;
        font-size: 12px;
        padding-top: 6px
    }
    article:after,.news li:after,.xbutton:after,.news li:after,.popular li:after,.pagebreak:after,.page-article ul:after {
        clear: both;
        display: table;
        content: ""
    }
    footer {
        background-color: #FAFAFA;
        margin-top: 48px;
        border-top: 1px solid #E8EAF6;
    }
    .footer-nav {
        padding: 16px;
        text-align: center;
    }
    .footer-nav li {
        font-size: 14px;
    }
    .box-text {
        padding: 0 16px 16px;
        font-size: 12px;
        text-align: center;
        color: #9E9E9E;
        line-height: 1.8;
    }
    .clearfix:after {
        display: table;
        clear: both
    }
    .pl5 {
        padding-left: .5em
    }
    .mt10 {
        margin-top: 10px
    }
    .mt20 {
        margin-top: 20px
    }
    .mb10 {
        margin-bottom: 10px
    }
    .mb20 {
        margin-bottom: 20px
    }
    .tl {
        text-align: left
    }
    .adsquare {
        text-align: center
    }
    .social-share {
        padding: 16px 16px 0;
        height: 120px;
        overflow: hidden;
    }
    .xtitle {
        font-size: 14px;
        margin-bottom: 8px
    }
    .xbutton {
        padding: 8px 0
    }
    .xbutton a {
        margin: 0 12px;
        float: left;

    }
    .fa-whatsapp {
        color: #009688;
    }
    .fa-facebook-square {
        color: #43609C
    }
    .fa-twitter-square {
        color: #55ACEE
    }
    .fa-google-plus-square {
        color: #DB4437
    }
    .note-paging {
        text-align: center;
        color: #999999;
        font-size: 12px;
        margin: 8px 0
    }
    .box-paging {
        text-align: center
    }
    .article-paging {
        box-shadow: 0 0 5px 0 rgba(0,0,0,.4);
        -webkit-box-shadow: 0 0 5px 0 rgba(0,0,0,.4);
        -moz-box-shadow: 0 0 5px 0 rgba(0,0,0,.4);
        display: inline-block;
        font-size: 14px;
        border: 2px solid #fff;
        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px
    }
    .article-paging:after {
        clear: both;
        display: table;
        content: ''
    }
    .article-paging li {
        display: inline-block
    }
    .article-paging li.active {
        background-color: #ffffff;
        color: #282828;
        padding: 12px 22px;
    }
    .article-paging li a {
        display: block;
        background-color: #282828;
        color: #ffffff;
        padding: 12px 22px;
        text-decoration: none;
        border-right: 1px solid #090909;
        border-left: 1px solid #474747
    }
    .article-paging li a:hover {
        background-color: #eeeeee;
        border-right: 1px solid #fff;
        border-left: 1px solid #fff;
        color: #282828;
    }
    .mpaging {
        padding: 16px 0;
        width: 100%
    }
    .mpaging ul {
        text-align: center
    }
    .mpaging li {
        display: inline-block;
        font-size: 14px;
        margin: 0 4px;
    }

    .mpaging li i {
        color: #ffffff;
    }

    .mpaging li a {
        background-color: #1A237E;
        padding: 8px 14px;
        color: #ffffff;
        text-decoration: none;
    }

    .mpaging li a:hover {
        background-color: #3F51B5;
    }

    .mpaging li.active {
        background-color: #7986CB;
        padding: 6px 14px;
        color: #ffffff
    }
    amp-social-share {
        margin-right: 8px
    }

    .content-line {
        background: -webkit-linear-gradient(top,rgba(255,255,255,0) 0,rgba(255,255,255,.8) 40%,rgba(255,255,255,1) 100%);
        background: -moz-linear-gradient(top,rgba(255,255,255,0) 0,rgba(255,255,255,.8) 40%,rgba(255,255,255,1) 100%);
        background: -o-linear-gradient(top,rgba(255,255,255,0) 0,rgba(255,255,255,.8) 40%,rgba(255,255,255,1) 100%);
        background: linear-gradient(top,rgba(255,255,255,0) 0,rgba(255,255,255,.8) 40%,rgba(255,255,255,1) 100%);
        height: 20%;
        width: 100%;
        position: absolute;
        bottom: 8px;
        z-index: 2
    }

    .ctn-next-title {
        font-size: 12px;
        text-align: center;
        margin-bottom: 8px;
        color: #919191
    }

    .ctn-next {
        text-align: center;
        margin-bottom: 32px;
    }

    .ctn-next:after {
        display: table;
        content: "";
        clear: both
    }

    .ctn-next a {
        display: inline-block;
        padding: 12px 16px;
        background-color: #282828;
        color: #ffffff;
        font-size: 14px;
        border-radius: 8px;
        -webkit-border-radius: 8px;
        -moz-border-radius: 8px
    }

    .plr16 {
        padding-left: 16px;
        padding-right: 16px
    }

    .mlr16 {
        margin-left: 16px;
        margin-right: 16px
    }

    .rendering {
        color: #ffffff
    }

    .gfeed {
        width: 320px;
        margin: 16px auto 0
    }

    .admid {
        text-align: center;
    }

    .matched-content {
        padding: 0 16px;
        margin-top: 8px
    }

    .baca-inline ul {
        background-color: #F3F3F3;
        margin-bottom: -24px;
    }
    .baca-inline-head {
        font-size: 14px;
        font-weight: 700;
        border-bottom: 2px solid #282828;
        padding-bottom: 4px
    }
    .baca-inline li {
        font-size: 14px;
        border-bottom: 2px solid #ffffff
    }
    .baca-inline li a {
        display: block;
        font-size: 14px;
        padding: 12px 16px;
        font-weight: 700
    }
    .content-show {
        margin: 32px auto;
        border-top: 4px solid #969696;
        width: 60%;
    }
    .content-show a {
        padding: 8px 16px;
        display: block;
        background-color: #969696;
        color: #ffffff;
        width: 40%;
        text-align: center;
        margin: 0 auto;
        font-size: 14px;
        text-decoration: none;
        border-bottom-right-radius: 8px;
        border-bottom-left-radius: 8px;
    }
    .content-show a:hover {
        background-color: #ffffff;
        color: #282828;
        box-shadow: 0 0 5px 0 rgba(0,0,0,.5);
    }
    .article-next a {
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        -webkit-border-top-right-radius: 5px;
        -webkit-border-bottom-right-radius: 5px;
        -moz-border-top-right-radius: 5px;
        -moz-border-bottom-right-radius: 5px;
    }
    .article-prev a {
        border-top-left-radius: 5px;
        border-bottom-left-radius: 5px;
        -webkit-border-top-left-radius: 5px;
        -webkit-border-bottom-left-radius: 5px;
        -moz-border-top-left-radius: 5px;
        -moz-border-bottom-left-radius: 5px;
    }

    .v-video,.social-embed {
        margin: 16px 0
    }
    .image-content {
        width: 90%;
        height: auto;
        border: 2px solid #fff;
        box-shadow: 0 0 4px 0 rgba(0,0,0,.4);
        -webkit-box-shadow: 0 0 4px 0 rgba(0,0,0,.4);
        -moz-box-shadow: 0 0 4px 0 rgba(0,0,0,.4);
        margin: 0 auto;
    }
    .forum-box {
        padding: 0;
    }
    .forum-title {
        padding: 12px 16px 2px;
        font-weight: 700;
        margin-bottom: 8px;
        font-size: 120%;
        color: #880E4F;
        border-bottom: 2px solid #880E4F;
    }
    .forum-ctn {
        padding: 0 16px
    }
    .notif-box {
        margin: 0 0 16px;
        text-align: center
    }
    .notif-lead {
        font-size: 16px;
        font-weight: 700;
        text-align: center;
        margin-bottom: 16px
    }
    .notif-lead span {
        font-size: 12px;
        font-weight: 500;
    }
    amp-web-push-widget button.subscribe {
        display: inline-flex;
        align-items: center;
        border-radius: 8px;
        border: 0;
        box-sizing: border-box;
        margin: 0;
        padding: 10px 15px;
        cursor: pointer;
        outline: none;
        font-size: 14px;
        font-weight: 500;
        background: #282828;
        color: #ffffff;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        line-height: 1.5;
        text-align: left;
    }

    amp-web-push-widget button.subscribe .subscribe-icon {
        margin-right: 16px;
    }

    amp-web-push-widget button.subscribe:active {
        transform: scale(0.99);
    }

    amp-web-push-widget button.unsubscribe {
        display: inline-flex;
        align-items: center;
        justify-content: center;
        height: 45px;
        border: 0;
        margin: 0;
        cursor: pointer;
        outline: none;
        font-size: 15px;
        font-weight: 400;
        background: transparent;
        color: #B1B1B1;
        -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
    }

    .comment-go {
        text-align: center;
        background-color: #EFEFEF;
        box-shadow: inset 0 1px 4px 0 rgba(0,0,0,.4);
    }

    .comment-go a {
        color: #ffffff;
        display: inline-block;
        padding: 8px 16px;
        background-color: #930009;
        width: 60%;
        border-radius: 8px;
        margin: 8px;
        font-size: 14px;
    }
    
    .suggest-box-amp {
        position: relative;
        overflow: hidden;
        border-radius: 16px;
        font-family: tahoma, sans-serif;
        width: 90%;
        margin: 0 auto 16px
    }
    .suggest-pict-amp {
        position: relative;
    }
    .suggest-pict-amp img {
        display: block;
    }
    .suggest-title-amp {
        position: absolute;
        bottom: 0;
        background: linear-gradient(top,rgba(0,0,0,0) 0,rgba(0,0,0,.8) 40%,rgba(0,0,0,1) 100%);
        background: -webkit-linear-gradient(top,rgba(0,0,0,0) 0,rgba(0,0,0,.8) 40%,rgba(0,0,0,1) 100%);
        background: -moz-linear-gradient(top,rgba(0,0,0,0) 0,rgba(0,0,0,.8) 40%,rgba(0,0,0,1) 100%);
        background: -o-linear-gradient(top,rgba(0,0,0,0) 0,rgba(0,0,0,.8) 40%,rgba(0,0,0,1) 100%);
        width: 100%;
    }
    .suggest-title-amp a {
        text-decoration: none;
        color: #ffffff;
        display: block;
        padding: 12px 16px;
        font-size: 18px;
        font-weight: 700
    }
    .suggest-box:after {
        display: table;
        content: "";
        clear: both
    }
    .suggest-box {
        width: 90%;
        margin: 16px auto 64px;
        background-color: #282828;
        border-radius: 8px;
    }
    .suggest-pict {
        float: right;
        height: 90px;
        width: 90px;
        margin: 16px 16px 16px 0;
        overflow: hidden
    }
    .suggest-pict img {
        display: block;
        position: relative;
        transform: translate(-20%,0);
    }
    .suggest-title {
        overflow: hidden
    }
    .suggest-title a {
        display: block;
        font-size: 16px;
        padding: 12px 16px;
        color: #ffffff;
        font-family: 'roboto-bold',arial,sans-serif;
    }
    .video-box {
        width: 90%;
        padding: 8px;
        box-shadow: inset 0 0 5px 0 rgba(0,0,0,.5);
        -moz-box-shadow: inset 0 0 5px 0 rgba(0,0,0,.5);
        -webkit-box-shadow: inset 0 0 5px 0 rgba(0,0,0,.5);
        margin: 16px auto;
        border-radius: 8px;
        font-weight: 700;
    }
    .video-head {
        font-size: 14px;
        margin: 4px 4px 8px;
    }
    .video-title a {
        font-size: 14px;
        padding: 8px 8px 0;
        display: block;
        color: #282828
    }
    
    @media(min-width:320px) and (max-width:479px){
        header .logo {
            background: url(https://sm.sindonews.net/mobile/2016/images/sindonews-pict.png) 0 -290px no-repeat;
            width: 160px;
            height: 15px;
            display: block;
            margin-left: auto;
            margin-right: auto
        }
        .breaking-img {
            height: auto;
            width: 90px
        }
        .ximg amp-img {
            height: auto;
            width: 100%
        }
        .caption {
            border-bottom: 1px solid #eeeeee;
            padding: 8px 16px
        }
        .content {
            padding: 16px
        }
        h1 {
            font-size: 26px
        }
        .footer-nav ul {
            column-count: 2;
            -webkit-column-count: 2;
            -moz-column-count: 2
        }
        .footer-nav li a {
            color: #616161;
            display: block;
            padding: 8px;
        }
    }
    @media(min-width:480px) and (max-width:639px){
        header .logo {
            background: url(https://sm.sindonews.net/mobile/2016/images/sindonews-pict.png) 0 -315px no-repeat;
            width: 220px;
            height: 20px;
            display: block;
            margin-left: auto;
            margin-right: auto
        }
        .breaking-img {
            height: auto;
            width: 90px
        }
        .ximg amp-img {
            height: auto;
            width: 100%
        }
        .caption {
            border-bottom: 1px solid #eeeeee;
            padding: 8px 16px
        }
        .content {
            padding: 16px
        }
        h1 {
            font-size: 26px
        }
        .footer-nav ul {
            column-count: 2;
            -webkit-column-count: 2;
            -moz-column-count: 2
        }
        .footer-nav li a {
            color: #616161;
            display: block;
            padding: 8px;
        }
    }
    @media(min-width:640px) and (max-width:767px){
        section,article {
            width: 90%;
            margin: 0 auto
        }
        header .logo {
            background: url(https://sm.sindonews.net/mobile/2016/images/sindonews-pict.png)0 -315px no-repeat;
            width: 220px;
            height: 20px;
            display: block;
            margin-left: auto;
            margin-right: auto
        }
        .breaking-img {
            height: auto;
            width: 90px
        }
        .ximg {
            width: 100%
        }
        .caption {
            padding: 10px 0;
            text-align: center;
        }
        .content {
            padding: 16px;
        }
        .content img,.content amp-img {
            width: 70%
        }
        h1 {
            font-size: 22px
        }
        .footer-nav ul {
            column-count: 3;
            -webkit-column-count: 3;
            -moz-column-count: 3
        }
        .footer-nav li a {
            color: #616161;
            display: block;
            padding: 12px;
        }
    }
    @media(min-width:768px) and (max-width:1279px){
        section,article {
            width: 90%;
            margin: 0 auto
        }
        header .logo {
            background: url(https://sm.sindonews.net/mobile/2016/images/sindonews-pict.png)0 -315px no-repeat;
            width: 220px;
            height: 20px;
            display: block;
            margin-left: auto;
            margin-right: auto
        }
        .breaking-img {
            height: auto;
            width: 90px
        }
        .ximg {
            padding: 0 16px;
            width: 620px;
            margin: 0 auto;
        }
        .caption {
            padding: 10px 0;
            text-align: center;
        }
        .content {
            padding: 16px;
        }
        .content img,.content amp-img {
            width: 70%
        }
        h1 {
            font-size: 26px
        }
        .footer-nav ul {
            column-count: 3;
            -webkit-column-count: 3;
            -moz-column-count: 3
        }
        .footer-nav li a {
            color: #616161;
            display: block;
            padding: 12px;
        }
    }
    @media(min-width: 1280px){
        section,article {
            width: 80%;
            margin: 0 auto
        }
        header .logo {
            background: url(https://sm.sindonews.net/mobile/2016/images/sindonews-pict.png)0 -315px no-repeat;
            width: 220px;
            height: 20px;
            display: block;
            margin-left: auto;
            margin-right: auto
        }
        .breaking-img {
            height: auto;
            width: 140px
        }
        .ximg {
            padding: 0 16px;
            width: 620px;
            margin: 0 auto;
        }
        .caption {
            padding: 10px 0;
            text-align: center;
        }
        .content {
            padding: 16px;
        }
        .content img,.content amp-img {
            width: 70%
        }
        h1 {
            font-size: 26px
        }
        .footer-nav li {
            display: inline-block;
        }
        .footer-nav li a {
            color: #616161;
            padding: 12px 16px;
        }
    }
</style>