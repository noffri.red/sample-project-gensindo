<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View extends CI_Controller {

    function __construct(){
        parent::__construct();
        $this->load->config('site_channel');
        $this->load->library('articleimaji');
        $this->load->library("trackers");
        $this->load->model('news/mredisnews','mredisnews');
        $this->load->model('topic/mdbtopic','mdbtopic');
    }

	function index(){


        $a = array();

        $a['html']['bottom_js'] = google_analytics($this->config->item('google_id'));

        $a['html']['title'] = 'Gen SINDO | Berita GEN';
        $metaname = array(
            'title' => $a['html']['title'],
            'description' => 'Berita Gen SINDO',
            'Slurp' => 'all',
            'google-site-verification' => $this->config->item('google_site'),
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );
        $a['html']['metaname'] = metaname($metaname);

        $a['html']['css'] = add_css('/css/styles.css');
        $a['html']['bottom_css'] = add_external_css('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
        $a['html']['bottom_css'] .= add_external_css('https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css');

        $a['html']['js'] = add_js('/js/jquery-1.10.2.min.js');
        $a['html']['js'] .= add_js('/js/materialize.min.js');

        $a['template']['header'] = $this->load->view('template/vheader',NULL,TRUE);
        $a['template']['promoted'] = $this->load->view('template/vpromoted',NULL,TRUE);
        $a['template']['minimenu'] = $this->load->view('template/vminimenu',NULL,TRUE);
        $a['template']['kategori'] = $this->load->view('template/vkategori',NULL,TRUE);
        $a['template']['minifooter'] = $this->load->view('template/vminifooter',NULL,TRUE);
        $a['template']['newcss'] = $this->load->view('template/vnewcss',NULL,TRUE);

        $getarticleimaji = $this->articleimaji->getArticle();
        $t['details'] = $getarticleimaji;

        $uriread = $t['details']['id_imaji'] . '/' . $t['details']['slug_title'] . '-' . strtotime($t['details']['date_created']);
        $t['site_url'] = site_url('read') . '/' . $uriread;
        $t['slug_title'] = $t['details']['slug_title'];
        $a['content']['site_url'] = site_url('read') . '/' . $uriread;
        $a['content']['site_title'] = seo_title($t['details']['title']);
        $a['content']['site_publish'] = date('Y-m-d\TH:i:sP',strtotime($t['details']['date_published']));
        $a['content']['site_reporter'] = $t['details']['author'];
        $a['content']['site_summary'] = seo_description($t['details']['summary']);
        $a['content']['amp_url'] = site_url('newsread') . '/' . $uriread;
        $a['content']['forum_url'] = site_url('desktop-forum') . '/' . $uriread;
        $a['content']['disqus_url'] = site_url('disqus') . '/' . $uriread;
        $a['content']['path_url'] = '/read/' . $uriread;
        $a['content']['related'] = "";
        $a['content']['id_imaji'] = $t['details']['id_imaji'];

        $t['topic_id_list'] = '';
        $a['content']['topic_list'] = '';
        $relatedrich = array();
        $topic_keyword = array();

        
        if(!empty($t['details']['id_topic'])){
            $gettopic = $this->mdbtopic->getTopicData($t['details']['id_topic']);
            $gettopic = $gettopic['results'];
            $totaltopic = count($gettopic);
            if($totaltopic > 0){
                for($i = 0; $i < $totaltopic; $i++){
                    $topicdata[$i]['id_topic'] = $gettopic[$i]['id_topic'];
                    $topicdata[$i]['topic'] = $gettopic[$i]['topic'];
                    $topicdata[$i]['slug_topic'] = $gettopic[$i]['slug_topic'];

                    $topic_id_list[$i] = $gettopic[$i]['id_topic'];
                    $topic_keyword[$i] = strtolower($gettopic[$i]['topic']);
                }

                $t['topic_id_list'] = implode(',',$topic_id_list);
                $a['content']['topic_list'] = $t['topic_id_list'];
            }

            $get_relatedrich = $this->mredisnews->getRelatedNewsByTopic($t['details']['id_imaji'],$t['details']['id_topic'],3);
            $relatedrich = $get_relatedrich['results'];
        }
        

        $t['topic'] = array();
        $t['related'] = array();
        if(!empty($t['details']['id_topic'])){
            $t['topic'] = $topicdata;
            $related = $this->mredisnews->getRelatedNewsByTopic($t['details']['id_imaji'],$t['details']['id_topic'],10);
            $t['related'] = $related['results'];
        }

        $relatedLink = array();
        if(count($relatedrich) > 0){
            for($i = 0; $i < count($relatedrich); $i++){
                $dtime[$i] = strtotime($relatedrich[$i]['date_created']);
                $urlrelated[$i] = site_url('read') . '/' . $relatedrich[$i]['id_imaji'] . '/' . $relatedrich[$i]['id_subkanal'] . '/' . $relatedrich[$i]['slug_title'] . '-' . $dtime[$i];
                $relatedLink[$i] = $urlrelated[$i];
            }
        }
        $a['content']['relatedlink'] = $relatedLink;

        /* --- start articleimaji pagination --- */
        $listbaca = $this->mredisnews->getListNews(14);
        $getbaca = $listbaca['results'];
        $baca_cut_a = array_slice($getbaca,8,14);
        $baca = randomData($baca_cut_a,6,2);

        $totalData = $this->articleimaji->getArticleNumListPageBacaDesktop($t['details']);

        $start = $this->uri->segment(5,0);
        $limit = 500;
        if($totalData > ($limit + 2)){
            $offset = $limit;
        }else{
            $offset = 100;
        }

        $t['content'] = $this->articleimaji->getArticleListPageBacaDesktop($start,$offset,$t['details'],$baca);
        $t['totalData'] = $totalData;

        $t['per_page'] = $offset;
        $t['start'] = $start;

        if($t['totalData'] <= $offset && $start > 0){
            redirect($t['site_url'],'location',301);
        }

        $this->load->library('pagination');
        $this->config->load('pagination',FALSE,TRUE);
        $config = $this->config->item('artpaging');
        $config['base_url'] = site_url('read/' . $uriread);
        $config['total_rows'] = $t['totalData'];
        $config['per_page'] = $offset;
        $config['uri_segment'] = 5;

        $this->pagination->initialize($config);
        $t['pagination'] = $this->pagination->create_links();

        $currentPage = floor(($start / $config['per_page']) + 1);
        $pagetitle = ' | Halaman ' . $currentPage;
        if($start == 0 || $start == ''){
            $pagetitle = '';
        }
        $t['current_page'] = $currentPage;
        $t['last_page'] = floor(($t['totalData'] / $config['per_page']) + 1);

        if($t['totalData'] > $offset){
            $canonical = $this->articleimaji->pagingCanonical($t['site_url'],$start,$offset,$t['totalData']);
        }else{
            $canonical = add_canonical('canonical',$t['site_url']);
        }
        $a['html']['canonical'] = $canonical;
        /* --- end articleimaji pagination --- */

        /* ---- start SEO configuration ---- */
        $articleimaji_keyword = '';
        $get_topic_keyword = implode(',',$topic_keyword);
        $keyword = $this->articleimaji->newsKeywords($get_topic_keyword . $articleimaji_keyword);
        $t['keyword'] = $keyword;
        $rich_keywords = explode(',',$keyword);
        $a['content']['keyword'] = $rich_keywords;

        if(isset($t['details']['photo'])){
            $imgmeta = $this->config->item('images_uri') . $this->config->item('dyn_620') . '/gensindo/content/' . $t['details']['year'] . '/' . $t['details']['month'] . '/' . $t['details']['day'] . '/' . $t['details']['id_subkanal'] . '/' . $t['details']['id_imaji'] . '/' . $t['details']['images'];
        }else{
            $imgmeta = '';
        }
        $a['content']['site_image'] = $imgmeta;

        $metatitle = seo_title($t['details']['title']);
        if(empty($t['details']['summary'])){
            $t['details']['summary'] = $t['details']['content'];
        }
        $metadesc = seo_description($t['details']['summary']);
        $a['html']['title'] = $metatitle . $pagetitle;
        $metaname = array(
            'description' => $metadesc . $pagetitle,
            'title' => $metatitle . $pagetitle,
            'image_src' => $imgmeta,
            'google-site-verification' => $this->config->item('google_site'),
            'news_keywords' => $keyword,
            'msvalidate.01' => $this->config->item('bing'),
            'robots' => 'index,follow',
            'googlebot' => 'index,follow',
            'googlebot-news' => 'index,follow',
        );
        $metafb = array(
            'title' => cleanWords($t['details']['title']),
            'type' => 'articleimaji',
            'url' => $t['site_url'],
            'image' => $imgmeta,
            'description' => cleanWords($t['details']['summary']),
            'site_name' => 'SINDOnews.com'
        );
        $metatwit = array(
            'twitter:card' => 'summary_large_image',
            'twitter:site' => '@SINDOnews',
            'twitter:creator' => '@SINDOnews',
            'twitter:title' => cleanWords($t['details']['title']),
            'twitter:description' => cleanWords($t['details']['summary']),
            'twitter:image' => $imgmeta
        );
        $a['html']['metaname'] = metaname($metaname);
        $a['html']['metaname'] .= metafb($metafb);
        $a['html']['metaname'] .= metaname($metatwit);
        /* ---- end SEO configuration ---- */

        $t['others'] = $this->mredisnews->getListBreakingNews(8);

        $a['content']['rich'] = '';
        $contentrich = $t['others']['results'];
        if(count($contentrich) > 0){
            if(count($contentrich) < 3){
                $total_rich = count($contentrich);
            }else{
                $total_rich = '3';
            }
            $snippet = array();
            for($i = 0; $i < $total_rich; $i++){
                $snippet[$i]['@type'] = 'ListItem';
                $snippet[$i]['position'] = $i + 1;

                $dtime[$i] = strtotime($contentrich[$i]['date_created']);
                $urlsnippet[$i] = site_url('read') . '/' . $contentrich[$i]['id_imaji'] . '/' . $contentrich[$i]['id_subkanal'] . '/' . $contentrich[$i]['slug_title'] . '-' . $dtime[$i];
                $snippet[$i]['url'] = $urlsnippet[$i];
            }

            $a['content']['rich'] = json_encode($snippet);
        }

        $t['trending'] = $this->mredisnews->getKanalTrendingTopic();
        $a['template']['trending'] = $this->load->view('template/vtrending',$t,TRUE);

        $t['event'] = $this->mredisnews->getEvent();
        $a['template']['agenda'] = $this->load->view('template/vagenda',$t,TRUE);

        $a['content']['breadcrumb'] = $this->load->view('view/vbreadcrumb',$t,TRUE);
        $a['content']['detail'] = $this->load->view('view/vdetail',$t,TRUE);
        $a['content']['relatedtopic'] = $this->load->view('view/vrelatedtopic',$t,TRUE);
        $a['content']['profileeditor'] = $this->load->view('view/vprofileeditor',$t,TRUE);
        $a['content']['relatedcontent'] = $this->load->view('view/vrelatedcontent',$t,TRUE);
        $a['content']['comment'] = $this->load->view('view/vcomment',$t,TRUE);
//        $a['content']['others'] = $this->load->view('read/vothers',$t,TRUE);

        /* tracker */
        $garelated = $this->trackers->relatedTrack($t['details']['id_imaji'],$t['topic_id_list'],3);
        $a['content']['garelated'] = $garelated;

        $gaother = $this->trackers->latestTracker($t['details']['id_imaji'],3);
        $a['content']['gaothers'] = $gaother;

        $newsframe = $this->trackers->articleFrameTracker(3);
        $a['content']['newsframe'] = $newsframe;

        /* Hits Token */
//        $key = 's1nd0n3ws.c0m-MNCm3d14';
//        $token = md5($key);
//        $token_encode = base64_encode($token);
//        $a['content']['code'] = $token_encode;
//        $a['content']['random'] = randomString(11);
//        $a['content']['id_imaji'] = $t['details']['id_imaji'];

        $a['content']['tracker'] = $this->load->view('template/vtracker',$a,TRUE);

        $strict = array();
        $cid = $t['details']['id_imaji'];
        if(in_array($cid,$strict)){
            $this->load->view('pages/adult/vviewimaji',$a,false);
        }else{
            $this->load->view('pages/vviewimaji',$a,FALSE);
        }
    }
}
