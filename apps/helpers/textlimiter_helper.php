<?php

function textlimit($string_text,$number_limit){
    $tmp = ((strlen($string_text) > $number_limit)) ? (substr($string_text,0,$number_limit - 4) . " ...") : $string_text;
    return $tmp;
}

function climiter($str,$n = 500,$end_char = '...'){
    $len = strlen(trim($str)); //length of original string
    if(strlen($str) < $n){
        return $str;
    }
    $str = preg_replace("/\s+/",' ',preg_replace("/(\r\n|\r|\n)/"," ",$str));
    if(strlen($str) <= $n){
        return $str;
    }
    $out = "";
    foreach(explode(' ',trim($str)) as $val){
        $out .= $val . ' ';
        if(strlen(trim($out)) == $len){ // if length not change return original string
            return trim($out);
        }elseif(strlen($out) >= $n){
            return trim($out) . $end_char;
        }
    }
}

function cleanTrackTitle($str){
    $a = strip_tags($str);
    $b = trim(addslashes($a));
    $charnum = 60;
    if(strlen($b) > $charnum){
        $title = climiter($b,$charnum,'...');
    }else{
        $title = $b;
    }

    return $title;
}

function cleanTrackSummary($str){
    $a = strip_tags($str);
    $b = trim(addslashes($a));
    $charnum = 120;
    if(strlen($b) > $charnum){
        $title = climiter($b,$charnum,'...');
    }else{
        $title = $b;
    }

    return $title;
}

function cleanText($str){
    $char = array('\'','"');
    $replace_char = trim(str_replace($char,'',$str));
    $out = trim(strip_tags($replace_char));

    return $out;
}

function cleanHTML($htmltext){
    $patt = array('@<script[^>]*?>.*?</script>@si','@<[\\/\\!]*?[^<>]*?>@si','@<style[^>]*?>.*?</style>@siU','@<![\\s\\S]*?--[ \\t\\n\\r]*>@');
    $text = preg_replace($patt,'',$htmltext);

    $char = array('\'','"');
    $replace_char = str_replace($char,'',$text);
    $out = strip_tags($replace_char);

    $strout = trim(reduce_double_slashes($out));

    return $strout;
}

function cleanWords($str){
    $replace_char = str_replace('”','"',$str);
    $filter = strip_tags(stripslashes($replace_char),'<em>');
    $output = trim(preg_replace("/[^a-zA-Z0-9 \.\(\,\)\:\'\/\<\>\#\@\|\?\!\%\&\*\-\+\"]/","",$filter));
    return reduce_double_slashes($output);
}

function cleanTitle($str){
    $replace_char = str_replace('”','"',$str);
    $output = stripslashes($replace_char);
    return $output;
}

function filterText($str){
    $replace_lt = str_replace('&lt;','<',$str);
    $replace_gt = str_replace('&gt;','>',$replace_lt);
    $output = trim($replace_gt);

    return $output;
}

function filterTitle($string){
    $utf8 = utf8_encode($string);
    $out = trim($utf8);

    return $out;
}

function filterSummary($string){
    $clean_n = trim(str_replace("\n","",$string));

    $utf8 = utf8_encode($clean_n);
    $br_html5 = trim(str_replace('<br />','<br>',html_entity_decode($utf8)));

    $div_remove = trim(preg_replace('/\<[\/]{0,1}div[^\>]*\>/i','',$br_html5));
    $stripped = trim(preg_replace('/^(?:<br\s*\/?>\s*)+/','',$div_remove));
    $char_replace = trim(htmlspecialchars_decode($stripped,ENT_QUOTES));

    return $char_replace;
}

function fbFilter($input){
    $html_decode = trim(html_entity_decode(stripslashes($input)));
    $br_html5 = trim(str_replace('<br />','<br>',$html_decode));
    $br_image = trim(preg_replace('#<br><img(.*?)/><br>#is','<br><br><img$1><br><br>',$br_html5));
    $br_endstring = rtrim($br_image,'<br>');
    $br_p = trim(str_replace('<br><br>','</p><p>&nbsp;</p><p>',$br_endstring));
    $remove_single_br = trim(str_replace('<br>','',$br_p));
    $empty_tags = trim(preg_replace('#<(p|strong|span)><\/(p|strong|span)>#is','',$remove_single_br));
    $div_remove = trim(preg_replace('/\<[\/]{0,1}div[^\>]*\>/i','[ffr]',$empty_tags));
    $div_end_remove = trim(str_replace('</div','[ffr]',$div_remove));
    $ffr_remove = trim(str_replace('[ffr]','',$div_end_remove));
    $image_figure = trim(preg_replace('#<img(.*?)>#is','<figure><img$1></figure>',$ffr_remove));
    $iframe = trim(preg_replace('#<iframe(.*?)>(.*?)</iframe>#is','<figure class="op-interactive"><iframe$1>$2</iframe></figure>',$image_figure));
    $twitter = trim(preg_replace('#<blockquote(.*?)>(.*?)</blockquote>(.*?)<script(.*?)>(.*?)</script>#is','',$iframe));
    $table = trim(preg_replace('#<table(.*?)>(.*?)</table>#is','<figure class="op-interactive"><table>$2</table></figure>',$twitter));
    $figure_clear = trim(preg_replace('#<p(.*?)>(.*?)<figure(.*?)(.*?)</figure></p>#is','<figure$3$4</figure>',$table));
    $correct_figure_ads = trim(preg_replace('#<figure(.*?)>(.*?)<figure(.*?)>(.*?)</figure>(.*?)</figure>#is','<figure$1>$4</figure>',$figure_clear));

    $output = $correct_figure_ads;
    return $output;
}

function newsStandFilter($input,$alt){
    $article_content = preg_replace('/data:image([^"]*)/'," ",$input);
    $first = preg_replace('/\<[\/]{0,1}div[^\>]*\>/i','<br />',$article_content);
    $second = str_replace('/\<img [^>]*src=\"([^\"]+)\"[^>]*>/','/<figure><img [^>]*src=\"([^\"]+)\"[^>]*><\/figure>/',$first);
    $third = str_replace('<iframe','<figure><center><iframe',$second);
    $fourth = str_replace('iframe>','iframe></center></figure>',$third);
    $five = str_replace('alt=""','alt="' . cleanHTML($alt) . '"',$fourth);
    $six = str_replace('&nbsp;','',$five);

    $explode_content = explode('<br />',$six);
    $filteredarray = array_values(array_filter($explode_content,create_function('$a','return preg_match("#\S#", $a);')));

    $combine = implode('</p><p>',$filteredarray);
    return $combine;
}

function instagramRegex($tag){
    $pattern = '#\[instaOpen\](.*?)\[instaClose\]#is';
    $output = preg_replace_callback($pattern,function($match){
        return '[instaOpen]' . base64_encode($match[1]) . '[instaClose]';
    },$tag);

    return $output;
}

function instagramRollback($tag){
    $pattern = '#\[instaOpen\](.*?)\[instaClose\]#is';
    $output = preg_replace_callback($pattern,function($match){
        return '[instaOpen]' . base64_decode($match[1]) . '[instaClose]';
    },$tag);

    return $output;
}

function twitterRegex($twtag){
    $pattern = '#\[twOpen\](.*?)\[twClose\]#is';
    $output = preg_replace_callback($pattern,function($match){
        return '[twOpen]' . base64_encode($match[1]) . '[twClose]';
    },$twtag);

    return $output;
}

function twitterRollback($twtag){
    $pattern = '#\[twOpen\](.*?)\[twClose\]#is';
    $output = preg_replace_callback($pattern,function($match){
        return '[twOpen]' . base64_decode($match[1]) . '[twClose]';
    },$twtag);

    return $output;
}

function youtubeRegex($twtag){
    $pattern = '#\[twOpen\](.*?)\[twClose\]#is';
    $output = preg_replace_callback($pattern,function($match){
        return '[twOpen]' . base64_encode($match[1]) . '[twClose]';
    },$twtag);

    return $output;
}

function youtubeRollback($twtag){
    $pattern = '#\[twOpen\](.*?)\[twClose\]#is';
    $output = preg_replace_callback($pattern,function($match){
        return '[twOpen]' . base64_decode($match[1]) . '[twClose]';
    },$twtag);

    return $output;
}

function replaceSindoURL($url){
    $pattern = '#<a(.*?)href="(.*?):\/\/(.*?)\.(.*?)/(.*?)"(.*?)>(.*?)</a>#is';
    $output = preg_replace_callback($pattern,function($match){
        if($match[4] != 'sindonews.com'){
            $link = '<a target="blank" href="' . $match[2] . '://' . $match[3] . '.' . $match[4] . '/' . $match[5] . '">' . $match[7] . '</a>';
        }else{
            $link = '<a href="' . $match[2] . '://' . $match[3] . '.' . $match[4] . '/' . $match[5] . '">' . $match[7] . '</a>';
        }
        return $link;
    },$url);

    return $output;
}

function fixInlineURL($url){
    $pattern = '#<a(.*?)href="(.*?)">(.*?)</a>#is';
    $output = preg_replace_callback($pattern,function($match){
        $linkurl = trim(str_replace('" target="_blank','',$match[2]));
        $titleurl = $match[3];
        $urlparse = parse_url($linkurl);

        if(filter_var($linkurl,FILTER_VALIDATE_URL) && $urlparse['scheme'] == 'https'){
            $getHostExtract = explode('.',$urlparse['host']);
            $domainUrl = $getHostExtract[1] . '.' . $getHostExtract[2];

            if($domainUrl == 'sindonews.com' || $domainUrl == 'sindo.media'){
                $newurl = '<a href="' . $linkurl . '">' . trim($titleurl) . '</a>';
            }else{
                $newurl = '<a target="_blank" class="ext-link" href="' . $linkurl . '">' . trim($titleurl) . '</a>';
            }
        }else{
            $newurl = trim($titleurl);
        }

        return $newurl;
    },$url);

    return $output;
}

function getIframe($string){
    $pattern = "/<iframe(.*?)>(.*?)<\/iframe>/";
    $output = preg_replace_callback($pattern,function($match){
        $newurl = '<div class="v-youtube">' . $match[0] . '</div>';
        return $newurl;
    },$string);

    return $output;
}

function contentFilter($input,$alt){
    $clear_mso = trim(preg_replace('#<\!--\[(.*?)\]-->#is','',$input));
    $br_html5 = trim(str_replace('<br />','<br>',html_entity_decode($clear_mso)));
    $replace_div_ltr = trim(str_replace('<div dir="ltr"></div>','<br><br>',$br_html5));

    $endlineA = str_replace("\n","[space]",$replace_div_ltr);
    $endlineB = str_replace("\r","[space]",$endlineA);
    $get_nbsp = htmlentities($endlineB);

    $clean_nbsp = trim(str_replace('&nbsp;','',stripslashes($get_nbsp)));
    $decode = html_entity_decode($clean_nbsp);
    $remove_space_tags = trim(str_replace('[space]','',$decode));
    $cdn_replace = str_replace("http://pict.sindonews.net","https://pict-c.sindonews.net",$remove_space_tags);

    $instagram_patern = '#<blockquote class="instagram-media"(.*?)src="//www.instagram.com/embed.js"(.*?)<\/script>#is';
    $instagram_replace = '[instaOpen]<blockquote class="instagram-media"$1src="https://www.instagram.com/embed.js"$2</script>[instaClose]';
    $instagram_tag = trim(preg_replace($instagram_patern,$instagram_replace,$cdn_replace));
    $instagram_encode = instagramRegex($instagram_tag);

    $twitter_patern = '#<blockquote class="twitter-tweet"(.*?)platform.twitter.com/widgets.js(.*?)<\/script>#is';
    $twitter_replace = '[twOpen]<blockquote class="twitter-tweet"$1platform.twitter.com/widgets.js$2</script>[twClose]';
    $twitter_tag = trim(preg_replace($twitter_patern,$twitter_replace,$instagram_encode));
    $twitter_encode = twitterRegex($twitter_tag);

    $replace_url_sindo = trim(fixInlineURL($twitter_encode));

    $endline_remove = preg_replace("/\r|\n/","",$replace_url_sindo);
    $start_quote_replace = trim(str_replace('&ldquo;','"',$endline_remove));
    $end_quote_replace = trim(str_replace('&rdquo;','"',$start_quote_replace));
    $space_replace = trim(preg_replace('#(.*?)&nbsp;(.*?)#is','$1$2',$end_quote_replace));

    $br_strong_switch = trim(str_replace('<br></strong>','</strong><br>',$space_replace));
    $br_more = trim(preg_replace('/(<br>)+$/','',$br_strong_switch));
    $div_remove = trim(preg_replace('/\<[\/]{0,1}div[^\>]*\>/i','',$br_more));
    $br_to_enter = trim(str_replace('<br><br>','[enter]',$div_remove));
    $remove_last_br = trim(preg_replace('/(<br>)+$/','',$br_to_enter));
    $singlebr_to_enter = trim(str_replace('<br>','[enter]',$remove_last_br));

    $clean_img_strong = trim(preg_replace('#<strong><img(.*?)></strong>#is','<img$1>',$remove_last_br));
    $image_tag = trim(preg_replace('#<img src="(.*?)"(.*?)width="(.*?)" height="(.*?)"(.*?)>#is','[imgOpen]<img class="lazyload" data-src="$1" width="$3" height="$4" alt="' . $alt . '">[imgClose]',$clean_img_strong));
    $imageOpen_to_figure = trim(str_replace('[imgOpen]','<div class="image-content">',$image_tag));
    $imageClose_to_figure = trim(str_replace('[imgClose]','</div>',$imageOpen_to_figure));

    $empty_strong = trim(str_replace('<strong></strong>','',$imageClose_to_figure));
    $replace_enter = trim(str_replace('[enter]','<br><br>',$empty_strong));
    $arr_br = array('<br><br><br><br><br><br>','<br><br><br><br><br>','<br><br><br><br>','<br><br><br>');
    $multiple_br = trim(str_replace($arr_br,'<br><br>',$replace_enter));

    $clear_colgroup = trim(preg_replace('#<colgroup(.*?)>(.*?)<\/colgroup>#is','',$multiple_br));
    $clear_col = trim(preg_replace('#<col(.*?)>(.*?)<\/col>#is','',$clear_colgroup));
    $clear_table = trim(preg_replace('#<table(.*?)>#is','<table>',$clear_col));
    $clear_thead = trim(preg_replace('#<thead(.*?)>#is','<thead>',$clear_table));
    $clear_tbody = trim(preg_replace('#<tbody(.*?)>#is','<tbody>',$clear_thead));
    $clear_tr = trim(preg_replace('#<tr(.*?)>#is','<tr>',$clear_tbody));
    $clear_td = trim(preg_replace('#<td(.*?)>#is','<td>',$clear_tr));

    $insert_box_table = trim(preg_replace('#<table>(.*?)<\/table>#is','<div class="table-box"><table>$1</table></div>',$clear_td));

    /*
      $youtube_patern = '#<iframe(.*?)src="https://www.youtube.com/embed(.*?)"(.*?)><\/iframe>#is';
      $youtube_replace = '<div class="v-youtube"><iframe$1src="https://www.youtube.com/embed$2"$3></iframe></div>';
      $youtube_tag = trim(preg_replace($youtube_patern,$youtube_replace,$multiple_br));
      //$youtube_slash = str_replace('//www.youtube','https://www.youtube',$youtube_tag);
      //$youtube_tag_slash = str_replace('https:https://www.youtube','https://www.youtube',$youtube_slash);

      $sindonews_patern = '#<iframe(.*?)video.sindonews.com/embedjw(.*?)<\/iframe>#is';
      $sindonews_replace = '<div class="v-youtube"><iframe$1video.sindonews.com/embedjw$2$3</iframe></div>';
      $sindonews_tag = trim(preg_replace($sindonews_patern,$sindonews_replace,$youtube_tag));
     */

    $convertiframe = getIframe($insert_box_table);

    $insta_rollback = instagramRollback($convertiframe);
    $instaOpen = trim(str_replace('[instaOpen]','<div class="social-embed">',$insta_rollback));
    $instaClose = trim(str_replace('[instaClose]','</div>',$instaOpen));

    $tw_rollback = twitterRollback($instaClose);
    $twOpen = trim(str_replace('[twOpen]','<div class="social-embed">',$tw_rollback));
    $twClose = trim(str_replace('[twClose]','</div>',$twOpen));

    $br_4 = trim(str_replace('<br><br> <br><br>','<br><br>',$twClose));
    $double_slash = trim(reduce_double_slashes($br_4));
    $replace_link = trim(replaceSindoURL($double_slash));
    
    $space_before_link = trim(str_replace('<a',' <a',$replace_link));
    $space_after_link = trim(str_replace('a>','a> ',$space_before_link));
    $remove_over_space = trim(preg_replace('!\s+!', ' ', $space_after_link));

    $output = stripslashes($remove_over_space);
    return $output;
}

function ampFilter($input,$alt){
    $clear_mso = trim(preg_replace('#<\!--\[(.*?)\]-->#is','',$input));
    $br_html5 = trim(str_replace('<br />','<br>',html_entity_decode($clear_mso)));
    $replace_div_ltr = trim(str_replace('<div dir="ltr"></div>','<br><br>',$br_html5));

    $replace_url_sindo = trim(fixInlineURL($replace_div_ltr));

    $endlineA = str_replace("\n","[space]",$replace_url_sindo);
    $endlineB = str_replace("\r","[space]",$endlineA);
    $get_nbsp = htmlentities($endlineB);

    $clean_nbsp = trim(str_replace('&nbsp;','',stripslashes($get_nbsp)));
    $decode = html_entity_decode($clean_nbsp);
    $remove_space_tags = trim(str_replace('[space]','',$decode));

    $clear_blockquote = trim(preg_replace('#<blockquote(.*?)>(.*?)<\/blockquote>#is','',$remove_space_tags));
    $clear_image = trim(preg_replace('#<img(.*?)>#is','',$clear_blockquote));
    $clear_iframe = trim(preg_replace('#<iframe(.*?)><\/iframe>#is','',$clear_image));
    $clear_noscript = trim(preg_replace('#<noscript(.*?)>(.*?)<\/noscript>#is','',$clear_iframe));
    $clear_js = trim(preg_replace('#<script(.*?)>(.*?)<\/script>#is','',$clear_noscript));
    $clear_span = trim(preg_replace('#<span(.*?)>(.*?)<\/span>#is','$2',$clear_js));
    $clear_em = trim(preg_replace('#<em(.*?)>(.*?)<\/em>#is','<em>$2</em>',$clear_span));
    $clear_strong = trim(preg_replace('#<strong(.*?)>(.*?)<\/strong>#is','<strong>$2</strong>',$clear_em));

    $endline_remove = preg_replace("/\r|\n/","",$clear_strong);
    $start_quote_replace = trim(str_replace('&ldquo;','"',$endline_remove));
    $end_quote_replace = trim(str_replace('&rdquo;','"',$start_quote_replace));
    $space_replace = trim(preg_replace('#(.*?)&nbsp;(.*?)#is','$1$2',$end_quote_replace));

    $br_strong_switch = trim(str_replace('<br></strong>','</strong><br>',$space_replace));
    $br_more = trim(preg_replace('/(<br>)+$/','',$br_strong_switch));
    $div_remove = trim(preg_replace('/\<[\/]{0,1}div[^\>]*\>/i','',$br_more));
    $br_to_enter = trim(str_replace('<br><br>','[enter]',$div_remove));
    $remove_last_br = trim(preg_replace('/(<br>)+$/','',$br_to_enter));
    $singlebr_to_enter = trim(str_replace('<br>','[enter]',$remove_last_br));

    $replace_enter = trim(str_replace('[enter]','<br><br>',$singlebr_to_enter));
    $arr_br = array('<br><br><br><br><br><br>','<br><br><br><br>');
    $multiple_br = trim(str_replace($arr_br,'<br><br>',$replace_enter));

    $clear_colgroup = trim(preg_replace('#<colgroup(.*?)>(.*?)<\/colgroup>#is','',$multiple_br));
    $clear_col = trim(preg_replace('#<col(.*?)>(.*?)<\/col>#is','',$clear_colgroup));
    $clear_table = trim(preg_replace('#<table(.*?)>#is','<table>',$clear_col));
    $clear_thead = trim(preg_replace('#<thead(.*?)>#is','<thead>',$clear_table));
    $clear_tbody = trim(preg_replace('#<tbody(.*?)>#is','<tbody>',$clear_thead));
    $clear_tr = trim(preg_replace('#<tr(.*?)>#is','<tr>',$clear_tbody));
    $clear_td = trim(preg_replace('#<td(.*?)>#is','<td>',$clear_tr));

    $insert_box_table = trim(preg_replace('#<table>(.*?)<\/table>#is','<div class="table-box"><table>$1</table></div>',$clear_td));

    $arabOpen = trim(str_replace('[arabOpen]','<div class="content-arab">',$insert_box_table));
    $arabClose = trim(str_replace('[arabClose]','</div>',$arabOpen));

    $br_4 = trim(str_replace('<br><br> <br><br>','<br><br>',$arabClose));
    $double_slash = trim(reduce_double_slashes($br_4));
    
    $space_before_link = trim(str_replace('<a',' <a',$double_slash));
    $space_after_link = trim(str_replace('a>','a> ',$space_before_link));
    $remove_over_space = trim(preg_replace('!\s+!', ' ', $space_after_link));

    $output = stripslashes($remove_over_space);

    return $output;
}

function ampFilterOld($input,$alt){
    $article_content = trim(preg_replace('/data:image([^"]*)/'," ",html_entity_decode($input)));

    $img_a = trim(preg_replace('/border="\d*"\s/','',$article_content));
    $img_b = trim(preg_replace('/alt="\d*"\s/','',$img_a));
    $img_c = trim(preg_replace('/(<img[^><]*)\/>/i','$1 alt="' . cleanHTML($alt) . '">',$img_b));

    $first = preg_replace('/<img (.*?)>/','<amp-img layout="responsive" $1></amp-img>',$img_c);
    $second = trim(str_replace('<br />','<br>',$first));
    $third = str_replace("http://pict.sindonews.net","https://pict.sindonews.net",$second);
    $four = trim(preg_replace('/(<?)[ ]?target="_blank"(.*?)/','',$third));
    $five = trim(preg_replace('/\<[\/]{0,1}div[^\>]*\>/i','<br>',$four));
    $six = trim(str_replace('<iframe','<div class="v-youtube"><iframe',$five));
    $seven = trim(str_replace('iframe>','iframe></div>',$six));
    $eight = trim(str_replace('alt=""','alt="' . cleanHTML($alt) . '"',$seven));
    $msword_remove = trim(preg_replace('/<!--\[if[^\]]*]>.*?<!\[endif\]-->/i','',$eight));
    $style_remove = trim(preg_replace('#(<[a-z ]*)(style=("|\')(.*?)("|\'))([a-z ]*>)#','\\1\\6',$msword_remove));
    $strict = trim(preg_replace('/(<[^>]+) style=".*?"/i','$1',$msword_remove));
    $span_remove = trim(preg_replace('#</?span[^>]*>#is','',$strict));

    //$twitter = trim(preg_replace('/<blockquote class="twitter-tweet"[^>]*>.*\n*.*twitter.com\/\w*\/status\/(\d*)".*<\/blockquote>/','<amp-twitter width=486 height=657 layout="responsive" data-tweetid="$1"></amp-twitter>',$span_remove));
    $twitter = trim(preg_replace('/<blockquote class="twitter-tweet"[^>]*>.*<\/blockquote>/','',$span_remove));
    $youtube = trim(preg_replace('/<iframe\b[^>]*width="([^"]*)"[^>]*height="([^"]*)"[^>]*youtube.com\/embed\/([^"]*)[^>]*>(.*?)>/','<amp-youtube width="$1" height="$2" data-videoid="$3" layout="responsive"></amp-youtube>',$twitter));
    $vimeo = trim(preg_replace('/<iframe\b[^>]*vimeo.com\/video\/(\d*).*"[^>]*width="([^"]*)"[^>]*height="([^"]*)"[^>]*[^>]*>(.*?)>/','<amp vimeo layout="responsive" width="$2" height="$3" data-videoid="$1"></amp-vimeo>',$youtube));

    $output = trim(stripslashes(html_entity_decode($vimeo)));

    return $output;
}

function ucFilter($input){
    $clear_mso = trim(preg_replace('#<\!--\[(.*?)\]-->#is','',$input));
    $br_html5 = trim(str_replace('<br />','<br>',html_entity_decode($clear_mso)));
    $replace_div_ltr = trim(str_replace('<div dir="ltr"></div>','<br><br>',$br_html5));

    $endlineA = str_replace("\n","[space]",$replace_div_ltr);
    $endlineB = str_replace("\r","[space]",$endlineA);
    $get_nbsp = htmlentities($endlineB);

    $clean_nbsp = trim(str_replace('&nbsp;','',stripslashes($get_nbsp)));
    $decode = html_entity_decode($clean_nbsp);
    $remove_space_tags = trim(str_replace('[space]','',$decode));

    $image_replace = preg_replace('/<img src=(.*?) width=(.*?) height=(.*?) alt=(.*?)>/','<res-image data-src=$1 data-width=$2 data-height=$3></res-image>',$remove_space_tags);

    $clear_blockquote = trim(preg_replace('#<blockquote(.*?)>(.*?)<\/blockquote>#is','',$image_replace));
    $clear_iframe = trim(preg_replace('#<iframe(.*?)><\/iframe>#is','',$clear_blockquote));
    $clear_noscript = trim(preg_replace('#<noscript(.*?)>(.*?)<\/noscript>#is','',$clear_iframe));
    $clear_js = trim(preg_replace('#<script(.*?)>(.*?)<\/script>#is','',$clear_noscript));
    $clear_span = trim(preg_replace('#<span(.*?)>(.*?)<\/span>#is','$2',$clear_js));
    $clear_em = trim(preg_replace('#<em(.*?)>(.*?)<\/em>#is','<em>$2</em>',$clear_span));
    $clear_strong = trim(preg_replace('#<strong(.*?)>(.*?)<\/strong>#is','<strong>$2</strong>',$clear_em));

    $endline_remove = preg_replace("/\r|\n/","",$clear_strong);
    $start_quote_replace = trim(str_replace('&ldquo;','"',$endline_remove));
    $end_quote_replace = trim(str_replace('&rdquo;','"',$start_quote_replace));
    $space_replace = trim(preg_replace('#(.*?)&nbsp;(.*?)#is','$1$2',$end_quote_replace));

    $br_strong_switch = trim(str_replace('<br></strong>','</strong><br>',$space_replace));
    $br_more = trim(preg_replace('/(<br>)+$/','',$br_strong_switch));
    $div_remove = trim(preg_replace('/\<[\/]{0,1}div[^\>]*\>/i','',$br_more));
    $br_to_enter = trim(str_replace('<br><br>','[enter]',$div_remove));
    $remove_last_br = trim(preg_replace('/(<br>)+$/','',$br_to_enter));
    $singlebr_to_enter = trim(str_replace('<br>','[enter]',$remove_last_br));

    $replace_enter = trim(str_replace('[enter]','<br><br>',$singlebr_to_enter));
    $arr_br = array('<br><br><br><br><br><br>','<br><br><br><br>');
    $multiple_br = trim(str_replace($arr_br,'<br><br>',$replace_enter));

    $double_slash = trim(reduce_double_slashes($multiple_br));
    $output = stripslashes($double_slash);

    return $output;
}

function clearMeta($str){
    $char = array('-');
    $out = str_replace($char,'',cleanHTML($str));
    $output = preg_replace("/[^0-9a-zA-Z-]/",',',$out);

    return $output;
}

function clearDesc($str){
    $start = strip_tags($str);
    $string = preg_replace("/[^A-Za-z]/"," ",$start);

    $char = array('\'','"');
    $a = str_replace($char,'',$string);
    $b = climiter($a,120,'.');

    $output = trim($b);

    return $output;
}

function seo_title($text){
    $input = html_entity_decode($text);
    $double_quoute = array('"','”');
    $remove_double_quoute = trim(str_replace($double_quoute,'',$input));
    $str = trim(strip_tags($remove_double_quoute));
    $charnum = 60;
    if(strlen($str) > $charnum){
        $title = climiter($str,$charnum,'...');
    }else{
        $title = $str;
    }

    return $title;
}

function seo_description($text){
    $input = html_entity_decode($text);
    $double_quoute = array('"','”');
    $remove_double_quoute = trim(str_replace($double_quoute,'',$input));
    $str = trim(strip_tags($remove_double_quoute));
    $charnum = 160;
    if(strlen($str) > $charnum){
        $title = climiter($str,$charnum,'...');
    }else{
        $title = $str;
    }

    return $title;
}

function cleanTitleArticle($str){
    $double_quoute = array('"','”');
    $remove_double_quoute = trim(str_replace($double_quoute,'',$str));
    $clear_tags = trim(strip_tags($remove_double_quoute,'<em>'));
    $output = stripslashes($clear_tags);
    
    return $output;
}

function cleanSummary($str){
    $double_quoute = array('"','”');
    $remove_double_quoute = trim(str_replace($double_quoute,'',$str));
    $clear_tags = trim(strip_tags($remove_double_quoute));
    $output = stripslashes($clear_tags);
    
    return $output;
}

function summaryMore($content){
    $replace_br = str_replace('<br /><br />','<br><br>',$content);
    $clear_double_quotes = str_replace('"','',$replace_br);
    $clear_tags = strip_tags($clear_double_quotes);
    $explode = array_values(explode('<br><br>',$clear_tags));
    $first_paragraph = $explode[0];
    if(strlen($first_paragraph) > 80){
        $out = climiter($first_paragraph,80,'...');
    }else{
        $out = $first_paragraph;
    }

    return $out;
}

function seo_keywords($string,$numWords = 2,$limit = 3){
    // make case-insensitive
    $string = xwords($string);

    // get all words. Assume any 1 or more letter, number or ' in a row is a word 
    preg_match_all('~[a-z\']+~',$string,$words);
    $words = $words[0];
    //$words = array_diff($words,$xwords);
    //$words = array_filter($words);
    // foreach word...
    foreach($words as $k => $v){
        // remove single quotes that are by themselves or wrapped around the word
        $words[$k] = trim($words[$k],"'");
    } // end foreach $words
    // remove any empty elements produced from ' trimming
    $words = array_filter($words);
    // reset array keys
    $words = array_values($words);
    // foreach word...  	
    foreach($words as $k => $word){
        // if there are enough words after the current word to make a $numWords length phrase... 
        if(isset($words[$k + $numWords])){
            // add the phrase to list of phrases
            $phrases[] = implode(' ',array_slice($words,$k,$numWords));
        } // end if isset
    } // end foreach $words
    // create an array of phrases => count
    $x = array_count_values($phrases);
    // reverse sort it (preserving keys, since the keys are the phrases
    arsort($x);
    // if limit is specified, return only $limit phrases. otherwise, return all of them
    $a = ($limit > 0) ? array_slice($x,0,$limit) : $x;
    $b = array_keys($a);
    $c = implode(',',$b);

    return $c;
}

function seo_keywordsB($string,$numWords = 2,$limit = 3){
    // make case-insensitive
    $string = xwords($string);

    // get all words. Assume any 1 or more letter, number or ' in a row is a word 
    preg_match_all('~[a-z\']+~',$string,$words);
    $words = $words[0];
    //$words = array_diff($words,$xwords);
    //$words = array_filter($words);
    // foreach word...
    foreach($words as $k => $v){
        // remove single quotes that are by themselves or wrapped around the word
        $words[$k] = trim($words[$k],"'");
    } // end foreach $words
    // remove any empty elements produced from ' trimming
    $words = array_filter($words);
    // reset array keys
    $words = array_values($words);
    // foreach word...  	
    foreach($words as $k => $word){
        // if there are enough words after the current word to make a $numWords length phrase... 
        if(isset($words[$k + $numWords])){
            // add the phrase to list of phrases
            $phrases[] = implode(' ',array_slice($words,$k,$numWords));
        }else{
            $phrases = $words;
        } // end if isset
    } // end foreach $words
    // create an array of phrases => count
    $x = array_count_values($phrases);
    // reverse sort it (preserving keys, since the keys are the phrases
    arsort($x);
    // if limit is specified, return only $limit phrases. otherwise, return all of them
    $a = ($limit > 0) ? array_slice($x,0,$limit) : $x;
    $b = array_keys($a);
    $c = implode('+',$b);

    return $c;
}

function xDebug($str){
    echo '<pre>';
    print_r($str);
    echo '</pre>';
}

function badKeywords($string){
    $str = strtolower($string);
    $key = array(
        'pemerkosa','diperkosa','memperkosa','perkosa','bunuh','pembunuhan','membunuh','pembunuh',
        'bercinta','seks','sex','sextoys','kondom','porno','kemaluan','tewas','penganiayaan','pisau',
        'pistol','kontrasepsi','miras','mati','perindo','tanoesoedibjo','orgasme','oral','ereksi','payudara',
        'seksi','mr p','miss v','alkohol','sabu','foreplay','skandal','bantai','pembantaian','membantai','telanjang',
        'bugil','nakal','nude','pemukulan','korban','kekerasan','penyiksaan','penganiayaan','sadis','tawuran',
        'perkelahian','perampokan','perampok','merampok','dirampok','dianiaya','disiksa','dipukul','mesum','cabul','biadab','psk');
    $implode = implode('|',$key);

    $x = FALSE;
    if(preg_match('/' . $implode . '/i',$str)){
        $x = TRUE;
    }

    return $x;
}

function xwords($string){
    $xwords = array(
        'pt','rp','juta','milyar','ada','adalah','adanya','adapun','agak','agaknya','agar','akan','akankah','akhir','akhiri',
        'akhirnya','aku','akulah','amat','amatlah','anda','andalah','antar','antara','antaranya','apa',
        'apaan','apabila','apakah','apalagi','apatah','artinya','asal','asalkan','atas','atau','ataukah',
        'ataupun','awal','awalnya','bagai','bagaikan','bagaimana','bagaimanakah','bagaimanapun','bagi',
        'bagian','bahkan','bahwa','bahwasanya','baik','bakal','bakalan','balik','banyak','bapak','baru',
        'bawah','beberapa','begini','beginian','beginikah','beginilah','begitu','begitukah','begitulah',
        'begitupun','bekerja','belakang','belakangan','belum','belumlah','benar','benarkah','benarlah',
        'berada','berakhir','berakhirlah','berakhirnya','berapa','berapakah','berapalah','berapapun',
        'berarti','berawal','berbagai','berdatangan','beri','berikan','berikut','berikutnya','berjumlah',
        'berkali-kali','berkata','berkehendak','berkeinginan','berkenaan','berlainan','berlalu',
        'berlangsung','berlebihan','bermacam','bermacam-macam','bermaksud','bermula','bersama',
        'bersama-sama','bersiap','bersiap-siap','bertanya','bertanya-tanya','berturut','berturut-turut',
        'bertutur','berujar','berupa','besar','betul','betulkah','biasa','biasanya','bila','bilakah',
        'bisa','bisakah','boleh','bolehkah','bolehlah','buat','bukan','bukankah','bukanlah','bukannya',
        'bulan','bung','cara','caranya','cukup','cukupkah','cukuplah','cuma','dahulu','dalam','dan','dapat',
        'dari','daripada','datang','dekat','demi','demikian','demikianlah','dengan','depan','di','dia',
        'diakhiri','diakhirinya','dialah','diantara','diantaranya','diberi','diberikan','diberikannya',
        'dibuat','dibuatnya','didapat','didatangkan','digunakan','diibaratkan','diibaratkannya','diingat',
        'diingatkan','diinginkan','dijawab','dijelaskan','dijelaskannya','dikarenakan','dikatakan',
        'dikatakannya','dikerjakan','diketahui','diketahuinya','dikira','dilakukan','dilalui','dilihat',
        'dimaksud','dimaksudkan','dimaksudkannya','dimaksudnya','diminta','dimintai','dimisalkan',
        'dimulai','dimulailah','dimulainya','dimungkinkan','dini','dipastikan','diperbuat','diperbuatnya',
        'dipergunakan','diperkirakan','diperlihatkan','diperlukan','diperlukannya','dipersoalkan',
        'dipertanyakan','dipunyai','diri','dirinya','disampaikan','disebut','disebutkan','disebutkannya',
        'disini','disinilah','ditambahkan','ditandaskan','ditanya','ditanyai','ditanyakan','ditegaskan',
        'ditujukan','ditunjuk','ditunjuki','ditunjukkan','ditunjukkannya','ditunjuknya','dituturkan',
        'dituturkannya','diucapkan','diucapkannya','diungkapkan','dong','dua','dulu','empat','enggak',
        'enggaknya','entah','entahlah','guna','gunakan','hal','hampir','hanya','hanyalah','hari',
        'harus','haruslah','harusnya','hendak','hendaklah','hendaknya','hingga','ia','ialah',
        'ibarat','ibaratkan','ibaratnya','ibu','ikut','ingat','ingat-ingat','ingin','inginkah',
        'inginkan','ini','inikah','inilah','itu','itukah','itulah','jadi','jadilah','jadinya','jangan',
        'jangankan','janganlah','jauh','jawab','jawaban','jawabnya','jelas','jelaskan','jelaslah',
        'jelasnya','jika','jikalau','juga','jumlah','jumlahnya','justru','kala','kalau','kalaulah',
        'kalaupun','kalian','kami','kamilah','kamu','kamulah','kan','kapan','kapankah','kapanpun',
        'karena','karenanya','kasus','kata','katakan','katakanlah','katanya','ke','keadaan','kebetulan',
        'kecil','kedua','keduanya','keinginan','kelamaan','kelihatan','kelihatannya','kelima','keluar',
        'kembali','kemudian','kemungkinan','kemungkinannya','kenapa','kepada','kepadanya','kesampaian',
        'keseluruhan','keseluruhannya','keterlaluan','ketika','khususnya','kini','kinilah','kira',
        'kira-kira','kiranya','kita','kitalah','kok','kurang','lagi','lagian','lah','lain','lainnya',
        'lalu','lama','lamanya','lanjut','lanjutnya','lebih','lewat','lima','luar','macam','maka',
        'makanya','makin','malah','malahan','mampu','mampukah','mana','manakala','manalagi','masa',
        'masalah','masalahnya','masih','masihkah','masing','masing-masing','mau','maupun','melainkan',
        'melakukan','melalui','melihat','melihatnya','memang','memastikan','memberi','memberikan',
        'membuat','memerlukan','memihak','meminta','memintakan','memisalkan','memperbuat','mempergunakan',
        'memperkirakan','memperlihatkan','mempersiapkan','mempersoalkan','mempertanyakan','mempunyai',
        'memulai','memungkinkan','menaiki','menambahkan','menandaskan','menanti','menanti-nanti',
        'menantikan','menanya','menanyai','menanyakan','mendapat','mendapatkan','mendatang',
        'mendatangi','mendatangkan','menegaskan','mengakhiri','mengapa','mengatakan','mengatakannya',
        'mengenai','mengerjakan','mengetahui','menggunakan','menghendaki','mengibaratkan','mengibaratkannya',
        'mengingat','mengingatkan','menginginkan','mengira','mengucapkan','mengucapkannya','mengungkapkan',
        'menjadi','menjawab','menjelaskan','menuju','menunjuk','menunjuki','menunjukkan','menunjuknya',
        'menurut','menuturkan','menyampaikan','menyangkut','menyatakan','menyebutkan','menyeluruh',
        'menyiapkan','merasa','mereka','merekalah','merupakan','meski','meskipun','meyakini','meyakinkan',
        'minta','mirip','misal','misalkan','misalnya','mula','mulai','mulailah','mulanya','mungkin',
        'mungkinkah','nah','naik','namun','nanti','nantinya','nyaris','nyatanya','oleh','olehnya','pada',
        'padahal','padanya','pak','paling','panjang','pantas','para','pasti','pastilah','penting',
        'pentingnya','per','percuma','perlu','perlukah','perlunya','pernah','persoalan','pertama',
        'pertama-tama','pertanyaan','pertanyakan','pihak','pihaknya','pukul','pula','pun','punya','rasa',
        'rasanya','rata','rupanya','saat','saatnya','saja','sajalah','saling','sama','sama-sama','sambil',
        'sampai','sampai-sampai','sampaikan','sana','sangat','sangatlah','satu','saya','sayalah','se',
        'sebab','sebabnya','sebagai','sebagaimana','sebagainya','sebagian','sebaik','sebaik-baiknya',
        'sebaiknya','sebaliknya','sebanyak','sebegini','sebegitu','sebelum','sebelumnya','sebenarnya',
        'seberapa','sebesar','sebetulnya','sebisanya','sebuah','sebut','sebutlah','sebutnya','secara',
        'secukupnya','sedang','sedangkan','sedemikian','sedikit','sedikitnya','seenaknya','segala',
        'segalanya','segera','seharusnya','sehingga','seingat','sejak','sejauh','sejenak','sejumlah',
        'sekadar','sekadarnya','sekali','sekali-kali','sekalian','sekaligus','sekalipun','sekarang',
        'sekarang','sekecil','seketika','sekiranya','sekitar','sekitarnya','sekurang-kurangnya',
        'sekurangnya','sela','selain','selaku','selalu','selama','selama-lamanya','selamanya','selanjutnya',
        'seluruh','seluruhnya','semacam','semakin','semampu','semampunya','semasa','semasih','semata',
        'semata-mata','semaunya','sementara','semisal','semisalnya','sempat','semua','semuanya','semula',
        'sendiri','sendirian','sendirinya','seolah','seolah-olah','seorang','sepanjang','sepantasnya',
        'sepantasnyalah','seperlunya','seperti','sepertinya','sepihak','sering','seringnya','serta',
        'serupa','sesaat','sesama','sesampai','sesegera','sesekali','seseorang','sesuatu','sesuatunya',
        'sesudah','sesudahnya','setelah','setempat','setengah','seterusnya','setiap','setiba','setibanya',
        'setidak-tidaknya','setidaknya','setinggi','seusai','sewaktu','siap','siapa','siapakah',
        'siapapun','sini','sinilah','soal','soalnya','suatu','sudah','sudahkah','sudahlah','supaya',
        'tadi','tadinya','tahu','tahun','tak','tambah','tambahnya','tampak','tampaknya','tandas',
        'tandasnya','tanpa','tanya','tanyakan','tanyanya','tapi','tegas','tegasnya','telah','tempat',
        'tengah','tentang','tentu','tentulah','tentunya','tepat','terakhir','terasa','terbanyak',
        'terdahulu','terdapat','terdiri','terhadap','terhadapnya','teringat','teringat-ingat','terjadi',
        'terjadilah','terjadinya','terkira','terlalu','terlebih','terlihat','termasuk','ternyata',
        'tersampaikan','tersebut','tersebutlah','tertentu','tertuju','terus','terutama','tetap',
        'tetapi','tiap','tiba','tiba-tiba','tidak','tidakkah','tidaklah','tiga','tinggi','toh',
        'tunjuk','turut','tutur','tuturnya','ucap','ucapnya','ujar','ujarnya','umum','umumnya',
        'ungkap','ungkapnya','untuk','usah','usai','waduh','wah','dan','atau','tetapi','tapi',
        'akantetapi','jika','kalau','karena','walau','walaupun','juga','jadi','maka','sehingga',
        'supaya','agar','hanya','lagi','lagipula','lalu','sambil','melainkan','namun','padahal',
        'sedangkan','demi','untuk','apabila','bilamana','sebab','sebabitu','karenaitu','bilamana',
        'asalkan','meskipun','biarpun','biar','seperti','daripada','bahkan','apalagi','yakni','adalah',
        'yaitu','ialah','bahwa','bahwasannya','kecuali','selain','misalnya','untukituwahai','waktu',
        'waktunya','walau','walaupun','wong','yaitu','yakin','yakni','yang','saya','kami','mereka',
        'kamu','anda','kita','aku','dia','kalian','engkau','kau'
    );

    $stepone = strtolower($string);
    $steptwo = strip_tags($stepone);
    $stepthree = preg_replace('/[^A-Za-z0-9?! ]/','',$steptwo);
    $explode = explode(' ',$stepthree);
    $compare = array_filter(array_diff($explode,$xwords));
    $implode = implode(' ',$compare);

    return $implode;
}
