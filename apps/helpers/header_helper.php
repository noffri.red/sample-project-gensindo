<?php

function metaname($meta = array()){
    $str = '';
    foreach($meta as $key => $val){
        $str .= '<meta name="' . $key . '" content="' . $val . '">';
    }
    return $str;
}

function metagoogle($meta = array()){
    $str = '';
    foreach($meta as $key => $val){
        $str .= '<meta itemprop="' . $key . '" content="' . $val . '">';
    }
    return $str;
}

function metafb($meta = array()){
    $str = '';
    foreach($meta as $key => $val){
        $str .= '<meta property="og:' . $key . '" content="' . $val . '">';
    }
    return $str;
}

function metaArticle($meta = array()){
    $str = '';
    foreach($meta as $key => $val){
        $str .= '<meta property="article:' . $key . '" content="' . $val . '">';
    }
    return $str;
}

function metaDable($meta = array()){
    $str = '';
    foreach($meta as $key => $val){
        $str .= '<meta property="dable:' . $key . '" content="' . $val . '">';
    }
    return $str;
}

function add_js($js = ''){
    $CI = & get_instance();
    $url = $CI->config->item('template_uri');
    $string = '<script src="' . $url . $js . '"></script>';
    return $string;
}

function async_m_add_js($js){
    $CI = &get_instance();
    $url = $CI->config->item('m_template_uri');
    $string = '<script async src="' . $url . $js . '"></script>';
    return $string;
}

function m_add_js($js){
    $CI = &get_instance();
    $url = $CI->config->item('m_template_uri');
    $string = '<script src="' . $url . $js . '"></script>';
    return $string;
}

function m_add_js_x($js = ''){
    $CI = & get_instance();
    $url = $CI->config->item('m_template_uri');
    $string = '<script src="' . $url . $js . '"></script>';
    return $string;
}

function add_external_js($js = ''){
    $string = '<script src="' . $js . '"></script>';
    return $string;
}

function add_css($css = ''){
    $CI = & get_instance();
    $url = $CI->config->item('template_uri');
    $string = '<link rel="stylesheet" href="' . $url . $css . '" media="all">';
    return $string;
}

function m_add_css($css = ''){
    $CI = & get_instance();
    $url = $CI->config->item('m_template_uri');
    $string = '<link rel="stylesheet" href="' . $url . $css . '" media="all">';
    return $string;
}

function add_external_css($css = ''){
    $string = '<link rel="stylesheet" href="' . $css . '">';
    return $string;
}

function add_preload_css($source){
    $CI = & get_instance();
    $url = $CI->config->item('template_uri_x');
    $string = '<link rel="preload" href="' . $url . $source . '" as="style">';
    return $string;
}

function m_add_preload_css($source){
    $CI = & get_instance();
    $url = $CI->config->item('m_template_uri_x');
    $string = '<link rel="preload" href="' . $url . $source . '" as="style">';
    return $string;
}

function add_preload_css_external($source){
    $string = '<link rel="preload" href="' . $source . '" as="style">';
    return $string;
}

function add_preload_js($source){
    $CI = & get_instance();
    $url = $CI->config->item('template_uri');
    $string = '<link rel="preload" href="' . $url . $source . '" as="script">';
    return $string;
}

function m_add_preload_js($source){
    $CI = & get_instance();
    $url = $CI->config->item('m_template_uri');
    $string = '<link rel="preload" href="' . $url . $source . '" as="script">';
    return $string;
}

function add_preload_js_external($source){
    $string = '<link rel="preload" href="' . $source . '" as="script">';
    return $string;
}

function core_cssjs(){
    $string = '<script>function loadCSS(e,t,n){"use strict"; var i=window.document.createElement("link");var o=t || window.document.getElementsByTagName("script")[0]; i.rel="stylesheet";i.href=e;i.media="only x";o.parentNode.insertBefore(i,o);setTimeout(function(){i.media=n || "all"})}</script>\n';
    return $string;
}

function csstojs($js){
    $CI = & get_instance();
    $string = '<script>loadCSS("' . $CI->config->item('template_uri') . $js . '");</script>\n';
    return $string;
}

function add_external_csstojs($js){
    $string = '<script type="text/javascript">loadCSS("' . $js . '");</script>\n';
    return $string;
}

function async_js($js){
    $CI = & get_instance();
    $file_loc = $CI->config->item('template_uri') . $js;
    $string = "
        <script>
            (function() {
                var sjs = document.createElement('script');
                sjs.type = 'text/javascript'; dr.async = true;
                sjs.src = '" . $file_loc . "';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dr);
            })();
        </script>";
    return $string;
}

function async_css($css){
    $CI = & get_instance();
    $file_loc = $CI->config->item('template_uri') . $css;
    $string = "
        <script>
            var element = document.createElement('link');
            element.rel = 'stylesheet';
            element.type = 'text/css';
            element.href = '" . $file_loc . "';
            element.media = 'all';
        </script>";
    return $string;
}

function m_async_css($css){
    $CI = &get_instance();
    $file_loc = $CI->config->item('m_template_uri') . $css;
    $string = "<script>(function(){var css = document.createElement('link');css.href='" . $file_loc . "';css.rel='stylesheet';css.type='text/css';document.getElementsByTagName('head')[0].appendChild(css);})();</script>";
    return $string;
}

function addMobileCss($csspath,$type = null){
    $media = (($type != null)) ? "media=\"" . $type . "\"" : "";

    $string = "<link rel=\"stylesheet\" " . $media . " href=\"" . $csspath . "\" type=\"text/css\">\n\t";
    return $string;
}

function addTitle($title){
    $string = "<title>" . $title . "</title>\n";
    return $string;
}

function add_canonical($type,$url){
    $string = "<link rel=\"" . $type . "\" href=\"" . $url . "\">\n";
    return $string;
}
