<?php

if (count($headline) > 0) {
    $i = 0;
    $dtime = strtotime($headline[$i]['date_created']);
    $img = images_uri() . '/dyn/1400/pena/news/' . $headline[$i]['created']['year'] . '/' . $headline[$i]['created']['month'] . '/' . $headline[$i]['created']['day'] . '/' . $headline[$i]['channel_id'] . '/' . $headline[$i]['content_id'] . '/' . $headline[$i]['image'];

    $link = site_url('read/' . $headline[$i]['content_id'] . '/' . $headline[$i]['channel_id'] . '/' . slug($headline[$i]['title']) . '-' . $dtime);
    $title = cleanWords($headline[$i]['title']);
    $summary = cleanWords($headline[$i]['summary']);
    $ago = time_difference($headline[$i]['publish']);
    // $ago = time_difference($headline[$i]['date_published']);


?>

    <div class="image"><a href="<?php echo $link ?>"><img class="lazyload" src="<?php echo $img ?>" alt="<?php echo $title ?>"></a></div>
    <div class="caption">
        <a href="<?php echo $link ?>" class="title"><?php echo $title ?></a>
        <div class="author">
            <spanArif class="time"><?php echo $ago ?></spanArif>
        </div>
        <div class="subtitle"><?php echo $summary ?></div>
    </div>
    
<?php } ?>