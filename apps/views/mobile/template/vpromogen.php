<section class="agenda">
    <div class="title-gensindo">Artikel Gen SINDO</div>
    <ul>
        <!--<li>
                <img src="<?php //echo template_uri(); ?>/images/ramadhan-gensindo-300-200.jpg" alt="Ramadhan Gen SINDO">
        </li>-->
        <li class="clearfix">
            <div class="text-msg">
                <div class="title">
                    Hai teman teman... <br><br>
                    Kami mengajak untuk mengembangkan bakat kamu menulis dengan mengirimkan artikel dalam bentuk word dengan panjang tulisan minimum 4000 karakter. <br><br>
                    Sertakan juga foto yang mendukung artikel kamu, identitas, serta foto terbaru kamu.<br><br>
                    Tunggu apalagi, segera kirim tulisan kamu ke :  <br><br>
                    <strong>gensindo[dot]mail[at]gmail[dot]com</strong>
                </div>
            </div>
        </li>
    </ul> 
</section>