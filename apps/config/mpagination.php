<?php
$config['mpaging']['full_tag_open'] = '<div class="mpaging"><ul>';
$config['mpaging']['full_tag_close'] = '</ul></div>';

$config['mpaging']['first_link'] = '<i class="fa fa-angle-double-left" aria-hidden="true"></i>';
$config['mpaging']['first_tag_open'] = '<li>';
$config['mpaging']['first_tag_close'] = '</li>';

$config['mpaging']['last_link'] = '<i class="fa fa-angle-double-right" aria-hidden="true"></i>';
$config['mpaging']['last_tag_open'] = '<li>';
$config['mpaging']['last_tag_close'] = '</li>';

$config['mpaging']['prev_link'] = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
$config['mpaging']['prev_tag_open'] = '<li>';
$config['mpaging']['prev_tag_close'] = '</li>';

$config['mpaging']['next_link'] = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
$config['mpaging']['next_tag_open'] = '<li>';
$config['mpaging']['next_tag_close'] = '</li>';

$config['mpaging']['cur_tag_open'] = '<li class="active">';
$config['mpaging']['cur_tag_close'] = '</li>';

$config['mpaging']['num_tag_open'] = '<li>';
$config['mpaging']['num_tag_close'] = '</li>';

$config['mpaging']['num_links'] = 1;