
<?php 
    
if (count($latest)>0){ 
    $total = count($latest);
    
    // var latest
    $data = $latest;
 
 
    echo '<ul class="scroll">';
    if (count($articledata) > 0) {
       
          $i = 0;
          $title[$i] = cleanWords($articledata[$i]['title']);
          $channel_name[$i] = $articledata[$i]['kanal'];
          $icon[$i] = '';
          $l[$i] = $articledata[$i]['url'];
          $data_summary[$i] = trim(base64url_decode(strip_tags($articledata[$i]['summary'])));
          $summ[$i] = $data_summary[$i];
          $img[$i] = $articledata[$i]['thumb'];
  
          $time_date_start[$i] = parseDateTime($articledata[$i]['date_start']);
          $time[$i] = $time_date_start[$i]['day_ind_name'] . ", " . $time_date_start[$i]['day'] . " " . $time_date_start[$i]['month_ind_name'] . " " . $time_date_start[$i]['year'];
          $time[$i] .= ' - ' . $time_date_start[$i]['hour'] . ":" . $time_date_start[$i]['minute'] . " WIB";
          $ago[$i] = time_difference($articledata[$i]['date_start']);
    
          echo '<li>
                    <div class="image"><a href="'.site_url('gnews').'"><img data-src="'.$img[$i].'" alt="'.$title[$i].'"></a></div>
                    <div class="block-caption">
                        <div class="title"><a href="'.$l[$i].'">'.$title[$i].'</a></div>
                        <div class="category gen-news"><a href="#">Gen News</a></div>
                        <div class="time">'.$ago[$i].'</div>
                    </div>
                </li>';
    
    }
   

 

        if(count($articledata)>0){ 
            // var article
            $i = 1;
            $title[$i] = cleanWords($articledata[$i]['title']);
            $channel_name[$i] = $articledata[$i]['kanal'];
            $icon[$i] = '';
            $l[$i] = $articledata[$i]['url'];
            $data_summary[$i] = trim(base64url_decode(strip_tags($articledata[$i]['summary'])));
            $summ[$i] = $data_summary[$i];
            $img[$i] = $articledata[$i]['thumb'];
    
            $time_date_start[$i] = parseDateTime($articledata[$i]['date_start']);
            $time[$i] = $time_date_start[$i]['day_ind_name'] . ", " . $time_date_start[$i]['day'] . " " . $time_date_start[$i]['month_ind_name'] . " " . $time_date_start[$i]['year'];
            $time[$i] .= ' - ' . $time_date_start[$i]['hour'] . ":" . $time_date_start[$i]['minute'] . " WIB";
    
            $ago[$i] = time_difference($articledata[$i]['date_start']);

            echo '<li>
                    <div class="image"><a href="'.site_url('gnews').'"><img data-src="'.$img[$i].'" alt="'.$title[$i].'"></a></div>
                    <div class="block-caption">
                        <div class="title"><a href="'.$l[$i].'">'.$title[$i].'</a></div>
                        <div class="category gen-news"><a href="#">Gen News</a></div>
                        <div class="time">'.$ago[$i].'</div>
                    </div>
                </li>';

           
        }

          
        for($i = 0; $i < $total; $i++){

            $id_content[$i] = $data[$i]['content_id'];
            $img[$i] = images_uri() . '/dyn/300/pena/news/' . date('Y/m/d/',strtotime($data[$i]['date_created'])) . '/' . $data[$i]['channel_id'] . '/' . $id_content[$i] . '/' . $data[$i]['image'];
            $subakanal[$i] = $data[$i]['channel_name'];
            $dtime[$i] = strtotime($data[$i]['date_created']);
            $link[$i] = site_url('read/' . $id_content[$i] . '/' . $data[$i]['channel_id'] . '/' . slug($data[$i]['title']) . '-' . $dtime[$i]);
        
            $title[$i] = cleanTitleArticle($data[$i]['title']);
            $summary[$i] = cleanSummary($data[$i]['summary']);
            $ago[$i] = time_difference($data[$i]['publish']);

            echo ' <li>
                        <div class="img image"><a href="' . $link[$i] . '"><img class="lazyload" data-src="' . $img[$i] . '" alt="'.$title[$i].'"></a></div>
                        <div class="block-caption">
                            <div class="title"><a href="' . $link[$i] . '">' . $title[$i] . '</a></div>
                            <div class="category gen-news"><a href="#">Gen News</a></div>
                            <div class="time">' . $ago[$i] . '</div>
                        </div>
                    </li>';

            }
        
        echo '
        <div class="news-link-more ">
            <div class="news-more">
                <a href="'. site_url('go/10').'">Load More</a>
            </div>
        </div>';
        echo '</ul>';
          
    }