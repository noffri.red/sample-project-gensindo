<?php

if(count($details) > 0){
    echo '
        <div class="bread">
            <a href="' . base_url() . '">home</a> <i class="fa fa-angle-double-right"></i> <a href="' . site_url('loadmore/' . $details['id_subkanal']) . '">' . strtolower($details['subkanal']) . '</a>
        </div>';

    echo '
        <div class="ads320 mb10">
            <amp-ad width="320" height="100"
                type="doubleclick"
                data-slot="' . $this->config->item('amp_gam_id') . '"
                json=\'{"targeting":{"pos":["billboard"]}}\'>
            </amp-ad>
        </div>';

    if(!empty($details['subtitle'])){
        $sub = '<h2>' . $details['subtitle'] . '</h2>';
    }else{
        $sub = '';
    }

    echo $sub;
    echo '<h1>' . cleanTitleArticle($details['title']) . '</h1>';

    echo '
        <div class="reporter">
            <a rel="author" href="https://index.sindonews.com/blog/' . $details['id_author'] . '/' . slug($details['author']) . '">' . ucwords($details['author']) . '</a>
        </div>';

    $datepublish = parseDateTime((string) $details['date_published']);
    $date_published = $datepublish['day_ind_name'] . ", " . $datepublish['day'] . " " . $datepublish['month_ind_name'] . " " . $datepublish['year'];
    $date_published .= ' - ' . $datepublish['hour'] . ":" . $datepublish['minute'] . " WIB";

    echo '<div class="time">' . $date_published . '</div>';

    $imgctn = '';
    if($details['photo'] != ''){
        $img = $this->config->item('images_uri') . '/dyn/620/pena/news/' . date('Y/m/d',strtotime($details['date_created'])) . '/' . $details['id_subkanal'] . '/' . $details['id_news'] . '/' . $details['photo'];
        
        if($details['caption_photo'] != ''){
            $caption = cleanWords($details['caption_photo']);
        }else{
            $caption = cleanWords($details['title']);
        }
        
        $imgctn .= '
            <div class="ximg">
                <amp-img src="' . $img . '" layout="responsive" width="620" height="413" alt="' . cleanHTML($details['title']) . '"></amp-img>            
                <div class="caption">' . $caption . '</div>
            </div>';
    }
    
    $imgctn .= '
        <div class="ads300 mt20">
            <amp-ad width=300 height=250
                type="doubleclick"
                data-slot="' . $this->config->item('amp_gam_id') . '"
                json=\'{"targeting":{"pos":["middle_1"]}}\'>
            </amp-ad>
        </div>';

    $ampcontent = trim($content);
    if(!empty($details['location'])){
        $location = '<span class="location">' . strtoupper($details['lokasi']) . '</span>';
        $location .= ' - ';
    }else{
        $location = '';
    }
    
    $lasturl = ($per_page * $last_page);
    if($totalData > $per_page){
        if($start == $lasturl){
            $editor = '<div class="editor">(' . strtolower($details['kode_user']) . ')</div>';
        }else{
            $editor = '';
        }
    }else{
        $editor = '<div class="editor">(' . strtolower($details['kode_user']) . ')</div>';
    }

    if(!empty($details['location'])){
        if($start == 0 || $start == ''){
            $showcontent = '<span class="lokasi">' . strtoupper($details['lokasi']) . '</span> - ' . $ampcontent;
        }else{
            $showcontent = $ampcontent;
        }
    }else{
        $showcontent = $ampcontent;
    }

    if($start == 0 || $start == ''){
        echo $imgctn;
    }else{
        echo '
            <div class="ads300 mt20 mb20 adsload">
                <amp-ad width=300 height=250
                    type="doubleclick"
                    data-slot="' . $this->config->item('amp_gam_id') . '"
                    json=\'{"targeting":{"pos":["middle_1"]}}\'>
                </amp-ad>
            </div>';
    }

    $gradient = '';
    $pagination = '';
    if($totalData > $per_page){
        $gradient = '<div class="content-line"></div>';
        $pagination = '
            <div class="ctn-next">
                <a href="' . $site_url . '">Baca Selanjutnya</a>
            </div>';
    }

    echo '<div class="content" itemprop="articleBody">' . $showcontent . $gradient . '</div>' . $editor;
    echo $pagination;

    $title_share = strip_tags(cleanWords($details['title']));

    echo '
        <div class="social-share">
            <div class="xtitle">Bagikan artikel ini:</div>
            <amp-addthis width="320" height="92"  data-pub-id="ra-5f702e08c7fac04c" data-widget-id="61x8" data-widget-type="inline"></amp-addthis>
        </div>';
    
    echo '
        <div class="ads300 mt20 mb20 adsload">
            <amp-ad width=300 height=250
                type="doubleclick"
                data-slot="' . $this->config->item('amp_gam_id') . '"
                json=\'{"targeting":{"pos":["middle_3"]}}\'>
            </amp-ad>
        </div>';
    
    if(count($suggest)){
        $site_domain = $this->config->item('read_domain');
        $dtime_suggest = strtotime($suggest['date_created']);
        $url_suggest = 'https://' . $site_domain[$suggest['channel_id']] . '/read/' . $suggest['content_id'] . '/' . $suggest['channel_id'] . '/' . slug($suggest['title']) . '-' . $dtime_suggest;
        $title_suggest = cleanWords($suggest['title']);

        if($suggest['image']){
            $img_suggest = images_uri() . '/dyn/135/pena/news/' . date('Y/m/d',strtotime($suggest['date_created'])) . '/' . $suggest['channel_id'] . '/' . $suggest['content_id'] . '/' . $suggest['image'];
            $showimg_suggest = '
                <div class="suggest-pict">
                    <a href="' . $url_suggest . '"><amp-img src="' . $img_suggest . '" layout="responsive" width="3" height="2" alt="' . cleanHTML($suggest['title']) . '"></amp-img></a>
                </div>';
        }else{
            $showimg_suggest = '';
        }

        echo '
            <div class="suggest-box">
                ' . $showimg_suggest . '
                <div class="suggest-title">
                    <a href="' . $url_suggest . '">' . $title_suggest . '</a>
                </div>
            </div>';
    }
}