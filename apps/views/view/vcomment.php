<div class="comment">
    <div class="head-comment"><span>KOMENTAR</span> (pilih salah satu dibawah ini)</div>
    <div class="option">
        <ul>
            <li><a class="dfb active">Disqus</a></li>
            <li><a class="afb">Facebook</a></li>
        </ul>
    </div>
    <div class="sindodisqus"></div>
    <div class="fb-comment">
        <div class="img-loading">
            <img src="<?php echo template_uri(); ?>/images/bx_loader.gif" alt="loading gif" width="32" height="32">
        </div>
        <div id="forum"></div>
    </div>
</div>